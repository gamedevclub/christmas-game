using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : SpawnableObject
{
    private void Start()
    {
        damage = 1;
    }

    protected override void OnCollisionEnter2D(Collision2D collision)
    {
        FindObjectOfType<AudioManager1>().Play("obstacle1");
        base.OnCollisionEnter2D(collision);
    }
}
