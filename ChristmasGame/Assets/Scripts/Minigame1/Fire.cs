using UnityEngine;

public class Fire : SpawnableObject
{
    // Start is called before the first frame update
    void Start()
    {
        damage = 0;
    }

    protected override void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("Fire");
        FindObjectOfType<AudioManager1>().Play("fire");
        base.OnCollisionEnter2D(collision);
        ScoreManager.instance.IncreaseTorch();
        Torch.instance.Activate();
    }
}
