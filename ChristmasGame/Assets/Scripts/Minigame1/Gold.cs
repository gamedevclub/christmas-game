using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gold : SpawnableObject
{
    // Start is called before the first frame update
    void Start()
    {
        damage = 0;
    }

    protected override void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("Gold");
        FindObjectOfType<AudioManager1>().Play("gold1");
        Collect(CollectItemType1.Gold);
        base.OnCollisionEnter2D(collision);
        ScoreManager.instance.IncreaseGold();
    }
}
