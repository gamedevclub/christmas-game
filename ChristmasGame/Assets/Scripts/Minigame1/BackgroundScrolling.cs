using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundScrolling : MonoBehaviour
{
    public float speed;

    // Update is called once per frame
    void Update()
    {
        Scrolling();
        RePosition();
    }

    void Scrolling()
    {
        Vector2 pos = transform.position;
        pos.x -= speed * Time.deltaTime;
        transform.position = pos;
    }
    void RePosition()
    {
        if (transform.position.x <= -22)
        {
            transform.position = new Vector2(22, 0);
        }
    }
}
