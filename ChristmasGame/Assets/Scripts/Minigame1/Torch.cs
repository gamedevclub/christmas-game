using UnityEngine;

public class Torch : MonoBehaviour
{
    #region Singleton
    public static Torch instance;
    private void Awake()
    {
        if (instance == null)
            instance = this;
    }
    #endregion

    public bool strongLightOffset;

    private void Start()
    {
        Activate();
    }

    public void Activate()
    {
        CancelInvoke();
        if(!gameObject.activeSelf)
            gameObject.SetActive(true); // activate torch instantly
        SpotlightScript.instance.IncreaseIntensity(strongLightOffset);
        SpotlightScript.instance.IncreaseRadius(strongLightOffset);
    }
}
