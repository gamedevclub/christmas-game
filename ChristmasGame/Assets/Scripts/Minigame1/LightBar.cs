using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LightBar : MonoBehaviour
{
    #region Singleton
    public static LightBar instance;
    private void Awake()
    {
        if (instance == null)
            instance = this;
    }
    #endregion

    public Slider slider;
    
    public void SetIntensity(float intensity)
    {
        slider.value = intensity;
    }

    public void SetMaxIntensity(float intensity)
    {
        slider.maxValue = intensity;
        slider.value = intensity;
    }
}
