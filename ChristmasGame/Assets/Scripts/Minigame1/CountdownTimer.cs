using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class CountdownTimer : MonoBehaviour
{
    #region Singleton
    public static CountdownTimer instance;
    private void Awake()
    {
        if (instance == null)
            instance = this;
    }
    #endregion
    public float currentTime;
    public float startMinute = 3f; 
    [SerializeField] Text countdownText;
    // Start is called before the first frame update
    void Start()
    {
        currentTime = startMinute * 60;
    }

    // Update is called once per frame
    void Update()
    {
        currentTime = currentTime - Time.deltaTime;
        if (currentTime <= 10)
        {
            countdownText.color = Color.red;
        }
        if (currentTime <= 0)
        {
            currentTime = 0;
            ScoreManager.instance.GameOver(Result.Victory);
        }
        TimeSpan time = TimeSpan.FromSeconds(currentTime);
        countdownText.text = String.Format("{0:00}:{1:00}", time.Minutes, time.Seconds);
    }
}
