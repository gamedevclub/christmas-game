using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class InstantKillEye : MonoBehaviour
{
    private Light2D m_Light;

    // Start is called before the first frame update
    void Start()
    {
        m_Light = GetComponent<Light2D>();
        m_Light.color = Color.red;
        m_Light.intensity = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (SpotlightScript.instance.HadFireGoneOut)
            m_Light.intensity = 30;
    }
}
