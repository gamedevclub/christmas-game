using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Player : MonoBehaviour
{
    #region Singleton

    public static Player instance;

    private void Awake()
    {
        if (instance == null)
            instance = this;
    }

    #endregion

    [SerializeField] private float playerSpeed;

    public int health = 3;

    public Camera cam;

    private Rigidbody2D rb;
    private Vector2 playerDirection;
    public Animator animator;
    /// <summary>
    /// * i = 0 -> Top position
    /// * i = 1 -> Mid position
    /// * i = 2 -> Bot position
    /// </summary>
    public float[] movePosition;
    /// <summary>
    /// * 0 = Top
    /// * 1 = Mid
    /// * 2 = Bot
    /// </summary>
    private int lane;

    private void Start()
    {
        lane = 1;
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    private void Update()
    {
        if (health <= 0)
        {
            LightBar.instance.SetIntensity(0);
            ScoreManager.instance.GameOver();
        }

        float directionY = Input.GetAxisRaw("Vertical");
        animator.SetBool("IsJumping", (directionY != 0));
        Movement();
    }

    void Movement()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
        {
            lane--;
            lane = Mathf.Clamp(lane, 0, 2);
        }
        if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
        {
            lane++;
            lane = Mathf.Clamp(lane, 0, 2);
        }
        transform.position = new Vector3(transform.position.x, movePosition[lane], transform.position.z);
    }

    public void TakeDamage(int damage)
    {
        health -= damage;
        ScoreManager.instance.hearts.text = health.ToString();
        if (health <= 1)
        {
            ScoreManager.instance.hearts.color = Color.red;
        }
    }

}
