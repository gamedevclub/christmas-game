using UnityEngine;

public class Depth1 : MonoBehaviour
{
    SpriteRenderer tempRend;
    private float timer = 3;
    public bool isPlayer;
    // Start is called before the first frame update
    void Start()
    {
        tempRend = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        tempRend.sortingOrder = (int)Camera.main.WorldToScreenPoint(this.transform.position).y * -1;
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            tempRend.color = new Color(1, 1, 1, 1);
            timer = 3;
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        try
        {
            if (isPlayer == false && other.GetComponent<Depth>().isPlayer == true)
            {
                tempRend.color = new Color(1, 1, 1, 0.5f);
                timer = 3;
            }
        }
        catch
        {

        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        try
        {

            if (isPlayer == false && other.GetComponent<Depth>().isPlayer == true)
            {
                tempRend.color = new Color(1, 1, 1, 1);
                timer = 3;
            }
        }
        catch
        {

        }
    }
}
