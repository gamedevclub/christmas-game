using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetJade : SpawnableObject
{
    // Start is called before the first frame update
    void Start()
    {
        damage = 0;
    }

    protected override void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("PetJade");
        FindObjectOfType<AudioManager1>().Play("obstacle2");
        Collect(CollectItemType1.PetJade);
        base.OnCollisionEnter2D(collision);
        ScoreManager.instance.IncreaseJade();
    }
}
