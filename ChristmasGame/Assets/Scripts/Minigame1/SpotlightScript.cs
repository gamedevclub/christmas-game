using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class SpotlightScript : MonoBehaviour
{
    #region Singleton
    public static SpotlightScript instance;
    private void Awake()
    {
        if (instance == null)
            instance = this;
    }
    #endregion

    private Light2D light2d;
    [Header("Intensity")]
    public float currentIntensity;
    private float newIntensity;
    private float normalIntensityOffset;
    private float strongIntensityOffset;
    [SerializeField] private float minIntensity = 0.1f;
    [SerializeField] private float normalIntensity = 1f;
    private float calculatedIntensity = 0;

    [Header("Inner Radius")]
    [SerializeField] private float currentInnerRadius;
    private float newInnerRadius;
    private float normalInnerRadiusOffset;
    private float strongInnerRadiusOffset;
    [SerializeField] private float minInnerRadius = 0.1f;
    [SerializeField] private float normalInnerRadius = 15f;
    private float calculatedInnerRadius = 0;

    [Header("Outer Radius")]
    [SerializeField] private float currentOuterRadius;
    private float newOuterRadius;
    private float normalOuterRadiusOffset;
    private float strongOuterRadiusOffset;
    [SerializeField] private float minOuterRadius = 0.1f;
    [SerializeField] private float normalOuterRadius = 20f;
    private float calculatedOuterRadius = 0;

    [Header("Divisor")]
    [SerializeField] private int normalDivisor = 30;
    [SerializeField] private int strongDivisor = 4;

    private float currentVelocity = 0f;

    [Header("Smooth Time")]
    [SerializeField] private float smoothTime = 0f;

    private int calls; // for Fixed Update 

    public bool HadFireGoneOut => light2d.intensity < minIntensity;

    public bool ReachNewSecond => calls % Constants.CallsPerSecond == 0;

    // Start is called before the first frame update
    void Start()
    {
        light2d = GetComponent<Light2D>();
        currentIntensity = light2d.intensity;
        newIntensity = currentIntensity;
        normalIntensityOffset = normalIntensity / normalDivisor;
        strongIntensityOffset = normalIntensity / strongDivisor;

        LightBar.instance.SetMaxIntensity(normalIntensity);

        currentInnerRadius = light2d.pointLightInnerRadius;
        currentOuterRadius = light2d.pointLightOuterRadius;
        newInnerRadius = currentInnerRadius;
        newOuterRadius = currentOuterRadius;
        normalInnerRadiusOffset = normalInnerRadius / normalDivisor;
        strongInnerRadiusOffset = normalInnerRadius / strongDivisor;
        normalOuterRadiusOffset = normalOuterRadius / normalDivisor;
        strongOuterRadiusOffset = normalOuterRadius / strongDivisor;

        calls = 0;
    }

    //Update is called once per frame
    void Update()
    {
        currentIntensity = Mathf.SmoothDamp(currentIntensity, newIntensity, ref currentVelocity, smoothTime);
        if (currentIntensity > normalIntensity)
        {
            currentIntensity = normalIntensity;
        }
        light2d.intensity = currentIntensity;

        currentInnerRadius = Mathf.SmoothDamp(currentInnerRadius, newInnerRadius, ref currentVelocity, smoothTime);
        light2d.pointLightInnerRadius = currentInnerRadius;

        currentOuterRadius = Mathf.SmoothDamp(currentOuterRadius, newOuterRadius, ref currentVelocity, smoothTime);
        light2d.pointLightOuterRadius = currentOuterRadius;

        if (light2d.intensity == 0)
        {
            LightBar.instance.SetIntensity(0);
        }
    }

    private void FixedUpdate()
    {
        calls++;

        if (ReachNewSecond)
        {
            ReduceIntensity(false);
            ReduceRadius(false);
        }
    }

    public void ReduceIntensity(bool isStrongOffset)
    {
        if (HadFireGoneOut) return;
        calculatedIntensity = currentIntensity - (isStrongOffset ? strongIntensityOffset : normalIntensityOffset);
        newIntensity = (calculatedIntensity < minIntensity) ? 0 : calculatedIntensity;
        LightBar.instance.SetIntensity(newIntensity);
    }

    public void IncreaseIntensity(bool isStrongOffset)
    {
        calculatedIntensity = currentIntensity + (isStrongOffset ? strongIntensityOffset * 2 : normalIntensityOffset);
        newIntensity = (calculatedIntensity > normalIntensity) ? normalIntensity : calculatedIntensity;
        LightBar.instance.SetIntensity(newIntensity);
    }

    public void ReduceRadius(bool isStrongOffset)
    {
        if (HadFireGoneOut) return;
        ReduceInnerRadius(isStrongOffset);
        ReduceOuterRadius(isStrongOffset);
    }

    public void IncreaseRadius(bool isStrongOffset)
    {
        IncreaseInnerRadius(isStrongOffset);
        IncreaseOuterRadius(isStrongOffset);
    }

    private void ReduceInnerRadius(bool isStrongOffset)
    {
        calculatedInnerRadius = currentInnerRadius - (isStrongOffset ? strongInnerRadiusOffset : normalInnerRadiusOffset);
        newInnerRadius = (calculatedInnerRadius < minInnerRadius) ? 0 : calculatedInnerRadius;
    }

    private void IncreaseInnerRadius(bool isStrongOffset)
    {
        calculatedInnerRadius = currentInnerRadius + (isStrongOffset ? strongInnerRadiusOffset * 2 : normalInnerRadiusOffset);
        newInnerRadius = (calculatedInnerRadius > normalInnerRadius) ? normalInnerRadius : calculatedInnerRadius;
    }

    private void ReduceOuterRadius(bool isStrongOffset)
    {
        calculatedOuterRadius = currentOuterRadius - (isStrongOffset ? strongOuterRadiusOffset : normalOuterRadiusOffset);
        newOuterRadius = (calculatedOuterRadius < minOuterRadius) ? 0 : calculatedOuterRadius;
    }

    private void IncreaseOuterRadius(bool isStrongOffset)
    {
        calculatedOuterRadius = currentOuterRadius + (isStrongOffset ? strongOuterRadiusOffset * 2 : normalOuterRadiusOffset);
        newOuterRadius = (calculatedOuterRadius > normalOuterRadius) ? normalOuterRadius : calculatedOuterRadius;
    }
}
