using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    #region Singleton
    public static Spawner instance;
    private void Awake()
    {
        if (instance == null)
            instance = this;
    }
    #endregion


    [SerializeField] private float startSpawnInterval;
    [SerializeField] private float timeDecrease;
    [SerializeField] private float minTime;
    [SerializeField] private float fireSpawnCounter;

    private float spawnInterval;

    public GameObject[] obstacleTemplate;
    public GameObject[] fireTemplate;
    public GameObject[] goldTemplate;
    public float timeBtwUpdateSpeedValue;
    private float timeBtwUpdateSpeed;
    public float fireSpawnCounterValue;

    private bool shouldSpawnGold => ((new System.Random()).Next(100) <= 20) ? true : false;

    private void Start()
    {
        timeBtwUpdateSpeed = timeBtwUpdateSpeedValue;
        spawnInterval = startSpawnInterval;
        fireSpawnCounter = 3;

        transform.position = new Vector3(transform.position.x, Player.instance.gameObject.transform.position.y);
    }

    private void Update()
    {
        if (timeBtwUpdateSpeed > 0)
        {
            timeBtwUpdateSpeed -= Time.deltaTime;
        }
        else
        {
            SpawnableObject.speed += 0.5f;

            var backgrounds = FindObjectsOfType<BackgroundScrolling>();
            foreach (var background in backgrounds)
            {
                background.speed += 0.5f;
            }

            fireSpawnCounterValue += 0.2f;
            timeBtwUpdateSpeed = timeBtwUpdateSpeedValue;
        }
        if (spawnInterval > 0)
        {
            spawnInterval -= Time.deltaTime;
            return;
        }

        if (fireSpawnCounter <= 0)
        {
            int rand = Random.Range(0, fireTemplate.Length - 1);
            Instantiate(fireTemplate[rand], transform.position, Quaternion.identity);
            fireSpawnCounter = fireSpawnCounterValue;
            // fireSpawnCounter = 2;
        }
        else
        {
            if (shouldSpawnGold)
            {
                int rand = Random.Range(0, goldTemplate.Length - 1);
                Instantiate(goldTemplate[rand], transform.position, Quaternion.identity);
            }
            else
            {
                int rand = Random.Range(0, obstacleTemplate.Length - 1);
                Instantiate(obstacleTemplate[rand], transform.position, Quaternion.identity);
            }
            fireSpawnCounter --;
        }
        // startSpawnInterval = 2;
        spawnInterval = startSpawnInterval;
        if (startSpawnInterval > minTime)
        {
            startSpawnInterval -= timeDecrease;
            // startSpawnInterval -= Time.deltaTime;
        }

    }
}
