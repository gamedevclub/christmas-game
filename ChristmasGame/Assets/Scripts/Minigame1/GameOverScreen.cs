using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverScreen : MonoBehaviour
{
    public Text Score;
    public Text AwardMoney;
    public Text AwardExp;
    public GameObject openChestButton;
    public void SetUp()
    {
        gameObject.SetActive(true);
        Score.text = Minigame1.score.ToString() + " POINTS";
        AwardMoney.text = "Money: " + ScoreManager.money.ToString();
        AwardExp.text = "Exp: " + ScoreManager.expNum.ToString();
        if (ScoreManager.openChest)
        {
            openChestButton.SetActive(true);
        }
    }

    public void RestartButton()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void OpenChestButton()
    {
        Debug.Log("Open chest!");
    }
}
