using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    #region Singleton
    public static CameraMovement instance;
    private void Awake()
    {
        if (instance == null)
            instance = this;
    }
    #endregion

    public float speed = 20;
    public float speedThreshhold = 50;

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(speed);
        transform.Translate(Vector2.right * speed * Time.deltaTime);
        if (speed >= speedThreshhold) 
            return;
        else
            speed += Time.deltaTime / 10;
    }
}
