using UnityEngine;

public class DestroyObstacle : MonoBehaviour
{
    [SerializeField] int longevity;

    private void Start()
    {
        Destroy(gameObject, longevity);
    }
}
