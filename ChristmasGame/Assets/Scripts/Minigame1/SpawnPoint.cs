using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    public GameObject spawnObject;

    private void Start()
    {
        Instantiate(spawnObject, transform.position, Quaternion.identity);
    }
}
