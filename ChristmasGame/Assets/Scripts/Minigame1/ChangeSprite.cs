using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeSprite : MonoBehaviour
{
    private SpriteRenderer spriteRenderer;
    public Sprite[] spriteArray;

    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        ChooseSprite();
    }

    void ChooseSprite()
    {
        int rand = Random.Range(0, spriteArray.Length);
        spriteRenderer.sprite = spriteArray[rand];
    }
}
