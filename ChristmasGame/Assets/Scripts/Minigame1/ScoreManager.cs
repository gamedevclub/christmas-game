using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoreManager : MonoBehaviour
{
    #region Singleton
    public static ScoreManager instance;
    private void Awake()
    {
        if (instance == null)
            instance = this;
    }
    #endregion

    public int batNumber;
    public int torchNumber;
    public int goldNumber;
    public int jadeNumber;

    public Text batDisplay;
    public Text goldDisplay;
    public Text jadeDisplay;
    public Text torchDisplay;
    //public Text countDownTimer;
    public Text scoreDisplay;
    public Text hearts;

    static public int money;
    static public int expNum;
    static public bool openChest;

    public int ToanScoreFormula => goldNumber + jadeNumber * 15; // Na's formula => Toan's formula

    private void Start()
    {
        SpawnableObject.speed = 5f;
    }

    public void IncreaseBat()
    {
        batNumber++;
        batDisplay.text = batNumber.ToString();
    }

    public void IncreaseTorch()
    {
        torchNumber++;
        torchDisplay.text = torchNumber.ToString();
    }

    public void IncreaseGold()
    {
        goldNumber++;
        goldDisplay.text = goldNumber.ToString();
        scoreDisplay.text = ToanScoreFormula.ToString();
    }

    public void IncreaseJade()
    {
        jadeNumber++;
        jadeDisplay.text = jadeNumber.ToString();
        scoreDisplay.text = ToanScoreFormula.ToString();
    }

    public void GameOver(Result result = Result.GameOver)
    {
        FindObjectOfType<AudioManager1>().Play("die");
        FindObjectOfType<AudioManager1>().StopPlaying("Theme1");
        Destroy(Player.instance.gameObject);
        Spawner.instance.gameObject.SetActive(false);
        BackgroundScrolling[] backgrounds = FindObjectsOfType<BackgroundScrolling>();
        for (int i = 0; i < backgrounds.Length; i++)
        {
            backgrounds[i].speed = 0;
        }
        UpdateQuest();
        GetAward(result);
    }

    private void UpdateQuest()
    {
        Minigame1.score = ToanScoreFormula;
        Minigame1.tourches = torchNumber; // before: Minigame1.score / 15;
        Minigame1.bats = batNumber;
        DataManager.instance.SaveMinigameData(1);
    }

    private void GetAward(Result result)
    {
        money = Minigame1.score;
        expNum = jadeNumber;
        openChest = Minigame1.score >= 100;
        AwardManager.instance.GetGameAward(money, expNum, openChest, result);
    }
}
