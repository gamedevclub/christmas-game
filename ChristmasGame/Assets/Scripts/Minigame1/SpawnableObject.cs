using UnityEngine;

public class SpawnableObject : MonoBehaviour
{
    public static float speed = 5f;
    public static float speedThreshhold = 10f;

    protected int damage;
    [SerializeField] private GameObject goldCollectEffect;
    [SerializeField] private GameObject jadeCollectEffect;
    [SerializeField] private GameObject batCollectEffect;


    protected virtual void Update()
    {
        transform.Translate(Vector2.left * speed * Time.deltaTime);
    }

    protected virtual void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Player.instance.TakeDamage(damage);
            Destroy(gameObject);
        }
    }
    protected virtual void Collect(CollectItemType1 type)
    {
        switch (type)
        {
            case CollectItemType1.Gold:
                Instantiate(goldCollectEffect, transform.position, Quaternion.identity);
                break;
            case CollectItemType1.PetJade:
                Instantiate(jadeCollectEffect, transform.position, Quaternion.identity);
                break;
            case CollectItemType1.Bat:
                var effect = Instantiate(batCollectEffect, transform.position, Quaternion.identity);
                Destroy(effect, 1f);
                break;
            default:
                break;
        }
    }
}

public enum CollectItemType1
{
    Gold, 
    PetJade,
    Bat
}
