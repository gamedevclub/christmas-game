using UnityEngine;

public class Bat : SpawnableObject
{
    // Start is called before the first frame update
    void Start()
    {
        damage = 0;
    }
    protected override void Update()
    {
        base.Update();
        if (SpotlightScript.instance.HadFireGoneOut)
        {
            damage = Player.instance.health;
        }
    }

    protected override void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("Bat");
        FindObjectOfType<AudioManager1>().Play("bat1");
        Collect(CollectItemType1.Bat);
        base.OnCollisionEnter2D(collision);
        ScoreManager.instance.IncreaseBat();
        SpotlightScript.instance.ReduceIntensity(true);
        SpotlightScript.instance.ReduceRadius(true);
    }
}
