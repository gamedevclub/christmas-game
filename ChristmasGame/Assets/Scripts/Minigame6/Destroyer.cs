using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyer : MonoBehaviour
{
    public GameObject room;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("SpawnPointMinigame6"))
        {
            Debug.Log("Destroy!");
            //Destroy(room);
            Destroy(gameObject);
        }
    }
}
