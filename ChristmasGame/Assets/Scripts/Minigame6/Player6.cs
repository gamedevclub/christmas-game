using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player6 : MonoBehaviour
{
    #region Singleton
    public static Player6 instance;
    private void Awake()
    {
        if (instance == null)
            instance = this;
    }
    #endregion
    public float playerSpeed = 10f;
    public const float speedThreshold = 20f;
    public float additionalSpeed = 0.25f;

    private Rigidbody2D rb2D;

    private float directionX;
    private float directionY;

    private Vector2 playerDirection;

    public bool isDead;

    public bool inCorridor;

    private void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();

        directionX = 0;
        directionY = 1;
        playerDirection = Vector2.zero;
        isDead = false;
        inCorridor = true;
    }
    // Update is called once per frame
    void Update()
    {
        if (isDead)
        {
            playerDirection = Vector2.zero;
        }
        else
        {
            if (!inCorridor)
            {
                directionX = Input.GetAxisRaw("Horizontal");
                directionY = Input.GetAxisRaw("Vertical");
            }

            if (directionX == -1)
            {
                Rotate(new Vector2(-1, 0));
                playerDirection = Vector2.left;
            }
            else if (directionX == 1)
            {
                Rotate(new Vector2(1, 0));
                playerDirection = Vector2.right;
            }

            if (directionY == -1)
            {
                Rotate(new Vector2(0, -1));
                playerDirection = Vector2.down;
            }
            else if (directionY == 1)
            {
                Rotate(new Vector2(0, 1));
                playerDirection = Vector2.up;
            }
        }
    }

    private void FixedUpdate()
    {
        if (playerSpeed <= speedThreshold)
        {
            rb2D.velocity = playerDirection * playerSpeed;
        }
    }

    public void IncreaseSpeed()
    {
        if (isDead) return;

        if (playerSpeed > speedThreshold) return;

        playerSpeed += additionalSpeed;
    }

    void Rotate(Vector2 nextDirection)
    {
        if (playerDirection == Vector2.left)
        {
            if (nextDirection == Vector2.right)
            {
                transform.localScale = new Vector2(transform.localScale.x, -transform.localScale.y);
                return;
            }
            if (nextDirection == Vector2.up)
            {
                transform.rotation = Quaternion.Euler(0, 0, transform.rotation.eulerAngles.z - 90);
                return;
            }
            if (nextDirection == Vector2.down)
            {
                transform.rotation = Quaternion.Euler(0, 0, transform.rotation.eulerAngles.z + 90);
                return;
            }
        }
        if (playerDirection == Vector2.right)
        {
            if (nextDirection == Vector2.left)
            {
                transform.localScale = new Vector2(transform.localScale.x, -transform.localScale.y);
                return;
            }
            if (nextDirection == Vector2.up)
            {
                transform.rotation = Quaternion.Euler(0, 0, transform.rotation.eulerAngles.z + 90);
                return;
            }
            if (nextDirection == Vector2.down)
            {
                transform.rotation = Quaternion.Euler(0, 0, transform.rotation.eulerAngles.z - 90);
                return;
            }
        }
        if (playerDirection == Vector2.up)
        {
            if (nextDirection == Vector2.down)
            {
                transform.localScale = new Vector2(transform.localScale.x, -transform.localScale.y);
                return;
            }
            if (nextDirection == Vector2.left)
            {
                transform.rotation = Quaternion.Euler(0, 0, transform.rotation.eulerAngles.z + 90);
                return;
            }
            if (nextDirection == Vector2.right)
            {
                transform.rotation = Quaternion.Euler(0, 0, transform.rotation.eulerAngles.z - 90);
                return;
            }
        }
        if (playerDirection == Vector2.down)
        {
            if (nextDirection == Vector2.up)
            {
                transform.localScale = new Vector2(transform.localScale.x, -transform.localScale.y);
                return;
            }
            if (nextDirection == Vector2.left)
            {
                transform.rotation = Quaternion.Euler(0, 0, transform.rotation.eulerAngles.z - 90);
                return;
            }
            if (nextDirection == Vector2.right)
            {
                transform.rotation = Quaternion.Euler(0, 0, transform.rotation.eulerAngles.z + 90);
                return;
            }
        }

    }
}
