using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TornadoManager : MonoBehaviour
{
    public GameObject BaseTornadoPrefab;

    [SerializeField] private int calls;

    public int DetectCycle = 3;

    public bool ReachNewDetectCycle => calls % (Constants.CallsPerSecond * DetectCycle) == 0;

    // Start is called before the first frame update
    void Start()
    {
        calls = 0;
    }

    private void FixedUpdate()
    {
        if (Player6.instance.isDead) return;
        if (!GameManager6.instance.CanSpawnTornado) return;

        calls++;

        if (ReachNewDetectCycle)
        {
            Transform playerTransform = Player6.instance.transform;
            GameObject baseTornado = Instantiate(BaseTornadoPrefab, playerTransform.position, transform.rotation);
            baseTornado.transform.SetParent(transform, worldPositionStays: true);
        }
    }
}
