using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Multiplier { x2, x3 };

public class Item6 : MonoBehaviour
{
    public Multiplier multiplier;

    virtual protected void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player6"))
        {
            FindObjectOfType<AudioManager6>().Play("X2");
            GameManager6.instance.PickupItem();
            if (multiplier == Multiplier.x2)
            {
                GameManager6.instance.X2Scores();
                //StartCoroutine( GameManager6.instance.X2());
            }
            else
            {
                GameManager6.instance.X3Scores();
                //StartCoroutine( GameManager6.instance.X3());
            }
            Destroy(gameObject);
        }
    }
}
