using UnityEngine;
using UnityEngine.Audio;
using System;

public class AudioManager6 : MonoBehaviour
{
    public Sound6[] sounds;

    public AudioMixerGroup volume;

    private void Awake()
    {
        foreach (Sound6 s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
            s.source.outputAudioMixerGroup = volume;
        }
    }

    private void Start()
    {
        Play("Theme");
    }

    public void Play(string name)
    {
        Sound6 s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return;
        }
        s.source.Play();
    }

    public void StopPlaying(string sound)
    {
        Sound6 s = Array.Find(sounds, item => item.name == sound);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return;
        }

        s.source.volume = s.volume * (1f + UnityEngine.Random.Range(-0.5f / 2f, 0.5f / 2f));
        s.source.pitch = s.pitch * (1f + UnityEngine.Random.Range(-0.5f / 2f, 0.5f / 2f));

        s.source.Stop();
    }
}
