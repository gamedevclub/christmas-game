using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseTornado : MonoBehaviour
{
    public GameObject TornadoPrefab;

    public int spawnTime = 2;

    private int calls;

    public bool CanSpawnTornado => calls == spawnTime * Constants.CallsPerSecond;

    // Start is called before the first frame update
    void Start()
    {
        calls = 0;
    }

    private void FixedUpdate()
    {
        calls++;

        if (CanSpawnTornado)
        {
            GameObject tornado = Instantiate(TornadoPrefab, transform.position, transform.rotation);
            tornado.transform.SetParent(transform, worldPositionStays: true);
        }
    }
}
