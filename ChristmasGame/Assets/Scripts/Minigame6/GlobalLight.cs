using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class GlobalLight : MonoBehaviour
{
    #region Singleton
    public static GlobalLight instance;
    private void Awake()
    {
        if (instance == null)
            instance = this;
    }
    #endregion

    private Light2D light2D;
    // Start is called before the first frame update
    void Start()
    {
        light2D = GetComponent<Light2D>();
        light2D.intensity = 0.5f;
    }

    public void Back2Normal()
    {
        light2D.intensity = 1f;
    }
}
