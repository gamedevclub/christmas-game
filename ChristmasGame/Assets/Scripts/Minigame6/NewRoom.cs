using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class NewRoom : MonoBehaviour
{
    public GameObject[] roomTemplate;

    public GameObject[] spawners;

    public bool existedRoom;

    public NewRoom[] rooms;

    public List<GameObject> readySpawners;

    void Start()
    {
        existedRoom = false;
        readySpawners = new List<GameObject>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player6"))
        {
            FindObjectOfType<AudioManager6>().Play("Door");
            readySpawners.Clear();
            rooms = FindObjectsOfType<NewRoom>();
            foreach (var spawner in spawners)
            {
                foreach (var room in rooms)
                    if (room.transform.position == spawner.transform.position)
                        existedRoom = true;

                if (!existedRoom)
                    readySpawners.Add(spawner);

                existedRoom = false;
            }

            readySpawners = readySpawners.Distinct().ToList();

            foreach (var spawner in readySpawners)
            {
                int rand = Random.Range(0, roomTemplate.Length);
                int degreeRand = Random.Range(0, 4) * 90; // 0/90/180/270 degree
                Instantiate(roomTemplate[rand], spawner.transform.position, Quaternion.AngleAxis(degreeRand, Vector3.forward));
            }

            rooms = FindObjectsOfType<NewRoom>(); // Update new rooms after instantiating rooms

            foreach (var room in rooms)
            {
                float dist = Vector2.Distance(transform.position, room.transform.position);
                if (dist >= 2 * Constants.BoxColliderSize // too far rooms
                    || dist >= Mathf.Sqrt(2) * Constants.BoxColliderSize) // corner rooms
                    Destroy(room.gameObject);
            }
        }
    }
}
