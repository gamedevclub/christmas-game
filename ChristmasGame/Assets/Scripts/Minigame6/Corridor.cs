using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Corridor : MonoBehaviour
{
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player6"))
        {
            GlobalLight.instance.Back2Normal();
            Player6.instance.inCorridor = false;
            Destroy(gameObject);
        }
    }
}
