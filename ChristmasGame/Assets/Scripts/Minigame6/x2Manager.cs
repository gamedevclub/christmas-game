using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class x2Manager : MonoBehaviour
{
    // Start is called before the first frame update
    public Item6[] items;
    void Start()
    {
        items = GetComponentsInChildren<Item6>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager6.instance.CanSpawnItem)
        {
            foreach (Transform child in transform)
                child.gameObject.SetActive(true);
        }
    }
}
