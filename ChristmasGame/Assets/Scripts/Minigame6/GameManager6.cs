﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager6 : MonoBehaviour
{
    #region Singleton
    public static GameManager6 instance;
    private void Awake()
    {
        if (instance == null)
            instance = this;
    }
    #endregion
    [SerializeField] private int calls; // number of calls in 
    public int score;
    private int scoresPerSecond;
    public int itemCount;
    public int shieldCount;
    public bool shieldOn;

    [SerializeField] private int immuneTime = 5;
    [SerializeField] private int shieldTime = 10;
    [SerializeField] private int scoreMultiplyingTime = 5;
    [SerializeField] private int timeSpawnItem = 10;
    [SerializeField] private int timeSpawnX3Item = 60;
    [SerializeField] private int timeX2ScorePerSecond = 30;
    [SerializeField] private int timeSpawnTornado = 6;

    public Text pointDisplay;
    public Text itemDisplay;
    public Text shieldDisplay;
    public Text readyDisplay;

    public ParticleSystem Shield;
    public GameObject ShieldBrokenVFX;

    public bool isGameOverRunning;

    public bool IsPlayerImmune() => calls < immuneTime * Constants.CallsPerSecond || shieldOn;
    public bool CanSpawnItem => calls >= timeSpawnItem * Constants.CallsPerSecond;
    public bool CanSpawnX3Item => calls >= timeSpawnX3Item * Constants.CallsPerSecond;
    public bool ReachNewSecond => calls % Constants.CallsPerSecond == 0;
    public bool CanX2ScorePerSecond => calls == timeX2ScorePerSecond * Constants.CallsPerSecond;
    public bool CanSpawnTornado => calls >= timeSpawnTornado * Constants.CallsPerSecond;


    // Start is called before the first frame update
    void Start()
    {
        calls = 0;
        score = 0;
        scoresPerSecond = 1;
        itemCount = 0;
        shieldCount = 0;
        shieldOn = false;
        isGameOverRunning = false;

        Shield.Stop();

        StartCoroutine(ReadyGo());
    }

    private void FixedUpdate()
    {
        calls++;

        if (ReachNewSecond)
        {
            if (!Player6.instance.isDead)
                score += scoresPerSecond;
            pointDisplay.text = "Điểm:  " + score.ToString();
            Player6.instance.IncreaseSpeed();
        }

        if (CanX2ScorePerSecond)
            scoresPerSecond = 2;
    }

    IEnumerator ReadyGo()
    {
        readyDisplay.text = "READY";
        yield return new WaitForSeconds(1);
        readyDisplay.text = "GO!!";
        yield return new WaitForSeconds(1);
        readyDisplay.gameObject.SetActive(false);
    }

    public void X2Scores()
    {
        Debug.Log("X2");
        scoresPerSecond *= 2;
        Invoke("UndoX2", scoreMultiplyingTime);
    }

    public void UndoX2()
    {
        scoresPerSecond /= 2;
    }

    public void X3Scores()
    {
        Debug.Log("X3");
        scoresPerSecond *= 3;
        Invoke("UndoX3", scoreMultiplyingTime);
    }

    public void UndoX3()
    {
        scoresPerSecond /= 3;
    }

    public void PickupItem()
    {
        itemCount++;
        itemDisplay.text = "Item x " + itemCount.ToString();
    }

    public void PickupShield()
    {
        CancelInvoke();
        Debug.Log("Shield");
        shieldOn = true;
        Shield.Play();
        shieldCount++;
        shieldDisplay.text = "Shield x " + shieldCount.ToString();
        Invoke("Unarm", shieldTime);
    }

    public void Unarm()
    {
        Debug.Log("Unarmed");
        var effect = Instantiate(ShieldBrokenVFX, Player6.instance.transform.position, Quaternion.identity);
        effect.transform.SetParent(Player6.instance.transform, worldPositionStays: true);
        shieldOn = false;
        Shield.Stop();
    }

    public void GameOver()
    {
        if (isGameOverRunning) return;
        Debug.Log("Game over!");
        isGameOverRunning = true;
        StopAllCoroutines();
        FindObjectOfType<AudioManager6>().StopPlaying("Theme");

        int coin = 2 * score / 1;
        int exp = 2 * score / 10;
        bool chest = score >= 70;

        if (score > Minigame6.score)
            Minigame6.score = score;
        if (itemCount + shieldCount > Minigame6.items)
            Minigame6.items = itemCount + shieldCount;
        if (shieldCount > Minigame6.armors)
            Minigame6.armors = shieldCount;

        Debug.Log("Score: " + score);
        Debug.Log("Items: " + (itemCount + shieldCount).ToString());
        Debug.Log("Armors: " + shieldCount);

        DataManager.instance.SaveMinigameData(6);

        AwardManager.instance.GetGameAward(coin, exp, chest, Result.GameOver);
    }
}
