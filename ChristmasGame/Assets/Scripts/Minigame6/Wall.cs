using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Wall : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag != "Player6") return;
        FindObjectOfType<AudioManager6>().Play("HitWall");
        if (GameManager6.instance.IsPlayerImmune()) return;
        Player6.instance.isDead = true;
        GameManager6.instance.GameOver();
    }
}
