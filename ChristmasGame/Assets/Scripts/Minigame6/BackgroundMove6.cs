using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMove6 : MonoBehaviour
{
    public GameObject player;
    public float maxDistance;


    Rigidbody2D rb;
    Rigidbody2D playerRb;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        playerRb = player.GetComponent<Rigidbody2D>();
    }

    private void Update()
    {

        Vector2 direction = player.transform.position - transform.position;
        float distance = direction.magnitude;

        rb.velocity = direction.normalized * playerRb.velocity.magnitude * (distance / maxDistance);


    }
}
