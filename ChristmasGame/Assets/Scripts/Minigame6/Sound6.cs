using UnityEngine;
using UnityEngine.Audio;

[System.Serializable]
public class Sound6
{
    public string name;

    public AudioClip clip;

    [Range(0, 1)]
    public float volume;

    [Range(1f, 3f)]
    public float pitch;

    public bool loop;

    [HideInInspector]
    public AudioSource source;
}
