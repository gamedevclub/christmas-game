using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tornado : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.CompareTag("Player6")) return;
        if (GameManager6.instance.IsPlayerImmune()) return;
        Player6.instance.isDead = true;
        GameManager6.instance.GameOver();
    }
}
