using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player6"))
        {
            FindObjectOfType<AudioManager6>().Play("Shield");
            GameManager6.instance.PickupShield();
            Destroy(gameObject);
        }
    }
}
