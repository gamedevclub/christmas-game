using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController_5 : MonoBehaviour
{
    private Rigidbody2D myBody;

    [SerializeField]
    private Transform arrow;

    [SerializeField]
    private float force;

    [SerializeField]
    private GameObject food;

    private GameObject foodThrow;

    private bool isFollowing;

    [SerializeField]
    private Stat fillForce;

    public float minForce = 10;

    public float maxForce = 100;
    public Transform effectPivotArrow;

    // Start is called before the first frame update
    void Start()
    {
        myBody = GetComponent<Rigidbody2D>();
        force = minForce;
        fillForce.InitializeValue(maxForce);
        fillForce.CurrentValue = 0f;
        foodThrow = null;
        isFollowing = false;
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.GetComponent<SpriteRenderer>().sortingOrder = 10000 - (int)(transform.position.y * 100);
        ArrowRotate();
        ThrowFood();
        FoodIsExist();
        FoodCollision();
    }

    void ArrowRotate()
    {
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 direction = (mousePosition - transform.position).normalized;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90f;
        arrow.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
    }
    void ThrowFood()
    {
        if (isFollowing) return;
        if (GamePlay_Manager_5.instance.FoodAmount > 0)
        {
            if (Input.GetMouseButton(0))
            {
                if (force < maxForce)
                {
                    force += Time.deltaTime * 50;
                    fillForce.CurrentValue = force;
                }
            }
            if (Input.GetMouseButtonUp(0))
            {
                GamePlay_Manager_5.instance.FoodUsed++;
                Instantiate(GamePlay_Manager_5.instance.afterThrowingEffect, effectPivotArrow.position, Quaternion.identity);
                SoundManager_5.instance.ThrowFood();
                Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                Vector2 direction = (mousePosition - transform.position);
                direction = direction.normalized;

                Debug.Log("direction 1 : " + direction);

                foodThrow = Instantiate(food, transform.position, Quaternion.identity);
                foodThrow.GetComponent<FoodController_5>().Throw(direction, force);
                force = minForce;
                GamePlay_Manager_5.instance.FoodAmount--;
                isFollowing = true;

                Debug.Log("direction 2 : " + direction);

                fillForce.CurrentValue = 0;
            }
        }
    }
    void FoodIsExist()
    {
        if (foodThrow == null)
        {
            isFollowing = false;
        }
    }

    public void FollowFood(Vector3 foodPosition)
    {
        Vector2 direction = foodPosition - transform.position;
        direction = direction.normalized;
        PlayerRotate(direction.x, direction.y);
        PlayerMoverment(direction.x, direction.y);
    }

    void PlayerRotate(float xRect, float yRect)
    {
        float angle = Mathf.Atan2(yRect, xRect) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
    }

    void PlayerMoverment(float xRect, float yRect)
    {
        myBody.velocity = new Vector2(xRect, yRect) * 1.7f;
    }

    void RequestCameraFollow()
    {
        Camera main = Camera.main;
        main.gameObject.GetComponent<CameraFollow_5>().RequestFollow();
    }


    void FoodCollision()
    {
        if (foodThrow != null)
        {
            if (Vector2.Distance(foodThrow.transform.position, transform.position) < 0.2f)
            {
                if (foodThrow.GetComponent<FoodController_5>().IsStopped())
                {
                    Destroy(foodThrow.gameObject);
                    foodThrow = null;
                    myBody.velocity = Vector2.zero;
                    //isFollowing = false;
                    fillForce.CurrentValue = 0;
                    RequestCameraFollow();
                }
            }
        }
    }
}
