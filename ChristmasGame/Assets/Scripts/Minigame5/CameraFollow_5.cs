using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class CameraFollow_5 : MonoBehaviour
{
    private GameObject player;

    [SerializeField]
    private Tilemap map;

    private bool followRequest;

    private float xMin, yMin, xMax, yMax;

    Vector2 destination;
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        Vector2 minTile = map.CellToWorld(map.cellBounds.min);
        Vector2 maxTile = map.CellToWorld(map.cellBounds.max);
        SetLimit(minTile, maxTile);
    }
    void Update()
    {

    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (!followRequest) return;  
        Vector3 smootPosition = Vector3.Lerp(transform.position, destination, 0.01f);
        transform.position = new Vector3(smootPosition.x, smootPosition.y, -10);
        if (Vector2.Distance(transform.position, destination) < 0.25f)
        {
            followRequest = false;
            if (!followRequest)
            {
                if (player.transform.position.y > 97 && Mathf.Abs(player.transform.position.x) < 5)
                {
                    Destroy(player);
                    SoundManager_5.instance.Victory();
                    return;
                }
            }
            if (followRequest == false && GamePlay_Manager_5.instance.isLose == false)
            {
                if (GamePlay_Manager_5.instance.FoodAmount == 0)
                {
                    SoundManager_5.instance.Lost();
                    GamePlay_Manager_5.instance.isLose = true;
                }
            }
        }
    }

    public void RequestFollow()
    {
        followRequest = true;
        CalculateDestination();
    }

    void CalculateDestination()
    {
        destination = new Vector2(player.transform.position.x, player.transform.position.y);
        if (destination.x < xMin) destination.x = xMin;
        if (destination.x > xMax) destination.x = xMax;
        if (destination.y < yMin) destination.y = yMin;
        if (destination.y > yMax) destination.y = yMax;
    }

    void SetLimit(Vector2 minTile, Vector2 maxTile)
    {
        Camera cam = Camera.main;
        float height = cam.orthographicSize * 2f;
        float width = height * cam.aspect;
        xMin = minTile.x + width / 2;
        xMax = maxTile.x - width / 2;

        yMin = minTile.y + height / 2;
        yMax = maxTile.y - height / 2;
    }
}
