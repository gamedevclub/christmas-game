using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodController_5 : MonoBehaviour
{
    public GameObject foodCollisionEffect;
    private Rigidbody2D myBody;

    private GameObject player;

    [SerializeField]
    private float alpha;

    private bool firtZero; //Dont call player follow when the first velocity equal 0

    private bool isStopped;

    private GameObject wolf;
    float angle = 0;

    private bool isAlive;
    void Start()
    {
        myBody = GetComponent<Rigidbody2D>();
        alpha = 0.95f;
        player = GameObject.FindGameObjectWithTag("Player");
        isStopped = false;
        isAlive = true;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (player == null) return;
        if ( Mathf.Abs(myBody.velocity.x) > 0.1f && Mathf.Abs(myBody.velocity.y) > 0.1f)
        {
            myBody.velocity = new Vector2(myBody.velocity.x * alpha, myBody.velocity.y * alpha);
            transform.rotation = Quaternion.Euler(0, 0, angle);
            angle += 5;
        }
        else
        {
            if (!firtZero)
            {
                myBody.velocity = Vector2.zero;
                if (wolf != null)
                {
                    AttractWolf();
                }
                else
                {
                    player.GetComponent<PlayerController_5>().FollowFood(transform.position);
                    isStopped = true;
                }
            }
        }
        EatenByWolf();
        firtZero = false;
    }

    public bool IsStopped()
    {
        return isStopped;
    }

    public void Throw(Vector2 direction, float force)
    {
        myBody = GetComponent<Rigidbody2D>();
        myBody.AddForce(direction * force);
        firtZero = true;
    }

    void AttractWolf()
    {
        wolf.GetComponent<WolfController_5>().MoveTo(this.gameObject);
    }

    void EatenByWolf()
    {
        if (wolf != null)
        {
            if (Vector2.Distance(transform.position, wolf.transform.position) < 0.2f)
            {
                SoundManager_5.instance.Wolf();
                wolf.transform.position = transform.position;
                wolf.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                if (isAlive)
                {
                    if (GamePlay_Manager_5.instance.FoodAmount == 0)
                    {
                        SoundManager_5.instance.Lost();
                        GamePlay_Manager_5.instance.isLose = true;
                    }
                    isAlive = false;
                }
                Destroy(gameObject);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Wolf")
        {
            wolf = collision.gameObject;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Wolf")
        {
            wolf = null;
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.collider.CompareTag("Tree"))
        {
            Instantiate(foodCollisionEffect, transform.position, Quaternion.identity);
        }
    }

}
