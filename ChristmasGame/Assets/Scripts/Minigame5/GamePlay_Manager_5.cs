using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class GamePlay_Manager_5 : MonoBehaviour
{
    public static GamePlay_Manager_5 instance;

    [SerializeField]
    private int foodAmount;

    [SerializeField]
    private TextMeshProUGUI foodAmountUI;

    private float flagValue;

    [SerializeField]
    private TextMeshProUGUI flagValueUI;
    [SerializeField]
    private int foodUsed;

    private Transform player;

    private int coin;
    public GameObject afterThrowingEffect;
    public bool isLose;

    public int FoodAmount { get => foodAmount; set => foodAmount = value; }
    public int Coin { get => coin; set => coin = value; }
    public float FlagValue { get => flagValue;}
    public int FoodUsed {get => foodUsed; set => foodUsed = value;}

    private void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        player = GameObject.FindGameObjectWithTag("Player").transform;
        isLose = false;
        coin = 0;
        flagValue = 0;
        foodAmount = 15;
        foodUsed = 0;
    }
    void Update()
    {
        foodAmountUI.text = "x" + FoodAmount;
        if (player != null)
        {
            flagValue = Mathf.Round(player.position.y);
            flagValue = Mathf.Clamp(flagValue, 0, 100);
            flagValueUI.text = FlagValue + "%";
        }
    }

}
