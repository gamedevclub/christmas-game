using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodPackageController_5 : MonoBehaviour
{
    const int quantity = 10;
    public GameObject foodPackageEffect;
    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<SpriteRenderer>().sortingOrder = 10000 - (int)(transform.position.y * 100);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Instantiate(foodPackageEffect, transform.position, Quaternion.identity);
            GamePlay_Manager_5.instance.FoodAmount += quantity;
            SoundManager_5.instance.FoodPackage();
            Destroy(gameObject);
        }
    }
}
