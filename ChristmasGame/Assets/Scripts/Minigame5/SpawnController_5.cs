using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class SpawnController_5 : MonoBehaviour
{
    public int treeAmount;

    [SerializeField]
    private GameObject tree;

    public int wolfAmount;

    [SerializeField]
    private GameObject wolf;

    [SerializeField]
    private Tilemap map;

    public int foodPackageAmount;

    [SerializeField]
    private GameObject foodPackage;

    public int coinAmout;

    [SerializeField]
    private GameObject coin;

    private float xMin, yMin, xMax, yMax;

    [SerializeField]
    private Transform treeList;
    [SerializeField]
    private Transform wolfList;
    [SerializeField]
    private Transform foodPackageList;
    [SerializeField]
    private Transform coinList;

    void Start()
    {
        Vector2 minTile = map.CellToWorld(map.cellBounds.min);
        Vector2 maxTile = map.CellToWorld(map.cellBounds.max);
        SetLimit(minTile, maxTile);
        SpawnTree();
        SpawnWolf();
        SpawnFoodPackage();
        SpawnCoin();
    }

    void SetLimit(Vector2 minTile, Vector2 maxTile)
    {
        xMin = minTile.x;
        xMax = maxTile.x;

        yMin = minTile.y;
        yMax = maxTile.y - 4;
        Debug.Log(yMax);
    }

    void SpawnTree()
    {
        int count = 0;
        Vector2 pos = Vector2.zero;
        while (count < treeAmount)
        {
            pos.x = Random.Range(xMin, xMax);
            pos.y = Random.Range(yMin, yMax);
            if (Mathf.Abs(pos.x) < 2 && Mathf.Abs(pos.y) < 2)
            {
                continue;
            }
            GameObject newTree = Instantiate(tree, pos, Quaternion.identity, treeList);
            if (newTree.GetComponent<TreeController_5>().IsIntoAreaOtherTree())
            {
                Destroy(newTree);
            }
            else
            {
                count++;
            }
        }
        Debug.Log("Done");
    }
    void SpawnWolf()
    {
        int count = 0;
        Vector2 pos = Vector2.zero;
        while (count < wolfAmount)
        {
            pos.x = Random.Range(xMin, xMax);
            pos.y = Random.Range(yMin, yMax);
            if (Mathf.Abs(pos.x) < 2 && Mathf.Abs(pos.y) < 2)
            {
                continue;
            }
            Instantiate(wolf, pos, Quaternion.identity, wolfList);
            count++;
        }
        Debug.Log("Done");
    }
    void SpawnFoodPackage()
    {
        int count = 0;
        float tempYMin = 0;
        float tempYmax = 100 / foodPackageAmount;
        Vector2 pos = Vector2.zero;
        while (count < foodPackageAmount)
        {
            pos.x = Random.Range(xMin, xMax);
            pos.y = Random.Range(tempYMin, tempYmax);
            if (Mathf.Abs(pos.x) < 2 && Mathf.Abs(pos.y) < 2)
            {
                continue;
            }
            Instantiate(foodPackage, pos, Quaternion.identity, foodPackageList);
            count++;
            tempYMin = tempYmax;
            tempYmax += 100 / foodPackageAmount;
        }
        Debug.Log("Done");
    }
    void SpawnCoin()
    {
        int count = 0;
        Vector2 pos = Vector2.zero;
        while (count < coinAmout)
        {
            pos.x = Random.Range(xMin, xMax);
            pos.y = Random.Range(yMin, yMax);
            Instantiate(coin, pos, Quaternion.identity, coinList);
            count++;
        }
        Debug.Log("Done");
    }
}
