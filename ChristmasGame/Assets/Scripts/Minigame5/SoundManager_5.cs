using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager_5 : MonoBehaviour
{
    public static SoundManager_5 instance;
    private AudioSource source;

    public AudioClip background;
    public AudioClip throwFood;
    public AudioClip pickItem;
    public AudioClip wolf;
    public AudioClip lost;
    public AudioClip foodPackage;

    // Start is called before the first frame update
    void Start()
    {
        if (instance == null) instance = this;
        source = GetComponent<AudioSource>();
        Background();
    }

    void Background()
    {
        source.clip = background;
        source.loop = true;
        source.Play();
    }
    public void ThrowFood()
    {
        source.PlayOneShot(throwFood);
    }
    public void PickItem()
    {
        source.PlayOneShot(pickItem);
    }
    public void Wolf()
    {
        source.PlayOneShot(wolf);
    }
    public void FoodPackage()
    {
        source.PlayOneShot(foodPackage);
    }
    public void Lost()
    {
        source.Stop();
        //source.PlayOneShot(lost);
        Minigame5.distance = (int)GamePlay_Manager_5.instance.FlagValue > Minigame5.distance ? (int)GamePlay_Manager_5.instance.FlagValue : Minigame5.distance;
        DataManager.instance.SaveMinigameData(5);
        AwardManager.instance.GetGameAward(money: GamePlay_Manager_5.instance.Coin * 2 + (int)(GamePlay_Manager_5.instance.FlagValue * 2), expNum: (int)(GamePlay_Manager_5.instance.FlagValue / 2), result: Result.GameOver);
    }
    public void Victory()
    {
        source.Stop();
        //source.PlayOneShot(lost);
        Minigame5.distance = (int)GamePlay_Manager_5.instance.FlagValue > Minigame5.distance ? (int)GamePlay_Manager_5.instance.FlagValue : Minigame5.distance;
        Minigame5.meat = GamePlay_Manager_5.instance.FoodUsed < Minigame5.meat ? GamePlay_Manager_5.instance.FoodUsed : Minigame5.meat;
        print(Minigame5.meat);
        DataManager.instance.SaveMinigameData(5);
        AwardManager.instance.GetGameAward(money: GamePlay_Manager_5.instance.Coin * 2 + (int)(GamePlay_Manager_5.instance.FlagValue * 2), expNum: (int)(GamePlay_Manager_5.instance.FlagValue / 2), openChest: true, result: Result.Victory);
    }
}
