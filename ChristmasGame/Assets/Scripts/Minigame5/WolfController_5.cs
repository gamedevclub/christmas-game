using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WolfController_5 : MonoBehaviour
{
    private Rigidbody2D myBody;

    private GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        myBody = GetComponent<Rigidbody2D>();
    }
    // Update is called once per frame
    void Update()
    {
        gameObject.GetComponent<SpriteRenderer>().sortingOrder = 10000 - (int)(transform.position.y * 100);
        AttackPlayer();
    }

    public void MoveTo(GameObject target)
    {
        Vector2 direction = (target.transform.position - transform.position).normalized;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg + 90;
        transform.rotation = Quaternion.Euler(0, 0, angle);
        myBody.velocity = direction * 2f;
    }

    void AttackPlayer()
    {
        if (player != null)
        {
            if (Vector2.Distance(transform.position, player.transform.position) < 2f)
            {
                transform.position = player.transform.position;
                Destroy(player);
                SoundManager_5.instance.Wolf();
                SoundManager_5.instance.Lost();
                myBody.velocity = Vector2.zero;              
            }
        }
    }
}
