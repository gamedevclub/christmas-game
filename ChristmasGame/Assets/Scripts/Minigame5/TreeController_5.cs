using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeController_5 : MonoBehaviour
{
    private bool intoAreaOfOtherTree;

    void Start()
    {
        intoAreaOfOtherTree = false;
        gameObject.GetComponent<SpriteRenderer>().sortingOrder = 10000 - (int)(transform.position.y * 100);
    }

    public bool IsIntoAreaOtherTree()
    {
        return intoAreaOfOtherTree;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Tree")
        {
            intoAreaOfOtherTree = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Tree")
        {
            intoAreaOfOtherTree = false;
        }
    }
}
