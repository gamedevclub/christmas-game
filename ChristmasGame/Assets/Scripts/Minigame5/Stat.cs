using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Stat : MonoBehaviour
{
    private float maxValue;
    private float currentValue;

    public float MaxValue { get => maxValue; set => maxValue = value; }
    public float CurrentValue { get => currentValue; 
        set { if (value > maxValue) currentValue = maxValue; else if (value < 0) currentValue = 0; else currentValue = value; }}

    [SerializeField]
    private Image fill;

    void Start()
    {
        fill.GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        fill.fillAmount = currentValue / maxValue;
    }
    public void InitializeValue(float value)
    {
        currentValue = value;
        maxValue = value;
    }
}
