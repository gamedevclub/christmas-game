using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum DayTime
{
    Day, Night
}

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public Text alertMessage;
    public GameObject alertPanel;
    public GameObject loadPanel;
    public Image loadBarProgress;

    // * mapPositions[0] : Home
    // * mapPositions[1 -> 9] : Minigame

    // * scenes[0] : HomeScene
    // * scenes[1 -> 9] : MinigameScene
    // * scenes[10] : MainScene
    public List<UnityEngine.Object> scenes;
    WaitForSeconds wait = new WaitForSeconds(0.5f);

    int lastIndex;
    bool isAtHome;
    public List<Transform> mapPositions;

    void Start()
    {
        if (instance == null)
        {
            instance = this;
            lastIndex = 0;
            isAtHome = true;
            DontDestroyOnLoad(gameObject);
            StartCoroutine(Refresh());
            StartCoroutine(SetSthInHome());
        }
        else
            Destroy(gameObject);
    }

    // Repeat every minute
    IEnumerator Refresh()
    {
        yield return new WaitForSeconds(60);
        // Check new day begin
        int currentday = DateTime.Now.Day;
        Debug.Log("Current day : " + DateTime.Now);

        int lastday = DataManager.instance.IngameData.timeSave.lastSaveTime.Subtract(new TimeSpan(0, 1, 0)).Day;
        Debug.Log("Last day : " + DataManager.instance.IngameData.timeSave.lastSaveTime.Subtract(new TimeSpan(0, 1, 0)));

        if (currentday - lastday != 0)
        {
            Debug.Log("Day passed");

            //Reset remaining exp items
            DataManager.instance.IngameData.shopExpRemain = 500;

            if (isAtHome)
                ShopUIManager.instance.ResetDisplayExpItem();

            //Reset quest
            Minigame1.Reset();
            Minigame2.Reset();
            Minigame3.Reset();
            Minigame4.Reset();
            Minigame5.Reset();
            Minigame6.Reset();
            Minigame7.Reset();
            Minigame8.Reset();
            Minigame9.Reset();

            DataManager.instance.SaveMinigameData();
            QuestInitialization.InitialzeQuestList();

            //Save
            DataManager.instance.SaveUnit();
            DataManager.instance.SaveQuest();
        }

        // * Only refresh when in HomeScene !!!
        if (isAtHome)
        {
            // Refresh egg time
            UpgradeManager.instance.UpdateEggTime();
        }
        else
        {
            foreach (var egg in DataManager.instance.IngameData.eggList)
            {
                if (egg.remainTime >= 1)
                    egg.remainTime -= 1;
            }
            DataManager.instance.SaveEgg();
        }

        StartCoroutine(Refresh());

    }
    public void Alert(string message)
    {
        alertMessage.text = message;
        alertPanel.SetActive(true);
    }

    public void LoadMinigame(int index)
    {
        if (!PlayerMove.isBlock)
        {
            // Turn off main sound
            AudioManager.instance.StopAudio();

            lastIndex = index;

            StartCoroutine(LoadMinigameHandler(index));
        }
    }

    public void ReturnHome()
    {
        if (!PlayerMove.isBlock)
        {
            // Turn off main sound
            Debug.Log("Return Main Scene");
            isAtHome = true;
            StartCoroutine(ReturnHomeHandler());
        }
    }
    public void ReturnMain()
    {
        if (!PlayerMove.isBlock)
        {
            // Turn off home sound
            isAtHome = false;
            Debug.Log("Return Main Scene");
            StartCoroutine(ReturnMainHandler());
        }
    }

    IEnumerator LoadMinigameHandler(int index)
    {
        loadPanel.SetActive(true);
        loadBarProgress.fillAmount = 0;
        yield return new WaitForSeconds(1);
        AsyncOperation scene = SceneManager.LoadSceneAsync(index);

        while (!scene.isDone)
        {
            loadBarProgress.fillAmount = scene.progress;
            yield return wait;
        }
        loadPanel.SetActive(false);

    }

    IEnumerator ReturnMainHandler()
    {
        loadPanel.SetActive(true);
        loadBarProgress.fillAmount = 0;
        yield return new WaitForSeconds(1);
        AsyncOperation mainScene = SceneManager.LoadSceneAsync(10);

        while (!mainScene.isDone)
        {
            loadBarProgress.fillAmount = mainScene.progress;
            yield return null;
        }
        loadPanel.SetActive(false);

        PlayerMove.isBlock = false;

        mapPositions = new List<Transform>(GameObject.Find("Locations").GetComponentsInChildren<Transform>());
        mapPositions.RemoveAt(0);

        // For chest
        AwardManager.instance.chestPanel = DialogueManager.instance.chestPanel;

        // Todo : set sound 
        AudioManager.instance.MainBackground();

        // Todo : Set Player position
        GameObject.Find("Player").transform.position = mapPositions[lastIndex].position;
        Camera.main.transform.position = mapPositions[lastIndex].position;
    }

    IEnumerator ReturnHomeHandler()
    {
        lastIndex = 0;
        loadPanel.SetActive(true);
        loadBarProgress.fillAmount = 0;
        yield return new WaitForSeconds(1);
        AsyncOperation homeScene = SceneManager.LoadSceneAsync(11);

        while (!homeScene.isDone)
        {
            loadBarProgress.fillAmount = homeScene.progress;
            yield return wait;
        }
        loadPanel.SetActive(false);

        // Todo : set sound 
        AudioManager.instance.HomeBackground();

        StartCoroutine(SetSthInHome());
    }

    IEnumerator SetSthInHome()
    {
        yield return new WaitUntil(() => HomeManager.instance != null);

        Button leaveButton = HomeManager.instance.leaveButton.GetComponent<Button>();
        leaveButton.onClick.AddListener(ReturnMain);

        AwardManager.instance.chestPanel = HomeManager.instance.chestPanel;
        alertPanel = HomeManager.instance.alertPanel;
        alertMessage = HomeManager.instance.alertMessage;
    }

}
