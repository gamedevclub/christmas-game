using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CursorManager : MonoBehaviour
{
    public GameObject clickEffect;
    string currentSceneName;
    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
    void Start()
    {
        currentSceneName = SceneManager.GetActiveScene().name;
        Cursor.visible = false;
    }
    // Update is called once per frame
    void Update()
    {
        
        if (currentSceneName != SceneManager.GetActiveScene().name)
        {
            Cursor.visible = false;
        }
        Vector2 cursorPosition = Helper7.Camera.ScreenToWorldPoint(Input.mousePosition);
        transform.position = cursorPosition;
        if (Input.GetMouseButtonDown(0))
        {
            Instantiate(clickEffect, transform.position, Quaternion.identity);
        }
    }
}
