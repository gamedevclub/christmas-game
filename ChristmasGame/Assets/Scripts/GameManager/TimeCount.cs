using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class TimeCount : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("CheckHatchTime", 60, 60);
    }

    // Update is called once per frame
    void CheckHatchTime()
    {
        DataManager.instance.IngameData.eggList.ForEach(element => element.remainTime = element.remainTime > 0 ? element.remainTime -=1 : 0);
        DataManager.instance.SaveEgg();
    }
}
