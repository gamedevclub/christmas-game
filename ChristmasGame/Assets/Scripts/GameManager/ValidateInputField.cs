using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ValidateInputField : MonoBehaviour
{
    InputField inputField;
    private void Start()
    {
        inputField = GetComponentInChildren<InputField>();
        inputField.contentType = InputField.ContentType.IntegerNumber;
    }
    public void ValueChanged()
    {
        if (inputField.text == "-" || inputField.text == "0") inputField.text = "";
    }
}
