using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Status9
{
    Go, In, Out
}
public class Node9 : MonoBehaviour
{
    public KeyCode keySnowman;
    public KeyCode keyPrincess;
    public float speed;
    public int line;
    public GameObject keySnowmanSprite;
    public GameObject keyPrincessSprite;
    public GameObject particleHit;
    public GameObject particleMiss;
    Status9 status = Status9.Go;

    private void Update()
    {
        // ! Only first node in line
        if (GameManager9.instance.lines[line].IndexOf(this) == 0 && GameManager9.instance.isGameActive)
        {
            bool hit = true;
            // Todo: GetKeyUp Handler
            if (Input.GetKeyUp(keySnowman))
            {
                if (status == Status9.In) // * Hit
                {
                    keySnowman = KeyCode.None;
                    keySnowmanSprite.SetActive(false);
                    GameManager9.instance.AddScore(Character9.Snowman);
                    GameManager9.instance.comboSnowman += 1;
                    GameManager9.instance.comboSnowmanText.text = "Combo của Người tuyết : " + GameManager9.instance.comboSnowman.ToString();
                    Instantiate(particleHit, keySnowmanSprite.transform.position, Quaternion.identity);
                }
                else if (status == Status9.Go)
                {
                    GameManager9.instance.Die(line, true, false);
                }
                else if (status == Status9.Out)
                {
                    hit = false;
                    keySnowman = KeyCode.None;
                    keySnowmanSprite.SetActive(false);
                    GameManager9.instance.comboSnowman = 0;
                    GameManager9.instance.comboSnowmanText.text = "Combo của Người tuyết : " + GameManager9.instance.comboSnowman.ToString();
                    Instantiate(particleMiss, keySnowmanSprite.transform.position, Quaternion.identity);

                }
            }
            if (Input.GetKeyUp(keyPrincess))
            {
                if (status == Status9.In)  // * Hit
                {
                    keyPrincess = KeyCode.None;
                    keyPrincessSprite.SetActive(false);
                    GameManager9.instance.AddScore(Character9.Princess);
                    GameManager9.instance.comboPrincess += 1;
                    GameManager9.instance.comboPrincessText.text = "Combo của Công chúa : " + GameManager9.instance.comboPrincess.ToString();
                    Instantiate(particleHit, keyPrincessSprite.transform.position, Quaternion.identity);

                }
                else if (status == Status9.Go)
                {
                    GameManager9.instance.Die(line, false, true);
                }
                else if (status == Status9.Out)
                {
                    hit = false;
                    keyPrincess = KeyCode.None;
                    keyPrincessSprite.SetActive(false);
                    GameManager9.instance.comboPrincess = 0;
                    GameManager9.instance.comboPrincessText.text = "Combo của Công chúa : " + GameManager9.instance.comboPrincess.ToString();
                    Instantiate(particleMiss, keyPrincessSprite.transform.position, Quaternion.identity);

                }
            }

            // Todo: Destroy node when Hit (both Snownman and Princess)
            if (keySnowman == keyPrincess) // * KeyCode.None
            {
                DestroyNode(hit);
            }
        }
    }

    private void FixedUpdate()
    {
        transform.position += Vector3.down * speed * Time.fixedDeltaTime;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "LimitLine9")
        {
            DestroyNode(hit: false);
            return;
        }

        if (other.tag == "HitLine9")
        {
            status = Status9.In;
        }

    }

    private void OnTriggerExit2D(Collider2D other)
    {
        // Todo: Remove node from line 
        if (other.tag == "HitLine9")
        {
            status = Status9.Out;
        }

    }

    void DestroyNode(bool hit)
    {

        // Todo: Remove node from line and Run animation -> animation will destroy gameobject
        speed = 0;
        //GetComponent<Animator>().SetTrigger("Destroy");
        Destroy(gameObject, 0.5f);
        if (hit)
        {
            AudioManager9.instance.Play("Hit", 1, 0);
            //Debug.Log("Hit in line : " + line);
        }
        else
        {
            //Debug.Log("Miss in line : " + line);
            bool missInSnowman = keySnowmanSprite.activeSelf;
            bool missInPrincess = keyPrincessSprite.activeSelf;
            GameManager9.instance.Die(line, missInSnowman, missInPrincess);
            if (keyPrincessSprite.activeSelf)
            {
                keyPrincessSprite.SetActive(false);
                Instantiate(particleMiss, keyPrincessSprite.transform.position, Quaternion.identity);
            }
            if (keySnowmanSprite.activeSelf)
            {
                keySnowmanSprite.SetActive(false);
                Instantiate(particleMiss, keySnowmanSprite.transform.position, Quaternion.identity);
            }
        }
        StartCoroutine(RemoveNode());
    }

    IEnumerator RemoveNode()
    {
        yield return new WaitForEndOfFrame();
        GameManager9.instance.lines[line].Remove(this);
    }
}
