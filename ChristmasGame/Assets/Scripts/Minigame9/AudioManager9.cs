using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using System;

[System.Serializable]
public class Sound9
{
    public string name;
    public AudioClip clip;

    public bool loop;

    [Range(0f, 1f)]
    public float volume;

    [HideInInspector]
    public AudioSource source;
}

public class AudioManager9 : MonoBehaviour
{
    public static AudioManager9 instance;
    public AudioMixerGroup volume;
    public Sound9[] sounds;

    bool canPlay;
    private void Awake()
    {
        if (instance == null)
            instance = this;

        canPlay = true;

        foreach (Sound9 s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();

            s.source.clip = s.clip;

            s.source.volume = s.volume;

            s.source.loop = s.loop;

            s.source.outputAudioMixerGroup = volume;
        }
    }


    public void Play(string name, float speed = 1, float delay = 0)
    {
        if (name == "Theme")
            Debug.Log("Time play : " + name + " - " + (Time.time + delay));
        Sound9 s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
            Debug.LogWarning("Sound: " + name + " not found !!");
        else if (canPlay)
        {
            s.source.pitch = speed;
            if (delay <= 0.01f)
                s.source.Play();
            else
                s.source.PlayDelayed(delay);
        }
    }

    public void StopAll()
    {
        canPlay = false;
        foreach (var s in sounds)
        {
            s.source.Stop();
        }
    }
}
