using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum Mode9
{
    Single, Dual, Fast
}
public enum Character9
{
    Snowman, Princess
}
public class GameManager9 : MonoBehaviour
{
    public static GameManager9 instance;
    public GameObject princessGFX;
    public List<KeyCode> snowmanKey;
    public List<KeyCode> princessKey;
    public List<Transform> spawnPoints;
    public GameObject nodePrefab;
    public Mode9 mode;
    public List<int> listNode;
    public float speed;
    public float timeEachNode;
    public float firstMusicDelay;
    public float nodeDelay;
    public float speedUpFactor;
    public List<List<Node9>> lines;
    public GameObject lifeParticle;
    public List<GameObject> linesMiss;


    [Space(10)]
    [Header("UI")]
    public Text timeText;
    public Text scoreText;
    public List<Image> lifes;
    public Text comboSnowmanText;
    public Text comboPrincessText;
    public Text timePrepare;
    public Sprite heartSprite;
    public Sprite emptySprite;

    WaitForSeconds timeWait;
    WaitForSeconds nodeWait;

    int time;
    int index;
    int life;
    int score;
    int startLife;
    [System.NonSerialized] public int comboSnowman;
    [System.NonSerialized] public int comboPrincess;
    [System.NonSerialized] public bool isGameActive;

    int maxComboSnow;
    int maxComboPrin;

    Coroutine generateNode;


    private void Start()
    {
        if (instance == null)
            instance = this;

        timeWait = new WaitForSeconds(1);
        nodeWait = new WaitForSeconds(timeEachNode);

        princessGFX.SetActive(false);

        time = 0;
        timeText.text = "Thời gian : 00:00";
        score = 0;
        scoreText.text = "Điểm : 0";
        life = lifes.Count;
        startLife = life;

        foreach (Image life in lifes)
            life.sprite = heartSprite;

        comboSnowman = 0;
        maxComboSnow = 0;
        comboSnowmanText.text = "Combo của Người tuyết : " + comboSnowman.ToString();
        comboPrincess = 0;
        maxComboPrin = 0;
        comboPrincessText.text = "Combo của Công chúa : " + comboPrincess.ToString();

        mode = Mode9.Single;

        index = 0;

        lines = new List<List<Node9>>();
        lines.Add(new List<Node9>());
        lines.Add(new List<Node9>());
        lines.Add(new List<Node9>());

        PlayGame();
    }

    private void Update()
    {
        if (lines[0].Count == 0)
        {
            if (Input.GetKeyDown(snowmanKey[0]))
                Die(0, true, false);
            if (Input.GetKeyDown(princessKey[0]))
                Die(0, false, true);
        }
        if (lines[1].Count == 0)
        {
            if (Input.GetKeyDown(snowmanKey[1]))
                Die(1, true, false);
            if (Input.GetKeyDown(princessKey[1]))
                Die(1, false, true);
        }
        if (lines[2].Count == 0)
        {
            if (Input.GetKeyDown(snowmanKey[2]))
                Die(2, true, false);
            if (Input.GetKeyDown(princessKey[2]))
                Die(2, false, true);
        }

    }

    public void PlayGame()
    {
        StartCoroutine(Prepare());
        ActiveMode(Mode9.Single);
        StartCoroutine(UpdateTime());
    }

    IEnumerator Prepare()
    {
        timePrepare.text = "3";
        AudioManager9.instance.Play("Count");
        yield return timeWait;
        timePrepare.text = "2";
        AudioManager9.instance.Play("Count");
        yield return timeWait;
        timePrepare.text = "1";
        AudioManager9.instance.Play("Count");
        yield return timeWait;
        timePrepare.text = "Cùng nhảy nào !!!";
        yield return timeWait;
        timePrepare.transform.parent.gameObject.SetActive(false);
        isGameActive = true;
    }

    IEnumerator UpdateTime()
    {
        while (true)
        {
            yield return timeWait;
            time += 1;
            timeText.text = "Thời gian : " + ConvertTime(time);
        }
    }

    IEnumerator GenerateNode(float firstTimeWait)
    {
        if (firstTimeWait != 0)
            yield return new WaitForSeconds(firstTimeWait);

        Debug.Log("Time arrive first node : " + (Time.time + 10 / speed));
        while (true)
        {
            CreateNode(listNode[index]);
            index++;
            if (index >= listNode.Count)
            {
                index = 0;
                if (mode == Mode9.Single)
                    ActiveMode(Mode9.Dual);
                else if (mode == Mode9.Dual)
                    ActiveMode(Mode9.Fast);
                else if (mode == Mode9.Fast)
                    GameOver(Result.Victory);
                break;
            }

            yield return nodeWait;
        }
        Debug.Log("End mode at : " + Time.time);
    }

    void ActiveMode(Mode9 mode)
    {
        this.mode = mode;
        if (mode == Mode9.Single)
        {
            AudioManager9.instance.Play("Theme", 1, firstMusicDelay);
            generateNode = StartCoroutine(GenerateNode(firstMusicDelay - nodeDelay));
        }
        else if (mode == Mode9.Dual)
        {
            AudioManager9.instance.Play("Theme", 1, nodeDelay);
            generateNode = StartCoroutine(GenerateNode(0));
            princessGFX.SetActive(true);
        }
        else if (mode == Mode9.Fast)
        {
            timeEachNode *= speedUpFactor;
            float speedup = 1 / speedUpFactor;
            speed *= speedup;
            nodeWait = new WaitForSeconds(timeEachNode);
            AudioManager9.instance.Play("Theme", speedup, nodeDelay * speedUpFactor);
            generateNode = StartCoroutine(GenerateNode(0));

        }
    }

    void CreateNode(int numOfNode)
    {
        if (numOfNode == 1)
        {
            int line = Random.Range(0, 3);
            SpawnNode(line);
        }
        else if (numOfNode == 2)
        {
            int secondLine = 0;
            int firstLine = Random.Range(0, 3);
            if (firstLine == 0)
            {
                secondLine = Random.Range(1, 3);
            }
            else if (firstLine == 2)
            {
                secondLine = Random.Range(0, 1);
            }
            else
            {
                secondLine = (Random.Range(0, 1) == 0 ? -1 : 1) + firstLine;
            }
            SpawnNode(firstLine);
            SpawnNode(secondLine);
        }
        else if (numOfNode == 3)
        {
            SpawnNode(0);
            SpawnNode(1);
            SpawnNode(2);
        }
    }

    void SpawnNode(int line)
    {
        GameObject nodeObj = Instantiate(nodePrefab, spawnPoints[line].position, Quaternion.identity);
        Node9 node = nodeObj.GetComponent<Node9>();
        if (mode == Mode9.Single)
        {
            node.keySnowman = snowmanKey[line];
            node.keySnowmanSprite.transform.localPosition = Vector3.zero;
            node.keySnowmanSprite.SetActive(true);
            node.keyPrincess = KeyCode.None;
            node.keyPrincessSprite.SetActive(false);
        }
        else
        {
            node.keySnowman = snowmanKey[line];
            node.keySnowmanSprite.SetActive(true);
            node.keyPrincess = princessKey[line];
            node.keyPrincessSprite.SetActive(true);
        }
        node.speed = speed;

        node.line = line;
        lines[line].Add(node);
    }

    string ConvertTime(int seconds)
    {
        int minutes = seconds / 60;
        seconds -= minutes * 60;
        return (minutes > 9 ? "" : "0") + minutes.ToString() + ":" + (seconds > 9 ? "" : "0") + seconds.ToString();
    }

    public void Die(int missLine, bool missInSnowman = false, bool missInPrincess = false)
    {
        if (isGameActive)
        {
            AudioManager9.instance.Play("Miss", 1, 0);
            if (missInSnowman)
            {
                if (comboSnowman > maxComboSnow)
                    maxComboSnow = comboSnowman;
                comboSnowman = 0;
                comboSnowmanText.text = "Combo của Người tuyết : " + comboSnowman.ToString();
            }
            if (missInPrincess)
            {
                if (comboPrincess > maxComboPrin)
                    maxComboPrin = comboPrincess;
                comboPrincess = 0;
                comboPrincessText.text = "Combo của Công chúa : " + comboPrincess.ToString();
            }

            if (life == startLife)
            {
                if (time > Minigame9.time)
                    Minigame9.time = time;
            }

            life -= 1;

            lifes[life].sprite = emptySprite;
            Instantiate(lifeParticle, lifes[life].transform.position, Quaternion.identity);

            StartCoroutine(DeactiveMissLine(missLine));

            if (life <= 0)
                GameOver();
        }
    }
    public void AddScore(Character9 character)
    {
        if (isGameActive)
        {
            if (character == Character9.Snowman && comboSnowman > 30)
                score += 2;
            else if (character == Character9.Princess && comboPrincess > 30)
                score += 2;
            else
                score += 1;

            scoreText.text = "Điểm : " + score.ToString();
        }
    }

    void GameOver(Result result = Result.GameOver)
    {
        StopAllCoroutines();
        isGameActive = false;

        int coin = score;
        int exp = score / 10;
        bool chest = mode == Mode9.Fast;

        if (comboSnowman > maxComboSnow)
            maxComboSnow = comboSnowman;
        if (comboPrincess > maxComboPrin)
            maxComboPrin = comboPrincess;

        if (maxComboSnow > Minigame9.comboSnowman)
            Minigame9.comboSnowman = maxComboSnow;
        if (maxComboPrin > Minigame9.comboPrincess)
            Minigame9.comboPrincess = maxComboPrin;

        Debug.Log("Max combo snowman : " + maxComboSnow);
        Debug.Log("Max combo princess : " + maxComboPrin);
        Debug.Log("Time play : " + time + " seconds");

        AudioManager9.instance.StopAll();

        DataManager.instance.SaveMinigameData(9);

        AwardManager.instance.GetGameAward(coin, exp, chest, result);

        Debug.Log("Game Over");
    }

    IEnumerator DeactiveMissLine(int line)
    {
        linesMiss[line].SetActive(true);
        yield return new WaitForSeconds(0.25f);
        linesMiss[line].SetActive(false);
    }
}
