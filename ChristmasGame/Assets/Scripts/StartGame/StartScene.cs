using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class StartScene : MonoBehaviour
{
    public Object sceneWantToLoad;
    public GameObject newGameBox;

    // public int firstGame = 1;

    public List<Button> LoadButton;
    public List<Button> NewGame;
    public List<Text> slotName;
    public InputField nameInput;
    public GameObject loadPanel;
    public Image loadBarProgress;

    WaitForSeconds waitLoadScene = new WaitForSeconds(0.1f);

    private void Start()
    {
        newGameBox.SetActive(false);
        AudioManager.instance.LoginBackground();
    }

    private void Update()
    {
        //display slot game
        //slot1
        if (PlayerPrefs.GetString("slot1", "empty") == "empty")
        {
            LoadButton[0].gameObject.SetActive(false);
            NewGame[0].gameObject.SetActive(true);
        }
        else
        {
            NewGame[0].gameObject.SetActive(false);
            LoadButton[0].gameObject.SetActive(true);
            slotName[0].text = PlayerPrefs.GetString("slot1");
        }
        //slot2
        if (PlayerPrefs.GetString("slot2", "empty") == "empty")
        {
            LoadButton[1].gameObject.SetActive(false);
            NewGame[1].gameObject.SetActive(true);
        }
        else
        {
            //Debug.Log(PlayerPrefs.GetString("slot2"));
            NewGame[1].gameObject.SetActive(false);
            LoadButton[1].gameObject.SetActive(true);
            slotName[1].text = PlayerPrefs.GetString("slot2");
        }
        //slot3
        if (PlayerPrefs.GetString("slot3", "empty") == "empty")
        {
            LoadButton[2].gameObject.SetActive(false);
            NewGame[2].gameObject.SetActive(true);
        }
        else
        {
            NewGame[2].gameObject.SetActive(false);
            LoadButton[2].gameObject.SetActive(true);
            slotName[2].text = PlayerPrefs.GetString("slot3");
        }
    }

    // Choose old game slot 
    public void ChooseSlotGame(string slotID)
    {
        PlayerPrefs.SetString("slotID", slotID);
        PlayerPrefs.SetInt("firstPlay", 0);
        // load scene
        StartCoroutine(LoadSceneAsync());
    }

    // Choose new game slot
    public void ShowNewGameBox(string slotID)
    {
        newGameBox.SetActive(true);
        PlayerPrefs.SetString("slotID", slotID);
    }

    public void MakeNewGame()
    {
        PlayerPrefs.SetInt("firstPlay", 1);
        // get name
        PlayerPrefs.SetString(PlayerPrefs.GetString("slotID"), nameInput.text);
        StartCoroutine(LoadSceneAsync());
    }

    // Delete Slot
    public void DeleteData(string slotID)
    {
        BinarySerializer.DeleteSlotData(slotID);
        PlayerPrefs.DeleteKey(slotID);
    }

    public void Back()
    {
        newGameBox.SetActive(false);
    }

    IEnumerator LoadSceneAsync()
    {

        loadPanel.SetActive(true);

        loadBarProgress.fillAmount = 0;

        // Todo: Load Data
        DataManager.instance.StartGame();
        yield return new WaitUntil(() => DataManager.instance.loadFinish);

        loadBarProgress.fillAmount = 0.5f;

        //yield return new WaitForSeconds(1);
        AsyncOperation homeScene = SceneManager.LoadSceneAsync(11);
        // if (Application.isPlaying)
        //     homeScene = SceneManager.LoadSceneAsync(11);
        // else
        //     homeScene = SceneManager.LoadSceneAsync(sceneWantToLoad.name);

        // Todo : Load Scene
        while (!homeScene.isDone)
        {
            loadBarProgress.fillAmount = 0.5f + homeScene.progress / 2;
            yield return null;
        }

    }
}
