using System.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

public class ShowPlayerPrefs : MonoBehaviour
{
    public static ShowPlayerPrefs instance;
    public Text playerInfo;
    private void Start()
    {
        if (!instance)
            instance = this;
        ShowInfo();
    }

    public void GoStartScene()
    {
        SceneManager.LoadScene("StartScene");
    }

    public void ShowInfo()
    {
        playerInfo.text = "SlotID: " + PlayerPrefs.GetString("slotID");
        playerInfo.text += "\nName: " + PlayerPrefs.GetString(PlayerPrefs.GetString("slotID"));
    }
}


