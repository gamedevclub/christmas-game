
using System;
public class Minigame1
{
    public static int bats;
    public static int tourches;
    public static int score;
    public static void Reset()
    {
        bats = 0;
        tourches = 0;
        score = 0;
    }
}
public class Minigame2
{
    public static int score;
    public static int hitTime;
    public static void Reset()
    {
        score = 0;
        hitTime = 0;
    }
}
public class Minigame3
{
    public static int score;
    public static int logs;
    public static int balanceTime;
    public static void Reset()
    {
        score = 0;
        logs = 0;
        balanceTime = 0;
    }
}
public class Minigame4
{
    public static int gold;
    public static int exp;
    public static int time;
    public static void Reset()
    {
        gold = 0;
        exp = 0;
        time = 0;
    }
}
public class Minigame5
{
    public static int distance;
    public static int meat;
    public static void Reset()
    {
        distance = 0;
        meat = Int32.MaxValue;;
    }
}
public class Minigame6
{
    public static int armors;
    public static int items;
    public static int score;
    public static void Reset()
    {
        armors = 0;
        items = 0;
        score = 0;
    }
}
public class Minigame7
{
    public static int items;
    public static void Reset()
    {
        items = 0;
    }
}
public class Minigame8
{
    public static int time;
    public static int items;
    public static void Reset()
    {
        time = 0;
        items = 0;
    }
}
public class Minigame9
{
    public static int comboSnowman;
    public static int comboPrincess;
    public static int time;
    public static void Reset()
    {
        comboSnowman = 0;
        comboPrincess = 0;
        time = 0;
    }
}