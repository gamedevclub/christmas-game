using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class GiftManager : MonoBehaviour
{
    public GameObject getPanel;
    public GameObject moneyPanel;
    public Text money;
    public List<GameObject> stars;
    public Text giftName;
    public Image giftImage;
    public Button getButton;
    public GameObject getEffect;
    public Button moneyButton;

    [SerializeField]
    private TMP_InputField field;

    void Start()
    {
        if (DataManager.instance.IngameData.giftReceived)
        {
            field.gameObject.SetActive(false);
        }
    }

    public void GetLinkContest()
    {
        System.Diagnostics.Process.Start(new ProcessStartInfo
        {
            FileName = "https://forms.gle/A1rDJSH8L2sBYGtM9",
            UseShellExecute = true
        });
    }
    public void GetLinkSurvey()
    {
        System.Diagnostics.Process.Start(new ProcessStartInfo
        {
            FileName = "https://forms.gle/Ycvw8VjiZfoztaTZ9",
            UseShellExecute = true
        });
    }
    public void GetGift()
    {
        string code = field.text;
        if (code == "MERRYCHRISTMAS2021")
        {
            //PlayerPrefs.SetInt("GiftReceived", 1);
            field.gameObject.SetActive(false);

            Pet newPet = DataLoader.instance.GetPetRandom(2);
            Decoration newDeco1 = DataLoader.instance.GetDecoRandom(2);
            Decoration newDeco2 = DataLoader.instance.GetDecoRandom(1);

            PetGenerate.instance.AddPet(newPet);
            DataManager.instance.IngameData.petList.Add(newPet);
            DataManager.instance.SavePet();
            DataManager.instance.IngameData.decorationList.Add(newDeco1);
            DataManager.instance.IngameData.decorationList.Add(newDeco2);
            DataManager.instance.SaveDeco();

            DataManager.instance.IngameData.money += 5000;
            DataManager.instance.IngameData.giftReceived = true;
            DataManager.instance.SaveUnit();


            OpenGift(0, newPet, newDeco1, newDeco2);
        }
        else
            GameManager.instance.Alert("Mã của bạn bị sai, hãy kiểm tra lại thử nhé");
    }

    public void OpenGift(int index, Pet newPet, Decoration newDeco1, Decoration newDeco2)
    {
        AudioManager.instance.Gacha();
        switch (index)
        {
            case 0:
                getPanel.SetActive(true);

                giftName.text = newPet.name;

                giftImage.sprite = Resources.Load<Sprite>(newPet.art);
                stars[0].SetActive(true);
                stars[1].SetActive(true);
                stars[2].SetActive(true);

                getButton.onClick.RemoveAllListeners();
                getButton.onClick.AddListener(delegate { OpenGift(1, newPet, newDeco1, newDeco2); });

                Instantiate(getEffect, Vector3.zero, Quaternion.identity);

                break;
            case 1:

                giftName.text = newDeco1.name;

                giftImage.sprite = Resources.Load<Sprite>(newDeco1.art);
                stars[0].SetActive(true);
                stars[1].SetActive(true);
                stars[2].SetActive(true);

                getButton.onClick.RemoveAllListeners();
                getButton.onClick.AddListener(delegate { OpenGift(2, newPet, newDeco1, newDeco2); });

                Instantiate(getEffect, Vector3.zero, Quaternion.identity);

                break;
            case 2:

                giftName.text = newDeco2.name;

                giftImage.sprite = Resources.Load<Sprite>(newDeco2.art);
                stars[0].SetActive(true);
                stars[1].SetActive(true);
                stars[2].SetActive(false);

                getButton.onClick.RemoveAllListeners();
                getButton.onClick.AddListener(delegate { getPanel.SetActive(false); GetMoney(5000); });

                Instantiate(getEffect, Vector3.zero, Quaternion.identity);

                break;

            default:
                getButton.onClick.AddListener(delegate { getPanel.SetActive(false); });
                break;
        }
    }

    public void GetMoney(int quantity)
    {
        moneyPanel.SetActive(true);
        money.text = quantity.ToString();
        moneyButton.onClick.AddListener(delegate { AudioManager.instance.BuySell(); });
        Instantiate(getEffect, Vector3.zero, Quaternion.identity);

    }
}
