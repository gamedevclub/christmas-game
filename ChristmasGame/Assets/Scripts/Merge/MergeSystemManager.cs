using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MergeSystemManager : MonoBehaviour
{
    [SerializeField] MergeInventory inventory;
    [SerializeField] MergeGrid mergeGrid;

    //public List<MergeItem> higherRarityItems;
    public GameObject mergeEffect;
    public GetItemMerge itemPanel;


    private void Awake()
    {
        inventory.OnItemClickedEvent += InventoryClick;
        mergeGrid.OnItemClickedEvent += GridClick;
    }

    private void InventoryClick(MergeItem mergeItem)
    {
        Place(mergeItem);
    }

    private void GridClick(MergeItem mergeItem)
    {
        Unplace(mergeItem);
    }

    public void Place(MergeItem item)
    {
        if (inventory.RemoveItem(item)) // ko dc xoa lien ma doi an nut xong moi xoa
        {
            MergeItem previousItem;
            if (mergeGrid.AddItem(item, out previousItem))
            {
                if (previousItem != null)
                {
                    inventory.AddItem(previousItem);
                }
            }
            else
            {
                inventory.AddItem(item);
            }
        }
    }

    public void Unplace(MergeItem item)
    {
        if (!inventory.IsFull() && mergeGrid.RemoveItem(item))
        {
            inventory.AddItem(item);
        }
    }

    public void UnplaceGridItems()
    {
        var items = mergeGrid.GetAllMergeItems();
        foreach (var item in items)
        {
            Unplace(item);
        }
    }

    public void MergeButton()
    {
        // trung bình level -> làm tròn -> x
        // 70% x - 30 % x + 1 -> lưu ý x = 2 -> 100% x

        if (!mergeGrid.IsAllSlotsFull()) return;

        float level = 0;
        foreach (var item in mergeGrid.mergeSlots)
            level += item.MergeItem.deco.level / 3.0f;

        if (!inventory.IsFull() && mergeGrid.RemoveAllItem())
        {
            //inventory.Append(higherRarityItems[Random.Range(0, higherRarityItems.Count)]);

            int newLevel = Mathf.RoundToInt(level);
            if (newLevel != 2)
            {
                int random = Random.Range(0, 100);
                if (random > 70)
                    newLevel++;
            }

            Decoration newDeco = DataLoader.instance.GetDecoRandom(newLevel);
            Sprite art = Resources.Load<Sprite>(newDeco.art);

            GameObject effect = Instantiate(mergeEffect, Vector3.zero, Quaternion.identity);

            itemPanel.itemSprite.sprite = art;

            for (int i = 0; i <= newDeco.level; i++)
                itemPanel.stars[i].gameObject.SetActive(true);
            for (int i = newDeco.level + 1; i < itemPanel.stars.Count; i++)
                itemPanel.stars[i].gameObject.SetActive(false);

            itemPanel.itemName.text = newDeco.name;

            itemPanel.gameObject.SetActive(true);

            inventory.Append(new MergeItem(newDeco));

            AudioManager.instance.MergeUpgrade();
        }
    }

    public void ConfirmHandler()
    {
        inventory.RefreshAfterMerging();
    }
}
