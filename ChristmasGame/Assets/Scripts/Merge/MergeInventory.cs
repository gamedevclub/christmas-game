using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class MergeInventory : MonoBehaviour
{
    #region Singleton

    public static MergeInventory instance;

    #endregion

    [SerializeField] List<MergeItem> items = new List<MergeItem>();
    [SerializeField] protected Transform itemsParent;
    [SerializeField] protected List<BaseItemSlot> itemSlots;

    [SerializeField] GameObject itemSlotPrefab;

    public event Action<MergeItem> OnItemClickedEvent;

    [SerializeField] MergeItem[] itemsArray; // array that allows null elements to indicate those elements are on mergeGrid

    protected virtual void Start()
    {
        instance = this;

        Debug.Log("Merge start");

        //PopulateItemSlots();

        //for (int i = 0; i < itemSlots.Count; i++)
        //{
        //    itemSlots[i].OnClickEvent += OnItemClickedEvent;
        //}
    }

    private int getMaxSlotsCount()
    {
        return (DataManager.instance.IngameData.decorationList.Count / 12 + 1) * 12;
    }

    private void PopulateItemSlots()
    {
        int maxSlotsCount = getMaxSlotsCount();
        for (int i = 0; i < maxSlotsCount; i++)
        {
            CreateNewSlot();
        }
        Refresh();
    }

    private void CreateNewSlot(bool withClicked = false)
    {
        GameObject itemSlotGameObj = Instantiate(itemSlotPrefab);
        itemSlotGameObj.transform.SetParent(itemsParent, worldPositionStays: false);
        
        BaseItemSlot newSlot = itemSlotGameObj.GetComponentInChildren<BaseItemSlot>();
        
        if (withClicked)
        {
            newSlot.OnClickEvent += OnItemClickedEvent;
        }
        itemSlots.Add(newSlot);
    }

    private void AdjustItemSlot()
    {
        int maxSlots = getMaxSlotsCount();
        Debug.Log("Max Slot: " + maxSlots);
        Debug.Log("Item slots count " + itemSlots.Count);
        if (maxSlots < itemSlots.Count)
        {
            for (int i = maxSlots; i < itemSlots.Count; i++)
            {
                Destroy(itemSlots[i].transform.parent.gameObject);
            }
            int diff = itemSlots.Count - maxSlots;
            itemSlots.RemoveRange(maxSlots, diff);
        }
        else if (maxSlots > itemSlots.Count)
        {
            int diff = maxSlots - itemSlots.Count;
            for (int i = 0; i < diff; i++)
            {
                CreateNewSlot(withClicked: true);
            }
        }
    }

    // Load decorations from DataManager
    private void LoadDecorations()
    {
        items.Clear();
        for (int i = 0; i < DataManager.instance.IngameData.decorationList.Count; i++)
        {
            Decoration deco = DataManager.instance.IngameData.decorationList[i];
            if (deco.type == 0) // merge item
            {
                items.Add(new MergeItem(deco));
            }
        }
    }

    public void Load()
    {
        Debug.Log("Load Merge");
        AdjustItemSlot();
        Refresh();
        LoadDecorations();
        Refresh();
        CopyListToArray();
    }

    /// sync items with UI
    public void Refresh()
    {
        int i = 0;
        for (; i < items.Count && i < itemSlots.Count; i++)
            itemSlots[i].MergeItem = items[i];

        for (; i < itemSlots.Count; i++)
            itemSlots[i].MergeItem = null;
    }

    public void RefreshAfterMerging()
    {
        CopyArrayToList(); // push items together to delete holes
        Refresh(); // sync after-pushed items with UI
        CopyListToArray(); // assign new value to array
        DataManager.instance.SaveDeco(); // sync items with DataManager
    }

    public void CopyListToArray()
    {
        itemsArray = new MergeItem[items.Count];
        for (int i = 0; i < items.Count; i++)
        {
            itemsArray[i] = items[i];
        }
    }

    public void CopyArrayToList()
    {
        items.Clear();
        for (int i = 0; i < itemsArray.Length; i++)
        {
            if (itemsArray[i] != null)
            {
                itemsArray[i].rememberedIndex = -1;
                items.Add(itemsArray[i]);
            }
        }
    }

    public bool IsFull()
    {
        return items.Count >= itemSlots.Count;
    }

    public virtual bool AddItem(MergeItem item)
    {
        if (item == null) return false;

        if (IsFull())
            return false;
        items.Add(item);

        int itemIndex = item.rememberedIndex;
        if (itemIndex == -1) // never be removed
        {
            // append to itemsArray
            AppendToItemsArray(item);
        }
        else // used to be removed
        {
            // check index and assign to that index in array
            itemsArray[itemIndex] = item;
            itemSlots[itemIndex].ReturnNormal();
        }
        return true;

    }

    public bool Append(MergeItem item) // append new item to both items and itemsArray
    {
        if (IsFull())
            return false;
        items.Add(item);
        AppendToItemsArray(item);

        DataManager.instance.IngameData.decorationList.Add(item.deco);

        return true;
    }

    private void AppendToItemsArray(MergeItem item)
    {
        int arrayLength = itemsArray.Length;
        Array.Resize(ref itemsArray, arrayLength + 1);
        itemsArray[arrayLength] = item;
    }

    public virtual bool RemoveItem(MergeItem item)
    {
        if (items.Remove(item))
        {
            for (int i = 0; i < itemsArray.Length; i++)
            {
                if (itemsArray[i] == item)
                {
                    item.rememberedIndex = i; // get array index for add method to re assign to array
                    itemsArray[i] = null; // remove from array
                    return true;
                }
            }
        }
        return false;
    }
}
