using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class BaseItemSlot : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] protected Image image;

    public event Action<BaseItemSlot> OnPointerEnterEvent;
    public event Action<BaseItemSlot> OnPointerExitEvent;
    public event Action<MergeItem> OnClickEvent;

    protected bool isPointerOver;

    protected Color normalColor = Color.white;
    protected Color disabledColor = new Color(1, 1, 1, 0);

    [SerializeField] MergeItem _item;
    public MergeItem MergeItem
    {
        get { return _item; }
        set
        {
            _item = value;
            if (_item == null && Amount != 0) Amount = 0;

            if (_item == null)
            {
                image.sprite = null;
                image.color = disabledColor;
            }
            else
            {
                image.sprite = Resources.Load<Sprite>(_item.deco.art);
                image.color = normalColor;
            }

            if (isPointerOver)
            {
                OnPointerExit(null);
                OnPointerEnter(null);
            }
        }
    }

    private int _amount;
    public int Amount
    {
        get { return _amount; }
        set
        {
            _amount = value;
            if (_amount < 0) _amount = 0;
            if (_amount == 0 && MergeItem != null) MergeItem = null;
        }
    }

    public virtual bool CanReceiveItem(MergeItem item)
    {
        return false;
    }

    protected virtual void OnValidate()
    {
        if (image == null)
            image = GetComponent<Image>();

        MergeItem = _item;
        Amount = _amount;
    }

    protected virtual void OnDisable()
    {
        if (isPointerOver)
        {
            OnPointerExit(null);
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData != null && eventData.button == PointerEventData.InputButton.Left)
        {
            if (OnClickEvent != null)
            {
                Debug.Log("Clicked!");

                MakeDarker();
                OnClickEvent(MergeItem);
            }
        }
    }

    public void MakeDarker()
    {
        var tempColor = image.color;
        tempColor.r = 0.5f;
        tempColor.g = 0.5f;
        tempColor.b = 0.5f;
        image.color = tempColor;
    }

    public void ReturnNormal()
    {
        var tempColor = image.color;
        tempColor.r = 1f;
        tempColor.g = 1f;
        tempColor.b = 1f;
        image.color = tempColor;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        isPointerOver = true;

        if (OnPointerEnterEvent != null)
            OnPointerEnterEvent(this);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        isPointerOver = false;

        if (OnPointerExitEvent != null)
            OnPointerExitEvent(this);
    }
}
