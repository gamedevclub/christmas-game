using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class MergeGrid : MonoBehaviour
{
    #region Singleton

    public static MergeGrid instance;

    private void Awake()
    {
        instance = this;
    }

    #endregion

    private List<MergeItem> items;
    [SerializeField] Transform mergeSlotsParent;
    [SerializeField] public MergeSlot[] mergeSlots;

    public event Action<MergeItem> OnItemClickedEvent;

    private void Start()
    {
        for (int i = 0; i < mergeSlots.Length; i++)
        {
            mergeSlots[i].OnClickEvent += OnItemClickedEvent;
        }
    }

    private void OnValidate()
    {
        mergeSlots = mergeSlotsParent.GetComponentsInChildren<MergeSlot>();
    }

    public List<MergeItem> GetAllMergeItems()
    {
        items = new List<MergeItem>();
        for (int i = 0;i < mergeSlots.Length;i++)
        {
            if (mergeSlots[i].MergeItem != null)
            {
                items.Add(mergeSlots[i].MergeItem);
            }
        }
        return items;
    }

    public bool AddItem(MergeItem item, out MergeItem previousItem)
    {
        for (int i = 0; i < mergeSlots.Length; i++)
        {
            if (mergeSlots[i].MergeItem == null)
            {
                previousItem = mergeSlots[i].MergeItem;
                mergeSlots[i].MergeItem = item;
                return true;
            }
        }
        previousItem = null;
        return false;
    }

    public bool RemoveItem(MergeItem item)
    {
        for (int j = 0; j < mergeSlots.Length; j++)
        {
            if (mergeSlots[j].MergeItem == item)
            {
                mergeSlots[j].MergeItem = null;
                return true;
            }
        }

        return false;
    }


    public bool RemoveAllItem()
    {
        for (int j = 0; j < mergeSlots.Length; j++)
        {
            DataManager.instance.IngameData.decorationList.Remove(mergeSlots[j].MergeItem.deco);
            mergeSlots[j].MergeItem = null;
        }
        return true;
    }

    public bool IsAllSlotsFull()
    {
        for (int j = 0; j < mergeSlots.Length; j++)
        {
            if (mergeSlots[j].MergeItem == null) return false;
        }
        return true;
    }
}
