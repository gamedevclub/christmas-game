using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MergeItem
{
    public int rememberedIndex;
    public Decoration deco;
    public MergeItem()
    {
        rememberedIndex = -1; // -1 means item being untouched
    }

    public MergeItem(Decoration decoration)
    {
        deco = decoration;
        rememberedIndex = -1; // -1 means item being untouched
    }
}
