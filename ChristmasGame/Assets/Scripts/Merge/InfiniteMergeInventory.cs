using UnityEngine;

public class InfiniteMergeInventory : MergeInventory
{
	[SerializeField] GameObject itemSlotPrefab;

	[SerializeField] int maxSlots;
	public int MaxSlots
	{
		get { return maxSlots; }
		set { SetMaxSlots(value); }
	}

	protected override void Start()
	{
		SetMaxSlots(maxSlots);
		base.Start();
	}

    public override bool AddItem(MergeItem item)
    {
		while (!base.IsFull())
        {
			MaxSlots += 1;
        }
        return base.AddItem(item);
    }

    private void SetMaxSlots(int value)
	{
		if (value <= 0)
		{
			maxSlots = 1;
		}
		else
		{
			maxSlots = value;
		}

		if (maxSlots < itemSlots.Count)
		{
			for (int i = maxSlots; i < itemSlots.Count; i++)
			{
				Destroy(itemSlots[i].transform.parent.gameObject);
			}
			int diff = itemSlots.Count - maxSlots;
			itemSlots.RemoveRange(maxSlots, diff);
		}
		else if (maxSlots > itemSlots.Count)
		{
			int diff = maxSlots - itemSlots.Count;

			for (int i = 0; i < diff; i++)
			{
				GameObject gameObject = Instantiate(itemSlotPrefab);
				gameObject.transform.SetParent(itemsParent, worldPositionStays: false);
				itemSlots.Add(gameObject.GetComponentInChildren<MergeSlot>());
			}
		}
	}
}
