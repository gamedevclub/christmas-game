using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CountdownGame2 : MonoBehaviour
{
    // Start is called before the first frame update
    public AudioSource sound;
    public GameObject number3, number2, number1;
    Vector3 pos = new Vector3(-0.0f, -0.0f, 0);
    public static bool play;
    //public AudioSource sound;
    void Start()
    {
        play = false;
        StartCoroutine(Count());
        
    }

    public IEnumerator Count()
    {
        GameObject clone = Instantiate(number3, pos, Quaternion.identity);
        sound.Play();
        yield return new WaitForSeconds(1);
        Destroy(clone);
        sound.Play();
        clone = Instantiate(number2, pos, Quaternion.identity);
        yield return new WaitForSeconds(1);
        Destroy(clone);
        sound.Play();
        clone = Instantiate(number1, pos, Quaternion.identity);
        yield return new WaitForSeconds(1);
        Destroy(clone);
        play = true;
    }
}
