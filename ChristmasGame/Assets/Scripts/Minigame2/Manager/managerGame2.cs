using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class managerGame2 : MonoBehaviour
{
    public int level;

    public Text scoreText;
    private int score = 0;
    public static managerGame2 instance;
    int minionSpawn;

    public GameObject minion;

    int scoreAdd;
    //Hit chain
    public int chain;
    int maxChain;

    int playerLives;
    public GameObject lives;

    //For game over
    bool gameOver;
    //public GameObject gameOverUI;

    //bool deactive;

    public AudioSource bgSound1;
    public AudioSource bgSound2;
    public AudioSource hitSound;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    void Start()
    {
        score = 0;
        scoreText.text = "Score: " + score.ToString();
        level = 1;
        scoreAdd = 10;
        minionSpawn = 0;
        gameOver = false;
        chain = 0;
        maxChain = 0;
        playerLives = 2;

        bgSound1.PlayDelayed(3f);
    }

    // Update is called once per frame
    public void Addscore(int type)
    {
        if (type == 0)
        {
            score += 10;
        }
        else
        {
            score += scoreAdd;
            if (level >= 4)
            {
                minionSpawn++;
                if (minionSpawn >= 2)
                {
                    Instantiate(minion, OpponentGame2.trans.position - new Vector3(0, 0.5f, 0), Quaternion.identity);
                    if (level == 5) Instantiate(minion, OpponentGame2.trans.position - new Vector3(0, 0.5f, 0), Quaternion.identity);
                    minionSpawn = 0;
                }
            }
        }
        scoreText.text = "Score: " + score.ToString();
    }

    private void FixedUpdate()
    {

        if (score == 50)
        {
            level = 2;
            scoreAdd = 15;
        }
        else if (score == 140)
        {
            bgSound1.Stop();
            if (!bgSound2.isPlaying && !gameOver) bgSound2.Play();
            level = 3;
            scoreAdd = 20;
        }
        else if (score == 300)
        {
            level = 4;
            scoreAdd = 30;
            minionSpawn = 4;
        }
        else if (score >= 500)
        {
            level = 5;
        }

        if (chain > maxChain)
        {
            maxChain = chain;
        }
    }

    public void SubLives()
    {
        lives.transform.GetChild(playerLives).gameObject.SetActive(false);
        playerLives--;
        if (playerLives < 0)
        {
            gameOver = true;
            bgSound1.Stop();
            bgSound2.Stop();

            GameOver();
        }
    }

    public void HitSoundPlay()
    {
        hitSound.Play();
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    private void GameOver()
    {
        GameObject.FindGameObjectsWithTag("Opponent")[0].SetActive(false);
        GameObject.FindGameObjectsWithTag("Player")[0].SetActive(false);


        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Minion"))
        {
            obj.SetActive(false);
        }

        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("GameController"))
        {
            obj.SetActive(false);
        }

        if (DataManager.instance.IngameData.minigameData[1][0] < score) DataManager.instance.IngameData.minigameData[1][0] = score;
        if (DataManager.instance.IngameData.minigameData[1][1] < maxChain) DataManager.instance.IngameData.minigameData[1][1] = maxChain;

        Minigame2.score = score > Minigame2.score ? score : Minigame2.score;
        Minigame2.hitTime = maxChain > Minigame2.hitTime ? maxChain : Minigame2.hitTime;

        DataManager.instance.SaveMinigameData(2);
        AwardManager.instance.GetGameAward(score / 5, score / 50, score >= 400);
    }
}
