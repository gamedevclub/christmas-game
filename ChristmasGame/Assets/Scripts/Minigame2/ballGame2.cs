using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ballGame2 : MonoBehaviour
{
    public ParticleSystem snow;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Opponent")
        {
            managerGame2.instance.chain++;
            Debug.Log(managerGame2.instance.chain);
            managerGame2.instance.Addscore(1);
            Destroy(this.gameObject);
            
        }
        else if (other.tag == "Minion")
        {
            managerGame2.instance.Addscore(0);
            Destroy(this.gameObject);
        }
    }

    void OnBecameInvisible()
    {
        Destroy(this.gameObject);
    }
    private void Update()
    {
        float x = transform.position.x;
        float y = transform.position.y;
        if (x < -9 || x > 9 || y < -5 || y > 5)
        {
            managerGame2.instance.chain = 0;
        }
    }
    private void OnDestroy()
    {
        managerGame2.instance.HitSoundPlay();
        Instantiate(snow, transform.position, Quaternion.identity);
    }
}
