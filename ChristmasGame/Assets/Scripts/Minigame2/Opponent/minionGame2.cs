using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class minionGame2 : MonoBehaviour
{
    // Start is called before the first frame update
    private float bulletSpeed;
    public GameObject oppBullet;
    public Transform firePosition;

    private Vector2 opponentPosition;
    private float ShootingCycle;

    private float wait;

    Transform trans;
    Rigidbody2D rig;
    Vector2 ranMove;

    private float speed = 1;

    void Start()
    {
        wait = Time.time;
        ShootingCycle = 2f;
        bulletSpeed = 6;

        trans = GetComponent<Transform>();
        rig = GetComponent<Rigidbody2D>();

        ranMove = RandomMove();
    }

    // Update is called once per frame
    void Update()
    {
        opponentPosition.x = trans.position.x;
        opponentPosition.y = trans.position.y;

        Vector2 look = moveGame2.playerPosition - opponentPosition;

        if (Time.time > wait + ShootingCycle)
        {
            GameObject bullet = Instantiate(oppBullet);
            Rigidbody2D rigBullet = bullet.GetComponent<Rigidbody2D>();
            bullet.transform.position = firePosition.position;
            rigBullet.velocity = look * bulletSpeed / look.magnitude;
            wait = Time.time;
        }

        Vector2 route = ranMove - (Vector2)trans.position;

        if (route == Vector2.zero)
        {
            ranMove = RandomMove();
        }
        else
        {
            transform.position = Vector2.MoveTowards(transform.position, ranMove, speed * Time.deltaTime);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Ball")
        {
            Destroy(this.gameObject);
        }
    }

    Vector2 RandomMove()
    {
        Vector2 pos;

        float x = Random.Range(-3.0f, 3.0f);
        float y = Random.Range(0.7f, 1.0f);

        pos.x = x;
        pos.y = y;

        return pos;
    }
}
