using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpponentGame2 : MonoBehaviour
{
    private float bulletSpeed;
    public GameObject oppBullet;
    public GameObject bigBullet;
    public Transform firePosition;

    private float ShootingCycle;

    private float wait;

    public static Transform trans;
    Rigidbody2D rig;

    private float speed;

    private int bigOne = 0;

    Vector2 ranMove;

    //no shoot first
    bool first;
    void Start()
    {
        wait = Time.time;
        ShootingCycle = 1.5f;
        bulletSpeed = 6.5f;

        trans = GetComponent<Transform>();
        rig = GetComponent<Rigidbody2D>();

        ranMove = RandomMove();
        speed = 1.5f;

        first = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (CountdownGame2.play == true)
        {

            Vector2 look = moveGame2.playerPosition - (Vector2)firePosition.position;

            if (Time.time > wait + ShootingCycle)
            {
                if (!first)
                {
                    if (managerGame2.instance.level >= 2 && bigOne >= 4)
                    {
                        CreateBullet(look, bigBullet);
                        bigOne = 0;
                    }
                    else
                    {
                        CreateBullet(look, oppBullet);
                        //Shoot 3 ball
                        if (managerGame2.instance.level >= 3)
                        {
                            CreateBullet(look, oppBullet,3);
                            CreateBullet(look, oppBullet,-3);
                        }
                    }
                }
                first = false;
                wait = Time.time;
                bigOne++;
            }
            else
            {
                Vector2 route = ranMove - (Vector2)trans.position;

                if (route == Vector2.zero)
                {
                    ranMove = RandomMove();
                }
                else
                {
                    transform.position = Vector2.MoveTowards(transform.position, ranMove, speed * Time.deltaTime);
                }
            }
        }
        if (managerGame2.instance.level > 4)
        {
            speed = 2.5f;
            bulletSpeed = 8f;
        }
        else if (managerGame2.instance.level > 2)
        {
            speed = 2f;
            bulletSpeed = 7f;
        }
        
    }

    Vector2 RandomMove()
    {
        Vector2 pos;
        
        float x = Random.Range(-4.0f, 4.0f);
        float y = Random.Range(2.0f, 4.0f);

        pos.x = x;
        pos.y = y;

        return pos;
    }

    void CreateBullet(Vector2 look, GameObject oppbullet, float angle = 0)
    {
        look.x += angle; 
        
        GameObject bullet = Instantiate(oppbullet);

        Rigidbody2D rigBullet = bullet.GetComponent<Rigidbody2D>();
        bullet.transform.position = firePosition.position;
        rigBullet.velocity = look * bulletSpeed / look.magnitude;
    }
}
