using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveGame2 : MonoBehaviour
{
    public float moveSpeed = 3f;
    public Rigidbody2D rb;
    public static Vector2 playerPosition;

    Transform trans;

    Vector2 movement;
    void Start()
    {
        trans = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (CountdownGame2.play == true)
        {
            playerPosition.x = trans.position.x;
            playerPosition.y = trans.position.y;

            movement.x = Input.GetAxisRaw("Horizontal");
            movement.y = Input.GetAxisRaw("Vertical");
        }
    }
    private void FixedUpdate()
    {
        if (!shootGame2.is_clicking)
        {
            rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime);
        }
    }
}
