using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class shootGame2 : MonoBehaviour
{
    public GameObject Ball;
    public Transform firePosition;

    public static bool is_clicking;
    private float delay;
    public Image circle_fill;
    public GameObject arrow;

    //public AudioSource sound;
    private float speed = 10;
    // Start is called before the first frame update

    // Update is called once per frame
    void Update()
    {
        if (CountdownGame2.play == true)
        {
            if (Input.GetMouseButtonDown(0))
            {
                is_clicking = true;
                delay = Time.time;
                arrow.SetActive(true);
            }

            if (Input.GetMouseButton(0))
            {
                if (circle_fill.fillAmount <= 1f)
                {
                    circle_fill.fillAmount = (Time.time - delay) * 2;
                }
            }

            if (Input.GetMouseButtonUp(0) && is_clicking)
            {
                if (Time.time > delay + 0.5f)
                {
                    GameObject bullet = Instantiate(Ball);
                    Rigidbody2D rig = bullet.GetComponent<Rigidbody2D>();
                    bullet.transform.position = firePosition.position + new Vector3(0,0.8f,0);
                    Vector2 look = directGame2.mousePosition - directGame2.playerPosition;
                    rig.rotation = Mathf.Atan2(look.y, look.x) * Mathf.Rad2Deg - 90f;
                    rig.velocity = look * speed / look.magnitude;
                    //sound.Play();
                }
                is_clicking = false;
                arrow.SetActive(false);
                circle_fill.fillAmount = 0f;
            }
        }
    }
}
