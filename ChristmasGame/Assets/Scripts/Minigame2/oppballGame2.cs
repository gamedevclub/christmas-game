using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class oppballGame2 : MonoBehaviour
{
    public ParticleSystem snow;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            managerGame2.instance.SubLives();
            Destroy(this.gameObject);
        }
    }

    void OnBecameInvisible()
    {
        Destroy(this.gameObject);
    }
    private void OnDestroy()
    {
        managerGame2.instance.HitSoundPlay();
        Instantiate(snow, transform.position, Quaternion.identity);
    }
}
