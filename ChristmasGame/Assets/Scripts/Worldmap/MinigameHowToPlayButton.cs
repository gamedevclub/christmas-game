using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinigameHowToPlayButton : MonoBehaviour
{
    public void OnExitButtonClick()
    {
        DialogueManager.instance.OffTutorial();
        gameObject.SetActive(false);
    }
}
