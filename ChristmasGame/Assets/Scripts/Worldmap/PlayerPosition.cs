using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlayerPosition : MonoBehaviour
{
    public Transform min;
    public Transform max;
    public Transform player;
    public RectTransform playerImage;
    public RectTransform map;

    float width;
    float height;

    private void Start()
    {
        height = max.position.y - min.position.y;
        width = max.position.x - min.position.x;
    }

    private void Update()
    {
        float x = (player.position.x - min.position.x) / width;
        float y = (player.position.y - min.position.y) / height;

        float xMap = x * map.rect.width;
        float yMap = y * map.rect.height;

        //Debug.Log(map.rect.width);
        //Debug.Log(map.rect.height);

        playerImage.anchoredPosition = new Vector2(xMap, yMap);
    }
}
