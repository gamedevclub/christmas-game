using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// * Class for a single item info panel
/// </summary>
public class ItemInfoPanel : MonoBehaviour
{
    public GameObject itemName;
    public GameObject itemIcon;
    public GameObject itemStartTemplate;
    public GameObject itemStarContent;
    public GameObject itemDescription;

    /// <summary>
    /// * Set up the element for each field in the info panel
    /// TODO: Assign all value to the UI as many as possible
    /// ! No bugs found currently
    /// </summary>
    /// <param name="_name"></param>
    /// <param name="_icon"></param>
    /// <param name="_stars"></param>
    /// <param name="_description"></param>
    public void Show(Decoration _decoration)
    {
        // print("ItemInfoPanel::Show() Called");
        itemName.GetComponent<TMPro.TextMeshProUGUI>().text = _decoration.name;
        itemIcon.GetComponent<Image>().sprite = DataLoader.instance.GetDecorationSprite(_decoration);
        foreach (Transform star in itemStarContent.transform)
        {
            Destroy(star.gameObject);
        }
        for (int i = 0; i < _decoration.level + 1; i++)
        {
            Instantiate(itemStartTemplate, itemStarContent.transform);
        }
        itemDescription.GetComponent<TMPro.TextMeshProUGUI>().text = _decoration.description;
    }
}
