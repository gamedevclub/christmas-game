using System.Linq;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DemoData : MonoBehaviour
{
    public int numberOfDecorationSlot = 19;

    // * Store the item's name of current items in the inventory
    // * Do not contain null variables
    // ! Size is equal to the number of current items player owned
    public List<string> itemName = new List<string>();
    
    // * Store the item's icon of current items in the inventory
    // * Do not contain null variables
    // ! Size is equal to the number of current items player owned
    public List<Sprite> itemIconInventory = new List<Sprite>();

    // * Store the item's price of current items in the inventory
    // * Do not contain null variables
    // ! Size is equal to the number of current items player owned
    public List<float> itemPrice = new List<float>();

    // * Store the item's description of current items in the inventory
    // * Do not contain null variables
    // ! Size is equal to the number of current items player owned
    public List<string> itemDescription = new List<string>();

    // * Store the number of star for each current item in the inventory
    // * Do not contain null variables
    // ! Size is equal to the number of current items player owned
    public List<int> itemStars = new List<int>();

    // * Store the item's icon when decorating in CURRENT tree (used for save-load data and position of item on tree)
    // * Can contain null sprite
    // ! Size is equal to the total number of slots on the tree
    public List<Sprite> itemIconDecoration = new List<Sprite>();

    // * Store the item's icon when decorating in CURRENT tree (used for keep track of the decorated item)
    // * Do not contain null variables
    // ! Size is equal to the number of current items player owned
    public List<Sprite> itemIconDecorationCorrespondToItemInventory = new List<Sprite>();

    public List<int> itemPoint;

    public List<Decoration> items = new List<Decoration>();

    public void PourDataIntoValues()
    {
        // TODO: Initialize empty slots for tree
        for (int i = 0; i < numberOfDecorationSlot; i ++)
        {
            itemIconDecoration.Add(null);
        }
        for (int i = 0; i < DataManager.instance.IngameData.decorationList.Count; i++)
        {
            if (DataManager.instance.IngameData.decorationList[i].slot == 0)
            {
                // TODO: Put all inventory item
                itemName.Add(DataManager.instance.IngameData.decorationList[i].name);
                itemDescription.Add(DataManager.instance.IngameData.decorationList[i].description);
                itemIconInventory.Add(DataLoader.instance.GetDecorationSprite(DataManager.instance.IngameData.decorationList[i]));
                itemIconDecorationCorrespondToItemInventory.Add(DataLoader.instance.GetDecorationSpriteOnTree(DataManager.instance.IngameData.decorationList[i]));
                itemStars.Add(DataManager.instance.IngameData.decorationList[i].level);
                itemPrice.Add(DataManager.instance.IngameData.decorationList[i].price);
                itemPoint.Add(DataManager.instance.IngameData.decorationList[i].point);
            }
            else
            {
                itemIconDecoration[DataManager.instance.IngameData.decorationList[i].slot - 1] = (DataLoader.instance.GetDecorationSpriteOnTree(DataManager.instance.IngameData.decorationList[i]));
            }
        }
    }

    // public void LoadItemsFromResources()
    // {
    //     Object[] dataItemObject = Resources.LoadAll("Sprites\\Decorations", typeof(Decoration));
    //     for (int i = 0; i < dataItemObject.Length; i++)
    //     {
    //         items.Add(dataItemObject[i] as Decoration);
    //     }
    // }
}
