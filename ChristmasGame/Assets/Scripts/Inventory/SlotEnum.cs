// * Determine whether a slot is empty or begin taken
public enum StateSlot
{
    Empty,
    Taken
}

// * Determine whether a slot is an inventory slot or a tree slot
public enum TypeSlot
{
    Inventory,
    Tree
}

// * Determine whethter an item is on inventory or on tree
public enum ItemIdentification
{
    Inventory,
    Tree
}

// * Determine whether the player is in edit mode or not
public enum DecorationMode
{
    Edit,
    Watch
}
