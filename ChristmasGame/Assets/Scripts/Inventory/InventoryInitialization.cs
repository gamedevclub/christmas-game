using System.Net.WebSockets;
using System.Linq;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

/// <summary>
/// * Implement the Inventory initialization of the inventory such as generating slots, items,...
/// * Run once when the scene is activated
/// </summary>
public class InventoryInitialization : MonoBehaviour
{
    private static InventoryInitialization instance;
    public Transform contentSlot;

    // ! Make sure that capacity is always greater than or equal to the atual number of decoration items.
    // ! capacity >= DataManager.instance.IngameData.decorationList.Count
    private int capacity;
    private GameObject slotTemplate;
    private GameObject itemTemplate;


    public static InventoryInitialization Instance { get => instance; set => instance = value; }

    private void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    public void Load()
    {
        slotTemplate = InventoryManager.Instance.slotTemplate;
        itemTemplate = InventoryManager.Instance.itemTemplate;
        capacity = InventoryManager.Instance.capacity;
        InventoryManager.Instance.coinText.GetComponent<TextMeshProUGUI>().text = DataManager.instance.IngameData.money.ToString();

        InitializeSlots();
        QueueInventory();
        ResizeMainContent();
    }

    /// <summary>
    /// * Slot initialization Method
    /// TODO: Implement the initialization of all slots and items
    /// ! No bugs found currently
    /// </summary>
    void InitializeSlots()
    {
        // print("InventoryInitialization::InitializeSlots() Called");
        InventoryManager.Instance.slotObjects = new List<GameObject>();
        InventoryManager.Instance.slots = new List<Slot>();

        capacity = ((int)(DataManager.instance.IngameData.decorationList.Count / 15) + 1) * 15;
        for (int i = contentSlot.childCount - 1; i >= 0; i--)
        {
            Destroy(contentSlot.GetChild(i).gameObject);
        }
        // TODO: Loop through all slots in inventory
        for (int i = 0; i < capacity; i++)
        {
            // TODO: Spawn the slot objects in the hierachy 
            GameObject slotObject = Instantiate(slotTemplate);
            slotObject.transform.SetParent(contentSlot, false);
            slotObject.tag = "Inventory field";

            // TODO: Format constraints of the gridlayoutgroup containing slot objects
            contentSlot.GetComponent<GridLayoutGroup>().CalculateLayoutInputHorizontal();
            contentSlot.GetComponent<GridLayoutGroup>().CalculateLayoutInputVertical();
            contentSlot.GetComponent<GridLayoutGroup>().SetLayoutHorizontal();
            contentSlot.GetComponent<GridLayoutGroup>().SetLayoutVertical();

            // TODO: Get the slot from the slotObject above
            Slot slot = slotObject.GetComponent<Slot>();
            slot.slotId = i;

            //  TODO: Put the item into slot correspondingly
            if (slot.slotId < DataManager.instance.IngameData.decorationList.Count)
            {
                // TODO: Create a new item
                Decoration decoration = new Decoration(DataManager.instance.IngameData.decorationList[i]);
                decoration.slot = slot.slotId;
                if (decoration.type == 0)
                {
                    slot.SetItem(decoration);
                    slot.ShowInInventory();
                }
            }
            else
            {
                slot.state = StateSlot.Empty;
                slot.ShowInInventory();
            }
            slot.type = TypeSlot.Inventory;

            if (slot.state == StateSlot.Empty)
            {
                slotObject.GetComponent<CanvasGroup>().interactable = false;
                slotObject.GetComponent<CanvasGroup>().blocksRaycasts = false;
            }
            else
            {
                slotObject.GetComponent<CanvasGroup>().interactable = true;
                slotObject.GetComponent<CanvasGroup>().blocksRaycasts = true;
            }

            // TODO: Add the slot into list of slots
            InventoryManager.Instance.slots.Add(slot);
            InventoryManager.Instance.slotObjects.Add(slotObject);
        }
    }

    /// <summary>
    /// * Resize the content of scroll view based on the items it owned
    /// TODO: Implement the method to resize the content of scroll view based on the number of items it has
    /// ! No bugs found currently
    /// </summary>
    public void ResizeMainContent()
    {
        float verticalSpacing = contentSlot.GetComponent<GridLayoutGroup>().spacing.y;
        float sizeASlot = slotTemplate.GetComponent<RectTransform>().rect.height;
        float paddingTop = contentSlot.GetComponent<GridLayoutGroup>().padding.top;
        int numberOfCollumns = contentSlot.GetComponent<GridLayoutGroup>().constraintCount;
        float numberOfRows = InventoryManager.Instance.slotObjects.Count / numberOfCollumns;
        numberOfRows = (float)numberOfRows == (int)numberOfRows ? numberOfRows : (int)numberOfRows + 1;
        float finalVerticalSize = paddingTop + (numberOfRows + 1) * verticalSpacing + numberOfRows * sizeASlot;

        gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(gameObject.GetComponent<RectTransform>().sizeDelta.x, finalVerticalSize);
    }

    public void QueueInventory()
    {
        // TODO: Queue the InventoryManager.Instance.slotObjects
        for (int i = InventoryManager.Instance.slotObjects.Count - 1; i >= 0; i--)
        {
            if (InventoryManager.Instance.slotObjects[i].GetComponent<Slot>().state == StateSlot.Taken)
            {
                InventoryManager.Instance.slotObjects[i].transform.SetAsFirstSibling();
            }
        }
    }

    public static void Swap<T>(IList<T> list, int indexA, int indexB)
    {
        T tmp = list[indexA];
        list[indexA] = list[indexB];
        list[indexB] = tmp;
    }

    public void Sell(Slot slot)
    {
        int index = DataManager.instance.IngameData.decorationList.IndexOf(DataManager.instance.IngameData.decorationList.Find(d => d.Equals(slot.decoration)));

        // print("Sell at : " + index.ToString() + " OR : " + slot.decoration.slot.ToString());
        DataManager.instance.IngameData.decorationList.RemoveAt(slot.decoration.slot);
        AwardManager.instance.GetQuestAward(slot.decoration.price);
        //DataManager.instance.IngameData.money += slot.decoration.price;
        //DataManager.instance.SaveUnit();
        slot = null;
        DataManager.instance.SaveDeco();
        Load();
    }
}
