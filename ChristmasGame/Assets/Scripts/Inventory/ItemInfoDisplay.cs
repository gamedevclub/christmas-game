using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/// <summary>
/// * Used to display the item info when player hovers an item in a specific time
/// </summary>
public class ItemInfoDisplay : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public GameObject glowEffect;

    /// <summary>
    /// * On pointer enter Method
    /// TODO: Implement the show-item-info when player hovers the item in certain amount of time
    /// ! No bugs found currently
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerEnter(PointerEventData eventData)
    {
        // print("ItemInfoDisplay::OnPointerEnter() Called");
        if (gameObject.GetComponent<Slot>().state == StateSlot.Taken && InventoryManager.Instance.state != InventoryState.Selling)
        {
            glowEffect.SetActive(true);
            Invoke("ShowItemInfo", InventoryManager.Instance.timeShowItemInformation);
        }
    }

    /// <summary>
    /// * On pointer exit Method
    /// TODO: Implement the hide-item-info when player no longer hovers the item
    /// ! No bugs found currently
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerExit(PointerEventData eventData)
    {
        // print("ItemInfoDisplay::OnPointerExit() Called");
        glowEffect.SetActive(false);
        CancelInvoke();
        HideItemInfo();
    }

    /// <summary>
    /// * Show item info Method
    /// TODO: Implement the show item info method
    /// ! No bugs found currently
    /// </summary>
    public void ShowItemInfo()
    {
        // print("ItemInfoDisplay::ShowItemInfo() Called");
        float itemCurrentPosition = GetComponent<RectTransform>().position.y;
        // print(itemCurrentPosition);
        float posY;
        if (itemCurrentPosition >= 2f)
        {
            // print("TOP");
            posY = 277;
        }
        else if (itemCurrentPosition < 2f && itemCurrentPosition >= 0)
        {
            // print("MID");
            posY = 77;
        }
        else
        {
            // print("BOTTOM");
            posY = -123;
        }
        GameObject itemInfoPanel = transform.root.GetChild(7).gameObject;
        itemInfoPanel.GetComponent<RectTransform>().anchoredPosition = new Vector2(GetComponent<RectTransform>().anchoredPosition.x, posY);
        // print(GetComponent<RectTransform>().position.y);
        // itemInfoPanel.GetComponent<RectTransform>().pivot = new Vector2(itemInfoPanel.GetComponent<RectTransform>().pivot.x, pivotYAxis);
        itemInfoPanel.SetActive(true);

        // TODO: Call the method show for each info panel
        itemInfoPanel.GetComponent<ItemInfoPanel>().Show(GetComponent<Slot>().decoration);
    }

    /// <summary>
    /// * Hide item info Method
    /// TODO: Implement the hide item info method
    /// ! No bugs found currently
    /// </summary>
    public void HideItemInfo()
    {
        // print("ItemInfoDisplay::HideItemInfo() Called");
        GameObject itemInfoPanel = transform.root.GetChild(7).gameObject;
        itemInfoPanel.SetActive(false);
    }
}
