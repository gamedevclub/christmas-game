using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// * Implement the Manager of the Inventory system
/// * Containing main variables such as : slotTemplate, itemTemplate,...
/// * Run all the time since the scene was activated
/// </summary>
public class InventoryManager : MonoBehaviour
{
    private static InventoryManager instance;
    public static InventoryManager Instance { get => instance; set => instance = value; }
    [HideInInspector]
    public List<Slot> slots = new List<Slot>();

    public GameObject slotTemplate;
    public GameObject itemTemplate;
    public List<GameObject> slotObjects;

    public int capacity;
    public float timeShowItemInformation;
    public GameObject coinText;
    public InventoryState state = InventoryState.Normal;
    private void Awake()
    {
        Instance = this;
    }

}

public enum InventoryState
{
    Normal,
    Selling
}
