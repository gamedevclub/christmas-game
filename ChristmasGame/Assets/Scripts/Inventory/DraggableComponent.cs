using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/// <summary>
/// * Implement the draggable component for items (in both inventory and decoration tree)
/// * It is required for the item prefab
/// * Run when item is dragged
/// </summary>
[RequireComponent(typeof(CanvasGroup))]
public class DraggableComponent : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler, IDropHandler, IInitializePotentialDragHandler, IPointerClickHandler
{
    public GameObject item;
    private RectTransform rectTransform;
    private Canvas canvas;
    private CanvasGroup canvasGroup;
    private GameObject slotDecorationDetected = null;
    [HideInInspector]
    public Vector3 startPosition;
    private static DraggableComponent instance;

    public static DraggableComponent Instance { get => instance; set => instance = value; }

    private void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        item = gameObject.GetComponent<Slot>().itemIconObject;
        canvas = transform.root.gameObject.GetComponent<Canvas>();
        rectTransform = item.GetComponent<RectTransform>();
        canvasGroup = transform.GetComponent<CanvasGroup>();
    }

    /// <summary>
    /// * On Begin Drag item Method
    /// * Automatically called when item is started to drag 
    /// TODO: Implement the method when player started dragging
    /// ! No bugs found currently
    /// </summary>
    /// <param name="eventData"></param>
    public void OnBeginDrag(PointerEventData eventData)
    {
        // print("DraggableComponent::OnBeginDrag(PointerEventData) Called");
        // TODO: Blur the item icon when being dragged
        canvasGroup.alpha = 0.6f;
        canvasGroup.blocksRaycasts = false;
    }

    /// <summary>
    /// * On Dragging item Method
    /// * Automatically called when item is being dragged
    /// TODO: Implement the method when player is dragging item
    /// ! No bugs found currently 
    /// </summary>
    /// <param name="eventData"></param>
    public void OnDrag(PointerEventData eventData)
    {
        // print("DraggableComponent::OnDrag(PointerEventData) Called");
        // if (eventData.pointerClick.GetComponent<Slot>().state == StateSlot.Empty)
        // {
        //     return;
        // }
        // print(eventData.pointerDrag.name);
        RaycastHit2D[] slotDecorations;
        // Debug.Log(eventData.pointerDrag.gameObject.GetComponent<Slot>().itemIconObject.name);
        // TODO: Activate the decoration slots when dragging
        DecorationManager.Instance.OnActivateDecorationSlots();

        // TODO: Move the item following the mouse
        rectTransform.anchoredPosition += eventData.delta / canvas.scaleFactor;
        item.transform.SetParent(canvas.transform);

        // TODO: Set the state of current slot
        gameObject.GetComponent<Slot>().state = StateSlot.Empty;

        // TODO: Detect the decoration slot if item is dropped on it
        slotDecorations = Physics2D.RaycastAll(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.up, 0.001f);
        foreach (RaycastHit2D slotDecoration in slotDecorations)
        {
            if (slotDecoration.collider.gameObject.GetComponent<DroppableComponentDecoration>())
            {
                // print("GET HERE !!!!");
                slotDecorationDetected = slotDecoration.collider.gameObject;
            }
            // else
            // {
            //     slotDecorationDetected = null;
            // }
        }
        // print(string.Format("Size casted : {0}",slotDecorations.Length));
        if (slotDecorations.Length == 0)
        {
            slotDecorationDetected = null;
        }

        // TODO: Close inventory when dragging
        InventoryButtons.Instance.CloseInventory();

    }

    /// <summary>
    /// * On Drop Method
    /// * Automatically called when the current-dragged item is dropped
    /// TODO: Implement the method when player drop item on somewhere
    /// ! No bugs found currently
    /// </summary>
    /// <param name="eventData"></param>
    public void OnDrop(PointerEventData eventData)
    {
        // print("DraggableComponent::OnDrop(PointerEventData) Called");
        // TODO: Detect the item on Decoration tree to swap them
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventData, results);
        DraggableComponent item = null;
        foreach (RaycastResult result in results)
        {
            item = result.gameObject.transform.parent.gameObject.GetComponent<DraggableComponent>();
            if (item != null)
            {
                //Debug.Log("Found Item");
                break;
            }
        }
    }

    /// <summary>
    /// * On end drag Method
    /// * Automatically called when player releases dragging item
    /// TODO: Implement the method when player releases mouse button when dragging item
    /// ! No bugs found currently
    /// </summary>
    /// <param name="eventData"></param>
    public void OnEndDrag(PointerEventData eventData)
    {
        // print("DraggableComponent::OnEndDrag(PointerEventData) Called");
        // TODO: De-activate the decoration slot
        DecorationManager.Instance.OnDeactiveDecorationSlots();

        // TODO: Clarify the item icon
        canvasGroup.alpha = 1;
        canvasGroup.blocksRaycasts = true;

        // TODO: Detect the decoration slot
        if (slotDecorationDetected != null)
        {
            // print("slotDecorationDetected != null");
            if (slotDecorationDetected.GetComponent<Slot>().state == StateSlot.Empty)
            {
                // print("slotDecorationDetected.GetComponent<Slot>().state == StateSlot.Empty");
                slotDecorationDetected.GetComponent<DroppableComponentDecoration>().OnDropOnDecorationSlot(gameObject);
                item.transform.SetParent(transform);
                rectTransform.anchoredPosition = startPosition;
                gameObject.GetComponent<Slot>().ClearItem();
                // gameObject.GetComponent<Slot>().ShowInInventory();
                slotDecorationDetected = null;
                

                DataManager.instance.SaveDeco();

                return;
            }
            else
            {
                slotDecorationDetected.GetComponent<DroppableComponentDecoration>().OnSwapBetweenDecorationAndInventory(gameObject);
                gameObject.GetComponent<Slot>().state = StateSlot.Taken;
                // InventoryButtons.Instance.OpenInventory();
                item.transform.SetParent(transform);
                rectTransform.anchoredPosition = startPosition;

                DataManager.instance.SaveDeco();
                return;
            }
        }
        else
        {
            gameObject.GetComponent<Slot>().state = StateSlot.Taken;
            InventoryButtons.Instance.OpenInventory();
            item.transform.SetParent(transform);
            rectTransform.anchoredPosition = startPosition;

            DataManager.instance.SaveDeco();

            return;
        }

        // TODO: Detect the dropped position of the item, return 
        // List<RaycastResult> results = new List<RaycastResult>();
        // EventSystem.current.RaycastAll(eventData, results);
        // Slot slot = null;
        // foreach (RaycastResult result in results)
        // {
        //     // TODO: return the item back to the position-when-begin-dragging
        //     if (slot == null)
        //     {
        //         // TODO: Set the state of current slot
        //         gameObject.GetComponent<Slot>().state = StateSlot.Taken;
        //         InventoryButtons.Instance.OpenInventory();
        //         item.transform.SetParent(transform);
        //         rectTransform.anchoredPosition = startPosition;

        //         DataManager.instance.SaveDeco();

        //         return;
        //     }
        // }


    }

    /// <summary>
    /// * On initialize potential drag Method
    /// * Automatically called when player press mouse button on screen
    /// TODO: Implement the method when player started touch the screen
    /// ! No bugs found currently
    /// </summary>
    /// <param name="eventData"></param>
    public void OnInitializePotentialDrag(PointerEventData eventData)
    {
        // TODO: Set the start position of each item when clicked
        startPosition = rectTransform.anchoredPosition;
    }

    /// <summary>
    /// * Format Position of the item
    /// * Called when the item is swap
    /// TODO: Implement the method when swapping items
    /// ! No bugs found currently
    /// </summary>
    public void FormatPosition()
    {
        // TODO: Set the start position of each item when being swapped
        startPosition = rectTransform.anchoredPosition;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        // print("Click");
        if (InventoryManager.Instance.state == InventoryState.Selling)
        {
            InventoryInitialization.Instance.Sell(GetComponent<Slot>());
        }
    }
}
