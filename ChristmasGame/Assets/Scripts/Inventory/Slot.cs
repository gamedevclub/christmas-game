using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// * Slot class
/// * Containing all the information of a single slot
/// </summary>
public class Slot : MonoBehaviour
{
    public int slotId;
    public Decoration decoration;
    public StateSlot state;
    public TypeSlot type;
    public GameObject itemIconObject;
    public Color takenColor;
    public Color emptyColor;

    /// <summary>
    /// * Show Item in inventory method
    /// TODO: Used to show the item in inventory when there is something changed
    /// ! No bug found currently
    /// </summary>
    public void ShowInInventory()
    {
        // print("Slot::ShowInInventory() Called");
        if (state == StateSlot.Empty)
        {
            // itemIconObject.GetComponent<Image>().color = new Color(0,0,0,0);
            itemIconObject.GetComponent<Image>().sprite = null;
            itemIconObject.GetComponent<Image>().color = emptyColor;
        }
        else
        {
            itemIconObject.GetComponent<Image>().sprite = DataLoader.instance.GetDecorationSprite(this.decoration);
            itemIconObject.GetComponent<Image>().color = takenColor;
            itemIconObject.transform.SetAsFirstSibling();
        }
    }

    /// <summary>
    /// * Set item method
    /// * Set the Item for this gameobject, switch the state of this slot to Taken
    /// * Used for both inventory and tree slot
    /// ! No bug found currently
    /// </summary>
    /// <param name="_decoration"></param>
    public void SetItem(Decoration _decoration)
    {
        // print("Slot::SetItem(Decoration) Called");
        this.decoration = _decoration;
        state = StateSlot.Taken;
    }

    /// <summary>
    /// * Clear item method
    /// * Clear the item on slot  
    /// * Used for both inventory and tree slot
    /// ! No bug found currently
    /// </summary>
    public void ClearItem()
    {
        // print("Slot::ClearItem() Called");
        itemIconObject.transform.localPosition = Vector2.zero;
        if (itemIconObject.GetComponent<Image>())
        {
            itemIconObject.GetComponent<Image>().sprite = null;
            // itemIconObject.GetComponent<Image>().color = new Color(0,0,0,0);
            // print("Clear UI item");
        }
        if (itemIconObject.GetComponent<SpriteRenderer>())
        {
            itemIconObject.GetComponent<SpriteRenderer>().sprite = null;
            // print("Clear game item");
        }
        decoration = null;
        state = StateSlot.Empty;
    }

    /// <summary>
    /// * Show item in decoration method
    /// * Used to show the item on tree using the art on tree
    /// ! No bug found currently
    /// </summary>
    public void ShowInDecoration()
    {
        // print("Slot::ShowInDecoration() Called");
        // print("ID ShowInDecoration : " + slotId);
        // if (DataManager.instance.IngameData.decorationList[slotId].slot == 0)
        // {
        //     print("Get into check if");
        //     itemIconObject.GetComponent<SpriteRenderer>().sprite = null;
        //     itemIconObject.transform.localPosition = Vector2.zero;
        //     return;
        // }
        if (decoration == null)
        {
            DeleteAllEffectInSlot();
        }
        else
        {
            itemIconObject.GetComponent<SpriteRenderer>().sprite = DataLoader.instance.GetDecorationSpriteOnTree(this.decoration);
            switch (decoration.level)
            {
                case 0:
                    DeleteAllEffectInSlot();
                    itemIconObject.GetComponent<Renderer>().material = DecorationManager.Instance.MaterialDecorationItems[0];
                    break;
                case 1:
                    DeleteAllEffectInSlot();
                    itemIconObject.GetComponent<Renderer>().material = DecorationManager.Instance.MaterialDecorationItems[1];
                    Instantiate(DecorationManager.Instance.EffectAItem, transform);
                    break;
                case 2:
                    DeleteAllEffectInSlot();
                    Instantiate(DecorationManager.Instance.EffectSItem, transform);
                    itemIconObject.GetComponent<Renderer>().material = DecorationManager.Instance.MaterialDecorationItems[2];
                    break;
                default:
                    break;
            }
        }

        itemIconObject.transform.localPosition = Vector2.zero;
    }

    public void DeleteAllEffectInSlot()
    {
        foreach (Transform child in transform)
        {
            if (child.gameObject.GetComponent<ParticleSystem>())
            {
                Destroy(child.gameObject);
            }
        }
    }

    /// <summary>
    /// * Hide the slot decoration
    /// </summary>
    public void HideSlotDecoration()
    {
        gameObject.GetComponent<SpriteRenderer>().color = new Color(gameObject.GetComponent<SpriteRenderer>().color.r, gameObject.GetComponent<SpriteRenderer>().color.g, gameObject.GetComponent<SpriteRenderer>().color.b, 0);
    }

    /// <summary>
    /// * Show the slot decoration
    /// </summary>
    public void ShowSlotDecoration()
    {
        gameObject.GetComponent<SpriteRenderer>().color = new Color(gameObject.GetComponent<SpriteRenderer>().color.r, gameObject.GetComponent<SpriteRenderer>().color.g, gameObject.GetComponent<SpriteRenderer>().color.b, 0.5f);
    }
}
