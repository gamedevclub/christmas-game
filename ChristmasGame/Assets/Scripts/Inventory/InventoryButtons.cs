using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// * Implement all the buttons in the inventory
/// * Run all the time since the scene was activated
/// </summary>
public class InventoryButtons : MonoBehaviour
{
    private static InventoryButtons instance;
    private Vector3 closePosition = new Vector3(280, 85, 0);
    private Vector3 openPosition = new Vector3(280 - 550, 85, 0);
    enum STATE
    {
        OPEN,
        CLOSE
    }
    private string inventoryCurrentState = "";
    private STATE state = STATE.CLOSE;
    public Animator inventoryAnimator;
    public Color sellColor;
    public Color normalColor;
    public Image sellButton;

    public static InventoryButtons Instance { get => instance; set => instance = value; }
    private void Awake()
    {
        Instance = this;
    }

    /// <summary>
    /// * On click inventory button Method
    /// * Called when player click the open-close button
    /// TODO: Implement the open-close button for the inventory
    /// ! No bugs found currently
    /// </summary>
    public void OnClickInventoryButton()
    {
        if (state == STATE.CLOSE)
        {
            OpenInventory();
        }
        else
        {
            CloseInventory();
        }
    }

    /// <summary>
    /// * Close inventory Method
    /// TODO: Implement the close action for inventory
    /// ! No bugs found currently
    /// </summary>
    public void CloseInventory()
    {
        print("InventoryButton::CloseInventory() Called");
        // TODO: Set the suitable state and run the close animation
        if (state == STATE.CLOSE)
            return;
        InventoryManager.Instance.state = InventoryState.Normal;
        print("Close " + transform.localPosition);
        // ChangeStateAnimation("CloseInventory");
        // GetComponent<RectTransform>().localPosition = closePosition;
        Vector3 result = transform.localPosition + new Vector3(GetComponent<RectTransform>().rect.width, 0, 0);
        while (Vector3.Distance(result, transform.localPosition) > 0.05f)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, result, 2f);
        }
        state = STATE.CLOSE;

        sellButton.color = normalColor;
        InventoryManager.Instance.state = InventoryState.Normal;
        // print("Close");
    }

    /// <summary>
    /// * Open inventory Method
    /// TODO: Implement the open action for inventory
    /// ! No bugs found currently
    /// </summary>
    public void OpenInventory()
    {
        print("InventoryButton::OpenInventory() Called");
        // TODO: Set the suitable state and run the open animation
        if (state == STATE.OPEN)
            return;
        // Vector3 result = transform.localPosition - new Vector3(GetComponent<RectTransform>().rect.width, 0, 0);
        Vector3 result = transform.localPosition + new Vector3( - GetComponent<RectTransform>().rect.width, 0, 0);
        while (Vector3.Distance(result, transform.localPosition) > 0.05f)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, result, 2f);
        }
        state = STATE.OPEN;
        print("Open " + transform.localPosition);
        InventoryInitialization.Instance.Load();

        sellButton.color = normalColor;
        InventoryManager.Instance.state = InventoryState.Normal;
    }

    /// <summary>
    /// * Change state animation Method
    /// * Use the change the animation clip properly without parameter
    /// </summary>
    /// <param name="nextState"></param>
    void ChangeStateAnimation(string nextState)
    {
        if (inventoryCurrentState == nextState)
            return;
        inventoryCurrentState = nextState;
        inventoryAnimator.Play(inventoryCurrentState);
    }

    public void OnSellingButtonClick(Image icon)
    {
        if (InventoryManager.Instance.state == InventoryState.Normal)
        {
            icon.color = sellColor;
            InventoryManager.Instance.state = InventoryState.Selling;
        }
        else if (InventoryManager.Instance.state == InventoryState.Selling)
        {
            icon.color = normalColor;
            InventoryManager.Instance.state = InventoryState.Normal;
        }
    }
}
