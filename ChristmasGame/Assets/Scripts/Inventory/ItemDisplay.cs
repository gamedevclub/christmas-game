using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// * Implement the display method to show the item information to the UI
/// * Called when inventory is called
/// </summary>
public class ItemDisplay : MonoBehaviour
{
    public int id;
    public string itemName;
    public float price;
    public int stars;
    public string description;
    public int point;
    public Slot slot;
    public GameObject icon;

    /// <summary>
    /// * Like a contructor but Monobehaviour is not allowed to be create by "new" syntax
    /// ! No bugs found currently
    /// </summary>
    /// <param name="item"></param>
    public void SetupValue(Decoration decoration)
    {
        id = decoration.ID;
        itemName = decoration.name;
        price = decoration.price;
        stars = decoration.level + 1;
        description = decoration.description;
        point = decoration.point;
    }

    /// <summary>
    /// * Set the display information of the item
    /// TODO: Implement the show method of the item in inventory
    /// ! No bugs found currently
    /// </summary>
    /// <param name="item"></param>
    public void SetItemDisplay(Decoration decoration)
    {
        icon.GetComponent<Image>().sprite = DataLoader.instance.GetDecorationSprite(decoration);
    }
}
