using System.Net.Mime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Timer4 : MonoBehaviour
{
    public static Timer4 instance;
    public Text timeText;
    int maxTime = 261;
    public int timeCount = 0;

    private void Start()
    {
        if (!instance)
            instance = this;
        StartCoroutine(time());
    }
    IEnumerator time()
    {
        while (timeCount != maxTime)
        {
            if (GameManager4.instance.end)
                break;
            timeCount++;
            ShowTime();
            yield return new WaitForSeconds(1);
        }
        if (!GameManager4.instance.end)
            GameManager4.instance.GameOver(false);
    }
    void ShowTime()
    {
        int cur = maxTime - timeCount;
        int min = cur / 60;
        int sec = cur % 60;
        if (sec < 10)
            timeText.text = "0" + min + ":" + "0" + sec;
        else
            timeText.text = "0" + min + ":" + sec;
    }

}
