using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class GameManager4 : MonoBehaviour
{
    public static GameManager4 instance;

    // public float soundVolume = 1;
    public AudioMixerGroup volume;
    public Text coinText;
    public Text expText;
    public bool end;
    int curCoin;
    int curExp;

    int maxCoin;
    int maxExp;
    bool firstPlayer = true;
    AudioSource coinSound;
    AudioSource expSound;


    private void Awake()
    {
        if (!instance)
            instance = this;
    }
    private void Start()
    {
        maxCoin = GameObject.FindGameObjectsWithTag("coin4").Length;
        maxExp = GameObject.FindGameObjectsWithTag("exp4").Length;
        firstPlayer = true;
        curCoin = curExp = 0;
        ShowItem();
        end = false;
        AudioSource gameSound = GetComponents<AudioSource>()[0];
        gameSound.outputAudioMixerGroup = volume;
        coinSound = GetComponents<AudioSource>()[1];
        coinSound.outputAudioMixerGroup = volume;
        expSound = GetComponents<AudioSource>()[2];
        expSound.outputAudioMixerGroup = volume;
    }

    private void Update()
    {
        if (PlayerPosition4.instance != null && firstPlayer)
        {
            PlayerPosition4.instance.RandomPosition();
            firstPlayer = false;
        }

    }

    public void GameOver(bool win = false)
    {
        GetComponents<AudioSource>()[0].Stop();
        end = true;
        if (Minigame4.time == 0)
            Minigame4.time = 300;
        if (!win)
        {
            AwardManager.instance.GetGameAward(curCoin, curExp, false, Result.GameOver);
        }
        else
        {
            Debug.Log("win game: " + Timer4.instance.timeCount);
            Debug.Log("best game: " + Minigame4.time);
            if (Timer4.instance.timeCount < Minigame4.time)
            {
                Minigame4.time = Timer4.instance.timeCount;
            }
            AwardManager.instance.GetGameAward(curCoin, curExp, true, Result.Victory);
        }
        // Destroy(Play)
        // QUEST
        if (curCoin > Minigame4.gold)
            Minigame4.gold = curCoin;
        if (curExp > Minigame4.exp)
            Minigame4.exp = curExp;
        DataManager.instance.SaveMinigameData(4);
    }

    public void IncreaseCoin()
    {
        coinSound.Play();
        curCoin++;
        ShowItem();
    }

    public void IncreaseExp()
    {
        expSound.Play();
        curExp++;
        ShowItem();
    }
    void ShowItem()
    {
        coinText.text = "x" + curCoin;
        expText.text = "x" + curExp;
    }
}
