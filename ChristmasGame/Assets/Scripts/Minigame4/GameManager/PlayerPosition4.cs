using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPosition4 : MonoBehaviour
{
    public static PlayerPosition4 instance;
    public Transform[] playerPosition;
    public GameObject player;
    public Transform[] botPosition;
    public GameObject bot;
    GameObject curPlayer = null;
    GameObject curBot = null;
    private void Start()
    {
        if (!instance)
            instance = this;
    }

    public void RandomPosition()
    {
        int playerPos = Random.Range(0, playerPosition.Length);
        int botPos;
        if (!curPlayer)
        {
            curPlayer = Instantiate(player);
        }
        curPlayer.transform.position = playerPosition[playerPos].position;

        while (true)
        {
            botPos = Random.Range(0, botPosition.Length);
            if (botPos != playerPos)
                break;
        }
        if (!curBot)
        {
            curBot = Instantiate(bot);
        }

        curBot.transform.position = botPosition[botPos].position;
    }
}
