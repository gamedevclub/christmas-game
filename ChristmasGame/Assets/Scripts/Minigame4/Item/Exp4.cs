using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Exp4 : MonoBehaviour
{
    public GameObject expPickup;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!GameManager4.instance.end)
            if (other.gameObject.tag == "player4")
            {
                // GameManager4.instance.expList.Add(transform.position);
                GameObject temp = Instantiate(expPickup);
                temp.transform.position = transform.position;
                GameManager4.instance.IncreaseExp();
                Destroy(this.gameObject);
            }
    }
}
