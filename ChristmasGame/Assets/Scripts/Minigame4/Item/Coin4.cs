using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Coin4 : MonoBehaviour
{

    public GameObject coinPickup;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!GameManager4.instance.end)
            if (other.gameObject.tag == "player4")
            {
                GameObject temp = Instantiate(coinPickup);
                temp.transform.position = transform.position;
                GameManager4.instance.IncreaseCoin();
                Destroy(this.gameObject);
            }
    }
}
