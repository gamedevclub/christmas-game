using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow4 : MonoBehaviour
{
    private Transform player;
    public Vector2 maxPosition;
    public Vector2 minPosition;
    Vector3 offset = new Vector3(0, 0, -50);
    float smoothing;

    private void FixedUpdate()
    {
        if (!player && GameObject.FindGameObjectWithTag("player4"))
        {
            player = GameObject.FindGameObjectWithTag("player4").transform;
        }
        if (player)
            if (transform.position != player.position)
            {
                Vector3 newPosition = new Vector3(player.position.x, player.position.y, transform.position.z);
                newPosition.x = Mathf.Clamp(newPosition.x, minPosition.x, maxPosition.x);
                newPosition.y = Mathf.Clamp(newPosition.y, minPosition.y, maxPosition.y);
                transform.position = newPosition;
            }
    }

}
