using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;


public class PlayerLife4 : MonoBehaviour
{
    public static PlayerLife4 instance;
    public Text lifeText;
    int maxLife = 3;
    int life;
    float time;
    float invulnerableTime = 2;
    GameObject player;
    Vector2 position;
    AudioSource dieSound;
    private void Start()
    {
        if (!instance)
            instance = this;
        // StartGame();
        life = maxLife;
        time = 0;
        ShowLife();
        dieSound = GetComponent<AudioSource>();
        dieSound.outputAudioMixerGroup = GameManager4.instance.volume;
        dieSound.volume = dieSound.volume * 0.9f;
    }
    private void Update()
    {
        if (!player && GameObject.FindGameObjectWithTag("player4"))
        {
            player = GameObject.FindGameObjectWithTag("player4");
            position = player.transform.position;
        }
        if (time + invulnerableTime < Time.time)
        {
            player.GetComponent<Animator>().SetBool("flicker", false);
            player.GetComponent<Player4>().invulnerable = false;
        }
    }

    public void Die()
    {
        dieSound.Play();
        life--;
        if (life < 0)
            GameManager4.instance.GameOver();
        else
        {
            player.GetComponent<Animator>().SetBool("flicker", true);
            player.GetComponent<Player4>().invulnerable = true;
            time = Time.time;
            player.transform.position = position;
            ShowLife();
            // UIManager4.instance.SetLife(life);
        }
    }

    void ShowLife()
    {
        lifeText.text = "x" + life;
    }

}
