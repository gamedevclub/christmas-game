using System.Threading;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player4 : MonoBehaviour
{
    // public static Player4 instance;
    public float speed = 4f;
    public Animator anim;
    public bool invulnerable = false;
    int curDirect = 0;
    GameObject flashLight;
    Rigidbody2D rb;
    RaycastHit2D[] hitRay;
    private void Start()
    {
        // if (!instance)
        //     instance = this;
        rb = GetComponent<Rigidbody2D>();
        flashLight = transform.GetChild(0).gameObject;
        anim.SetFloat("Blend", 1f);
        hitRay = Physics2D.RaycastAll(transform.position, Vector2.up, 3.5f);
    }

    private void Update()
    {
        if (!GameManager4.instance.end)
        {
            Move();
            CheckBot();
        }
        else
        {
            rb.velocity = new Vector2(0, 0);
        }
    }

    void Move()
    {
        float direcHor = Input.GetAxisRaw("Horizontal");
        float direcVer = Input.GetAxisRaw("Vertical");

        if (direcVer == 1)
        {
            curDirect = 0;
            rb.velocity = new Vector2(0, 1) * speed;
            anim.SetFloat("Blend", 1f);
            var angle = Mathf.Atan2(0, 0) * Mathf.Rad2Deg;
            flashLight.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

            flashLight.GetComponent<Renderer>().sortingOrder = 6;
            // if (Input.anyKey)
            //     AdventageMove(top);
        }
        else if (direcVer == -1)
        {
            curDirect = 1;
            flashLight.GetComponent<Renderer>().sortingOrder = 12;
            rb.velocity = new Vector2(0, -1) * speed;
            anim.SetFloat("Blend", 0.6f);
            var angle = Mathf.Atan2(0, -1) * Mathf.Rad2Deg;
            flashLight.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

            // if (Input.anyKey)
            //     AdventageMove(bottom);
        }
        else if (direcHor == -1)
        {
            curDirect = 2;
            flashLight.GetComponent<Renderer>().sortingOrder = 12;
            rb.velocity = new Vector2(-1, 0) * speed;
            anim.SetFloat("Blend", 0);
            var angle = Mathf.Atan2(1, 0) * Mathf.Rad2Deg;
            flashLight.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

            // if (Input.anyKey)
            //     AdventageMove(left);
        }
        else if (direcHor == 1)
        {
            curDirect = 3;
            flashLight.GetComponent<Renderer>().sortingOrder = 6;
            rb.velocity = new Vector2(1, 0) * speed;
            anim.SetFloat("Blend", 0.3f);
            var angle = Mathf.Atan2(-1, 0) * Mathf.Rad2Deg;
            flashLight.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

            // if (Input.anyKey)
            //     AdventageMove(right);
        }
        else
        {
            rb.velocity = new Vector2(0, 0);
        }

    }
    void CheckBot()
    {
        switch (curDirect)
        {
            case 0:
                hitRay = Physics2D.RaycastAll(transform.position, Vector2.up, 3.5f);
                break;
            case 1:
                hitRay = Physics2D.RaycastAll(transform.position, Vector2.down, 3.5f);
                break;
            case 2:
                hitRay = Physics2D.RaycastAll(transform.position, Vector2.left, 3.5f);
                break;
            case 3:
                hitRay = Physics2D.RaycastAll(transform.position, Vector2.right, 3.5f);
                break;
        }
        foreach (RaycastHit2D hit in hitRay)
        {
            if (hit.collider.gameObject.GetComponent<Bot4>() != null)
            {
                hit.collider.gameObject.GetComponent<Bot4>().show = false;
                hit.collider.gameObject.GetComponent<Bot4>().timeShow = Time.time;
            }
        }
    }

    // void AdventageMove(string key)
    // {
    //     Vector2 temp = new Vector2();
    //     if (Input.GetKey(top) && key != top)
    //     {
    //         temp = new Vector2(0, 1) * speed;
    //         rb.velocity += temp;
    //     }
    //     else if (Input.GetKey(bottom) && key != bottom)
    //     {
    //         temp = new Vector2(0, -1) * speed;
    //         rb.velocity += temp;
    //     }
    //     else if (Input.GetKey(left) && key != left)
    //     {
    //         temp = new Vector2(-1, 0) * speed;
    //         rb.velocity += temp;
    //     }
    //     else if (Input.GetKey(right) && key != right)
    //     {
    //         temp = new Vector2(1, 0) * speed;
    //         rb.velocity += temp;
    //     }
    //     else
    //     {
    //         rb.velocity -= temp;
    //     }
    // }
}
