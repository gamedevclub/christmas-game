
using System.Collections.Generic;
using UnityEngine;

public class BotMove4 : MonoBehaviour
{
    Rigidbody2D rig;
    float speed = 2.5f;
    Vector2 target;
    // Vector2 firstPosition;
    Vector2 prevPos;
    List<int> direction = new List<int>();
    GameObject curNode = null;
    List<GameObject> nodeList = new List<GameObject>();
    int rand = 0;
    bool firstRun = true;
    Animator anim;
    enum direct
    {
        UP = 0,
        DOWN = 1,
        LEFT = 2,
        RIGHT = 3
    }
    private void Start()
    {
        anim = GetComponent<Animator>();
        rig = GetComponent<Rigidbody2D>();
        // firstPosition = transform.position;
        prevPos = transform.position;
        Move();
    }

    private void Update()
    {
        if (GameManager4.instance.end)
            rig.velocity = new Vector2(0, 0);
        else
        if (!firstRun)
        {
            if (direction[rand] == (int)direct.UP)
            {
                if (transform.position.y >= curNode.transform.position.y)
                {
                    rig.velocity = new Vector2(0, 0);
                    direction.Clear();
                    // prevPos = transform.position;
                    Move();
                }
            }
            else if (direction[rand] == (int)direct.DOWN)
            {
                if (transform.position.y <= curNode.transform.position.y)
                {
                    rig.velocity = new Vector2(0, 0);
                    direction.Clear();
                    // prevPos = transform.position;
                    Move();
                }
            }
            else if (direction[rand] == (int)direct.LEFT)
            {
                if (transform.position.x <= curNode.transform.position.x)
                {
                    rig.velocity = new Vector2(0, 0);
                    direction.Clear();
                    // prevPos = transform.position;
                    Move();
                }
            }
            else if (direction[rand] == (int)direct.RIGHT)
            {
                if (transform.position.x >= curNode.transform.position.x)
                {
                    rig.velocity = new Vector2(0, 0);
                    direction.Clear();
                    // prevPos = transform.position;
                    Move();
                }
            }
        }
    }

    // public void ReStartGame()
    // {
    //     if (!firstRun)
    //     {
    //         transform.position = firstPosition;
    //         Move();
    //     }
    // }
    void Move()
    {
        FindNode();
        if (nodeList.Count == 0)
        {
            transform.position = prevPos;
            nodeList.Clear();
            direction.Clear();
            Move();
            return;
        }
        rand = Random.Range(0, direction.Count);
        curNode = nodeList[rand];

        if (direction[rand] == (int)direct.UP)
        {
            rig.velocity = new Vector2(0, 1) * speed;
            gameObject.GetComponent<Bot4>().direction = (int)direct.UP;
        }
        else if (direction[rand] == (int)direct.DOWN)
        {
            rig.velocity = new Vector2(0, -1) * speed;
            gameObject.GetComponent<Bot4>().direction = (int)direct.DOWN;
        }
        else if (direction[rand] == (int)direct.LEFT)
        {
            anim.SetBool("right", true);
            rig.velocity = new Vector2(-1, 0) * speed;
            gameObject.GetComponent<Bot4>().direction = (int)direct.LEFT;
        }
        else if (direction[rand] == (int)direct.RIGHT)
        {
            anim.SetBool("right", false);
            rig.velocity = new Vector2(1, 0) * speed;
            gameObject.GetComponent<Bot4>().direction = (int)direct.RIGHT;
        }
        if (firstRun)
        {
            firstRun = false;
        }
        nodeList.Clear();
    }

    void FindNode()
    {

        RaycastHit2D[] hitUp = Physics2D.RaycastAll(transform.position, Vector2.up, 25);
        RaycastHit2D[] hitDown = Physics2D.RaycastAll(transform.position, Vector2.down, 25);
        RaycastHit2D[] hitLeft = Physics2D.RaycastAll(transform.position, Vector2.left, 25);
        RaycastHit2D[] hitRight = Physics2D.RaycastAll(transform.position, Vector2.right, 25);
        for (int i = 0; i < hitUp.Length; i++)
        {
            if (hitUp[i].collider.tag == "wall4")
            {
                break;
            }

            if (hitUp[i].collider.tag == "node4")
            {
                if (hitUp[i].collider.gameObject == curNode)
                    continue;
                // Debug.Log(hitUp[i].collider.gameObject.name);
                direction.Add((int)direct.UP);
                nodeList.Add(hitUp[i].collider.gameObject);
                break;
            }
        }
        for (int i = 0; i < hitDown.Length; i++)
        {
            if (hitDown[i].collider.tag == "wall4")
            {
                // Debug.Log("Bottom");
                break;
            }

            if (hitDown[i].collider.tag == "node4")
            {
                if (hitDown[i].collider.gameObject == curNode)
                    continue;
                // Debug.Log(hitDown[i].collider.gameObject.name);
                direction.Add((int)direct.DOWN);
                nodeList.Add(hitDown[i].collider.gameObject);
                break;
            }
        }
        for (int i = 0; i < hitLeft.Length; i++)
        {
            if (hitLeft[i].collider.tag == "wall4")
            {
                // Debug.Log("Left");
                break;
            }
            if (hitLeft[i].collider.tag == "node4")
            {
                if (hitLeft[i].collider.gameObject == curNode)
                    continue;
                // Debug.Log(hitLeft[i].collider.gameObject.name);
                direction.Add((int)direct.LEFT);
                nodeList.Add(hitLeft[i].collider.gameObject);
                break;
            }
        }
        for (int i = 0; i < hitRight.Length; i++)
        {
            if (hitRight[i].collider.tag == "wall4")
            {
                // Debug.Log("Right");
                break;
            }
            if (hitRight[i].collider.tag == "node4")
            {
                if (hitRight[i].collider.gameObject == curNode)
                    continue;
                // Debug.Log(hitRight[i].collider.gameObject.name);
                direction.Add((int)direct.RIGHT);
                nodeList.Add(hitRight[i].collider.gameObject);
                break;
            }
        }

    }



}
