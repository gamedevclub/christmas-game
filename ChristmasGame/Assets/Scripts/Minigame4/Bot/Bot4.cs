using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class Bot4 : MonoBehaviour
{
    public GameObject footPrint;
    public bool win;
    public int direction = 0;
    public bool show;

    public float range = 5f;
    public float timeShow;
    GameObject player;
    float time;
    float delay = 0.5f;
    float delayDestroy = 0.7f;
    AudioSource stepSound;

    private void Start()
    {
        time = Time.time;
        timeShow = Time.time;
        stepSound = GetComponent<AudioSource>();
        stepSound.outputAudioMixerGroup = GameManager4.instance.volume;
        if (win)
            stepSound.volume = stepSound.volume * 0.4f;
        else
            stepSound.volume = stepSound.volume * 0.6f;
    }
    private void Update()
    {
        if (!player && GameObject.FindGameObjectWithTag("player4"))
        {
            player = GameObject.FindGameObjectWithTag("player4");
        }
        if (timeShow + 0.3 < Time.time)
        {
            show = true;
        }
        if (!GameManager4.instance.end)
            CheckPlayer();
    }

    void CheckPlayer()
    {

        if (Distance() <= range && Time.time > time + delay)
        {
            stepSound.Play();
            if (show)
            {
                time = Time.time;
                GameObject temp = Instantiate(footPrint);
                temp.transform.position = transform.position;
                switch (direction)
                {
                    case 0:
                        temp.transform.rotation = Quaternion.Euler(Vector3.forward * 0);
                        break;
                    case 1:
                        temp.transform.rotation = Quaternion.Euler(Vector3.forward * 180);
                        break;
                    case 2:
                        temp.transform.rotation = Quaternion.Euler(Vector3.forward * 90);
                        break;
                    case 3:
                        temp.transform.rotation = Quaternion.Euler(Vector3.forward * 270);
                        break;

                }
                Destroy(temp, delayDestroy);
            }
        }
    }

    float Distance()
    {
        if (player)
            return Vector3.Distance(transform.position, player.transform.position);
        return 100;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "player4")
        {
            if (!win)
            {
                if (!player.GetComponent<Player4>().invulnerable)
                    PlayerLife4.instance.Die();
            }
            else
                GameManager4.instance.GameOver(true);
        }
    }

}
