using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChestManager : MonoBehaviour
{
    public static ChestManager instance;
    public GameObject chestButton;
    public GameObject clickObject;
    public Sprite closeB, closeA, closeS;
    public Sprite openB, openA, openS;
    public GameObject chestEffect;
    public bool clicked = false;
    public bool isAtHome = false;

    WaitForSeconds waitOpenChest = new WaitForSeconds(7);
    WaitForSeconds waitChestEffect = new WaitForSeconds(1);

    Sprite itemSprite;
    private void Awake()
    {
        if (instance == null)
            instance = this;
    }
    public void OpenChest(int level)
    {
        AudioManager.instance.StopAudio();
        Debug.Log("Open chest level : " + level);
        
        QuestOpenAnimation.level = level;
        if (level == 0)
            chestButton.GetComponent<Image>().sprite = closeB;
        else if (level == 1)
            chestButton.GetComponent<Image>().sprite = closeA;
        else if (level == 2)
            chestButton.GetComponent<Image>().sprite = closeS;

        Decoration newDeco = DataLoader.instance.GetDecoRandom(level);
        DataManager.instance.IngameData.decorationList.Add(newDeco);
        DataManager.instance.SaveDeco();

        itemSprite = Resources.Load<Sprite>(newDeco.art);
    }

    public void ClickChest()
    {
        Debug.Log("Click on chest -> Open plssss");
        if (!clicked)
        {
            clicked = false;
            clickObject.SetActive(false);
            chestButton.GetComponent<Animator>().SetTrigger("Open");
            chestButton.GetComponent<Button>().interactable = false;
            AudioManager.instance.OpenChest();
            StartCoroutine(ClickChestWait());
        }
    }

    IEnumerator ClickChestWait()
    {
        // * Effect
        yield return waitChestEffect;
        GameObject effect = Instantiate(chestEffect, Camera.main.transform.position + Vector3.forward * 10, Quaternion.identity);
        effect.transform.GetChild(effect.transform.childCount - 1).gameObject.GetComponent<ParticleSystem>().textureSheetAnimation.SetSprite(0, itemSprite);
        Destroy(effect, 7);

        // * Wait end effect 
        yield return waitOpenChest;

        if (!isAtHome)
            GameManager.instance.ReturnMain();
        else
        {
            AudioManager.instance.HomeBackground();
            gameObject.SetActive(false);
        }
        chestButton.GetComponent<Button>().interactable = true;

    }
}
