using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum Result
{
    Victory, GameOver
}
public class AwardManager : MonoBehaviour
{
    [HideInInspector]
    public Canvas canvas;
    private string currentSceneName;
    const int ChestScene = 12;
    public static AwardManager instance;
    public GameObject awardPanel;
    public Text resultText;
    public Text coinText;
    public Text expText;
    public Text chestText;
    public GameObject questAwardPrefab;
    public GameObject winEffect;
    public GameObject loseEffect;
    public GameObject chestPanel;


    // private int lastWidth = 0;
    // private int lastHeight = 0;


    WaitForSeconds wait = new WaitForSeconds(0.5f);

    bool haveChest = false;

    private void Start()
    {
        currentSceneName = SceneManager.GetActiveScene().name;
        if (!instance)
        {
            canvas = GetComponent<Canvas>();
            instance = this;
            DontDestroyOnLoad(this);
        }
        canvas.renderMode = RenderMode.ScreenSpaceCamera;
        canvas.worldCamera = Camera.main;
        awardPanel.SetActive(false);
    }
    void Update()
    {
        //                                                                             // 
        // Todo : resize window
        // var width = Screen.width; var height = Screen.height;

        // if (lastWidth != width) // if the user is changing the width
        // {
        //     // update the height
        //     var heightAccordingToWidth = width / 16.0 * 9.0;
        //     Screen.SetResolution(width, (int)Mathf.Round((float)heightAccordingToWidth), false, 0);
        // }
        // else if (lastHeight != height) // if the user is changing the height
        // {
        //     // update the width
        //     var widthAccordingToHeight = height / 9.0 * 16.0;
        //     Screen.SetResolution((int)Mathf.Round((float)widthAccordingToHeight), height, false, 0);
        // }


        // lastWidth = width;
        // lastHeight = height;
        //                                                                             // 

        if (currentSceneName != SceneManager.GetActiveScene().name)
        {
            canvas = GetComponent<Canvas>();
            canvas.renderMode = RenderMode.ScreenSpaceCamera;
            Camera.main.farClipPlane = 1000;
            canvas.worldCamera = Camera.main;
            canvas.sortingOrder = 100;
            currentSceneName = SceneManager.GetActiveScene().name;
        }
    }
    // Minigame get award
    public void GetGameAward(int money = 0, int expNum = 0, bool openChest = false, Result result = Result.GameOver)
    {
        money *= 2;
        expNum *= 2;

        DataManager.instance.IngameData.money += money;
        DataManager.instance.IngameData.expItem += expNum;
        DataManager.instance.SaveUnit();

        if (result == Result.GameOver)
        {
            GameObject effect = Instantiate(loseEffect, Camera.main.transform);
            AudioManager.instance.LostGame();
            effect.transform.localPosition += Vector3.forward * 10;
            resultText.text = "Thua rồi hiccc";
        }
        else
        {
            GameObject effect = Instantiate(winEffect, Camera.main.transform);
            AudioManager.instance.WinGame();
            effect.transform.localPosition += Vector3.forward * 10;
            resultText.text = "Yeahhh!!! Thắng rồi";
        }

        coinText.text = money.ToString();
        expText.text = expNum.ToString();

        if (openChest)
        {
            chestText.text = "1";
            haveChest = true;
        }
        else
        {
            chestText.text = "0";
            haveChest = false;
        }

        awardPanel.SetActive(true);

    }

    // Quest get award
    public void GetQuestAward(int money = 0)
    {
        GameObject questAward = Instantiate(questAwardPrefab, awardPanel.transform.parent);
        questAward.GetComponent<RectTransform>().anchoredPosition = Input.mousePosition;
        questAward.GetComponentInChildren<Text>().text = "+ " + money.ToString();
        AudioManager.instance.BuySell();
        DataManager.instance.IngameData.money += money;
        DataManager.instance.SaveUnit();
    }

    public void GetQuestChest(int level)
    {
        OpenChest(level);
    }

    // Minigame only
    public void ConfirmReward()
    {
        if (haveChest)
        {
            awardPanel.SetActive(false);
            StartCoroutine(MinigameChestHandler());
        }
        else
        {
            awardPanel.SetActive(false);
            GameManager.instance.ReturnMain();
        }
    }

    IEnumerator MinigameChestHandler()
    {
        GameManager.instance.loadPanel.SetActive(true);
        GameManager.instance.loadBarProgress.fillAmount = 0;
        yield return new WaitForSeconds(1);

        AsyncOperation scene = SceneManager.LoadSceneAsync(ChestScene);
        while (!scene.isDone)
        {
            GameManager.instance.loadBarProgress.fillAmount = scene.progress;
            yield return wait;
        }
        GameManager.instance.loadPanel.SetActive(false);

        yield return new WaitUntil(() => ChestManager.instance != null);

        chestPanel = ChestManager.instance.gameObject;

        OpenChest(0);
    }

    public void OpenChest(int level)
    {
        chestPanel.SetActive(true);
        ChestManager.instance.OpenChest(level);
    }



}
