using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner7 : MonoBehaviour
{
    public static EnemySpawner7 Instance {get; set;}
    public GameObject[] enemies;
    private int numberOfEnemies;
    private int amount;
    public float radius;
    public float minX;
    public float maxX;
    public float minY;
    public float maxY;
    void Awake()
    {
        Instance = this;
    }
    public void SpawnEnemies(int level)
    {
        Minigame7Manager.Instance.enemies = new List<GameObject>();
        amount = 0;
        switch(level)
        {
            case 0:
                numberOfEnemies = 15;
                break;
            case 1:
                numberOfEnemies = 17;
                break;
            case 2:
                numberOfEnemies = 20;
                break;
            case 3:
                numberOfEnemies = 25;
                break;
            default:
                break;
        }
        while (amount < numberOfEnemies)
        {
            Vector2 spawnPos = new Vector2(Random.Range(minX, maxX), Random.Range(minY, maxY));
            //Check collisions
            if (DetectCollisions(spawnPos))
                continue;

            GameObject selectedObject = enemies[Random.Range(0, enemies.Length)];
            Instantiate(selectedObject, spawnPos, Quaternion.identity, transform);
            Minigame7Manager.Instance.enemies.Add(selectedObject);
            amount++;
        }
    }

    private bool DetectCollisions(Vector2 pos) 
    {        
        Collider2D[] hitColliders = Physics2D.OverlapCircleAll(pos, radius);
        foreach(Collider2D col in hitColliders)
        {
            if (col.CompareTag("obstacle 7") || col.CompareTag("player 7") || col.CompareTag("enemy 7") || col.CompareTag("house 7"))
            {
                return true;
            }
        }
        return false;
    }
}
