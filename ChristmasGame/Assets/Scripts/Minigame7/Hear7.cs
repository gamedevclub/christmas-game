using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hear7 : MonoBehaviour
{
    public float hearingZone;
    private Bat7 bat;
    private GameObject target;
    public LayerMask whatIsCastedObject;
    // Start is called before the first frame update
    void Start()
    {
        bat = GetComponent<Bat7>();
    }

    // Update is called once per frame
    void Update()
    {
        Hear();
    }

    public void Hear()
    {
        try
        {
            Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, hearingZone, whatIsCastedObject);
            foreach (Collider2D col in cols)
            {
                if (col.CompareTag("player 7"))
                {
                    if (FindObjectOfType<PlayerMovement7>().movement.sqrMagnitude > 0.01f)
                    {
                        bat.ChangeState(BatState.Attack);
                    }
                    else
                    {
                        bat.ChangeState(BatState.Patrol);
                    }
                }
            }
            if (bat.state == BatState.Attack)
            {
                if (FindObjectOfType<PlayerMovement7>().movement.sqrMagnitude <= 0.01f)
                {
                    bat.ChangeState(BatState.Patrol);
                }
            }

        }
        catch
        {
            Debug.Log("No bugs");
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, hearingZone);
    }
}
