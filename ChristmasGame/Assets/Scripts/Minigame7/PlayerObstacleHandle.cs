using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerObstacleHandle : MonoBehaviour
{
    public SpriteRenderer graphic;
    public float castRadius;
    private bool isFrontObstacle;
    // Update is called once per frame
    void Update()
    {
        isFrontObstacle = false;
        Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, castRadius);
        foreach (Collider2D col in cols)
        {
            if (col.gameObject.CompareTag("obstacle 7"))
            {
                isFrontObstacle = transform.position.y < col.transform.position.y;
                if (isFrontObstacle == true)
                {
                    if (col.TryGetComponent<SpriteRenderer>(out SpriteRenderer graphicObstacle))
                    {
                        graphicObstacle.sortingOrder = 0;
                    }
                }
                else
                {
                    if (col.TryGetComponent<SpriteRenderer>(out SpriteRenderer graphicObstacle))
                    {
                        graphicObstacle.sortingOrder = 2;
                    }
                }
            }
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, castRadius);
    }
}
