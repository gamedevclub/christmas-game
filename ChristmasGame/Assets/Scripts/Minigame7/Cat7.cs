using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cat7 : MonoBehaviour
{
    public CatState state;
    public float patrolSpeed;
    public float patrolWaitTime;
    public float attackSpeedValue;
    public Rigidbody2D rb;
    public Animator animator;
    public float speedUpPerSecond;
    private float attackSpeed;
    public GameObject minimapIcon;
    private GameObject arrow = null;
    // Start is called before the first frame update
    void Start()
    {
        ChangeState(CatState.Patrol);
    }

    // Update is called once per frame
    void Update()
    {
        if (Minigame7Manager.Instance.state != Minigame7State.Delivery)
        {
            return;
        }
        if (state == CatState.Attack)
        {
            AudioManager7.Instance.Play("Cat Attack", true);
        }
        UpdateState();
    }

    private void UpdateState()
    {
        switch (state)
        {
            case CatState.Patrol:
                if (arrow != null)
                {
                    if (PlayerMovement7.Instance.currentArrow.Count > 0)
                    {
                        PlayerMovement7.Instance.currentArrow.RemoveAt(0);
                    }
                    
                    if (PlayerMovement7.Instance.currentArrow.Count == 0)
                    {
                        PlayerMovement7.Instance.warning.SetActive(false);
                    }
                    Destroy(arrow);
                }
                minimapIcon.GetComponent<SpriteRenderer>().color = Minigame7Manager.Instance.patrolColor;
                attackSpeed = attackSpeedValue;
                GetComponent<Patrol7>().DoPatrol(patrolSpeed);
                break;
            case CatState.Attack:
                if (PlayerMovement7.Instance.currentArrow.Count == 0)
                {
                    PlayerMovement7.Instance.warning.SetActive(true);
                    PlayerMovement7.Instance.warning.GetComponentInChildren<Animator>().Play("Idle");
                }
                if (arrow == null)
                {
                    arrow = Instantiate(PlayerMovement7.Instance.arrowNotice, PlayerMovement7.Instance.transform);
                    arrow.GetComponent<ArrowNotice7>().SetUp(transform);
                    PlayerMovement7.Instance.currentArrow.Add(arrow);
                }
                minimapIcon.GetComponent<SpriteRenderer>().color = Minigame7Manager.Instance.attackColor;
                attackSpeed += speedUpPerSecond;
                attackSpeed = Mathf.Clamp(attackSpeed, 0, 3.5f);
                GetComponent<Move7>().Chasing((FindObjectOfType<PlayerMovement7>().transform.position - transform.position).normalized, attackSpeed);
                break;
            default:
                break;
        }
    }
    public void ChangeState(CatState newState)
    {
        if (state == newState)
            return;
        state = newState;
    }
}

public enum CatState
{
    Patrol,
    Attack
}
