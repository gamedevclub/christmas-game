using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sheep7 : MonoBehaviour
{
    public SheepState state;
    public float patrolSpeed;
    public float patrolWaitTime;
    public float attackSpeed;
    public Rigidbody2D rb;
    public Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        ChangeState(SheepState.Patrol);
    }

    // Update is called once per frame
    void Update()
    {
        UpdateState();
    }

    private void UpdateState()
    {
        switch (state)
        {
            case SheepState.Patrol:
                GetComponent<Patrol7>().DoPatrol(patrolSpeed);
                break;
            case SheepState.Attack:
                GetComponent<Move7>().Chasing((FindObjectOfType<PlayerMovement7>().transform.position - transform.position).normalized, attackSpeed);
                break;
            default:
                break;
        }
    }
    public void ChangeState(SheepState newState)
    {
        if (state == newState)
            return;
        state = newState;
    }
}

public enum SheepState
{
    Patrol,
    Attack
}