using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateLand7 : MonoBehaviour
{
    public Vector2 startPosition;
    public GameObject land;
    public int collumns;
    public int rows;
    // Start is called before the first frame update
    void Start()
    {
        Generate();
    }

    public void Generate()
    {
        for (int i = 0; i < collumns; i++)
        {
            for (int j = 0; j < rows; j++)
            {
                Vector2 spawnPosition = startPosition + new Vector2(i, -j);
                Instantiate(land, spawnPosition, Quaternion.identity, transform);
            }
        }
    }
}
