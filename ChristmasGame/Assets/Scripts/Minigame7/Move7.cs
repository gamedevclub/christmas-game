using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move7 : MonoBehaviour
{
    public Enemy7 typeOfEnemy;
    private Rooster7 rooster;
    private Dog7 dog;
    private Cat7 cat;
    private Bat7 bat;
    private void Awake() 
    {
        switch (typeOfEnemy)
        {
            case Enemy7.Rooster:
                rooster = GetComponent<Rooster7>();
                break;
            case Enemy7.Dog:
                dog = GetComponent<Dog7>();
                break;
            case Enemy7.Cat:
                cat = GetComponent<Cat7>();
                break;
            case Enemy7.Bat:
                bat = GetComponent<Bat7>();
                break;
            default:
                break;
        }
    }
    public void Move(Vector2 direction, float speed)
    {
        switch (typeOfEnemy)
        {
            case Enemy7.Rooster:
                rooster.rb.MovePosition(rooster.rb.position + direction * speed * Time.deltaTime);
                rooster.animator.SetFloat("Horizontal Facing", direction.x);
                rooster.animator.SetFloat("Vertical Facing", direction.y);
                rooster.animator.SetFloat("Horizontal Move", direction.x);
                rooster.animator.SetFloat("Vertical Move", direction.y);
                rooster.animator.SetBool("isAttacking", rooster.state == RoosterState.Attack);
                rooster.animator.SetFloat("Speed", speed);
                break;
            case Enemy7.Dog:
                dog.rb.MovePosition(dog.rb.position + direction * speed * Time.deltaTime);
                dog.animator.SetFloat("Horizontal Facing", direction.x);
                dog.animator.SetFloat("Vertical Facing", direction.y);
                dog.animator.SetFloat("Horizontal Move", direction.x);
                dog.animator.SetFloat("Vertical Move", direction.y);
                dog.animator.SetBool("isAttacking", dog.state == DogState.Attack);
                dog.animator.SetFloat("Speed", speed);
                break;
            case Enemy7.Cat:
                cat.rb.MovePosition(cat.rb.position + direction * speed * Time.deltaTime);
                cat.animator.SetFloat("Horizontal Facing", direction.x);
                cat.animator.SetFloat("Vertical Facing", direction.y);
                cat.animator.SetFloat("Horizontal Move", direction.x);
                cat.animator.SetFloat("Vertical Move", direction.y);
                cat.animator.SetBool("isAttacking", cat.state == CatState.Attack);
                cat.animator.SetFloat("Speed", speed);
                break;
            case Enemy7.Bat:
                bat.rb.MovePosition(bat.rb.position + direction * speed * Time.deltaTime);
                bat.animator.SetFloat("Horizontal Facing", direction.x);
                bat.animator.SetFloat("Vertical Facing", direction.y);
                bat.animator.SetFloat("Horizontal Move", direction.x);
                bat.animator.SetFloat("Vertical Move", direction.y);
                bat.animator.SetBool("isAttacking", bat.state == BatState.Attack);
                bat.animator.SetFloat("Speed", speed);
                break;
            default:
                break;
        }

        
    }

    public void Chasing(Vector2 direction, float speed)
    {
        // print("Attack");
        switch (typeOfEnemy)
        {
            case Enemy7.Rooster:
                rooster.rb.MovePosition(rooster.rb.position + direction * speed * Time.deltaTime);
                rooster.animator.SetFloat("Horizontal Facing", direction.x);
                rooster.animator.SetFloat("Vertical Facing", direction.y);
                rooster.animator.SetFloat("Horizontal Move", direction.x);
                rooster.animator.SetFloat("Vertical Move", direction.y);
                rooster.animator.SetBool("isAttacking", rooster.state == RoosterState.Attack);
                rooster.animator.SetFloat("Speed", speed);
                break;
            case Enemy7.Dog:
                dog.rb.MovePosition(dog.rb.position + direction * speed * Time.deltaTime);
                dog.animator.SetFloat("Horizontal Facing", direction.x);
                dog.animator.SetFloat("Vertical Facing", direction.y);
                dog.animator.SetFloat("Horizontal Move", direction.x);
                dog.animator.SetFloat("Vertical Move", direction.y);
                dog.animator.SetBool("isAttacking", dog.state == DogState.Attack);
                dog.animator.SetFloat("Speed", speed);
                break;
            case Enemy7.Cat:
                cat.rb.MovePosition(cat.rb.position + direction * speed * Time.deltaTime);
                cat.animator.SetFloat("Horizontal Facing", direction.x);
                cat.animator.SetFloat("Vertical Facing", direction.y);
                cat.animator.SetFloat("Horizontal Move", direction.x);
                cat.animator.SetFloat("Vertical Move", direction.y);
                cat.animator.SetBool("isAttacking", cat.state == CatState.Attack);
                cat.animator.SetFloat("Speed", speed);
                break;
            case Enemy7.Bat:
                bat.rb.MovePosition(bat.rb.position + direction * speed * Time.deltaTime);
                bat.animator.SetFloat("Horizontal Facing", direction.x);
                bat.animator.SetFloat("Vertical Facing", direction.y);
                bat.animator.SetFloat("Horizontal Move", direction.x);
                bat.animator.SetFloat("Vertical Move", direction.y);
                bat.animator.SetBool("isAttacking", bat.state == BatState.Attack);
                bat.animator.SetFloat("Speed", speed);
                break;
            default:
                break;
        }

        
    }
}
