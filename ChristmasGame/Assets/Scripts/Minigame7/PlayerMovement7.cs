using System.Xml;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement7 : MonoBehaviour
{
    public Rigidbody2D rb;
    public float movementSpeed;
    public Animator animator;
    public Vector2 movement;
    private Vector2 facing;
    [Header("Slowing down effect")]
    private bool isSlowingDown;
    public float timeSlowingDownValue;
    private float timeSlowingDown;
    public float movementSpeedSlowDown;
    public Color slowingDownColor;
    public HealthBar7 slowDownCountDownBar;
    public HealthBar7 deliveryCountDownBar;
    public float factorCountDownDelivery;
    public float maxDeliveryTime;
    [HideInInspector]
    public float deliveryTime;
    public GameObject batEffectGameOver;
    public GameObject arrowNotice;
    public GameObject warning;
    public List<GameObject> currentArrow = new List<GameObject>();
    private bool isMoving;
    public static PlayerMovement7 Instance {get; set;}
    void Awake()
    {
        Instance = this;
    }
    // Update is called once per frame
    void Update()
    {
        if (deliveryTime >= maxDeliveryTime)
        {
            Minigame7Manager.Instance.atFrontDoor = true;
        }
        else
        {
            Minigame7Manager.Instance.atFrontDoor = false;
        }
        if (Minigame7Manager.Instance.state != Minigame7State.Delivery)
        {
            movement = Vector2.zero;
            return;
        }
        else if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            movement.x = Input.GetAxisRaw("Horizontal");
            movement = new Vector2(movement.x, 0);
            facing = movement;
        }
        else if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            movement.y = Input.GetAxisRaw("Vertical");
            movement = new Vector2(0, movement.y);
            facing = movement;
        }
        else if (Minigame7Manager.Instance.atFrontDoor == true)
        {
            StopMoving();
            
            Minigame7Manager.Instance.score ++;
            if (Minigame7Manager.Instance.score == 4)
            {
                Minigame7Manager.Instance.UpdateState(Minigame7State.Win);
            }
            else
            {
                Minigame7Manager.Instance.UpdateState(Minigame7State.NextLevel);
            }
        }
        else
        {
            movement = Vector2.zero;
        }

        if (isSlowingDown)
        {
            slowDownCountDownBar.gameObject.SetActive(true);
            slowDownCountDownBar.SetMaxValue(timeSlowingDownValue);
            if (timeSlowingDown > 0)
            {
                GetComponent<SpriteRenderer>().color = slowingDownColor;
                timeSlowingDown -= Time.deltaTime;
                timeSlowingDown = Mathf.Clamp(timeSlowingDown, 0, timeSlowingDownValue);
                slowDownCountDownBar.SetValue(timeSlowingDown);
            }
            else
            {
                GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
                isSlowingDown = false;
                slowDownCountDownBar.gameObject.SetActive(false);
            }
        }
        else
        {
            slowDownCountDownBar.gameObject.SetActive(false);
        }

        if (Minigame7Manager.Instance.timer <= 0)
        {
            Minigame7Manager.Instance.UpdateState(Minigame7State.GameOver);
        }

        if (movement.sqrMagnitude > 0.01f)
        {
            isMoving = true;
        }
        else
        {
            isMoving = false;
        }

        if (isMoving == true)
        {
            AudioManager7.Instance.Play("Move", true);
        }
        else
        {
            AudioManager7.Instance.Stop("Move");
        }

        animator.SetFloat("Horizontal Facing", facing.x);
        animator.SetFloat("Vertical Facing", facing.y);
        animator.SetFloat("Horizontal", movement.x);
        animator.SetFloat("Vertical", movement.y);
        animator.SetFloat("Speed", movement.sqrMagnitude);

        CountDownDelivery();
    }

    void FixedUpdate()
    {
        rb.MovePosition(rb.position + movement * (isSlowingDown ? movementSpeedSlowDown : movementSpeed) * Time.fixedDeltaTime);
    }

    void CountDownDelivery()
    {
        deliveryTime -= Time.deltaTime * factorCountDownDelivery;
        deliveryTime = Mathf.Clamp(deliveryTime, 0, maxDeliveryTime);
        deliveryCountDownBar.SetValue(deliveryTime);
    }

    public void StopMoving()
    {
        movement = Vector2.zero;
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.collider.CompareTag("house 7"))
        {
            // other.collider.gameObject.GetComponent<House7>().ShowNotice();
            deliveryTime += 3 * Time.deltaTime;
            deliveryTime = Mathf.Clamp(deliveryTime, 0, maxDeliveryTime);
            deliveryCountDownBar.SetValue(deliveryTime);
            // Minigame7Manager.Instance.atFrontDoor = true;
        }
        else if (other.collider.CompareTag("enemy 7"))
        {
            AudioManager7.Instance.Play("Gameover", false);
            Minigame7Manager.Instance.UpdateState(Minigame7State.GameOver);
        }
        else if (other.collider.CompareTag("egg 7"))
        {
            timeSlowingDown = timeSlowingDownValue;
            isSlowingDown = true;
        }
    }

    void OnCollisionStay2D(Collision2D other)
    {
        if (other.collider.CompareTag("house 7"))
        {
            // other.collider.gameObject.GetComponent<House7>().ShowNotice();
            deliveryTime += 3 * Time.deltaTime;
            deliveryTime = Mathf.Clamp(deliveryTime, 0, maxDeliveryTime);
            deliveryCountDownBar.SetValue(deliveryTime);
            // Minigame7Manager.Instance.atFrontDoor = true;
        }
    }

    void OnCollisionExit2D(Collision2D other)
    {
        if (other.collider.CompareTag("house 7"))
        {
            // other.collider.gameObject.GetComponent<House7>().HideNotice();
            CountDownDelivery();
            // Minigame7Manager.Instance.atFrontDoor = false;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("enemy 7"))
        {
            AudioManager7.Instance.Play("Gameover", false);
            Instantiate(batEffectGameOver, transform.position, Quaternion.identity);
            Minigame7Manager.Instance.UpdateState(Minigame7State.GameOver);
        }    
    }
}
