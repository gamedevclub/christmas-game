using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Egg7 : MonoBehaviour
{
    public float rotateSpeed;
    public GameObject explode;
    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, 10f);
    }

    // Update is called once per frame
    void Update()
    {
        Rotate();
    }

    void Rotate()
    {
        transform.Rotate(Vector3.forward, rotateSpeed * Time.deltaTime);
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        AudioManager7.Instance.Play("Egg Crack", false);
        Instantiate(explode, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
