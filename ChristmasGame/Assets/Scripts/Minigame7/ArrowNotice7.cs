using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowNotice7 : MonoBehaviour
{
    private Transform target = null;
    // Update is called once per frame
    void Update()
    {
        if (target != null)
        {
            Vector2 direction = target.position - transform.position;
            direction.Normalize();
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90;
            transform.rotation = Quaternion.Euler(0, 0, angle);
        }
    }

    public void SetUp(Transform _target)
    {
        target = _target;
    }
}
