using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class IngameUI7 : MonoBehaviour
{
    public GameObject timerText;
    public GameObject giftChosenImage;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Minigame7Manager.Instance.state != Minigame7State.Delivery) return;
        Minigame7Manager.Instance.timer -= Time.deltaTime;
        UpdateTimer(Minigame7Manager.Instance.timer);
    }

    public void SetUp() 
    {
        UpdateTimer(Minigame7Manager.Instance.timer);
        giftChosenImage.GetComponent<Image>().sprite = Minigame7Manager.Instance.GetCurrentGiftImage().sprite;
    }

    public void UpdateTimer(float time)
    {
        int minute = (int)(time / 60);
        int second = (int)(time - minute * 60);
        string minuteInText = minute > 9 ? minute.ToString() : "0" + minute.ToString();
        string secondInText = second > 9 ? second.ToString() : "0" + second.ToString();

        timerText.GetComponent<TextMeshProUGUI>().text = minuteInText + ":" + secondInText;
    }
}
