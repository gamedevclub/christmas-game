using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollower7 : MonoBehaviour
{
    public Transform target;
    public float smoothSpeed = 0.125f;
    public Vector3 offset;
    public Vector2 minPosition;
    public Vector2 maxPosition;
    public Transform center;
    void FixedUpdate()
    {
        if (target == null)
        {
            return;
        }
        print("Follow");
        Vector3 desiredPosition = target.position + offset;
        Vector3 smoothPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
        smoothPosition.x = Mathf.Clamp(smoothPosition.x, minPosition.x, maxPosition.x);
        smoothPosition.y = Mathf.Clamp(smoothPosition.y, minPosition.y, maxPosition.y);
        transform.position = smoothPosition;
    }

    public void StopFollowingPlayer()
    {
        target = center;
    }

    public void FollowPlayer()
    {
        target = FindObjectOfType<PlayerMovement7>().transform;
    }
}
