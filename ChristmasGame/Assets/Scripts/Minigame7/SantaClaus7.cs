using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SantaClaus7 : MonoBehaviour
{
    public Rigidbody2D rb;
    public Animator animator;
    private Vector2 positionToMove = new Vector2(-0.8f, 0.4f);
    public float speed;
    private bool isMove = false;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(MoveTowardPlayer());
    }

    // Update is called once per frame
    void Update()
    {
        if (Minigame7Manager.Instance.state != Minigame7State.Lobby) return;
        if (isMove == true)
            MoveToCenter();
    }

    public void MoveToCenter()
    {
        if (Vector2.Distance(transform.position, positionToMove) < 0.5f)
        {
            Minigame7Manager.Instance.UpdateState(Minigame7State.ChooseGift);
            isMove = false;
            rb.velocity = Vector2.zero;
        }
        else
        {
            rb.velocity = Vector2.right * speed * Time.deltaTime;
        }
        if (isMove)
        {
            AudioManager7.Instance.Play("Move", true);
        }
        else
        {
            AudioManager7.Instance.Stop("Move");
        }
        animator.SetFloat("Speed", rb.velocity.x);
    }

    IEnumerator MoveTowardPlayer()
    {
        isMove = false;
        yield return Helper7.GetWait(1.5f);
        isMove = true;
    }
}
