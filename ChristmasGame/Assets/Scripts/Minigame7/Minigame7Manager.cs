using System.Net.Mime;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Minigame7Manager : MonoBehaviour
{
    public static Minigame7Manager Instance {get; set;}
    public Minigame7State state;
    public int numberOfRemainingGifts;
    public List<GameObject> gifts = new List<GameObject>();
    public int selectedGiftIndex;
    public GameObject santaClausTemplate;
    public Vector2 positionToSpawnSantaClaus;
    public GameObject santaClausAppear;
    public GameObject teleport;
    public Transform[] playerStartDeliveryPosition;
    public List<GameObject> houses = new List<GameObject>();
    private GameObject player;
    public float timer;
    private GameObject santa;
    public GameObject currentHouse;
    public int chosenHouse;
    public bool atFrontDoor;
    public GameObject gameOverEffect;
    public int score;
    public List<GameObject> enemies;
    public Color attackColor;
    public Color patrolColor;
    // public GameObject winEffect;
    // public GameObject loseEffect;
    public static event Action<Minigame7State> OnMinigame7StateChanged;
    void Awake()
    {
        Instance = this;
    }
    void Start()
    {
        score = 0;
        atFrontDoor = false;
        player = FindObjectOfType<PlayerMovement7>().gameObject;
        UpdateState(Minigame7State.Lobby);
    }

    public void UpdateState(Minigame7State newState)
    {
        state = newState;
        // print(state.ToString());

        switch (state)
        {
            case Minigame7State.ChooseGift:
                HandleChooseGift();
                break;
            case Minigame7State.Delivery:
                HandleDelivery();
                break;
            case Minigame7State.Lobby:
                HandleLobby();
                break;
            case Minigame7State.GameOver:
                HandleGameOver();
                break;
            case Minigame7State.Win:
                HandleWin();
                break;
            case Minigame7State.Teleport:
                HandleTeleport();
                break;
            case Minigame7State.NextLevel:
                HandleNextLevel();
                break;
            default:
                Debug.LogError("Invalid Minigame 7 State !");
                break;
        }

        OnMinigame7StateChanged?.Invoke(newState);
    }

    public void HandleChooseGift()
    {

    }

    public void HandleDelivery()
    {
        
        PlayerMovement7.Instance.deliveryCountDownBar.SetMaxValue(PlayerMovement7.Instance.maxDeliveryTime);
        PlayerMovement7.Instance.deliveryCountDownBar.SetValue(0);
        PlayerMovement7.Instance.deliveryTime = 0;

        chosenHouse = UnityEngine.Random.Range(0, houses.Count);
        currentHouse = Instantiate(houses[chosenHouse], Vector2.zero, Quaternion.identity);
        Destroy(santa);
    }

    public void HandleLobby()
    {
        StartCoroutine(Lobby());
    }

    public void HandleGameOver()
    {
        // print("game over");
        StartCoroutine(GameOver());
    }

    public void HandleWin()
    {
        FindObjectOfType<AudioManager7>().StopAll();
        GetAwardByScore(score, Result.Victory);
    }
    public void HandleTeleport()
    {
        StartCoroutine(Teleport());
    }
    public void HandleNextLevel()
    {
        StartCoroutine(NextLevel());
    }

    public Image GetCurrentGiftImage()
    {
        return gifts[selectedGiftIndex].transform.GetChild(0).GetComponent<Image>();
    }

    IEnumerator Lobby()
    {
        yield return Helper7.GetWait(0.5f);
        AudioManager7.Instance.Play("Santa Appear", false);
        Instantiate(santaClausAppear, positionToSpawnSantaClaus, Quaternion.identity);
        yield return Helper7.GetWait(0.1f);
        santa = Instantiate(santaClausTemplate, positionToSpawnSantaClaus, Quaternion.identity);
    }
    IEnumerator Teleport()
    {
        Camera.main.GetComponent<CameraFollower7>().StopFollowingPlayer();
        yield return Helper7.GetWait(0.5f);
        AudioManager7.Instance.Play("Teleport", false);
        Instantiate(teleport, Vector2.zero, Quaternion.identity);
        yield return Helper7.GetWait(1f);
        UpdateState(Minigame7State.Delivery);
        player.transform.position = playerStartDeliveryPosition[UnityEngine.Random.Range(0, playerStartDeliveryPosition.Length)].position;
        Helper7.Camera.transform.position = player.transform.position;
        EnemySpawner7.Instance.SpawnEnemies(4 - houses.Count);
        Invoke("RequestCameraFollow", 4f);
        // ObjectSpawner7.Instance.SpawnObjects();
    }

    IEnumerator NextLevel()
    {
        foreach (GameObject arrow in PlayerMovement7.Instance.currentArrow)
        {
            Destroy(arrow);
        }
        PlayerMovement7.Instance.warning.SetActive(false);

        selectedGiftIndex = 0;
        yield return Helper7.GetWait(0.5f);
        
        AudioManager7.Instance.Play("Teleport", false);
        Instantiate(teleport, player.transform.position, Quaternion.identity);

        yield return Helper7.GetWait(2.5f);

        gifts.RemoveAt(selectedGiftIndex);
        houses.RemoveAt(chosenHouse);
        Destroy(currentHouse);
        Helper7.DeletChildren(EnemySpawner7.Instance.transform);

        FindObjectOfType<EnemySpawner7>().gameObject.GetComponent<GenerateLand7>().Generate();
        player.transform.position = Vector2.zero;
        UpdateState(Minigame7State.Lobby);
    }

    IEnumerator GameOver()
    {
        Instantiate(gameOverEffect, player.transform.position, Quaternion.identity);
        yield return Helper7.GetWait(3f);
        FindObjectOfType<AudioManager7>().StopAll();
        GetAwardByScore(score, Result.GameOver);
    }

    public void RequestCameraFollow()
    {
        Camera.main.gameObject.GetComponent<CameraFollower7>().FollowPlayer();
    }
    /// <summary>
    /// * Score = 1 => 50 gold and 50 exp
    /// * Score = 2 => 100 gold and 100 exp
    /// * Score = 3 => 150 gold and 150 exp
    /// * Score = 4 => 250 gold and 250 exp
    /// </summary>
    /// <param name="score"></param>

    public void GetAwardByScore(int score, Result result)
    {
        Minigame7.items = score > Minigame7.items ? score : Minigame7.items;
        DataManager.instance.SaveMinigameData(7);
        switch (score)
        {
            case 0:
                AwardManager.instance.GetGameAward(0, 0, false, result);
                break;
            case 1:
                AwardManager.instance.GetGameAward(100, 10, false, result);
                break;
            case 2:
                AwardManager.instance.GetGameAward(250, 25, true, result);
                break;
            case 3:
                AwardManager.instance.GetGameAward(400, 40, true, result);
                break;
            case 4:
                AwardManager.instance.GetGameAward(600, 60, true, result);
                break;
            default:
                break;
        }
    }
}

public enum Minigame7State
{
    ChooseGift,
    Delivery,
    Lobby,
    GameOver,
    Teleport,
    NextLevel,
    Win
}
