using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eyes7 : MonoBehaviour
{
    public Enemy7 typeOfEnemy;
    private Rooster7 rooster;
    private Dog7 dog;
    private Cat7 cat;
    private object baseEnemy;
    private Patrol7 patrol;
    private float angle;
    public float dampingRotation = 10;
    public Transform eyes;
    public float visionDistance;
    public LayerMask whatIsCastedObject;
    void Awake()
    {
        switch (typeOfEnemy)
        {
            case Enemy7.Rooster:
                rooster = GetComponentInParent<Rooster7>();
                break;
            case Enemy7.Dog:
                dog = GetComponentInParent<Dog7>();
                break;
            case Enemy7.Cat:
                cat = GetComponentInParent<Cat7>();
                break;
            default:
                break;
        }
        patrol = GetComponentInParent<Patrol7>();
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Look();
        switch (typeOfEnemy)
        {
            case Enemy7.Rooster:
                if (rooster.state == RoosterState.Attack)
                {
                    RotateTowardsVelocity((FindObjectOfType<PlayerMovement7>().gameObject.transform.position - transform.position).normalized);
                }
                if (rooster.state == RoosterState.Patrol)
                {
                    RotateTowardsVelocity(patrol.direction);
                }
                break;
            case Enemy7.Dog:
                if (dog.state == DogState.Attack)
                {
                    RotateTowardsVelocity((FindObjectOfType<PlayerMovement7>().gameObject.transform.position - transform.position).normalized);
                }
                if (dog.state == DogState.Patrol)
                {
                    RotateTowardsVelocity(patrol.direction);
                }
                break;
            case Enemy7.Cat:
                if (cat.state == CatState.Attack)
                {
                    RotateTowardsVelocity((FindObjectOfType<PlayerMovement7>().gameObject.transform.position - transform.position).normalized);
                }
                if (cat.state == CatState.Patrol)
                {
                    RotateTowardsVelocity(patrol.direction);
                }
                break;
            default:
                break;
        }
    }

    public void RotateTowardsVelocity(Vector2 pos)
    {
        pos *= 10000;
        if ((pos - (Vector2)transform.position).normalized.Equals(Vector2.zero))
        {
            return;
        }
        Vector2 v = (pos - (Vector2)transform.position).normalized;
        angle = Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg - 90;
        var desiredRotQ = Quaternion.Euler(transform.eulerAngles.x, transform.eulerAngles.y, angle);
        transform.rotation = Quaternion.Lerp(transform.rotation, desiredRotQ, Time.deltaTime * dampingRotation);
    }

    public void DirectLineOfSight(Vector2 position, Color color)
    {
        Debug.DrawLine(transform.position, position, color);
    }
    public void Look()
    {
        // lineOfSight.SetPosition(0, transform.position);
        // if (rooster.state != RoosterState.Attack)
        // {
        //     lineOfSight.SetPosition(1, transform.position);
        // }
        RaycastHit2D hit2D = new RaycastHit2D();
        switch (typeOfEnemy)
        {
            case Enemy7.Rooster:
                if (rooster.state == RoosterState.Attack)
                {
                    hit2D = Physics2D.Raycast(transform.position, (FindObjectOfType<PlayerMovement7>().gameObject.transform.position - transform.position).normalized, Mathf.Infinity, whatIsCastedObject);
                }
                else
                {
                    hit2D = Physics2D.Raycast(transform.position, (eyes.position - transform.position).normalized, visionDistance, whatIsCastedObject);
                }
                break;
            case Enemy7.Dog:
                if (dog.state == DogState.Attack)
                {
                    hit2D = Physics2D.Raycast(transform.position, (FindObjectOfType<PlayerMovement7>().gameObject.transform.position - transform.position).normalized, Mathf.Infinity, whatIsCastedObject);
                }
                else
                {
                    hit2D = Physics2D.Raycast(transform.position, (eyes.position - transform.position).normalized, visionDistance, whatIsCastedObject);
                }
                break;
            case Enemy7.Cat:
                if (cat.state == CatState.Attack)
                {
                    hit2D = Physics2D.Raycast(transform.position, (FindObjectOfType<PlayerMovement7>().gameObject.transform.position - transform.position).normalized, Mathf.Infinity, whatIsCastedObject);
                }
                else
                {
                    hit2D = Physics2D.Raycast(transform.position, (eyes.position - transform.position).normalized, visionDistance, whatIsCastedObject);
                }
                break;
            default:
                break;
        }

        RaycastHit2D hit2DLeft = Physics2D.Raycast(transform.position, (eyes.position + eyes.right * 0.2f - transform.position).normalized, visionDistance, whatIsCastedObject);
        RaycastHit2D hit2DLeftLeft = Physics2D.Raycast(transform.position, (eyes.position + eyes.right * 0.1f - transform.position).normalized, visionDistance, whatIsCastedObject);
        RaycastHit2D hit2DRight = Physics2D.Raycast(transform.position, (eyes.position - eyes.right * 0.2f - transform.position).normalized, visionDistance, whatIsCastedObject);
        RaycastHit2D hit2DRightRight = Physics2D.Raycast(transform.position, (eyes.position - eyes.right * 0.1f - transform.position).normalized, visionDistance, whatIsCastedObject);

        if (hit2D.collider != null)
        {
            DirectLineOfSight(hit2D.point, Color.red);
            switch (typeOfEnemy)
            {
                case Enemy7.Rooster:
                    if (rooster.state == RoosterState.Attack)
                    {
                        if (hit2D.collider.CompareTag("obstacle 7"))
                        {
                            rooster.ChangeState(RoosterState.Patrol);
                        }
                    }
                    else
                    {
                        if (hit2D.collider.CompareTag("player 7"))
                        {
                            rooster.ChangeState(RoosterState.Attack);
                        }
                    }
                    break;
                case Enemy7.Dog:
                    if (dog.state == DogState.Attack)
                    {
                        if (hit2D.collider.CompareTag("obstacle 7"))
                        {
                            dog.ChangeState(DogState.Patrol);
                        }
                    }
                    else
                    {
                        if (hit2D.collider.CompareTag("player 7"))
                        {
                            dog.ChangeState(DogState.Attack);
                        }
                    }
                    break;
                case Enemy7.Cat:
                    if (cat.state == CatState.Attack)
                    {
                        if (hit2D.collider.CompareTag("obstacle 7"))
                        {
                            cat.ChangeState(CatState.Patrol);
                        }
                    }
                    else
                    {
                        if (hit2D.collider.CompareTag("player 7"))
                        {
                            cat.ChangeState(CatState.Attack);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        else
        {
            DirectLineOfSight((eyes.position - transform.position).normalized * visionDistance, Color.green);
        }

        if (hit2DLeft.collider != null)
        {
            DirectLineOfSight(hit2DLeft.point, Color.red);
            switch (typeOfEnemy)
            {
                case Enemy7.Rooster:
                    if (rooster.state != RoosterState.Attack)
                    {
                        if (hit2DLeft.collider.CompareTag("player 7"))
                        {
                            rooster.ChangeState(RoosterState.Attack);
                        }
                    }
                    break;
                case Enemy7.Dog:
                    if (dog.state != DogState.Attack)
                    {
                        if (hit2DLeft.collider.CompareTag("player 7"))
                        {
                            dog.ChangeState(DogState.Attack);
                        }
                    }
                    break;
                case Enemy7.Cat:
                    if (cat.state != CatState.Attack)
                    {
                        if (hit2DLeft.collider.CompareTag("player 7"))
                        {
                            cat.ChangeState(CatState.Attack);
                        }
                    }
                    break;
                default:
                    break;
            }

        }
        else
        {
            DirectLineOfSight((eyes.position + eyes.right * 0.2f - transform.position).normalized * visionDistance, Color.green);
        }

        if (hit2DRight.collider != null)
        {
            DirectLineOfSight(hit2DRight.point, Color.red);
            switch (typeOfEnemy)
            {
                case Enemy7.Rooster:
                    if (rooster.state != RoosterState.Attack)
                    {
                        if (hit2DRight.collider.CompareTag("player 7"))
                        {
                            rooster.ChangeState(RoosterState.Attack);
                        }
                    }
                    break;
                case Enemy7.Dog:
                    if (dog.state != DogState.Attack)
                    {
                        if (hit2DRight.collider.CompareTag("player 7"))
                        {
                            dog.ChangeState(DogState.Attack);
                        }
                    }
                    break;
                case Enemy7.Cat:
                    if (cat.state != CatState.Attack)
                    {
                        if (hit2DRight.collider.CompareTag("player 7"))
                        {
                            cat.ChangeState(CatState.Attack);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        else
        {
            DirectLineOfSight((eyes.position - eyes.right * 0.2f - transform.position).normalized * visionDistance, Color.green);
        }

        if (hit2DLeftLeft.collider != null)
        {
            DirectLineOfSight(hit2DLeftLeft.point, Color.red);
            switch (typeOfEnemy)
            {
                case Enemy7.Rooster:
                    if (rooster.state != RoosterState.Attack)
                    {
                        if (hit2DLeftLeft.collider.CompareTag("player 7"))
                        {
                            rooster.ChangeState(RoosterState.Attack);
                        }
                    }
                    break;
                case Enemy7.Dog:
                    if (dog.state != DogState.Attack)
                    {
                        if (hit2DLeftLeft.collider.CompareTag("player 7"))
                        {
                            dog.ChangeState(DogState.Attack);
                        }
                    }
                    break;
                case Enemy7.Cat:
                    if (cat.state != CatState.Attack)
                    {
                        if (hit2DLeftLeft.collider.CompareTag("player 7"))
                        {
                            cat.ChangeState(CatState.Attack);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        else
        {
            DirectLineOfSight((eyes.position + eyes.right * 0.1f - transform.position).normalized * visionDistance, Color.green);
        }

        if (hit2DRightRight.collider != null)
        {
            DirectLineOfSight(hit2DRightRight.point, Color.red);
            switch (typeOfEnemy)
            {
                case Enemy7.Rooster:
                    if (rooster.state != RoosterState.Attack)
                    {
                        if (hit2DRightRight.collider.CompareTag("player 7"))
                        {
                            rooster.ChangeState(RoosterState.Attack);
                        }
                    }
                    break;
                case Enemy7.Dog:
                    if (dog.state != DogState.Attack)
                    {
                        if (hit2DRightRight.collider.CompareTag("player 7"))
                        {
                            dog.ChangeState(DogState.Attack);
                        }
                    }
                    break;
                case Enemy7.Cat:
                    if (cat.state != CatState.Attack)
                    {
                        if (hit2DRightRight.collider.CompareTag("player 7"))
                        {
                            cat.ChangeState(CatState.Attack);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        else
        {
            DirectLineOfSight((eyes.position - eyes.right * 0.1f - transform.position).normalized * visionDistance, Color.green);
        }
    }
}
