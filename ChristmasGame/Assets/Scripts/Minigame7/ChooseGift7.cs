using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChooseGift7 : MonoBehaviour
{
    public GameObject giftPanel;
    private List<GameObject> giftUI;
    void Start() 
    {
    }
    void Update()
    {
        if (Minigame7Manager.Instance.selectedGiftIndex == -1) return;
        if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.S))
        {
            AudioManager7.Instance.Play("Choose Gift", false);
            Minigame7Manager.Instance.selectedGiftIndex--;
            Minigame7Manager.Instance.selectedGiftIndex = Minigame7Manager.Instance.selectedGiftIndex > Minigame7Manager.Instance.houses.Count - 1 ? 0 : (Minigame7Manager.Instance.selectedGiftIndex < 0 ? Minigame7Manager.Instance.houses.Count - 1 : Minigame7Manager.Instance.selectedGiftIndex);
            SelectGift(Minigame7Manager.Instance.selectedGiftIndex);
        }
        if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.W))
        {
            AudioManager7.Instance.Play("Choose Gift", false);
            Minigame7Manager.Instance.selectedGiftIndex++;
            Minigame7Manager.Instance.selectedGiftIndex = Minigame7Manager.Instance.selectedGiftIndex > Minigame7Manager.Instance.houses.Count - 1 ? 0 : (Minigame7Manager.Instance.selectedGiftIndex < 0 ? Minigame7Manager.Instance.houses.Count - 1 : Minigame7Manager.Instance.selectedGiftIndex);
            SelectGift(Minigame7Manager.Instance.selectedGiftIndex);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Minigame7Manager.Instance.UpdateState(Minigame7State.Teleport);
        }
    }

    public void Setup() 
    {
        Helper7.DeletChildren(giftPanel.transform);
        giftUI = new List<GameObject>();
        foreach (GameObject gift in Minigame7Manager.Instance.gifts)
        {
            GameObject temp = Instantiate(gift, giftPanel.transform);
            giftUI.Add(temp);
        }
        Minigame7Manager.Instance.selectedGiftIndex = giftUI.Count > 0 ? 0 : -1;
        for (int i = 0; i < giftUI.Count; i++)
        {
            if (i == Minigame7Manager.Instance.selectedGiftIndex)
            {
                HighlightItem(i);
            }
            else
            {
                UnHighlightItem(i);
            }
        }
    }

    public void SelectGift(int selectedIndex)
    {
        for (int i = 0; i < giftUI.Count; i++)
        {
            if (i == selectedIndex)
            {
                HighlightItem(i);
            }
            else
            {
                UnHighlightItem(i);
            }
        }
    }

    public void HighlightItem(int index)
    {
        giftUI[index].GetComponent<Image>().color = new Color(giftUI[index].GetComponent<Image>().color.r, giftUI[index].GetComponent<Image>().color.b, giftUI[index].GetComponent<Image>().color.g, 1);
    }
    
    public void UnHighlightItem(int index)
    {
        giftUI[index].GetComponent<Image>().color = new Color(giftUI[index].GetComponent<Image>().color.r, giftUI[index].GetComponent<Image>().color.b, giftUI[index].GetComponent<Image>().color.g, 0);
    }
}
