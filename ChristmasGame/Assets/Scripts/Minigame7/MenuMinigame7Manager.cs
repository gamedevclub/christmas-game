using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MenuMinigame7Manager : MonoBehaviour
{
    public static MenuMinigame7Manager Instance {get; set;}
    public GameObject chooseGiftPanel, ingameUI;
    void Awake()
    {
        Instance = this;
        Minigame7Manager.OnMinigame7StateChanged += Minigame7ManagerOnOnMinigame7ManagerStateChanged; 
    }
    void OnDestroy()
    {
        Minigame7Manager.OnMinigame7StateChanged -= Minigame7ManagerOnOnMinigame7ManagerStateChanged; 
    }
    private void Minigame7ManagerOnOnMinigame7ManagerStateChanged(Minigame7State state)
    {
        // print("Invoke : " + state.ToString());
        chooseGiftPanel.SetActive(state == Minigame7State.ChooseGift);
        ingameUI.SetActive(state == Minigame7State.Delivery);
        switch (state)
        {
            case Minigame7State.ChooseGift:
                chooseGiftPanel.GetComponent<ChooseGift7>().Setup();
                break;
            case Minigame7State.Delivery:
                ingameUI.GetComponent<IngameUI7>().SetUp();
                break;
            case Minigame7State.Lobby:
                break;
            case Minigame7State.GameOver:
                break;
            case Minigame7State.Win:
                return;
                // break;
            case Minigame7State.Teleport:
                break;
            case Minigame7State.NextLevel:
                break;
            default:
                Debug.LogError("Invalid Minigame 7 State !");
                break;
        }
    }
}
