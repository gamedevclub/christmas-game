using System.Net;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoosterShoot7 : MonoBehaviour
{
    public GameObject bulletPrefab;
    public float timeBtwShootValue;
    private float timeBtwShoot;
    private GameObject target;
    private Rooster7 rooster;
    public Transform firePoint;
    public float shootSpeed;
    // Start is called before the first frame update
    void Start()
    {
        target = FindObjectOfType<PlayerMovement7>().gameObject;
        rooster = GetComponent<Rooster7>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Minigame7Manager.Instance.state != Minigame7State.Delivery)
        {
            return;
        }
        if (rooster.state == RoosterState.Attack)
        {
            Shoot();
        }
    }

    void Shoot()
    {
        if (timeBtwShoot > 0)
        {
            timeBtwShoot -= Time.deltaTime;
        }
        else
        {
            AudioManager7.Instance.Play("Rooster Egg", false);
            GameObject bullet = Instantiate(bulletPrefab, firePoint.position, Quaternion.identity);
            Vector2 direction = target.transform.position - firePoint.position;
            bullet.GetComponent<Rigidbody2D>().velocity = direction.normalized * shootSpeed;
            timeBtwShoot = timeBtwShootValue;
        }
    }
}
