using UnityEngine.Audio;
using UnityEngine;
using System.Collections.Generic;

public class AudioManager7 : MonoBehaviour
{
    public List<Sound7> sounds = new List<Sound7>();
    public static AudioManager7 Instance { get; set; }
    public AudioMixerGroup volume;
    // Start is called before the first frame update
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        foreach (Sound7 s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
            s.source.outputAudioMixerGroup = volume;
            
        }
    }
    void Start()
    {
        Play("Theme Minigame 7", false);
    }
    public void Play(string name, bool isDelay)
    {
        if (isDelay)
        {

            Sound7 sound = sounds.Find(s => s.name == name);
            if (sound != null && sound.source.isPlaying == false)
            {
                sound.source.Play();
            }
            if (sound == null)
            {
                Debug.LogWarning("Sound : " + name + " not found !");
            }
        }
        else
        {

            Sound7 sound = sounds.Find(s => s.name == name);
            if (sound != null)
            {
                sound.source.Play();
            }
            if (sound == null)
            {
                Debug.LogWarning("Sound : " + name + " not found !");
            }
        }
    }

    public void Stop(string name)
    {
        Sound7 sound = sounds.Find(s => s.name == name);
        if (sound != null && sound.source.isPlaying == true)
        {
            sound.source.Stop();
        }
    }

    public void StopAll()
    {
        foreach (Sound7 s in sounds)
        {
            Stop(s.name);
        }
    }
}
