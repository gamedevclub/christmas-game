using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol7 : MonoBehaviour
{
    public Enemy7 typeOfEnemy;
    private Rooster7 rooster;
    private Dog7 dog;
    private Cat7 cat;
    private Bat7 bat;
    public float waitTimeValue;
    public float minMoveTimeValue;
    public float maxMoveTimeValue;
    public float minX;
    public float maxX;
    public float minY;
    public float maxY;
    public Vector2 direction;
    float waitTime;
    float moveTime;
    Vector2[] directions = { Vector2.left, Vector2.down, Vector2.right, Vector2.up };
    void Awake()
    {
        switch (typeOfEnemy)
        {
            case Enemy7.Rooster:
                rooster = GetComponentInParent<Rooster7>();
                break;
            case Enemy7.Dog:
                dog = GetComponentInParent<Dog7>();
                break;
            case Enemy7.Cat:
                cat = GetComponentInParent<Cat7>();
                break;
            case Enemy7.Bat:
                bat = GetComponentInParent<Bat7>();
                break;
            default:
                break;
        }
    }

    public void Start()
    {
        waitTime = waitTimeValue;
        moveTime = Random.Range(minMoveTimeValue, maxMoveTimeValue);
        direction = directions[Random.Range(0, directions.Length)];
    }
    public void DoPatrol(float speed)
    {
        if (moveTime > 0)
        {
            moveTime -= Time.deltaTime;
            GetComponent<Move7>().Move(direction, speed);
        }
        else
        {
            if (waitTime > 0)
            {
                waitTime -= Time.deltaTime;
                GetComponent<Move7>().Move(direction, 0);
            }
            else
            {
                direction = directions[Random.Range(0, directions.Length)];
                waitTime = waitTimeValue;
                moveTime = Random.Range(minMoveTimeValue, maxMoveTimeValue);
            }
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        switch (typeOfEnemy)
        {
            case Enemy7.Rooster:
                rooster.ChangeState(RoosterState.Patrol);
                break;
            case Enemy7.Dog:
                dog.ChangeState(DogState.Patrol);
                break;
            case Enemy7.Cat:
                cat.ChangeState(CatState.Patrol);
                break;
            case Enemy7.Bat:
                break;
            default:
                break;
        }
        direction = directions[Random.Range(0, directions.Length)];
        waitTime = waitTimeValue;
        moveTime = Random.Range(minMoveTimeValue, maxMoveTimeValue);
    }

    void OnCollisionStay2D(Collision2D collision)
    {
        direction = directions[Random.Range(0, directions.Length)];
        waitTime = waitTimeValue;
        moveTime = Random.Range(minMoveTimeValue, maxMoveTimeValue);
    }
}
