using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner7 : MonoBehaviour
{
    public static ObjectSpawner7 Instance {get; set;}
    public GameObject[] obstacles;
    public int numberOfObstacle;
    private int amount;
    public float radius;
    public float minX;
    public float maxX;
    public float minY;
    public float maxY;
    void Awake()
    {
        Instance = this;
    }
    public void SpawnWalls()
    {
    }
    public void SpawnObjects()
    {
        amount = 0;

        while (amount < numberOfObstacle)
        {
            Vector2 spawnPos = new Vector2(Random.Range(minX, maxX), Random.Range(minY, maxY));
            //Check collisions
            if (DetectCollisions(spawnPos))
                continue;

            GameObject selectedObject = obstacles[Random.Range(0, obstacles.Length)];
            Instantiate(selectedObject, spawnPos, Quaternion.identity, transform);
            amount++;
        }
    }

    private bool DetectCollisions(Vector2 pos) 
    {        
        Collider2D[] hitColliders = Physics2D.OverlapCircleAll(pos, radius);
        foreach(Collider2D col in hitColliders)
        {
            if (col.CompareTag("obstacle 7") || col.CompareTag("player 7"))
            {
                return true;
            }
        }
        return false;
    }
}
