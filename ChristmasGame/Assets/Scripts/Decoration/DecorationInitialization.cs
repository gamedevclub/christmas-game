using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// * Implement the initialization of the decoration system such as generate current-decorated item,...
/// * Run once when the scene is activated
/// </summary>
public class DecorationInitialization : MonoBehaviour
{
    private static DecorationInitialization instance;
    public Transform contentSlot;
    // * Template for decoration item to be spawned multiple time
    private GameObject itemDecorationTemplate;
    public static DecorationInitialization Instance { get => instance; set => instance = value; }

    private void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        Load();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Load()
    {
        itemDecorationTemplate = DecorationManager.Instance.itemDecorationTemplate;
        InitializeItem();
        DecorationManager.Instance.OnDeactiveDecorationSlots();
    }
    /// <summary>
    /// * InitializaItem Method.
    /// TODO: Assign the decoration slot corresponding to its position in content slot (id, state, type).
    /// TODO: Spawn and show the item due to the saved data.
    /// ! No bugs found currently.
    /// </summary>
    public void InitializeItem()
    {
        // print("DecorationInitialization::InitializeItem() Called");
        DecorationManager.Instance.slotObjects = new List<GameObject>();

        // TODO: Add the slot in Hierachy to the slotsObject list in Decoration Manager
        foreach (Transform child in contentSlot)
        {
            DecorationManager.Instance.slotObjects.Add(child.gameObject);
        }
        // TODO: Set all values as DEFAULT in slot component of all elements in slotObjects list in Decoration Manager
        int indexSlot = 0;
        foreach (GameObject slot in DecorationManager.Instance.slotObjects)
        {
            slot.GetComponent<Slot>().slotId = indexSlot; 
            slot.GetComponent<Slot>().type = TypeSlot.Tree;
            slot.GetComponent<Slot>().state = StateSlot.Empty;
            slot.GetComponent<Slot>().decoration = null;
            indexSlot++;
        }

        // TODO: Spawn items onto slots respectively due to the data in PlayerData
        // TODO: Set the state of the slots containing items and de-activate them
        for (int i = 0; i < DataManager.instance.IngameData.decorationListOnTree.Count; i++)
        {
            Decoration decoration = DataManager.instance.IngameData.decorationListOnTree[i];
            if (decoration.isNull())
            {
                // print("NOT FOUND DECORATION ON TREE");
                // TODO: If the decoration is in inventory => set the tree slot at this index to Empty
                DecorationManager.Instance.slotObjects[i].GetComponent<Slot>().state = StateSlot.Empty;
                DecorationManager.Instance.slotObjects[i].GetComponent<Slot>().HideSlotDecoration();
            }
            else 
            {
                // print("FOUND DECORATION ON TREE");
                // TODO: If the decoration is in tree => Put it on tree at the position "decoration.slot"
                DecorationManager.Instance.slotObjects[i].GetComponent<Slot>().SetItem(decoration);
                DecorationManager.Instance.slotObjects[i].GetComponent<Slot>().ShowInDecoration();
                DecorationManager.Instance.slotObjects[i].GetComponent<Slot>().HideSlotDecoration();

                InventoryInitialization.Instance.Load();
            }
        }
    }
}
