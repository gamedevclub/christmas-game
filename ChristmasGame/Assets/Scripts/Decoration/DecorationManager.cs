using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// * Implement the Manager of the decoration system
/// * Containing main variables, lists,... needed to run the system
/// * Run all the time since the scene was activated
/// </summary>
public class DecorationManager : MonoBehaviour
{
    private static DecorationManager instance;

    public static DecorationManager Instance { get => instance; set => instance = value; }

    public GameObject itemDecorationTemplate;
    public GameObject EffectAItem;
    public GameObject EffectSItem;
    public Material[] MaterialDecorationItems;

    // * Containing all slot objects in the hiarachy when playing
    public List<GameObject> slotObjects;

    private void Awake()
    {
        Instance = this;
    }

    /// <summary>
    /// * Activate the decoration slots Method
    /// TODO: Show the decoration slot
    /// ! No bugs found currently
    /// </summary>
    public void OnActivateDecorationSlots()
    {
        // print("DecorationManager::OnActivateDecorationSlots() Called");
        // TODO: Activate all slots
        foreach (GameObject slot in slotObjects)
        {
            if (slot.GetComponent<Slot>().state == StateSlot.Taken)
                continue;
            slot.GetComponent<Slot>().ShowSlotDecoration();
        }
    }

    /// <summary>
    /// * De-activate the decoration slots Method
    /// TODO: Hide the decoration slot
    /// ! No bugs found currently
    /// </summary>
    public void OnDeactiveDecorationSlots()
    {
        // print("DecorationManager::OnDeactiveDecorationSlots() Called");
        // TODO: De-activate all slots
        foreach (GameObject slot in slotObjects)
        {
            slot.GetComponent<Slot>().HideSlotDecoration();
        }
    }
}
