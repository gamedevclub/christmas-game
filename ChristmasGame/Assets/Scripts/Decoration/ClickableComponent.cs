using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/// <summary>
/// * Clickable component class
/// * Used for mouse-handling to drag and drop object in gameplay layer
/// </summary>
public class ClickableComponent : MonoBehaviour
{
    private Vector2 mOffset;
    private GameObject item;
    private bool isBeginDrag;
    private bool isDragging;
    // Start is called before the first frame update
    void Start()
    {
        isBeginDrag = false;
        isDragging = false;
    }

    /// <summary>
    /// * On Mouse Down method
    /// * Automatically called when clicking mouse on anything
    /// TODO: Detect whether the player clicks an item or something irrelevant.
    /// ! No bug currently found
    /// </summary>
    void OnMouseDown()
    {
        item = GetComponent<Slot>().itemIconObject;
        if (GetComponent<Slot>().decoration != null)
        {
            DecorationManager.Instance.OnActivateDecorationSlots();
            GetComponent<Slot>().ShowSlotDecoration();
            isBeginDrag = true;
        }
        else
        {
            isBeginDrag = false;
        }
        item.GetComponent<SpriteRenderer>().sortingOrder = 10;
        mOffset = (Vector2)item.gameObject.transform.position - GetMouseWorldPoint();
    }

    /// <summary>
    /// * On Mouse Drag method
    /// * Automatically called when something is dragged by mouse
    /// * This is just actually called when something is detected by mouse in the first click
    /// ! No bug currently found
    /// </summary>
    void OnMouseDrag()
    {
        if (isBeginDrag == true)
        {
            isDragging = true;
            isBeginDrag = false;
        }
        item.transform.position = GetMouseWorldPoint() - mOffset;
        InventoryButtons.Instance.CloseInventory();
    }

    /// <summary>
    /// * Get Mouse World Point Method
    /// TODO: Get the world space position of the mouse
    /// ! No bug currently found
    /// </summary>
    /// <returns></returns>
    Vector2 GetMouseWorldPoint()
    {
        return Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    /// <summary>
    /// * On Mouse Up method
    /// * Automatically called when the mouse button is up in the GAMEPLAY LAYER
    /// TODO: Detect whether the the item is dropped on slot, on another item or on empty space
    /// TODO: If the item is dropped on an empty slot => Put it onto the slot
    /// TODO: If the item is dropped on another item on slot => Swap them
    /// TODO: If the item is dropped on empty space => Put it back to inventory 
    /// ! No bug currently found
    /// </summary>
    void OnMouseUp()
    {
        if (isDragging == false)
        {
            return;
        }
        DecorationManager.Instance.OnDeactiveDecorationSlots();
        Collider2D[] hits = Physics2D.OverlapPointAll(item.transform.position);
        foreach (Collider2D hit in hits)
        {
            Slot droppedSlot = hit.gameObject.GetComponent<Slot>();
            if (droppedSlot != null && droppedSlot != GetComponent<Slot>())
            {
                if (droppedSlot.type == TypeSlot.Tree)
                {
                    if (droppedSlot.decoration != null)
                    {
                        SwapItemOnSlotDecoration(GetComponent<Slot>(), droppedSlot);
                        item.GetComponent<SpriteRenderer>().sortingOrder = 3;
                        return;
                    }
                    else
                    {
                        PutItemOnSlotDecoration(GetComponent<Slot>(), droppedSlot);
                        item.GetComponent<SpriteRenderer>().sortingOrder = 3;
                        return;
                    }
                }
            }
            if (droppedSlot == GetComponent<Slot>())
            {
                transform.GetChild(0).transform.localPosition = Vector2.zero;
                DataManager.instance.SaveDeco();
                return;
            }
        }

        PutItemOnSlotInventory(GetComponent<Slot>());
        item.GetComponent<SpriteRenderer>().sortingOrder = 3;
        return;
    }

    /// <summary>
    /// * Put item on slot decoration
    /// TODO: Put the dragged item onto empty slot - Of course this action takes place within the tree
    /// ! No bug currently found
    /// </summary>
    /// <param name="_slotPut"></param>
    /// <param name="_slotToBePut"></param>
    void PutItemOnSlotDecoration(Slot _slotPut, Slot _slotToBePut)
    {
        try
        {
            print("PutItemOnSlotDecoration");
            Decoration _itemPut = _slotPut.decoration;

            _slotToBePut.SetItem(_itemPut);
            _slotToBePut.decoration.type = 1;


            _slotPut.ClearItem();
            _slotPut.ShowInDecoration();
            _slotToBePut.ShowInDecoration();

            DataManager.instance.IngameData.decorationListOnTree[_slotPut.slotId] = new Decoration();
            DataManager.instance.IngameData.decorationListOnTree[_slotToBePut.slotId] = _itemPut;

            DataManager.instance.SaveDeco();
        }
        catch
        {
            Debug.Log("Dump try catch super algorithm :)))");
        }

    }

    /// <summary>
    /// * Swap Item method - Used within the tree
    /// TODO: Swap items within the decoration tree
    /// ! No bug currently found
    /// </summary>
    /// <param name="_slotSwapped"></param>
    /// <param name="_slotToBeSwapped"></param>
    void SwapItemOnSlotDecoration(Slot _slotSwapped, Slot _slotToBeSwapped)
    {
        try
        {
            Decoration _itemSwapped = _slotSwapped.decoration;
            Decoration _itemToBeSwapped = _slotToBeSwapped.decoration;

            int t_itemSwappedSlot = _itemSwapped.slot;
            int t_itemToBeSwappedSlot = _itemToBeSwapped.slot;

            _slotSwapped.SetItem(_itemToBeSwapped);
            _slotToBeSwapped.SetItem(_itemSwapped);

            DataManager.instance.IngameData.decorationListOnTree[_slotSwapped.slotId] = _itemToBeSwapped;
            DataManager.instance.IngameData.decorationListOnTree[_slotSwapped.slotId].slot = _slotSwapped.slotId;

            DataManager.instance.IngameData.decorationListOnTree[_slotToBeSwapped.slotId] = _itemSwapped;
            DataManager.instance.IngameData.decorationListOnTree[_slotToBeSwapped.slotId].slot = _slotToBeSwapped.slotId;

            _slotSwapped.ShowInDecoration();
            _slotToBeSwapped.ShowInDecoration();

            DataManager.instance.SaveDeco();
        }
        catch
        {
            Debug.Log("Dump try catch super algorithm :)))");
        }

    }

    /// <summary>
    /// * Put Item on slot inventory method
    /// TODO: Used to put the item back to inventory when it is dropped in empty space
    /// ! No bug currently found
    /// </summary>
    /// <param name="_slotPut"></param>
    public void PutItemOnSlotInventory(Slot _slotPut)
    {
        try
        {
            print("PutItemOnSlotInventory");
            Decoration item = _slotPut.decoration;
            for (int i = 0; i < DataManager.instance.IngameData.decorationList.Count; i++)
            {
                if (DataManager.instance.IngameData.decorationList[i].type == 1)
                {
                    item.type = 0;
                    item.slot = i;
                    DataManager.instance.IngameData.decorationList[i] = item;
                    InventoryManager.Instance.slotObjects[i].GetComponent<Slot>().SetItem(item);

                    // print(_slotPut.slotId);
                    // DataManager.instance.IngameData.decorationListOnTree[_slotPut.slotId].slot = -1;
                    DataManager.instance.IngameData.decorationListOnTree[_slotPut.slotId] = new Decoration();
                    _slotPut.ClearItem();
                    _slotPut.ShowInDecoration();
                    break;
                }
            }
            DataManager.instance.SaveDeco();
        }
        catch
        {
            Debug.Log("Dump try catch super algorithm :)))");
        }

    }
}
