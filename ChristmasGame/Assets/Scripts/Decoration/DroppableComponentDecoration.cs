using UnityEngine;

/// <summary>
/// * Implement the droppable component for the decoration slot on tree
/// * It is necessary to add this component to each decoration slot on tree
/// * Run when player drop item onto the decoration slot
/// </summary>
public class DroppableComponentDecoration : MonoBehaviour
{
    /// <summary>
    /// TODO : Implement the Drop method when player drop item on the decoration slot
    /// ! No bug found currently
    /// </summary>
    /// <param name="_item"></param>
    public void OnDropOnDecorationSlot(GameObject _slot)
    {
        // print("DroppableComponentDecoration::OnDropOnDecorationSlot(GameObject) Called");
        // if (_slot.GetComponent<Slot>().state == StateSlot.Empty)
        //     return;
        // * Catch any errors while implementing
        try
        {
            // Uncomment to see the log
            // Get the index of the dropped item
            int indexItem = _slot.GetComponent<Slot>().slotId;
            // Debug.Log(string.Format("id item : {0}", indexItem));

            // InventoryInitialization.Instance.PrintItemId();
            // TODO: Assign the icon value of item object in this slot object
            gameObject.GetComponent<Slot>().SetItem(_slot.GetComponent<Slot>().decoration);
            gameObject.GetComponent<Slot>().ShowInDecoration();

            // TODO: Change the state of the slot
            DecorationManager.Instance.slotObjects[GetComponent<Slot>().slotId].GetComponent<Slot>().state = StateSlot.Taken;
            // print(string.Format("Size : {0}",DataManager.instance.IngameData.decorationList.Count));
            DataManager.instance.IngameData.decorationList[indexItem].type = 1;

            DataManager.instance.IngameData.decorationListOnTree[GetComponent<Slot>().slotId] = _slot.GetComponent<Slot>().decoration; 
            DataManager.instance.IngameData.decorationListOnTree[GetComponent<Slot>().slotId].slot = GetComponent<Slot>().slotId; 
            DataManager.instance.IngameData.decorationListOnTree[GetComponent<Slot>().slotId].type = 1;

            // TODO: Remove item icon decoration from DemoData
            // FindObjectOfType<DemoData>().itemIconDecorationCorrespondToItemInventory.RemoveAt(indexItem);

            // TODO: Open the Inventory when finishing decoration
            InventoryButtons.Instance.OpenInventory();

            // TODO: Save
            DataManager.instance.SaveDeco();
        }
        catch
        {
            Debug.LogError("Lỗi trong đây nè đm fix lẹ coi : DroppableComponentDecoration.cs::OnDropOnDecorationSlot(GameObject _item)");
        }
    }

    /// <summary>
    /// * On Swap between decoration and inventory method
    /// * Called when the item on decoration tree is dragged and dropped onto an inventory slot
    /// * Which means we need to swap them from inventory slot to decoration slot and vice versa
    /// TODO: Swap the item between slot decoration and slot inventory
    /// ! No bug currently found
    /// </summary>
    /// <param name="_slot"></param>
    public void OnSwapBetweenDecorationAndInventory(GameObject _slot)
    {
        GetComponent<ClickableComponent>().PutItemOnSlotInventory(GetComponent<Slot>());
        OnDropOnDecorationSlot(_slot);
        InventoryButtons.Instance.OpenInventory();

        // DataManager.instance.SaveDeco();
    }
}
