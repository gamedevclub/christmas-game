### README FOR MERGING PROCESS

## Save and Load locations:

# Decoration system
-> Method used for putting decoration item to another empty slot on tree
ClickableComponent.cs, line 147

-> Method used for swapping decoration items with each other in a tree
ClickableComponent.cs, line 177

-> Method used for putting decoration item back to inventory
ClickableComponent.cs, line 221

-> Method used for putting item from inventory to tree
DroppableComponentDecoration.cs, line 46

-> Method used for swapping item in inventory with item on tree
DroppableComponentDecoration.cs, line 79
