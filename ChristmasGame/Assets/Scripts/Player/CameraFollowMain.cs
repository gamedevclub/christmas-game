using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowMain : MonoBehaviour
{
    public GameObject player;
    [Range(0, 1)] public float moveSmoothSpeed = 0.125f;
    [Range(0, 1)] public float zoomSmoothSpeed = 0.125f;
    public float zoomMove;
    public float zoomIdle;
    public static bool zoomIn = false;
    private Vector3 offset;
    private Camera cam;
    public Transform minPosition;
    public Transform maxPosition;

    void Start()
    {
        cam = Camera.main;
        offset = transform.position;
        offset.x = 0;
        offset.y = 0;
        transform.position = player.transform.position;
    }


    void FixedUpdate()
    {
        if (!PlayerMove.isBlock)
        {
            float height = Camera.main.orthographicSize * 2;
            float width = height * Camera.main.aspect;
            Vector3 targetVector = offset + player.transform.position;
            Vector3 smoothVector = Vector3.Lerp(transform.position, targetVector, moveSmoothSpeed);
            smoothVector.x = Mathf.Clamp(smoothVector.x, minPosition.position.x + width / 2, maxPosition.position.x - width / 2);
            smoothVector.y = Mathf.Clamp(smoothVector.y, minPosition.position.y + height / 2, maxPosition.position.y - height / 2);
            transform.position = smoothVector;
        }

        if (zoomIn)
        {
            cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, zoomIdle, zoomSmoothSpeed);
        }
        else
        {
            cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, zoomMove, zoomSmoothSpeed);
        }
    }
}
