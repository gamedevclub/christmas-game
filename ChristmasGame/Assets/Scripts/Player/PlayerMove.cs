using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlayerMove : MonoBehaviour
{
    public static bool isBlock;
    public float speed;
    public GameObject teleEffect;
    public GameObject map;
    public float teleSpeed;
    public float playerHeight;
    public GameObject mask;
    public Button openMapBtn;
    Animator anim;
    Rigidbody2D rb;
    Vector2 direction;
    Collider2D collider2d;
    bool isTeleDown;
    bool isTeleUp;
    float teleTime;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        collider2d = GetComponent<Collider2D>();
        teleTime = playerHeight / teleSpeed;
    }

    private void Update()
    {
        if (!isBlock)
        {
            float vertical = Input.GetAxis("Vertical");
            float horizontal = Input.GetAxis("Horizontal");

            direction.x = horizontal;
            direction.y = vertical;
            if (direction.magnitude >= 1)
                direction.Normalize();
        }
        else
        {
            direction = Vector2.zero;
        }

        //CameraFollowMain.zoomOut = direction.magnitude >= 0.01f;
    }

    private void FixedUpdate()
    {
        float angular = 0;
        if (isTeleDown)
        {
            rb.velocity = Vector2.down * teleSpeed;

        }
        else if (isTeleUp)
        {
            rb.velocity = Vector2.up * teleSpeed;
        }
        else
        {
            rb.velocity = direction * speed;
            if (direction.magnitude <= 0.1f)
                angular = 0;
            else
                angular = (Mathf.Atan2(direction.y, direction.x) + Mathf.PI) / (Mathf.PI * 2);
        }


        anim.SetFloat("Blend", angular);
    }

    public void OpenMap()
    {
        isBlock = true;
        map.SetActive(true);
    }

    public void CloseMap()
    {
        isBlock = false;
        map.SetActive(false);
    }

    public void Tele(int index)
    {
        StartCoroutine(TeleHandler(index));
    }

    IEnumerator TeleHandler(int index)
    {
        // Wait until the end of the button's animation
        yield return new WaitForSeconds(0.5f);

        isBlock = true;

        map.SetActive(false);

        collider2d.enabled = false;
        // Todo: Lặn
        GameObject effect;
        openMapBtn.interactable = false;

        effect = Instantiate(teleEffect, transform.position, Quaternion.identity);

        yield return new WaitForSeconds(0.1f);

        isTeleUp = false;
        isTeleDown = true;
        yield return new WaitForSeconds(teleTime);
        mask.SetActive(true);
        Destroy(effect);

        // Todo: Lên
        isTeleDown = false;

        transform.position = GameManager.instance.mapPositions[index].transform.position;
        Camera.main.transform.position = transform.position - Vector3.forward * 10;
        effect = Instantiate(teleEffect, transform.position, Quaternion.identity);
        mask.SetActive(false);

        isTeleUp = true;
        transform.position += Vector3.down * playerHeight;


        // Todo: End
        yield return new WaitForSeconds(teleTime);
        Destroy(effect);
        isTeleUp = false;
        mask.SetActive(false);

        openMapBtn.interactable = true;
        collider2d.enabled = true;
        isBlock = false;
    }

    IEnumerator DelayToEndButtonAnimation()
    {
        yield return Helper7.GetWait(1f);
    }

    public void Click()
    {
        isBlock = true;
        AudioManager.instance.ButtonClick();
    }

    public void CancleBlockPlayer()
    {
        isBlock = false;
    }
}
