using System.Net.Mime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerTrigger : MonoBehaviour
{
    public GameObject panel;
    public Text doorInfo;
    int doorIndex;
    bool triggerDoor = false;

    private void Start()
    {
        panel.SetActive(false);
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "door")
        {
            CameraFollowMain.zoomIn = true;
            doorIndex = other.gameObject.GetComponent<Door>().index;
            triggerDoor = true;
            panel.SetActive(true);
            if (doorIndex == 0)
                doorInfo.text = "Nhấn space để vào nhà";
            else
                doorInfo.text = "Nhấn space để vào game : " + doorIndex;
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "door")
        {
            CameraFollowMain.zoomIn = false;
            triggerDoor = false;
            panel.SetActive(false);
        }
    }

    private void Update()
    {
        if (triggerDoor && !PlayerMove.isBlock)
        {
            if (Input.GetKeyDown("space"))
            {
                Debug.Log(doorIndex);
                if (doorIndex == 0)
                    GameManager.instance.ReturnHome();
                else
                    GameManager.instance.LoadMinigame(doorIndex);
            }
        }
    }
}
