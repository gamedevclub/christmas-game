using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : MonoBehaviour
{
    public Dialogue dialogue;
    string nextButton;
    float time;
    bool isActive;
    GameObject message;
    // bool show = true;
    private void Start()
    {
        time = Time.time;
        nextButton = DialogueManager.nextButton;
        message = this.gameObject.transform.GetChild(0).gameObject;
        message.SetActive(false);
    }
    private void Update()
    {
        if (Input.GetKey(nextButton) && isActive)
            if (Time.time > time + 0.5)
            {
                message.SetActive(false);
                DialogueManager.instance.StartDialogue(this.dialogue);
                time = Time.time;
                isActive = false;
            }

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            isActive = true;
            message.SetActive(true);
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            isActive = false;
            message.SetActive(false);
        }
    }

}
