using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DialogueManager : MonoBehaviour
{

    public static DialogueManager instance;
    public GameObject chestPanel;
    public Queue<string> names;
    public Queue<string> sentences;
    public static string nextButton = "space";
    public float typeDelay;
    public float typeSpeed;
    public GameObject dialogueBox;
    public Text Name;
    public Text description;
    public List<GameObject> tutorials;
    public bool tutorial;
    public GameObject tutorialPanel;
    bool open = false;
    bool delay = true;
    int index;
    Coroutine typing = null;

    // Start is called before the first frame update
    void Start()
    {
        if (!instance)
            instance = this;
        sentences = new Queue<string>();
        names = new Queue<string>();
        dialogueBox.SetActive(false);

        // test------
        // DataManager.instance.IngameData.Name = "Người Tuyết";

    }

    public void StartDialogue(Dialogue dialogue)
    {
        index = dialogue.index;
        CameraFollowMain.zoomIn = true;
        PlayerMove.isBlock = true;
        dialogueBox.SetActive(true);
        sentences.Clear();
        foreach (string name in dialogue.names)
        {
            names.Enqueue(name);
        }
        foreach (string sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }

        open = true;
        DisplayNextSentence();
    }

    private void Update()
    {
        if (open)
        {
            if (Input.anyKeyDown && !delay && !tutorial)
            {
                DisplayNextSentence();
            }
        }
    }

    public void DisplayNextSentence()
    {
        delay = true;
        if (sentences.Count == 0)
        {
            CameraFollowMain.zoomIn = false;
            PlayerMove.isBlock = false;
            dialogueBox.SetActive(false);
            open = false;
            return;
        }
        string name = names.Dequeue();
        if (name == "player")
            Name.text = DataManager.instance.IngameData.Name;
        else Name.text = name;

        string sentence = sentences.Dequeue();

        if (typing != null)
            StopCoroutine(typing);
        typing = StartCoroutine(TypeSentence(sentence));
        //StartCoroutine(Delay());
    }

    IEnumerator TypeSentence(string sentence)
    {
        description.text = "";

        // foreach (char letter in sentence.ToCharArray())
        // {
        //     description.text += letter;
        //     yield return new WaitForSeconds(typeDelay);
        // }

        float t = 0;
        int charIndex = 0;
        while (charIndex < sentence.Length)
        {
            t += Time.fixedDeltaTime * typeSpeed;
            charIndex = Mathf.FloorToInt(t);
            charIndex = Mathf.Clamp(charIndex, 0, sentence.Length);

            description.text = sentence.Substring(0, charIndex);

            yield return null;
        }

        StartCoroutine(Delay());
    }
    IEnumerator Delay()
    {
        yield return new WaitForSeconds(typeDelay);
        delay = false;
    }

    public void ShowTutorial()
    {
        tutorialPanel.SetActive(true);
        tutorial = true;
        tutorials[index - 1].SetActive(true);
    }
    public void OffTutorial()
    {
        tutorial = false;
        tutorials[index - 1].GetComponentInChildren<Scrollbar>().value = 1;
    }

    public void StopDialoque()
    {
        if (typing != null)
            StopCoroutine(typing);

        dialogueBox.SetActive(false);
        names.Clear();
        sentences.Clear();

        delay = false;
        open = false;
        PlayerMove.isBlock = false;
        CameraFollowMain.zoomIn = false;
    }
}
