using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenPanel : MonoBehaviour
{
    public GameObject shopPanel;
    public GameObject petPanel;
    public GameObject upgradePanel;
    public GameObject mergePanel;

    public void OpenShop()
    {
        shopPanel.SetActive(true);
        ShopUIManager.instance.Load();
    }
    public void OpenPet()
    {
        petPanel.SetActive(true);
        PetManager.instance.On();
    }
    public void OpenUpgrade()
    {
        upgradePanel.SetActive(true);
        UpgradeManager.instance.Load();
    }
    public void OpenMerge()
    {
        mergePanel.SetActive(true);
    }
}
