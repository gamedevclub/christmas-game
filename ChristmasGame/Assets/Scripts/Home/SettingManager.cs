using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SettingManager : MonoBehaviour
{

    public GameObject setting;
    private Canvas settingCanvas;
    string currentSceneName;
    void Start()
    {
        settingCanvas = transform.GetChild(0).GetComponent<Canvas>();
        currentSceneName = SceneManager.GetActiveScene().name;
        DontDestroyOnLoad(this);
        settingCanvas.renderMode = RenderMode.ScreenSpaceCamera;
        settingCanvas.worldCamera = Camera.main;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Turn();
        }
        if (currentSceneName != SceneManager.GetActiveScene().name)
        {
            settingCanvas.renderMode = RenderMode.ScreenSpaceCamera;
            Camera.main.farClipPlane = 1000;
            settingCanvas.worldCamera = Camera.main;
            settingCanvas.sortingOrder = 100;
            currentSceneName = SceneManager.GetActiveScene().name;
        }
    }

    public void Turn()
    {
        if (setting.activeInHierarchy)
        {
            setting.SetActive(false);
        }
        else
        {
            setting.SetActive(true);
        }
    }

    public void Quit()
    {
        Application.Quit();
        Debug.Log("Quit");
    }
}
