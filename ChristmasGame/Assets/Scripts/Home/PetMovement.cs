using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetMovement : MonoBehaviour
{
    public float speed;
    public int index;
    public List<int> childOffset;
    Rigidbody2D rb;
    Vector2 velocity;
    Coroutine move;
    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        move = StartCoroutine(MoveAround());
    }
    void Update()
    {
        // Move range : 1 -> -3.5
        // Tree : -2.5 -> order = 0
        // -3.5 -> order = 10
        // order = ay + b
        // => order = -10x -25
        int order = -25 - (int)((transform.position.y) * 10);

        gameObject.GetComponent<SpriteRenderer>().sortingOrder = order;

        if (gameObject.transform.childCount != 0)
        {
            GameObject child1 = gameObject.transform.GetChild(0).gameObject;
            child1.GetComponent<ParticleSystemRenderer>().sortingOrder = order + childOffset[0];

            var child2 = child1.GetComponentsInChildren<ParticleSystemRenderer>();

            for (int i = 0; i < child2.Length; i++)
                child2[i].sortingOrder = order + childOffset[1 + i];
        }

    }

    IEnumerator MoveAround()
    {
        float x = Random.Range(-1f, 1f);
        float y = Random.Range(-1f, 1f);

        if (PetGenerate.instance != null)
        {
            float ratio = PetGenerate.instance.petRatio;

            int rand = Random.Range(0, 101);

            int rand2 = Mathf.RoundToInt((PetGenerate.instance.xMax - Mathf.Abs(transform.localPosition.x)) / PetGenerate.instance.xMax * 100);

            if (rand + rand2 > 30)
            {
                if (ratio > 0.7f && transform.localPosition.x > 0)
                {
                    //Debug.Log("Move to left : " + transform.position);
                    x = Mathf.Clamp(x, -1, -0.5f);
                }
                else if (ratio < 0.3f && transform.localPosition.x < 0)
                {
                    //Debug.Log("Move to right : " + transform.position);
                    x = Mathf.Clamp(x, 0.5f, 1);
                }
                else
                {
                    //Debug.Log("Move random : " + transform.position);
                }
            }

        }


        velocity.x = x;
        velocity.y = y;
        rb.velocity = velocity.normalized * speed;
        yield return new WaitForSeconds(Random.Range(3f, 8f));
        rb.velocity = Vector2.zero;
        yield return new WaitForSeconds(Random.Range(3f, 5f));

        move = StartCoroutine(MoveAround());
    }

    private void OnMouseDown()
    {
        PetGenerate.instance.indexSelect = index;
        Debug.Log("Click on pet index : " + index + " , " + transform.position);
    }

    private void OnMouseDrag()
    {
        if (PetGenerate.instance.indexSelect == index)
        {
            //Vector3 offset = transform.parent.parent.position;
            Vector3 mouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mouse.z = 0;
            mouse.x = Mathf.Clamp(mouse.x, PetGenerate.instance.xMin, PetGenerate.instance.xMax);
            mouse.y = Mathf.Clamp(mouse.y, PetGenerate.instance.yMin, PetGenerate.instance.yMax);
            transform.position = mouse;
        }

    }

    private void OnMouseUp()
    {
        PetGenerate.instance.indexSelect = -1;
    }

}
