using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetGenerate : MonoBehaviour
{
    public static PetGenerate instance;
    public float xMin;
    public float xMax;
    public float yMin;
    public float yMax;
    public float petRatio;
    public GameObject petPrefab;
    public List<GameObject> petList;
    public Transform petContainer;
    [Space(10)]
    [Header("Effect")]
    public GameObject effectSR;
    public GameObject effectSSR;
    public GameObject effectUR;

    [Space(10)]
    [Header("Collider")]
    public Transform xMinCollider;
    public Transform xMaxCollider;
    public Transform yMinCollider;
    public Transform yMaxCollider;

    [HideInInspector] public int indexSelect;

    private void Start()
    {
        if (instance == null)
            instance = this;

        foreach (var pet in DataManager.instance.IngameData.petList)
        {
            AddPet(pet);
        }
        StartCoroutine(StatisticPet());
        indexSelect = -1;

        float height = Camera.main.orthographicSize * 2;
        float width = height * Camera.main.aspect;

        yMaxCollider.position = new Vector3(yMaxCollider.position.x, 1f, yMaxCollider.position.z);
        yMinCollider.position = new Vector3(yMinCollider.position.x, -height / 2, yMinCollider.position.z);
        xMaxCollider.position = new Vector3(width / 2, xMaxCollider.position.y, xMaxCollider.position.z);
        xMinCollider.position = new Vector3(-width / 2, xMinCollider.position.y, xMinCollider.position.z);

        xMin = xMinCollider.position.x + 0.8f;
        xMax = xMaxCollider.position.x - 0.8f;
        yMin = yMinCollider.position.y + 1f;
        yMax = yMaxCollider.position.y - 0.5f;

    }

    public void RemovePet(int index)
    {
        var pet = petList[index];
        petList.RemoveAt(index);
        Destroy(pet);
    }

    public void AddPet(Pet pet)
    {
        GameObject newPet = Instantiate(petPrefab, petContainer.transform);

        int index = petList.Count;

        PetMovement petMovement = newPet.GetComponent<PetMovement>();
        petMovement.index = index;




        petList.Add(newPet);

        if (pet.level == 2) // * SR
        {
            GameObject effect = Instantiate(effectSR, newPet.transform);
            effect.transform.localPosition = new Vector3(-0.08f, 0.15f, 0);

            petMovement.childOffset.Clear();
            petMovement.childOffset.Add(effect.GetComponent<ParticleSystemRenderer>().sortingOrder);
            var childEffect = effect.GetComponentsInChildren<ParticleSystemRenderer>();
            for (int i = 0; childEffect != null && i < childEffect.Length; i++)
                petMovement.childOffset.Add(childEffect[i].sortingOrder);

        }
        else if (pet.level == 3) // * SSR
        {
            GameObject effect = Instantiate(effectSSR, newPet.transform);
            effect.transform.localPosition = new Vector3(-0.09f, -0.02f, 0);

            petMovement.childOffset.Clear();
            petMovement.childOffset.Add(effect.GetComponent<ParticleSystemRenderer>().sortingOrder);
            var childEffect = effect.GetComponentsInChildren<ParticleSystemRenderer>();
            for (int i = 0; childEffect != null && i < childEffect.Length; i++)
                petMovement.childOffset.Add(childEffect[i].sortingOrder);

        }
        else if (pet.level == 4) // * UR
        {
            GameObject effect = Instantiate(effectUR, newPet.transform);
            effect.transform.localPosition = new Vector3(-0.14f, -0.74f, 0);
            effect.transform.rotation = Quaternion.Euler(-113, 0, 0);

            petMovement.childOffset.Clear();
            petMovement.childOffset.Add(effect.GetComponent<ParticleSystemRenderer>().sortingOrder);
            var childEffect = effect.GetComponentsInChildren<ParticleSystemRenderer>();
            for (int i = 0; childEffect != null && i < childEffect.Length; i++)
                petMovement.childOffset.Add(childEffect[i].sortingOrder);
        }



        newPet.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(pet.art);
        int rand = Random.Range(0, 2);
        float x = 0;
        if (rand == 0)
            x = Random.Range(xMin, -3);
        else
            x = Random.Range(3, xMax);
        float y = Random.Range(yMin, yMax - 2f);

        newPet.transform.position = new Vector2(x, y);
    }

    IEnumerator StatisticPet()
    {
        float left = 0;
        float right = 0;

        if (petList.Count != 0)
        {
            foreach (GameObject pet in petList)
            {
                if (pet.transform.localPosition.x < petContainer.transform.localPosition.x)
                    left++;
                else
                    right++;
            }
            petRatio = right / (right + left);
        }

        //Debug.Log("Current pet ratio : 0." + Mathf.RoundToInt(petRatio * 100));

        yield return new WaitForSeconds(5f);

        StartCoroutine(StatisticPet());
    }


}
