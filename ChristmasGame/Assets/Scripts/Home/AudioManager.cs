using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;
    private AudioSource source;

    public AudioClip buttonClick;
    public AudioClip homeBackground;
    public AudioClip loginBackground;
    public AudioClip openChest;
    public AudioClip mainBackground;
    public AudioClip quest;
    public AudioClip buySell;
    public AudioClip mergeUpgrade;
    public AudioClip lostGame;
    public AudioClip winGame;
    public AudioClip gacha;

    // Start is called before the first frame update
    void Start()
    {
        source = GetComponent<AudioSource>();
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
    }

    public void LoginBackground()
    {
        source.clip = loginBackground;
        source.loop = true;
        source.Play();
    }

    public void HomeBackground()
    {
        source.clip = homeBackground;
        source.loop = true;
        source.Play();
    }

    public void ButtonClick()
    {
        source.PlayOneShot(buttonClick);
    }

    public void StopAudio()
    {
        source.Stop();
    }

    public void OpenChest()
    {
        source.PlayOneShot(openChest);
    }

    public void GetQuest()
    {
        source.PlayOneShot(quest);
    }
    public void MergeUpgrade()
    {
        source.PlayOneShot(mergeUpgrade);
    }

    public void BuySell()
    {
        source.PlayOneShot(buySell);
    }
    public void MainBackground()
    {
        source.clip = mainBackground;
        source.loop = true;
        source.Play();
    }

    public void WinGame()
    {
        source.PlayOneShot(winGame);
    }

    public void LostGame()
    {
        source.PlayOneShot(lostGame);
    }

    public void Gacha()
    {
        source.PlayOneShot(gacha);
    }
}
