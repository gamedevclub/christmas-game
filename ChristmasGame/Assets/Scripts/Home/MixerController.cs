using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class MixerController : MonoBehaviour
{
    [SerializeField]
    private AudioMixer mixer;

    private Slider slider;
    private void Start()
    {
        slider = GetComponent<Slider>();
        if (!PlayerPrefs.HasKey("Volume"))
        {
            PlayerPrefs.SetFloat("Volume", 0.5f);
        }
        else
        {
            slider.value = PlayerPrefs.GetFloat("Volume");
            mixer.SetFloat("MasterVolume", Mathf.Log10(slider.value) * 20);
        }
        transform.parent.gameObject.SetActive(false);
    }
    public void SetVolume()
    {
        mixer.SetFloat("MasterVolume", Mathf.Log10(slider.value) * 20);
        PlayerPrefs.SetFloat("Volume", slider.value);
    }
}
