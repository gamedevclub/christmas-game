using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HomeManager : MonoBehaviour
{
    public static HomeManager instance;
    public GameObject chestPanel;
    public GameObject leaveButton;
    public GameObject alertPanel;
    public Text alertMessage;
    void Start()
    {
        if (instance == null)
            instance = this;
        AudioManager.instance.HomeBackground();
    }

    public void ButtonClick()
    {
        AudioManager.instance.ButtonClick();
    }
}
