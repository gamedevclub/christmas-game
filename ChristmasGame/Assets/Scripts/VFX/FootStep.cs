using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootStep : MonoBehaviour
{
    public Transform player;
    public Vector3 offset;
    // Start is called before the first frame update
    void Start()
    {
        if (player == null)
        {
            Debug.LogWarning("You need to footstep on the scene and reference the main player to it");
            player = null;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (player == null) return;
        transform.position = player.position + offset;
    }
}
