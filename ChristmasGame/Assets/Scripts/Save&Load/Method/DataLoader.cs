using System.Text;
using System.Runtime.Versioning;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Xml;
using System.IO;

public class DataLoader : MonoBehaviour
{
    public static DataLoader instance;
    const int MaxId_Pet_N = 64;
    const int MaxId_Pet_R = 110;
    const int MaxId_Pet_SR = 206;
    const int MaxId_Pet_SSR = 307;
    const int MaxId_Pet_UR = 507;

    const int MaxId_Deco_B = 138;
    const int MaxId_Deco_A = 226;
    const int MaxId_Deco_S = 312;


    private void Awake()
    {
        instance = this;
    }

    public Pet GetPetRandom(int level)
    {
        int ID = GetRandomPet(level);

        return GetPet(ID);
    }

    public Decoration GetDecoRandom(int level)
    {
        int ID = GetRandomDeco(level);

        return GetDeco(ID);
    }

    public Pet GetPet(int ID)
    {
        XmlDocument petdata = new XmlDocument();
        TextAsset textAsset = (TextAsset)Resources.Load("XML/petdata");
        petdata.LoadXml(textAsset.text);

        return LoadPetInfo(petdata, ID);
    }

    public Decoration GetDeco(int ID)
    {
        XmlDocument decodata = new XmlDocument();
        TextAsset textAsset = (TextAsset)Resources.Load("XML/decodata");
        decodata.LoadXml(textAsset.text);

        return LoadDecoInfo(decodata, ID);
    }

    Pet LoadPetInfo(XmlDocument petdata, int ID)
    {
        Pet pet = new Pet();
        XmlNode node = petdata.DocumentElement.SelectSingleNode("/PetCollection/Pet[@ID=" + ID.ToString() + "]");

        if (node == null)
            Debug.LogWarning("Not found pet ID : " + ID);

        pet.ID = ID;
        pet.level = int.Parse(node["level"].InnerText);
        pet.name = node["name"].InnerText;
        pet.description = node["description"].InnerText;
        pet.point = int.Parse(node["point"].InnerText);
        pet.price = int.Parse(node["price"].InnerText);

        pet.art = "Sprites/Pet/" + ID.ToString();

        return pet;
    }

    Decoration LoadDecoInfo(XmlDocument decodata, int ID)
    {
        Decoration deco = new Decoration();
        XmlNode node = decodata.DocumentElement.SelectSingleNode("/DecoCollection/Deco[@ID=" + ID.ToString() + "]");

        if (node == null)
            Debug.LogWarning("Not found deco ID : " + ID);

        deco.ID = ID;
        deco.level = int.Parse(node["level"].InnerText);
        deco.name = node["name"].InnerText;
        deco.description = node["description"].InnerText;
        deco.point = int.Parse(node["point"].InnerText);
        deco.price = int.Parse(node["price"].InnerText);

        deco.art = "Sprites/Deco/Art/" + ID.ToString();

        return deco;
    }

    public Sprite GetDecorationSprite(Decoration decoration)
    {
        return Resources.Load<Sprite>(decoration.art);
    }

    public Sprite GetDecorationSpriteOnTree(Decoration decoration)
    {
        return Resources.Load<Sprite>(decoration.art);
    }

    int GetRandomPet(int level)
    {
        int id = 0;
        while (id == 0)
        {
            if (level == 0)
            {
                id = UnityEngine.Random.Range(50, MaxId_Pet_N + 1);
            }
            else if (level == 1)
            {
                id = UnityEngine.Random.Range(100, MaxId_Pet_R + 1);
            }
            else if (level == 2)
            {
                id = UnityEngine.Random.Range(200, MaxId_Pet_SR + 1);
            }
            else if (level == 3)
            {
                id = UnityEngine.Random.Range(300, MaxId_Pet_SSR + 1);
            }
            else
            {
                id = UnityEngine.Random.Range(500, MaxId_Pet_UR + 1);
            }
        }
        return id;

    }
    int GetRandomDeco(int level)
    {
        int id = 0;
        while (id == 0)
        {
            if (level == 0)
            {
                id = UnityEngine.Random.Range(100, MaxId_Deco_B + 1);
            }
            else if (level == 1)
            {
                id = UnityEngine.Random.Range(200, MaxId_Deco_A + 1);
            }
            else
            {
                id = UnityEngine.Random.Range(300, MaxId_Deco_S + 1);
            }
        }
        return id;
    }
}
