using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

class DataManager : MonoBehaviour
{
    // Start is called before the first frame update
    public static DataManager instance;
    private List<string> SaveList;
    public PlayerData IngameData;

    public string chooseID;
    public bool firstPlay = false;

    public bool loadFinish;

    DateTime accessTime;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    // Update is called once per frame
    void Start()
    {
        loadFinish = false;
    }

    public void StartGame()
    {
        firstPlay = PlayerPrefs.GetInt("firstPlay") == 1 ? true : false;
        chooseID = PlayerPrefs.GetString("slotID");
        IngameData.Name = PlayerPrefs.GetString(chooseID);

        accessTime = DateTime.Now;
        if (firstPlay)
        {
            StartCoroutine(CreateNew());
        }
        else
        {
            StartCoroutine(Load(chooseID));
        }
    }

    IEnumerator CreateNew()
    {
        Debug.Log("New game");

        IngameData.timeSave = new PlayerTime();
        IngameData.timeSave.playTime = TimeSpan.Zero;
        IngameData.timeSave.lastSaveTime = DateTime.Now;

        //IngameData.completeMissions = new List<int>();
        //IngameData.minigameHighScore = new List<int>();

        IngameData.petList = new List<Pet>();

        IngameData.decorationList = new List<Decoration>();
        IngameData.decorationListOnTree = new List<Decoration>();
        for (int i = 0; i < 19; i++)
        {
            IngameData.decorationListOnTree.Add(new Decoration());
        }

        int[] level = { 0, 0, 0, 0 };
        IngameData.levelItemList = new List<int>(level);

        IngameData.eggList = new List<PlayerEgg>();

        PlayerEgg newEgg = new PlayerEgg();
        newEgg.exp = 0;
        newEgg.level = 0;
        newEgg.remainTime = 360;
        IngameData.eggList.Add(newEgg);

        IngameData.money = 0;
        IngameData.expItem = 0;
        IngameData.maxEggLevel = 0;
        IngameData.shopExpRemain = 500;
        IngameData.giftReceived = false;

        IngameData.questList = new List<Quest>();

        IngameData.questCount = new List<int>();
        for (int i = 0; i < 4; i++)
            IngameData.questCount.Add(0);

        IngameData.minigameData = new List<List<int>>();
        for (int i = 0; i < 9; i++)
        {
            List<int> minigame_i = new List<int>();
            for (int j = 0; j < 3; j++)
                minigame_i.Add(0);
            IngameData.minigameData.Add(minigame_i);
        }

        QuestInitialization.InitialzeQuestList();

        // firstPlay = false;
        SaveAll();

        loadFinish = true;

        yield return null;
    }

    public void SaveAll()
    {
        SaveEgg();
        SaveDeco();
        SavePet();
        SaveLevelItem();
        SaveQuest();
        SaveUnit();
        SaveMinigameData();
    }

    public void SaveEgg()
    {
        BinarySerializer.Save<List<PlayerEgg>>(IngameData.eggList, chooseID + "Egg");
        Debug.Log("Binary save");
        SaveTime();
    }

    public void SaveDeco()
    {
        BinarySerializer.Save<List<Decoration>>(IngameData.decorationList, chooseID + "Deco");
        BinarySerializer.Save<List<Decoration>>(IngameData.decorationListOnTree, chooseID + "DecoTree");
        SaveTime();
    }

    public void SavePet()
    {
        BinarySerializer.Save<List<Pet>>(IngameData.petList, chooseID + "Pet");
        SaveTime();
    }

    public void SaveLevelItem()
    {
        BinarySerializer.Save<List<int>>(IngameData.levelItemList, chooseID + "LevelItem");
        SaveTime();
    }

    public void SaveUnit()
    {
        PlayerUnit unit = new PlayerUnit();
        unit.money = IngameData.money;
        unit.expItem = IngameData.expItem;
        unit.shopExpRemain = IngameData.shopExpRemain;
        unit.maxEggLevel = IngameData.maxEggLevel;
        unit.giftReceived = IngameData.giftReceived;
        BinarySerializer.Save<PlayerUnit>(unit, chooseID + "Unit");
        SaveTime();
    }

    public void SaveTime()
    {
        IngameData.timeSave.playTime += (DateTime.Now - IngameData.timeSave.lastSaveTime);
        IngameData.timeSave.lastSaveTime = DateTime.Now;
        BinarySerializer.Save<PlayerTime>(IngameData.timeSave, chooseID + "Time");
    }

    public void SaveQuest()
    {
        BinarySerializer.Save<List<Quest>>(IngameData.questList, chooseID + "Quest");
        BinarySerializer.Save<List<int>>(IngameData.questCount, chooseID + "QuestCount");
        SaveTime();
    }

    public void SaveMinigameData(int index = 0)
    {
        if (index == 0)
        {
            IngameData.minigameData[0][0] = Minigame1.bats;
            IngameData.minigameData[0][1] = Minigame1.tourches;
            IngameData.minigameData[0][2] = Minigame1.score;

            IngameData.minigameData[1][0] = Minigame2.score;
            IngameData.minigameData[1][1] = Minigame2.hitTime;

            IngameData.minigameData[2][0] = Minigame3.score;
            IngameData.minigameData[2][1] = Minigame3.logs;
            IngameData.minigameData[2][2] = Minigame3.balanceTime;

            IngameData.minigameData[3][0] = Minigame4.gold;
            IngameData.minigameData[3][1] = Minigame4.exp;
            IngameData.minigameData[3][2] = Minigame4.time;

            IngameData.minigameData[4][0] = Minigame5.distance;
            IngameData.minigameData[4][1] = Minigame5.meat;

            IngameData.minigameData[5][0] = Minigame6.armors;
            IngameData.minigameData[5][1] = Minigame6.items;
            IngameData.minigameData[5][2] = Minigame6.score;

            IngameData.minigameData[6][0] = Minigame7.items;

            IngameData.minigameData[7][0] = Minigame8.time;
            IngameData.minigameData[7][1] = Minigame8.items;

            IngameData.minigameData[8][0] = Minigame9.comboSnowman;
            IngameData.minigameData[8][1] = Minigame9.comboPrincess;
            IngameData.minigameData[8][2] = Minigame9.time;
        }
        else if (index == 1)
        {
            IngameData.minigameData[0][0] = Minigame1.bats;
            IngameData.minigameData[0][1] = Minigame1.tourches;
            IngameData.minigameData[0][2] = Minigame1.score;
        }
        else if (index == 2)
        {
            IngameData.minigameData[1][0] = Minigame2.score;
            IngameData.minigameData[1][1] = Minigame2.hitTime;
        }
        else if (index == 3)
        {
            IngameData.minigameData[2][0] = Minigame3.score;
            IngameData.minigameData[2][1] = Minigame3.logs;
            IngameData.minigameData[2][2] = Minigame3.balanceTime;
        }
        else if (index == 4)
        {
            IngameData.minigameData[3][0] = Minigame4.gold;
            IngameData.minigameData[3][1] = Minigame4.exp;
            IngameData.minigameData[3][2] = Minigame4.time;
        }
        else if (index == 5)
        {
            IngameData.minigameData[4][0] = Minigame5.distance;
            IngameData.minigameData[4][1] = Minigame5.meat;
        }
        else if (index == 6)
        {
            IngameData.minigameData[5][0] = Minigame6.armors;
            IngameData.minigameData[5][1] = Minigame6.items;
            IngameData.minigameData[5][2] = Minigame6.score;
        }
        else if (index == 7)
        {
            IngameData.minigameData[6][0] = Minigame7.items;

        }
        else if (index == 8)
        {
            IngameData.minigameData[7][0] = Minigame8.time;
            IngameData.minigameData[7][1] = Minigame8.items;
        }
        else
        {
            IngameData.minigameData[8][0] = Minigame9.comboSnowman;
            IngameData.minigameData[8][1] = Minigame9.comboPrincess;
            IngameData.minigameData[8][2] = Minigame9.time;
        }

        BinarySerializer.Save<List<List<int>>>(IngameData.minigameData, chooseID + "MinigameData");

        SaveTime();
    }
    IEnumerator Load(string slotID)
    {
        IngameData = new PlayerData();

        // Get name
        IngameData.Name = PlayerPrefs.GetString(slotID);

        // Get file
        IngameData.timeSave = BinarySerializer.Load<PlayerTime>(slotID + "Time");
        IngameData.petList = BinarySerializer.Load<List<Pet>>(slotID + "Pet");
        IngameData.eggList = BinarySerializer.Load<List<PlayerEgg>>(slotID + "Egg");
        IngameData.decorationList = BinarySerializer.Load<List<Decoration>>(slotID + "Deco");
        IngameData.decorationListOnTree = BinarySerializer.Load<List<Decoration>>(slotID + "DecoTree");
        IngameData.levelItemList = BinarySerializer.Load<List<int>>(slotID + "LevelItem");
        IngameData.questList = BinarySerializer.Load<List<Quest>>(slotID + "Quest");
        IngameData.questCount = BinarySerializer.Load<List<int>>(slotID + "QuestCount");
        IngameData.minigameData = BinarySerializer.Load<List<List<int>>>(slotID + "MinigameData");

        // Get other unit
        PlayerUnit unit = new PlayerUnit();
        unit = BinarySerializer.Load<PlayerUnit>(slotID + "Unit");
        IngameData.money = unit.money;
        IngameData.expItem = unit.expItem;
        IngameData.maxEggLevel = unit.maxEggLevel;
        IngameData.giftReceived = unit.giftReceived;

        foreach (PlayerEgg egg in IngameData.eggList)
        {
            egg.remainTime -= (int)(accessTime - IngameData.timeSave.lastSaveTime).TotalMinutes;
            if (egg.remainTime < 0)
                egg.remainTime = 0;
        }

        if (accessTime.Day - IngameData.timeSave.lastSaveTime.Subtract(new TimeSpan(0, 1, 0)).Day != 0)
        {
            QuestInitialization.InitialzeQuestList();
            IngameData.shopExpRemain = 500;
        }
        else IngameData.shopExpRemain = unit.shopExpRemain;



        // Debug.Log(IngameData.minigameData.Count.ToString());
        // Load Minigame Data
        Minigame1.bats = IngameData.minigameData[0][0];
        Minigame1.tourches = IngameData.minigameData[0][1];
        Minigame1.score = IngameData.minigameData[0][2];

        Minigame2.score = IngameData.minigameData[1][0];
        Minigame2.hitTime = IngameData.minigameData[1][1];

        Minigame3.score = IngameData.minigameData[2][0];
        Minigame3.logs = IngameData.minigameData[2][1];
        Minigame3.balanceTime = IngameData.minigameData[2][2];

        Minigame4.gold = IngameData.minigameData[3][0];
        Minigame4.exp = IngameData.minigameData[3][1];
        Minigame4.time = IngameData.minigameData[3][2];

        Minigame5.distance = IngameData.minigameData[4][0];
        Minigame5.meat = IngameData.minigameData[4][1];

        Minigame6.armors = IngameData.minigameData[5][0];
        Minigame6.items = IngameData.minigameData[5][1];
        Minigame6.score = IngameData.minigameData[5][2];

        Minigame7.items = IngameData.minigameData[6][0];

        Minigame8.time = IngameData.minigameData[7][0];
        Minigame8.items = IngameData.minigameData[7][1];

        Minigame9.comboSnowman = IngameData.minigameData[8][0];
        Minigame9.comboPrincess = IngameData.minigameData[8][1];
        Minigame9.time = IngameData.minigameData[8][2];

        SaveAll();

        loadFinish = true;

        yield return null;
    }

    public void DeleteData(string slotID)
    {
        BinarySerializer.DeleteSlotData(slotID);
        PlayerPrefs.DeleteKey(slotID);
    }

}
