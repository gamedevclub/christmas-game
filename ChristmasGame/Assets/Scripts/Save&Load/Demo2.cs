using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Demo2 : MonoBehaviour
{
    Pet mypet;

    public GameObject displayPet;
    public Text namePet;
    public Image artPet;

    public Text PetNumber;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame

    public void SavePet()
    {
        mypet = DataLoader.instance.GetPet(100);
        DataManager.instance.IngameData.petList.Add(mypet);
        DataManager.instance.SavePet();
    }

    private void Update()
    {
        PetNumber.text = DataManager.instance.IngameData.petList.Count.ToString();
        if (DataManager.instance.IngameData.petList.Count > 0)
        {
            Pet dragon = DataManager.instance.IngameData.petList[0];
            namePet.text = dragon.name;
            artPet.sprite = Resources.Load<Sprite>(dragon.art);
        }
    }

    public void QuiGame()
    {
        Application.Quit();
    }
}
