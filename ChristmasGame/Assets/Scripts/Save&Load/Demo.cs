using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Demo : MonoBehaviour
{
    // Start is called before the first frame update
    public Text moneyDisplay;
    private int gameMoney;

    public Pet mypet;
    public Egg myegg;
    public PlayerEgg playeregg;

    void Start()
    {
        gameMoney = DataManager.instance.IngameData.money;
        moneyDisplay.text = gameMoney.ToString();

        //mypet.name;
    }
    private void Update()
    {
        moneyDisplay.text = gameMoney.ToString();
    }
    // Update is called once per frame
    public void AddMoney()
    {
        gameMoney += 10;
        DataManager.instance.IngameData.money = gameMoney;
        DataManager.instance.SaveUnit();

        playeregg.level = myegg.level;

        //DataManager.instance.IngameData.decorationList.Add();
    }
}
