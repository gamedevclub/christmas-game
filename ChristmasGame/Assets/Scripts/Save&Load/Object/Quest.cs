using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Quest
{
    public int ID;
    public string name;
    public string description;
    // ! 0-2 : Easy-Hard
    public int level;
    // ! 0 is "On going"
    // ! 1 is "Finished"
    public int status;
    public int minigame;
    // ! Type of minigame (killing bats or collecting fire or reaching high score)
    public int type;

    // ? Upgrade prizes;
    public int prizes;

    public Quest(int _id, string _name, string _description, int _level, int _status, int _minigame, int _type, int _prizes = -1 )
    {
        ID = _id;
        name = _name;
        description = _description;
        level = _level;
        status = _status;
        minigame = _minigame;
        type = _type;
        prizes = _prizes;
    }

    public Quest(Quest _quest)
    {
        ID = _quest.ID;
        name = _quest.name;
        description = _quest.description;
        level = _quest.level;
        status = _quest.status;
        minigame = _quest.minigame;
        type = _quest.type;
        prizes = _quest.prizes;
    }
}
