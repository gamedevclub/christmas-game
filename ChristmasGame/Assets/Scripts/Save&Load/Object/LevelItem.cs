using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Level Item", menuName = "LevelItem")]
public class LevelItem : ScriptableObject
{
    public string Name;
    public string description;

    public Sprite art;

    public int level;
    public int price; 
}
