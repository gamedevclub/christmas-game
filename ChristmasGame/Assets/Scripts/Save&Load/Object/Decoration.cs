using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Decoration
{
    public int ID;
    public string name;
    public string description;

    public string art;
    //public string artOnTree;

    public int level;
    public int point;
    public int price;

    // ! type = 0 : item is on inventory
    // ! type = 1 : item is on tree
    // ! type = 2 : item is on house
    public int type;
    // ! Slot start from 0-->1 corresponding to the index of the actual slot on tree
    public int slot;

    public Decoration()
    {
        this.ID = 0;
        this.name = "";
        this.art = "";
        this.level = 0;
        this.point = 0;
        this.price = 0;
        this.type = 0;
        this.slot = 0;
        this.description = "";
    }
    public Decoration(int _id, string _name, string _art, string _artOnTree, int _level, int _point, int _price, string _description, int _type = -1, int _slot = -1)
    {
        this.ID = _id;
        this.name = _name;
        this.art = _art;
        this.level = _level;
        this.point = _point;
        this.price = _price;
        this.type = _type;
        this.slot = _slot;
        this.description = _description;
    }

    public Decoration(Decoration _decoration)
    {
        this.ID = _decoration.ID;
        this.name = _decoration.name;
        this.art = _decoration.art;
        this.level = _decoration.level;
        this.point = _decoration.point;
        this.price = _decoration.price;
        this.type = _decoration.type;
        this.slot = _decoration.slot;
        this.description = _decoration.description;
    }
    public bool isNull()
    {
        if (
            ID == 0 && 
            name.Equals("") &&
            art.Equals("") &&
            level == 0 &&
            point == 0 &&
            price == 0 &&
            type == 0 &&
            slot == 0
        )
            return true;
        else
            return false;
    }
}
