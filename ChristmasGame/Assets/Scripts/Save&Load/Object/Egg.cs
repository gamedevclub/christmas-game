using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Egg", menuName = "Egg")]
public class Egg : ScriptableObject
{
    public string Name;
    public string description;

    public Sprite art;

    public int level;
    public int maxExp;
    public int hatchTime;
    public int price;
}
