using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Pet
{
    public int ID;

    public string name;

    public string description;

    public int level;

    public int point;

    public int price;

    public string art;
}
