using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class PlayerData
{
    public string Name;
    public PlayerTime timeSave;

    //public List<int> completeMissions;
    public List<Quest> questList;

    //public List<int> minigameHighScore;

    public List<PlayerEgg> eggList;

    public List<Pet> petList;
    public List<Decoration> decorationList;
    public List<Decoration> decorationListOnTree;

    public List<int> levelItemList;

    // ! Contains 3 value {0, 0, 0} :
    // ! [0] : number of finished-easy-quest
    // ! [1] : number of finished-medium-quest
    // ! [2] : number of finished-hard-quest
    public List<int> questCount;

    // ! minigameData[i] contains data for quest of minigame i
    // ! minigameData[i][j] contains data [j] for quest of minigame i
    public List<List<int>> minigameData;

    public int money;
    public int expItem;
    public int shopExpRemain;
    public int maxEggLevel;
    public bool giftReceived;
    //public int shopLuck;
}
