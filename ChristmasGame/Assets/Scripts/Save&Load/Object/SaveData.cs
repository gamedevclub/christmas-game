using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class PlayerEgg
{
    public int level;
    public int status;
    public int remainTime;
    public int exp;
}
[System.Serializable]
public class PlayerTime
{
    public TimeSpan playTime;
    public DateTime lastSaveTime;
}
[System.Serializable]
public class PlayerUnit
{
    public int money;
    public int expItem;
    public int shopExpRemain;
    public int maxEggLevel;
    public bool giftReceived;
}
