using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyStorm8 : MonoBehaviour
{
    float Speed = 1.5f;
    Vector2 dire;
    GameObject player;
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        Destroy(gameObject,5);
        if(GameManager8.instance.timeToPlay<100)
        {
        dire = new Vector2(Random.Range(-10,10), Random.Range(-10,10)).normalized;
        }
        else{
            dire = (player.transform.position - transform.position).normalized;
        }
    }
    private void FixedUpdate() {
        if(GameManager8.instance.timeToPlay > 20)
        {
            gameObject.GetComponent<Rigidbody2D>().velocity = dire.normalized * Speed;
        }
    }
}
