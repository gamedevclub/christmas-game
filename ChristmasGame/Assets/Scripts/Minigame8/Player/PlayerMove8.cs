using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove8 : MonoBehaviour
{
    // Start is called before the first frame update
    private Rigidbody2D rb;
    private Vector3 dir;
    public int speed = 100;
    public Vector3 mousePos;
    public float timetouchStorm = 5;
    private Animator animator;
    void Start()
    {
        //mousePos = transform.position;
        rb = gameObject.GetComponent<Rigidbody2D>();
        animator = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(GameManager8.instance.isEndGame)
        {
            dir = Vector2.zero;
        }
        else
        {
        mousePos = mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        // mousePos = new Vector3(Mathf.Clamp(mousePos.x, -8.5f, 8.5f), Mathf.Clamp(mousePos.y, -4.5f, 3.5f), 0);
        // rb.velocity = dir*speed * Time.deltaTime;
        dir = (mousePos - transform.position).normalized;
        }
    }
    private void FixedUpdate() {
        UpdateAnimator();
        // if(Input.GetKeyDown(KeyCode.A))
        // {
        //     //rb.velocity = dir*speed*2*Time.fixedDeltaTime;
        //     rb.AddForce(Vector2.up*1000);
        //     Debug.Log(12345);
        // }
        
        if(GameManager8.instance.touchStorm)
        {
            if(timetouchStorm<=0)
            {
                GameManager8.instance.touchStorm = false;
                timetouchStorm = 5;
            }
            rb.velocity = -dir*speed*Time.fixedDeltaTime;
            timetouchStorm -= Time.fixedDeltaTime;
        }else{
            rb.velocity = dir*speed*Time.fixedDeltaTime;
        }
       
    }
    private void UpdateAnimator()   
    {
        if(GameManager8.instance.touchStorm)
        {
            if(dir.x < 0 && -0.2f< dir.y && dir.y < 0.2f)animator.SetFloat("Blend",1f);//left
            else if(dir.x > 0 && -0.2f< dir.y && dir.y < 0.2f)animator.SetFloat("Blend",0.5f);//right
            // else if(dir.y < 0 && -0.2f< dir.x && dir.x < 0.2f)animator.SetFloat("Blend",0.75f);//down
            // else if(dir.y > 0 && -0.2f< dir.x && dir.x < 0.2f)animator.SetFloat("Blend",0.25f);// top
            // else animator.SetFloat("Blend",0);//idle
        }
        else{
            if(dir.x < 0 && -0.2f< dir.y && dir.y < 0.2f)animator.SetFloat("Blend",0.5f);//left
            else if(dir.x > 0 && -0.2f< dir.y && dir.y < 0.2f)animator.SetFloat("Blend",1f);//right
            // else if(dir.y < 0 && -0.2f< dir.x && dir.x < 0.2f)animator.SetFloat("Blend",0.25f);//down
            // else if(dir.y > 0 && -0.2f< dir.x && dir.x < 0.2f)animator.SetFloat("Blend",0.75f);// top
            // else animator.SetFloat("Blend",0);//idle

        }
    }

}
