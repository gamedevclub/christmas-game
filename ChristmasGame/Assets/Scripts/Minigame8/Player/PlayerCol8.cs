using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCol8 : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject pickCoin;
    public GameObject vfx_hitTroll;
    public GameObject vfx_dropHole;
    public GameObject vfx_pickUpd;
    public bool check = false;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Coin8")
        {
            GameManager8.instance.coin++;
            GameObject pick = Instantiate(pickCoin, transform.position, Quaternion.identity);
            Destroy(pick, 1);
            Destroy(other.gameObject);
            AudioManager8.instance.Play("TakeItem", true);
        }
        if (other.tag == "UpdateItem8")
        {
            GameManager8.instance.updateItem++;
            GameObject vfx = Instantiate(vfx_pickUpd, transform.position, Quaternion.identity);
            Destroy(vfx,1);
            
            Destroy(other.gameObject);
            AudioManager8.instance.Play("TakeItem", true);
        }

        if (other.tag == "Storm8")
        {
            GameManager8.instance.touchStorm = true;
            StartCoroutine(TurnOffEff());
            AudioManager8.instance.Play("TouchStorm", true);
        }
        if (other.tag == "Troll8" && !check)
        {
            GameObject hitTroll = Instantiate(vfx_hitTroll, transform.position, Quaternion.identity);
            other.GetComponent<SpriteRenderer>().enabled = false;
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
            GameManager8.instance.EndGame();
            check = true;
        }
    }
    IEnumerator TurnOffEff()
    {
        GameManager8.instance.dizzyEffect.SetActive(!GameManager8.instance.dizzyEffect.activeSelf);
        yield return new WaitForSeconds(5f);
        GameManager8.instance.dizzyEffect.SetActive(!GameManager8.instance.dizzyEffect.activeSelf);
    }
    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "Hole8" && !check)
        {
            GameObject hitdrop = Instantiate(vfx_dropHole, transform.position, Quaternion.identity);
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
            GameManager8.instance.EndGame();
            check = true;
        }
    }

}
