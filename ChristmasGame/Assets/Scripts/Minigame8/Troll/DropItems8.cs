using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropItems8 : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject coin;
    public GameObject updateItem;
    public float timeReset = 5f;

    void Update()
    {
        if (timeReset <= 0 && !GameManager8.instance.isEndGame)
        {
            int x = Random.Range(0, 100);
            if (x < 20)
            {
                Instantiate(updateItem, transform.position, Quaternion.identity);
            }
            else
            {
                Instantiate(coin, transform.position, Quaternion.identity);
            }

            if (GameManager8.instance.timeToPlay <= 10) timeReset = Random.Range(2, 5);
            else if (GameManager8.instance.timeToPlay <= 30) timeReset = Random.Range(1.5f, 3);
            else timeReset = Random.Range(1, 2);
        }
        if (gameObject.GetComponent<TrollMove8>().isMove)
        {
            timeReset -= Time.deltaTime;
        }
    }
}
