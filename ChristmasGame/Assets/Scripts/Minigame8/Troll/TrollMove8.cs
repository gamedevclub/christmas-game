using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrollMove8 : MonoBehaviour
{
    // Start is called before the first frame update
    private GameObject player;
    private int speed = 100;
    private Vector2 dir;
    private Rigidbody2D rig;
    public bool isMove = true;
    public float freezeTime = 3;
    public Animator animator;
    void Start()
    {
        animator = gameObject.GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player");
        rig = gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameManager8.instance.isEndGame)
        {
            dir = (player.transform.position - transform.position).normalized;
        }
        else
        {
            dir = Vector2.zero;
        }
    }
    private void FixedUpdate()
    {

        UpdateAnimation();
        if (isMove)
        {
            StartCoroutine(enableCollder());
            rig.velocity = dir * speed * Time.fixedDeltaTime;
             gameObject.GetComponent<SpriteRenderer>().sortingOrder = 5;
        }
        else
        {
            gameObject.GetComponent<Collider2D>().enabled = false;
            rig.velocity = Vector2.zero;
            if (freezeTime <= 0)
            {
                isMove = true;
                freezeTime = 3;
            }
            else
            {
                freezeTime -= Time.fixedDeltaTime;
                //Debug.Log(freezeTime);
            }
        }

    }
    IEnumerator enableCollder()
    {
        yield return new WaitForSeconds(0.5f);
        // gameObject.GetComponent<SpriteRenderer>().enabled = false;
        gameObject.GetComponent<Collider2D>().enabled = true;
    }
    void UpdateAnimation()
    {
        if (isMove)
        {
            if (dir.x < 0) animator.SetFloat("Blend", 1f);//left
            else if (dir.x > 0) animator.SetFloat("Blend", 0.5f);//right
        }
        // else if (dir.y < 0 && -0.1f < dir.x && dir.x < 0.1f) animator.SetFloat("Blend", 0.75f);//down
        // else if (dir.y > 0 && -0.1f < dir.x && dir.x < 0.1f) animator.SetFloat("Blend", 0.25f);// top
        else animator.SetFloat("Blend", 0);//idle
    }
    void IncreaSpeed()
    {
        if (GameManager8.instance.timeToPlay <= 60 && GameManager8.instance.timeToPlay > 30) speed = 250;
        else if (GameManager8.instance.timeToPlay >= 60) speed = 350;
    }
}
