using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hole8 : MonoBehaviour
{
    public Sprite holeSprite;
    public Sprite crackSprite;

    private void Start()
    {
        gameObject.GetComponent<Collider2D>().enabled = false;
        gameObject.GetComponent<SpriteRenderer>().sprite = crackSprite;
        StartCoroutine(Cracking());
    }

    IEnumerator Cracking()
    {
        yield return new WaitForSeconds(2f);
        gameObject.GetComponent<Collider2D>().enabled = true;
        gameObject.GetComponent<SpriteRenderer>().sprite = holeSprite;
    }
}
