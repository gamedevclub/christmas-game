using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin8 : MonoBehaviour
{
    private GameObject player;
    private bool isFollowing;

    private Rigidbody2D myBody;

    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<SpriteRenderer>().sortingOrder = 10000 - (int)(transform.position.y * 100);
        myBody = GetComponent<Rigidbody2D>();
        player = GameObject.FindGameObjectWithTag("Player");
        isFollowing = false;
    }

    private void Update()
    {
        FollowPlayer();
    }

    void FollowPlayer()
    {
        if (player != null)
        {
            if (!isFollowing)
            {
                if (Vector2.Distance(transform.position, player.transform.position) < 2f)
                {
                    isFollowing = true;
                }
            }
            else
            {
                Vector2 direction = player.transform.position - transform.position;
                direction = direction.normalized;
                myBody.velocity = direction * 7;
            }
        }
    }
}
