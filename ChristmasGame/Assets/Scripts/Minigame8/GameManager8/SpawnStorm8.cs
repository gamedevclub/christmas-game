using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnStorm8 : MonoBehaviour
{
    public GameObject storm;
    public float timeSpawnStorm = 5;
    public Vector2 pos;

    public float maxTimeSpawn =10;
    public float minTimeSpawn = 3;
    void FixedUpdate()
    {
        if (timeSpawnStorm <= 0 && !GameManager8.instance.isEndGame)
        {
            bool check = false;
            do
            {
                check = false;
                pos = new Vector2(Random.Range(-8f, 8f), Random.Range(3f, -4f));
                Collider2D ob = Physics2D.OverlapCircle(pos, 1f);
                if (ob) check = true;
            } while (check);
            AudioManager8.instance.Play("Storm",true);
            Instantiate(storm, pos, Quaternion.identity);
            timeSpawnStorm = Random.Range(minTimeSpawn, maxTimeSpawn);
        }
        timeSpawnStorm -= Time.fixedDeltaTime;
    }
}
