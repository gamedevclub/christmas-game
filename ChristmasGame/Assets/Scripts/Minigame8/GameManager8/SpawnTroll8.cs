using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnTroll8 : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject troll;
    public float timeSpawnTroll = 5;
    public int maxQuantityTroll = 1;
    public Vector2 pos = new Vector2();
 
    void FixedUpdate()
    {
        if(timeSpawnTroll <= 0 && GameManager8.instance.quantityTroll < maxQuantityTroll && !GameManager8.instance.isEndGame)
        {
            int x = Random.Range(0,4);
            switch(x)
            {
                case 0:
                {
                    pos.x = Random.Range(-10f,10f);
                    pos.y = 6f;
                    break;
                }
                case 1:
                    pos.x = Random.Range(-10f,10f);
                    pos.y = -6f;
                    break;
                case 2:
                    pos.x = -10f;
                    pos.y = Random.Range(-6f,6f);
                    break;
                case 3:
                    pos.x = 10f;
                    pos.y = Random.Range(-6f,6f);
                    break;
            };

            Instantiate(troll,pos,Quaternion.identity);
            timeSpawnTroll = 5;
            GameManager8.instance.quantityTroll ++;
        }
        timeSpawnTroll -= Time.fixedDeltaTime;
    }

}
