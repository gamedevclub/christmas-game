using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetPnlEndGame : MonoBehaviour
{
    // Start is called before the first frame update
    public Text txtCoin;
    public Text txtUpdateItem;
    public void SetValue(int coin,int item)
    {
        txtCoin.text = coin.ToString();
        txtUpdateItem.text = item.ToString();
    }
    void GoToHome()
    {
        gameObject.SetActive(false);
        Debug.Log("Go Home");
    }
}
