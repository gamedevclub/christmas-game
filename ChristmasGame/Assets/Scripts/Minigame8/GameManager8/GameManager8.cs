using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager8 : MonoBehaviour
{
    public static GameManager8 instance;
    public int coin = 0;
    public Text txtCoin;
    public int updateItem = 0;
    public Text txtUpdItem;
    public int quantityTroll = 0;
    public int quantityHole = 0;
    public bool touchStorm = false;
    public float timeToPlay = 0;
    public GameObject pnl;
    public bool isEndGame = false;
    public GameObject dizzyEffect;

    private void Awake()
    {
        // pnl.enabled = false;
        if (instance == null)
            instance = this;
        pnl.SetActive(false);
    }
    void FixedUpdate()
    {
        Debug.Log(timeToPlay);
        if (!isEndGame)
        {
            timeToPlay += Time.fixedDeltaTime;
            txtCoin.text = coin.ToString();
            txtUpdItem.text = updateItem.ToString();
            IncreaseDifficulty();
        }
    }
    public void EndGame()
    {
        isEndGame = true;
        UpdateQuest();
        // GetAward();
        AudioManager8.instance.StopAll();
        AudioManager8.instance.Play("EndGame",false);
        StartCoroutine(CallGetAward());
        // Debug.Log("Kết thúc game 8");
    }
    IEnumerator CallGetAward()
    {
        yield return new WaitForSeconds(2);
        GetAward();
    }
    public void UpdateQuest()
    {
        if (Minigame8.time < (int)timeToPlay) Minigame8.time = (int)timeToPlay;
        if (Minigame8.items < updateItem) Minigame8.items = updateItem;
        DataManager.instance.SaveMinigameData(8);
    }
    public void GetAward()
    {
        if(timeToPlay > 50)AwardManager.instance.GetGameAward(coin, updateItem, true);
        else AwardManager.instance.GetGameAward(coin, updateItem, false);
    }
    public void IncreaseDifficulty()
    {
        if (timeToPlay > 30)
        {
            gameObject.GetComponent<SpawnHole8>().maxHole = 5;
            gameObject.GetComponent<SpawnTroll8>().maxQuantityTroll = 3;
            gameObject.GetComponent<SpawnStorm8>().minTimeSpawn = 2f;
            gameObject.GetComponent<SpawnStorm8>().maxTimeSpawn = 7f;
        }
        if (timeToPlay > 60)
        {
            gameObject.GetComponent<SpawnHole8>().maxHole = 10;
            gameObject.GetComponent<SpawnTroll8>().maxQuantityTroll = 5;
            gameObject.GetComponent<SpawnStorm8>().minTimeSpawn = 2f;
            gameObject.GetComponent<SpawnStorm8>().maxTimeSpawn = 4f;
        }
        if (timeToPlay > 120)
        {
            gameObject.GetComponent<SpawnHole8>().maxHole = 15;
            gameObject.GetComponent<SpawnTroll8>().maxQuantityTroll = 7;
            gameObject.GetComponent<SpawnStorm8>().minTimeSpawn = 1f;
            gameObject.GetComponent<SpawnStorm8>().maxTimeSpawn = 3f;
        }
        if (timeToPlay > 150)
        {
            gameObject.GetComponent<SpawnHole8>().maxHole = 40;
            gameObject.GetComponent<SpawnTroll8>().maxQuantityTroll = 15;
            gameObject.GetComponent<SpawnStorm8>().minTimeSpawn = 1f;
            gameObject.GetComponent<SpawnStorm8>().maxTimeSpawn = 3f;
        }

    }
}
