using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnHole8 : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject hole;
    public float timeSpawnHole = 5;
    public int maxHole = 5;
    public Vector2 pos;

    void FixedUpdate()
    {
        if (timeSpawnHole <= 0 && GameManager8.instance.quantityHole < maxHole && !GameManager8.instance.isEndGame)
        {
            bool check = false;
            do
            {
                check = false;
                pos = new Vector2(Random.Range(-8f, 8f), Random.Range(3f, -4f));
                Collider2D ob = Physics2D.OverlapCircle(pos, 1f);
                if (ob) check = true;
            } while (check);

            AudioManager8.instance.Play("SpawnHole",true);
            
            Instantiate(hole, pos, Quaternion.identity);
            timeSpawnHole = 5;
            GameManager8.instance.quantityHole++;
        }
        timeSpawnHole -= Time.fixedDeltaTime;
    }
}
