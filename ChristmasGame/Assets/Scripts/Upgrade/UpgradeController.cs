using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UpgradeController : MonoBehaviour
{
    public GameObject starPrefab;
    public Transform starContainer;
    public Text remainingTimeText;
    public Image eggSelectImage;
    public Image expBar;
    public Image expBackground;
    public Image itemSelectImage;
    public GameObject upgradeButton;
    public GameObject hatchButton;
    public EggSlotUpgrade eggSelect;
    public ExpSlotUpgrade expItemSelect;
    public LevelSlotUpgrade levelItemSelect;
    public Text quantityNeeded;
    public Toggle toggleUseAll;
    public GameObject upgradeEffect;
    private bool useAll;

    public void ChangeEggSelect(EggSlotUpgrade egg)
    {
        // Reset toggle
        toggleUseAll.isOn = false;

        eggSelect = egg;

        // Maximum level
        if (egg.eggData.level == 4)
        {
            toggleUseAll.gameObject.SetActive(false);
            upgradeButton.SetActive(false);
            itemSelectImage.transform.parent.gameObject.SetActive(false);
        }
        else
        {
            upgradeButton.SetActive(true);
            itemSelectImage.transform.parent.gameObject.SetActive(true);
            // Select item automatic
            if (egg.eggData.exp >= egg.maxExp)
            {
                ChangeLevelItemSelect(UpgradeManager.instance.levelItemList[egg.eggData.level].GetComponent<LevelSlotUpgrade>());
            }
            else
            {
                ChangeExpItemSelect(UpgradeManager.instance.expItemList[0].GetComponent<ExpSlotUpgrade>());
            }
        }
        Display();
    }
    public void ChangeExpItemSelect(ExpSlotUpgrade expItem)
    {
        toggleUseAll.gameObject.SetActive(true);
        quantityNeeded.text = "1";
        expItemSelect = expItem;
        levelItemSelect = null;
        itemSelectImage.sprite = expItemSelect.GetComponentsInChildren<Image>()[1].sprite;
    }
    public void ChangeLevelItemSelect(LevelSlotUpgrade levelItem)
    {
        toggleUseAll.gameObject.SetActive(false);
        quantityNeeded.text = "3";
        levelItemSelect = levelItem;
        expItemSelect = null;
        itemSelectImage.sprite = levelItemSelect.GetComponentsInChildren<Image>()[1].sprite;
    }

    public void Display()
    {
        ClearContainer(starContainer);

        // Egg information 
        for (int i = 0; i < eggSelect.eggData.level + 1; i++)
        {
            GameObject star = Instantiate(starPrefab, starContainer);
        }
        int currentExp = eggSelect.eggData.exp;
        int maxExp = eggSelect.maxExp;
        expBar.fillAmount = (float)currentExp / maxExp;
        eggSelectImage.sprite = eggSelect.art;

        if (eggSelect.eggData.level == 4)
        {
            expBackground.gameObject.SetActive(false);
            expBar.gameObject.SetActive(false);
            remainingTimeText.gameObject.SetActive(false);
            hatchButton.SetActive(true);
        }
        else
        {
            expBackground.gameObject.SetActive(true);
            expBar.gameObject.SetActive(true);
            remainingTimeText.gameObject.SetActive(true);
            hatchButton.SetActive(false);
        }

        Refresh();

    }

    public void Refresh()
    {
        int time = eggSelect.eggData.remainTime;
        remainingTimeText.text = convertTime(time);
        if (time < 1)
        {
            hatchButton.SetActive(true);
        }
    }

    public void Upgrade()
    {
        if (expItemSelect != null)
        {
            if (expItemSelect.quantity <= 0)
                GameManager.instance.Alert("Không đủ nguyên liệu, hãy vào Cửa hàng mua nhé !!!");
            else
            {
                int quantityUse = 0;
                if (useAll)
                {
                    quantityUse = UpdateEggExp(expItemSelect.exp, expItemSelect.quantity);
                    quantityNeeded.text = "0";
                }
                else
                {
                    quantityUse = UpdateEggExp(expItemSelect.exp, 1);
                }

                // Update exp item data
                expItemSelect.quantity -= quantityUse;
                expItemSelect.Display();

                DataManager.instance.IngameData.expItem -= quantityUse;
                // SaveExpItem();
                DataManager.instance.SaveUnit();

                // Check exp of egg
                int currentExp = eggSelect.eggData.exp;
                int maxExp = eggSelect.maxExp;
                expBar.fillAmount = (float)currentExp / maxExp;
                if (currentExp == maxExp)
                {
                    if (eggSelect.eggData.level < 4)
                        ChangeLevelItemSelect(UpgradeManager.instance.levelItemList[eggSelect.eggData.level].GetComponent<LevelSlotUpgrade>());
                    toggleUseAll.isOn = false;
                }


            }
        }

        else if (levelItemSelect != null)
        {
            if (levelItemSelect.quantity <= 2)
                GameManager.instance.Alert("Không đủ nguyên liệu, hãy vào Cửa hàng mua nhé !!!");
            else
            {
                // Update level item data
                levelItemSelect.quantity -= 3;
                levelItemSelect.Display();
                DataManager.instance.IngameData.levelItemList[levelItemSelect.level] -= 3;
                DataManager.instance.SaveLevelItem();

                // Level up and update egg data
                eggSelect.eggData.level += 1;
                var eggTemplate = UpgradeManager.instance.eggTemplates[eggSelect.eggData.level];
                eggSelect.maxExp = eggTemplate.maxExp;
                eggSelect.art = eggTemplate.art;
                eggSelect.eggData.exp = 0;
                eggSelect.Display();
                ChangeEggSelect(eggSelect);

                int level = eggSelect.eggData.level;
                int maxEggLevel = DataManager.instance.IngameData.maxEggLevel;

                if (level > maxEggLevel)
                {
                    DataManager.instance.IngameData.maxEggLevel = level;
                    DataManager.instance.SaveUnit();
                }

                GameObject effect = Instantiate(upgradeEffect, eggSelectImage.transform);
                effect.transform.SetParent(UpgradeManager.instance.gameObject.transform);
                AudioManager.instance.MergeUpgrade();
            }
        }
        else
        {
            Debug.Log("Upgrade Error");
        }

        DataManager.instance.SaveEgg();

    }

    // Return quantity of items used
    int UpdateEggExp(int exp, int quantity)
    {
        int increaseExp = 0;
        int remainExp = eggSelect.maxExp - eggSelect.eggData.exp;
        int maxIncreaseExp = exp * quantity;
        if (maxIncreaseExp >= remainExp)
        {
            increaseExp = remainExp;
        }
        else
        {
            increaseExp = maxIncreaseExp;
        }

        // eggSelect -> 
        eggSelect.eggData.exp += increaseExp;

        return increaseExp / exp;
    }

    public void SetUseAllValue()
    {
        useAll = toggleUseAll.isOn;
        if (useAll)
            quantityNeeded.text = expItemSelect.quantity.ToString();
        else
            quantityNeeded.text = expItemSelect == null ? "3" : "1";

    }

    void ClearContainer(Transform container)
    {
        for (int i = container.childCount - 1; i >= 0; i--)
        {
            Destroy(container.GetChild(i).gameObject);
        }
    }

    void ResetToggle()
    {
        toggleUseAll.isOn = false;
        useAll = false;
    }

    string convertTime(int time)
    {
        int hours = time < 0 ? 0 : time / 60;
        int minutes = time < 0 ? 0 : time - hours * 60;
        return (hours / 10 == 0 ? "0" : "") + hours.ToString() + ":" + (minutes / 10 == 0 ? "0" : "") + minutes.ToString();
    }
}
