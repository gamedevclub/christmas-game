using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SlotUpgrade : MonoBehaviour, IPointerDownHandler
{
    // Data
    protected bool clickReady = true;
    protected int clickCount = 0;
    protected float clickTime = 0f;
    [SerializeField] protected float clickDoubleTime = 0.2f;
    [SerializeField] protected float clickDelayTime = 0.2f;
    virtual public void SingleClick()
    {
        Debug.Log("Single Click");
        StartCoroutine(ClickDelay());
    }
    virtual public void DoubleClick()
    {
        Debug.Log("Double Click");
        StartCoroutine(ClickDelay());
    }
    public void OnPointerDown(PointerEventData pointerEventData)
    {
        if (clickReady) clickCount++;
        if (clickCount == 1)
        {
            clickTime = Time.time;
            StartCoroutine(ClickHandler());
        }
        if (clickCount > 1 && Time.time - clickTime < clickDoubleTime)
        {
            DoubleClick();
            clickCount = 0;
            clickTime = 0;
        }
    }
    IEnumerator ClickHandler()
    {
        yield return new WaitForSeconds(clickDoubleTime);
        if (clickCount == 1)
        {
            SingleClick();
            clickCount = 0;
            clickTime = 0;
        }
    }
    IEnumerator ClickDelay()
    {
        clickReady = false;
        yield return new WaitForSeconds(clickDelayTime);
        clickReady = true;
    }
}
