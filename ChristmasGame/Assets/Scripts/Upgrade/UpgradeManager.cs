using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UpgradeManager : MonoBehaviour
{
    public GameObject upgradePanel;
    public GameObject eggEffect;
    public List<Egg> eggTemplates;
    public UpgradeController controller;
    public static UpgradeManager instance;
    public List<GameObject> eggList;
    public List<GameObject> expItemList;
    public List<GameObject> levelItemList;
    public GameObject eggUpgradePrefab;
    public GameObject eggNullSlot;
    public Transform eggContainer;
    public GameObject mergePanel;
    public List<int> quantityMergeItem;
    public List<Text> quantityTextMergeInput;
    public List<Text> quantityTextMergeOutput;

    public List<InputField> inputQuantityMerge;

    void Start()
    {
        if (instance == null)
        {
            instance = this;
            //StartCoroutine(UpdateEggTime());
        }
    }

    public void Load()
    {
        var eggDataList = DataManager.instance.IngameData.eggList;

        ClearContainer(eggContainer);

        for (int i = 0; i < eggDataList.Count; i++)
        {
            GameObject eggSlotObject = Instantiate(eggUpgradePrefab, eggContainer);
            eggList.Add(eggSlotObject);
            // Set data
            var eggData = eggDataList[i];
            var eggSlot = eggSlotObject.GetComponent<EggSlotUpgrade>();
            eggSlot.index = i;
            eggSlot.art = eggTemplates[eggData.level].art;
            eggSlot.eggData = eggData;
            eggSlot.maxExp = eggTemplates[eggData.level].maxExp;
            eggSlot.Display();
        }
        for (int i = eggDataList.Count; i < 4; i++)
        {
            GameObject eggNullSlotObject = Instantiate(eggNullSlot, eggContainer);
        }

        // Set exp item data
        expItemList[0].GetComponent<ExpSlotUpgrade>().quantity = DataManager.instance.IngameData.expItem;
        expItemList[0].GetComponent<ExpSlotUpgrade>().Display();


        // Set level item data
        for (int i = 0; i < levelItemList.Count; i++)
        {
            int quantity = DataManager.instance.IngameData.levelItemList[i];
            levelItemList[i].GetComponent<LevelSlotUpgrade>().quantity = quantity;
            levelItemList[i].GetComponent<LevelSlotUpgrade>().Display();
        }

        if (eggList.Count >= 1)
            SelectEgg(eggList[0].GetComponent<EggSlotUpgrade>());
        else
            controller.gameObject.SetActive(false);

        upgradePanel.SetActive(true);
    }


    public void Exit()
    {
        eggList.Clear();
        controller.gameObject.SetActive(false);
        upgradePanel.transform.parent.gameObject.SetActive(false);
    }


    void ClearContainer(Transform container)
    {
        for (int i = container.childCount - 1; i >= 0; i--)
        {
            Destroy(container.GetChild(i).gameObject);
        }
    }
    public void SelectEgg(EggSlotUpgrade egg)
    {
        controller.gameObject.SetActive(true);
        controller.ChangeEggSelect(egg);
    }

    public void UpdateEggTime()
    {

        foreach (var egg in DataManager.instance.IngameData.eggList)
        {
            if (egg.remainTime >= 1)
                egg.remainTime -= 1;
        }
        if (controller.gameObject.activeInHierarchy == true)
        {
            controller.Refresh();
        }
        DataManager.instance.SaveEgg();
    }

    public void SetActiveMerge(bool active)
    {
        if (active)
        {
            // Reset value
            quantityMergeItem.Clear();
            quantityMergeItem.Add(0);
            RefreshMergeValue(0);
            quantityMergeItem.Add(0);
            RefreshMergeValue(1);
            quantityMergeItem.Add(0);
            RefreshMergeValue(2);

        }
        mergePanel.SetActive(active);
    }
    void RefreshMergeValue(int level)
    {
        inputQuantityMerge[level].text = "";
        quantityTextMergeInput[level].text = "";
        quantityTextMergeOutput[level].text = "";
        quantityMergeItem[level] = 0;
    }
    public void MergeLevelItem(int level)
    {
        if (quantityMergeItem[level] != 0)
        {
            int input = int.Parse(quantityTextMergeInput[level].text);
            int output = int.Parse(quantityTextMergeOutput[level].text);

            if (levelItemList[level].GetComponent<LevelSlotUpgrade>().quantity < input)
            {
                GameManager.instance.Alert("Không đủ nguyên liệu để ghép, hãy vào Cửa hàng mua nhé !!!");
            }
            else
            {
                levelItemList[level].GetComponent<LevelSlotUpgrade>().quantity -= input;
                DataManager.instance.IngameData.levelItemList[level] -= input;
                levelItemList[level].GetComponent<LevelSlotUpgrade>().Display();
                levelItemList[level + 1].GetComponent<LevelSlotUpgrade>().quantity += output;
                DataManager.instance.IngameData.levelItemList[level + 1] += output;
                levelItemList[level + 1].GetComponent<LevelSlotUpgrade>().Display();
                RefreshMergeValue(level);

                DataManager.instance.SaveLevelItem();
            }
        }
    }
    public void SetQuantityMergeItem(int level)
    {
        if (inputQuantityMerge[level].text != "" && inputQuantityMerge[level].text != "0" && inputQuantityMerge[level].text != "00")
        {
            quantityMergeItem[level] = int.Parse(inputQuantityMerge[level].text);
            quantityTextMergeInput[level].text = (quantityMergeItem[level] * 3).ToString();
            quantityTextMergeOutput[level].text = (quantityMergeItem[level]).ToString();
        }
        else
        {
            quantityMergeItem[level] = 0;
            inputQuantityMerge[level].text = "";
            quantityTextMergeInput[level].text = "";
            quantityTextMergeOutput[level].text = "";
        }
    }

    public void EggHatches()
    {
        var index = controller.eggSelect.index;

        Pet newPet = DataLoader.instance.GetPetRandom(controller.eggSelect.eggData.level);

        AudioManager.instance.Gacha();

        PetGenerate.instance.AddPet(newPet);

        Sprite petArt = Resources.Load<Sprite>(newPet.art);

        GameObject effect = Instantiate(eggEffect, Vector3.zero, Quaternion.identity);
        effect.GetComponent<ParticleSystem>().textureSheetAnimation.SetSprite(0, petArt);

        DataManager.instance.IngameData.petList.Add(newPet);

        eggList.RemoveAt(index);
        foreach (var egg in eggList)
        {
            if (egg.GetComponent<EggSlotUpgrade>().index > index)
                egg.GetComponent<EggSlotUpgrade>().index--;
        }

        DataManager.instance.IngameData.eggList.RemoveAt(index);

        Destroy(controller.eggSelect.gameObject);

        controller.eggSelect = null;
        controller.gameObject.SetActive(false);
        GameObject eggNullSlotObject = Instantiate(eggNullSlot, eggContainer);

        DataManager.instance.SavePet();
        DataManager.instance.SaveEgg();

    }
}
