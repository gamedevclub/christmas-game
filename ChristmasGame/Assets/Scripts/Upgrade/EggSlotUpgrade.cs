using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EggSlotUpgrade : SlotUpgrade
{
    // EggData
    public PlayerEgg eggData;
    public Sprite art;
    public int maxExp;
    public int index;
    public override void SingleClick()
    {
        UpgradeManager.instance.SelectEgg(this);
        Debug.Log("Egg single click");
    }
    public override void DoubleClick()
    {
        Debug.Log("Egg double click");
    }
    public void Display()
    {
        gameObject.GetComponentsInChildren<Image>()[1].sprite = art;
    }
}
