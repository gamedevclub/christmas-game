using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExpSlotUpgrade : MonoBehaviour
{
    // Exp Item Data
    public int quantity;
    public Text text;
    public int exp;

    public void Display()
    {
        text.text = quantity.ToString();
    }
}
