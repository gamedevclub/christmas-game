using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestManager : MonoBehaviour
{
    
    public static QuestManager Instance{get; set;}
    [HideInInspector]
    public GameObject contentQuests;
    [HideInInspector]
    public List<GameObject> questObjects;
    public GameObject questTemplate;
    public GameObject bigQuestTemplate;
    public GameObject contentBigQuest;
    [HideInInspector]
    public int easyQuestCount, mediumQuestCount, hardQuestCount;
    public int numberOfQuestsToClaimBigQuest = 9;
    public bool isClaimEasyBigQuest, isClaimMediumBigQuest, isClaimHardBigQuest; 
    public Sprite closeB,closeA,closeS;
    void Awake()
    {
        contentQuests = transform.gameObject;
        Instance = this;
    }
    /// <summary>
    /// * Called when minigame is finished to mark it "Finished"
    /// </summary>
    /// <param name="_minigame"></param>
    public void Finished(int _minigame)
    {
        if (questObjects.Find(qo => qo.GetComponent<QuestDisplay>().quest.minigame == _minigame))
            questObjects.Find(qo => qo.GetComponent<QuestDisplay>().quest.minigame == _minigame).GetComponent<QuestDisplay>().Finished();
    }
    /// <summary> 
    /// * Check if the big quests is finished or not
    /// </summary>
    public void BigQuest()
    {
        int level = QuestInitialization.Instance.GetCurrentMinigameLevel(-10);
        switch (level)
        {
            case (int)MinigameLevel.EASY:
                if (DataManager.instance.IngameData.questCount[0] == numberOfQuestsToClaimBigQuest)
                {
                    print("Check easy big quest");
                    Finished(-10);
                }
                break;
            case (int)MinigameLevel.MEDIUM:
                if (DataManager.instance.IngameData.questCount[1] == numberOfQuestsToClaimBigQuest)
                {
                    print("Check medium big quest");
                    Finished(-10);
                }
                break;
            case (int)MinigameLevel.HARD:
                if (DataManager.instance.IngameData.questCount[2] == numberOfQuestsToClaimBigQuest)
                {
                    print("Check hard big quest");
                    Finished(-10);
                }
                break;
            default:
                break;
        }
    }
    public void Minigame_1()
    {
        // TODO : Get type of the minigame (getting score, killing bats or collecting tourches)
        int type = QuestInitialization.Instance.GetCurrentMinigameType(0);
        int level = QuestInitialization.Instance.GetCurrentMinigameLevel(0);
        switch (type)
        {
            // TODO : GET SCORE
            case 0:
                int score = Minigame1.score;
                switch (level)
                {
                    case (int)MinigameLevel.EASY:
                        if (score >= 100)
                        {
                            Finished(0);
                        }
                        break;
                    case (int)MinigameLevel.MEDIUM:
                        if (score >= 150)
                        {
                            Finished(0);
                        }
                        break;
                    case (int)MinigameLevel.HARD:
                        if (score >= 200)
                        {
                            Finished(0);
                        }
                        break;
                }
                break;
            // TODO : COLLECTING TOURCHES
            case 1:
                int tourches = Minigame1.tourches;
                switch (level)
                {
                    case (int)MinigameLevel.EASY:
                        if (tourches >= 5)
                        {
                            Finished(0);
                        }
                        break;
                    case (int)MinigameLevel.MEDIUM:
                        if (tourches >= 10)
                        {
                            Finished(0);
                        }
                        break;
                    case (int)MinigameLevel.HARD:
                        if (tourches >= 15)
                        {
                            Finished(0);
                        }
                        break;
                }
                break;
            // TODO : KILLING BATS
            case 2:
                int bats = Minigame1.bats;
                switch (level)
                {
                    case (int)MinigameLevel.EASY:
                        if (bats >= 3)
                        {
                            Finished(0);
                             
                        }
                        break;
                    case (int)MinigameLevel.MEDIUM:
                        if (bats >= 5)
                        {
                            Finished(0);
                             
                        }
                        break;
                    case (int)MinigameLevel.HARD:
                        if (bats >= 7)
                        {
                            Finished(0);
                        }
                        break;
                }
                break;
            default:
                break;
        }
    }
    public void Minigame_2()
    {
        // TODO : Get type of the minigame (getting score, hitting constantly to opponent and dodging time)
        int type = QuestInitialization.Instance.GetCurrentMinigameType(1);
        int level = QuestInitialization.Instance.GetCurrentMinigameLevel(1);
        switch (type)
        {
            // TODO : GET SCORE
            case 0:
                int score = Minigame2.score;
                switch (level)
                {
                    case (int)MinigameLevel.EASY:
                        if (score >= 100)
                        {
                            Finished(1);
                             
                        }
                        break;
                    case (int)MinigameLevel.MEDIUM:
                        if (score >= 200)
                        {
                            Finished(1);
                             
                        }
                        break;
                    case (int)MinigameLevel.HARD:
                        if (score >= 400)
                        {
                            Finished(1);
                             
                        }
                        break;
                }
                break;
            // TODO : HITTING OPPONENT CONSTANTLY
            case 1:
                int hitTime = Minigame2.hitTime;
                switch (level)
                {
                    case (int)MinigameLevel.EASY:
                        if (hitTime >= 3)
                        {
                            Finished(1);
                             
                        }
                        break;
                    case (int)MinigameLevel.MEDIUM:
                        if (hitTime >= 8)
                        {
                            Finished(1);
                             
                        }
                        break;
                    case (int)MinigameLevel.HARD:
                        if (hitTime >= 17)
                        {
                            Finished(1);
                             
                        }
                        break;
                }
                break;
            default:
                break;
        }
    }
    public void Minigame_3()
    {
        // TODO : Get type of the minigame (getting score, collecting logs and balance temperature)
        int type = QuestInitialization.Instance.GetCurrentMinigameType(2);
        int level = QuestInitialization.Instance.GetCurrentMinigameLevel(2);
        switch (type)
        {
            // TODO : GET SCORE
            case 0:
                int score = Minigame3.score;
                switch (level)
                {
                    case (int)MinigameLevel.EASY:
                        if (score >= 60)
                        {
                            Finished(2);
                        }
                        break;
                    case (int)MinigameLevel.MEDIUM:
                        if (score >= 120)
                        {
                            Finished(2);
                        }
                        break;
                    case (int)MinigameLevel.HARD:
                        if (score >= 150)
                        {
                            Finished(2);
                        }
                        break;
                }
                break;
            // TODO : COLLECTING LOGS
            case 1:
                int logs = Minigame3.logs;
                switch (level)
                {
                    case (int)MinigameLevel.EASY:
                        if (logs >= 5)
                        {
                            Finished(2);
                        }
                        break;
                    case (int)MinigameLevel.MEDIUM:
                        if (logs >= 10)
                        {
                            Finished(2);
                        }
                        break;
                    case (int)MinigameLevel.HARD:
                        if (logs >= 15)
                        {
                            Finished(2);
                        }
                        break;
                }
                break;
            default:
                break;
        }
    }
    public void Minigame_4()
    {
        int type = QuestInitialization.Instance.GetCurrentMinigameType(3);
        int level = QuestInitialization.Instance.GetCurrentMinigameLevel(3);
        switch (type)
        {
            // TODO : GET SCORE
            case 0:
                float gold = Minigame4.gold;
                switch (level)
                {
                    case (int)MinigameLevel.EASY:
                        if (gold >= 150)
                        {
                            Finished(3);
                        }
                        break;
                    case (int)MinigameLevel.MEDIUM:
                        if (gold >= 250)
                        {
                            Finished(3);
                        }
                        break;
                    case (int)MinigameLevel.HARD:
                        if (gold >= 350)
                        {
                            Finished(3);
                        }
                        break;
                }
                break;
            // TODO : COLLECTING TOURCHES
            case 1:
                float exp = Minigame4.exp;
                switch (level)
                {
                    case (int)MinigameLevel.EASY:
                        if (exp >= 10)
                        {
                            Finished(3);
                        }
                        break;
                    case (int)MinigameLevel.MEDIUM:
                        if (exp >= 15)
                        {
                            Finished(3);
                        }
                        break;
                    case (int)MinigameLevel.HARD:
                        if (exp >= 20)
                        {
                            Finished(3);
                        }
                        break;
                }
                break;
            case 2:
                float time = Minigame4.time;
                switch (level)
                {
                    case (int)MinigameLevel.EASY:
                        if (time > 0 && time <= 120)
                        {
                            Finished(3);
                        }
                        break;
                    case (int)MinigameLevel.MEDIUM:
                        if (time > 0 && time <= 100)
                        {
                            Finished(3);
                        }
                        break;
                    case (int)MinigameLevel.HARD:
                        if (time > 0 && time <= 80)
                        {
                            Finished(3);
                        }
                        break;
                }
                break;
            default:
                break;
        }
    }
    public void Minigame_5()
    {
        int type = QuestInitialization.Instance.GetCurrentMinigameType(4);
        int level = QuestInitialization.Instance.GetCurrentMinigameLevel(4);
        switch (type)
        {
            // TODO : GET SCORE
            case 0:
                int distance = Minigame5.distance;
                switch (level)
                {
                    case (int)MinigameLevel.EASY:
                        if (distance >= 50)
                        {
                            Finished(4);
                        }
                        break;
                    case (int)MinigameLevel.MEDIUM:
                        if (distance >= 75)
                        {
                            Finished(4);
                        }
                        break;
                    case (int)MinigameLevel.HARD:
                        if (distance >= 100)
                        {
                            Finished(4);
                        }
                        break;
                }
                break;
            // TODO : COLLECTING TOURCHES
            case 1:
                int meat = Minigame5.meat;
                switch (level)
                {
                    case (int)MinigameLevel.EASY:
                        if (meat > 0 && meat <= 50)
                        {
                            Finished(4);
                        }
                        break;
                    case (int)MinigameLevel.MEDIUM:
                        if (meat > 0 && meat <= 40)
                        {
                            Finished(4);
                        }
                        break;
                    case (int)MinigameLevel.HARD:
                        if (meat > 0 && meat <= 25)
                        {
                            Finished(4);
                        }
                        break;
                }
                break;
            default:
                break;
        }
    }
    public void Minigame_6()
    {
        int type = QuestInitialization.Instance.GetCurrentMinigameType(5);
        int level = QuestInitialization.Instance.GetCurrentMinigameLevel(5);
        switch (type)
        {
            // TODO : COLLECTING TOURCHES
            case 0:
                int items = Minigame6.items;
                switch (level)
                {
                    case (int)MinigameLevel.EASY:
                        if (items >= 3)
                        {
                            Finished(5);
                        }
                        break;
                    case (int)MinigameLevel.MEDIUM:
                        if (items >= 4)
                        {
                            Finished(5);
                        }
                        break;
                    case (int)MinigameLevel.HARD:
                        if (items >= 5)
                        {
                            Finished(5);
                        }
                        break;
                }
                break;
            case 1:
                int score = Minigame6.score;
                switch (level)
                {
                    case (int)MinigameLevel.EASY:
                        if (score >= 30)
                        {
                            Finished(5);
                        }
                        break;
                    case (int)MinigameLevel.MEDIUM:
                        if (score >= 50)
                        {
                            Finished(5);
                        }
                        break;
                    case (int)MinigameLevel.HARD:
                        if (score >= 80)
                        {
                            Finished(5);
                        }
                        break;
                }
                break;
            default:
                break;
        }
    }
    public void Minigame_7()
    {
        int type = QuestInitialization.Instance.GetCurrentMinigameType(6);
        int level = QuestInitialization.Instance.GetCurrentMinigameLevel(6);
        switch (type)
        {
            // TODO : GET SCORE
            case 0:
                int items = Minigame7.items;
                switch (level)
                {
                    case (int)MinigameLevel.EASY:
                        if (items >= 2)
                        {
                            Finished(6);
                        }
                        break;
                    case (int)MinigameLevel.MEDIUM:
                        if (items >= 3)
                        {
                            Finished(6);
                        }
                        break;
                    case (int)MinigameLevel.HARD:
                        if (items >= 4)
                        {
                            Finished(6);
                        }
                        break;
                }
                break;
            default:
                break;
        }
    }
    public void Minigame_8()
    {
        int type = QuestInitialization.Instance.GetCurrentMinigameType(7);
        int level = QuestInitialization.Instance.GetCurrentMinigameLevel(7);
        switch (type)
        {
            // TODO : GET SCORE
            case 0:
                float time = Minigame8.time;
                switch (level)
                {
                    case (int)MinigameLevel.EASY:
                        if (time >= 60)
                        {
                            Finished(7);
                        }
                        break;
                    case (int)MinigameLevel.MEDIUM:
                        if (time >= 90)
                        {
                            Finished(7);
                        }
                        break;
                    case (int)MinigameLevel.HARD:
                        if (time >= 120)
                        {
                            Finished(7);
                        }
                        break;
                }
                break;
            // TODO : COLLECTING TOURCHES
            case 1:
                int items = Minigame8.items;
                switch (level)
                {
                    case (int)MinigameLevel.EASY:
                        if (items >= 10)
                        {
                            Finished(7);
                        }
                        break;
                    case (int)MinigameLevel.MEDIUM:
                        if (items >= 20)
                        {
                            Finished(7);
                        }
                        break;
                    case (int)MinigameLevel.HARD:
                        if (items >= 40)
                        {
                            Finished(7);
                        }
                        break;
                }
                break;
            default:
                break;
        }
    }
    public void Minigame_9()
    {
        int type = QuestInitialization.Instance.GetCurrentMinigameType(8);
        int level = QuestInitialization.Instance.GetCurrentMinigameLevel(8);
        switch (type)
        {
            // TODO : GET SCORE
            case 0:
                int comboSnowman = Minigame9.comboSnowman;
                switch (level)
                {
                    case (int)MinigameLevel.EASY:
                        if (comboSnowman >= 30)
                        {
                            Finished(8);
                        }
                        break;
                    case (int)MinigameLevel.MEDIUM:
                        if (comboSnowman >= 40)
                        {
                            Finished(8);
                        }
                        break;
                    case (int)MinigameLevel.HARD:
                        if (comboSnowman >= 50)
                        {
                            Finished(8);
                        }
                        break;
                }
                break;
            // TODO : COLLECTING TOURCHES
            case 1:
                int comboPrincess = Minigame9.comboPrincess;
                int comboSnowman2 = Minigame9.comboSnowman;
                switch (level)
                {
                    case (int)MinigameLevel.EASY:
                        if (comboSnowman2 >= 30)
                        {
                            Finished(8);
                        }
                        break;
                    case (int)MinigameLevel.MEDIUM:
                        if (comboPrincess >= 30)
                        {
                            Finished(8);
                        }
                        break;
                    case (int)MinigameLevel.HARD:
                        if (comboPrincess >= 60)
                        {
                            Finished(8);
                        }
                        break;
                }
                break;
            case 2:
                float time = Minigame9.time;
                switch (level)
                {
                    case (int)MinigameLevel.EASY:
                        if (time >= 30)
                        {
                            Finished(8);
                        }
                        break;
                    case (int)MinigameLevel.MEDIUM:
                        if (time >= 60)
                        {
                            Finished(8);
                        }
                        break;
                    case (int)MinigameLevel.HARD:
                        if (time >= 90)
                        {
                            Finished(8);
                        }
                        break;
                }
                break;
            default:
                break;
        }
    }
}