using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestContent
{
    // TODO: All 9 minigames quest
    public static List<List<Quest>> quests = new List<List<Quest>>()
    {
        #region MINIGAME 1
        new List<Quest>()
        {
            #region TYPE 1
            new Quest
            (
                // TODO : ID
                0, 
                // TODO : Name
                "Minigame 1",
                // TODO : Description
                "Đạt được ít nhất 100 điểm trong một lần chơi.",
                // TODO : Level
                0,
                // TODO : Status
                0,
                // TODO : Minigame
                0,
                // TODO : Type = 0
                0,
                // TODO : Prizes
                50
            ),

            new Quest
            (
                // TODO : ID
                1, 
                // TODO : Name
                "Minigame 1",
                // TODO : Description
                "Đạt được ít nhất 150 điểm trong một lần chơi.",
                // TODO : Level
                1,
                // TODO : Status
                0,
                // TODO : Minigame
                0,
                // TODO : Type = 0
                0,
                // TODO : Prizes
                75
            ),

            new Quest
            (
                // TODO : ID
                2, 
                // TODO : Name
                "Minigame 1",
                // TODO : Description
                "Đạt được ít nhất 200 điểm trong một lần chơi.",
                // TODO : Level
                2,
                // TODO : Status
                0,
                // TODO : Minigame
                0,
                // TODO : Type = 0
                0,
                // TODO : Prizes
                100
            ),
            #endregion
            #region TYPE 2
            new Quest
            (
                // TODO : ID
                3, 
                // TODO : Name
                "Minigame 1",
                // TODO : Description
                "Nhặt ít nhất 5 ngọn đuốc trong một lần chạy.",
                // TODO : Level
                0,
                // TODO : Status
                0,
                // TODO : Minigame
                0,
                // TODO : Type = 1
                1,
                // TODO : Prizes
                50
            ),

            new Quest
            (
                // TODO : ID
                4, 
                // TODO : Name
                "Minigame 1",
                // TODO : Description
                "Nhặt ít nhất 10 ngọn đuốc trong một lần chạy.",
                // TODO : Level
                1,
                // TODO : Status
                0,
                // TODO : Minigame
                0,
                // TODO : Type = 1
                1,
                // TODO : Prizes
                75
            ),

            new Quest
            (
                // TODO : ID
                5, 
                // TODO : Name
                "Minigame 1",
                // TODO : Description
                "Nhặt ít nhất 15 ngọn đuốc trong một lần chạy.",
                // TODO : Level
                2,
                // TODO : Status
                0,
                // TODO : Minigame
                0,
                // TODO : Type = 1
                1,
                // TODO : Prizes
                100
            ),
            #endregion
            #region TYPE 3
            new Quest
            (
                // TODO : ID
                6, 
                // TODO : Name
                "Minigame 1",
                // TODO : Description
                "Giết ít nhất 3 con dơi trong một lần chạy.",
                // TODO : Level
                0,
                // TODO : Status
                0,
                // TODO : Minigame
                0,
                // TODO : Type = 2
                2,
                // TODO : Prizes
                50
            ),

            new Quest
            (
                // TODO : ID
                7, 
                // TODO : Name
                "Minigame 1",
                // TODO : Description
                "Giết ít nhất 5 con dơi trong một lần chạy.",
                // TODO : Level
                1,
                // TODO : Status
                0,
                // TODO : Minigame
                0,
                // TODO : Type = 2
                2,
                // TODO : Prizes
                75
            ),

            new Quest
            (
                // TODO : ID
                8, 
                // TODO : Name
                "Minigame 1",
                // TODO : Description
                "Giết ít nhất 7 con dơi trong một lần chạy.",
                // TODO : Level
                2,
                // TODO : Status
                0,
                // TODO : Minigame
                0,
                // TODO : Type = 2
                2,
                // TODO : Prizes
                100
            )
            #endregion
        },
        #endregion
        #region MINIGAME 2
        new List<Quest>()
        {
            #region TYPE 1
            new Quest
            (
                // TODO : ID
                0, 
                // TODO : Name
                "Minigame 2",
                // TODO : Description
                "Đạt được ít nhất 100 điểm trong một lần chơi.",
                // TODO : Level
                0,
                // TODO : Status
                0,
                // TODO : Minigame
                1,
                // TODO : Type = 0
                0,
                // TODO : Prizes
                50
            ),

            new Quest
            (
                // TODO : ID
                1, 
                // TODO : Name
                "Minigame 2",
                // TODO : Description
                "Đạt được ít nhất 200 điểm trong một lần chơi.",
                // TODO : Level
                1,
                // TODO : Status
                0,
                // TODO : Minigame
                1,
                // TODO : Type = 0
                0,
                // TODO : Prizes
                75
            ),

            new Quest
            (
                // TODO : ID
                2, 
                // TODO : Name
                "Minigame 2",
                // TODO : Description
                "Đạt được ít nhất 400 điểm trong một lần chơi.",
                // TODO : Level
                2,
                // TODO : Status
                0,
                // TODO : Minigame
                1,
                // TODO : Type = 0
                0,
                // TODO : Prizes
                100
            ),
            #endregion
            #region TYPE 2
            new Quest
            (
                // TODO : ID
                3, 
                // TODO : Name
                "Minigame 2",
                // TODO : Description
                "Đánh trúng kẻ địch 3 lần liên tiếp.",
                // TODO : Level
                0,
                // TODO : Status
                0,
                // TODO : Minigame
                1,
                // TODO : Type = 1
                1,
                // TODO : Prizes
                50
            ),

            new Quest
            (
                // TODO : ID
                4, 
                // TODO : Name
                "Minigame 2",
                // TODO : Description
                "Đánh trúng kẻ địch 8 lần liên tiếp.",
                // TODO : Level
                1,
                // TODO : Status
                0,
                // TODO : Minigame
                1,
                // TODO : Type = 1
                1,
                // TODO : Prizes
                75
            ),

            new Quest
            (
                // TODO : ID
                5, 
                // TODO : Name
                "Minigame 2",
                // TODO : Description
                "Đánh trúng kẻ địch 17 lần liên tiếp.",
                // TODO : Level
                2,
                // TODO : Status
                0,
                // TODO : Minigame
                1,
                // TODO : Type = 1
                1,
                // TODO : Prizes
                100
            ),
            #endregion
        },
        #endregion
        #region MINIGAME 3
        new List<Quest>()
        {
            #region TYPE 1
            new Quest
            (
                // TODO : ID
                0, 
                // TODO : Name
                "Minigame 3",
                // TODO : Description
                "Đạt được ít nhất 60 điểm trong một lần chơi.",
                // TODO : Level
                0,
                // TODO : Status
                0,
                // TODO : Minigame
                2,
                // TODO : Type = 0
                0,
                // TODO : Prizes
                50
            ),

            new Quest
            (
                // TODO : ID
                1, 
                // TODO : Name
                "Minigame 3",
                // TODO : Description
                "Đạt được ít nhất 120 điểm trong một lần chơi.",
                // TODO : Level
                1,
                // TODO : Status
                0,
                // TODO : Minigame
                2,
                // TODO : Type = 0
                0,
                // TODO : Prizes
                75
            ),

            new Quest
            (
                // TODO : ID
                2, 
                // TODO : Name
                "Minigame 3",
                // TODO : Description
                "Đạt được ít nhất 150 điểm trong một lần chơi.",
                // TODO : Level
                2,
                // TODO : Status
                0,
                // TODO : Minigame
                2,
                // TODO : Type = 0
                0,
                // TODO : Prizes
                100
            ),
            #endregion
            #region TYPE 2
            new Quest
            (
                // TODO : ID
                3, 
                // TODO : Name
                "Minigame 3",
                // TODO : Description
                "Thu hoạch ít nhất 5 khúc gỗ trong một lần chơi.",
                // TODO : Level
                0,
                // TODO : Status
                0,
                // TODO : Minigame
                2,
                // TODO : Type = 1
                1,
                // TODO : Prizes
                50
            ),

            new Quest
            (
                // TODO : ID
                4, 
                // TODO : Name
                "Minigame 3",
                // TODO : Description
                "Thu hoạch ít nhất 10 khúc gỗ trong một lần chơi.",
                // TODO : Level
                1,
                // TODO : Status
                0,
                // TODO : Minigame
                2,
                // TODO : Type = 1
                1,
                // TODO : Prizes
                75
            ),

            new Quest
            (
                // TODO : ID
                5, 
                // TODO : Name
                "Minigame 3",
                // TODO : Description
                "Thu hoạch ít nhất 15 khúc gỗ trong một lần chơi.",
                // TODO : Level
                2,
                // TODO : Status
                0,
                // TODO : Minigame
                2,
                // TODO : Type = 1
                1,
                // TODO : Prizes
                100
            ),
            #endregion
        },
        #endregion
        #region MINIGAME 4
        new List<Quest>()
        {
            #region TYPE 1
            new Quest
            (
                // TODO : ID
                0, 
                // TODO : Name
                "Minigame 4",
                // TODO : Description
                "Thu thập ít nhất 150 vàng trong một lần chơi.",
                // TODO : Level
                0,
                // TODO : Status
                0,
                // TODO : Minigame
                3,
                // TODO : Type = 0
                0,
                // TODO : Prizes
                50
            ),

            new Quest
            (
                // TODO : ID
                1, 
                // TODO : Name
                "Minigame 4",
                // TODO : Description
                "Thu thập ít nhất 250 vàng trong một lần chơi.",
                // TODO : Level
                1,
                // TODO : Status
                0,
                // TODO : Minigame
                3,
                // TODO : Type = 0
                0,
                // TODO : Prizes
                75
            ),

            new Quest
            (
                // TODO : ID
                2, 
                // TODO : Name
                "Minigame 4",
                // TODO : Description
                "Thu thập ít nhất 350 vàng trong một lần chơi.",
                // TODO : Level
                2,
                // TODO : Status
                0,
                // TODO : Minigame
                3,
                // TODO : Type = 0
                0,
                // TODO : Prizes
                100
            ),
            #endregion
            #region TYPE 2
            new Quest
            (
                // TODO : ID
                3, 
                // TODO : Name
                "Minigame 4",
                // TODO : Description
                "Thu thập ít nhất 10 thú ngọc bội trong một lần chơi.",
                // TODO : Level
                0,
                // TODO : Status
                0,
                // TODO : Minigame
                3,
                // TODO : Type = 1
                1,
                // TODO : Prizes
                50
            ),

            new Quest
            (
                // TODO : ID
                4, 
                // TODO : Name
                "Minigame 4",
                // TODO : Description
                "Thu thập ít nhất 15 thú ngọc bội trong một lần chơi.",
                // TODO : Level
                1,
                // TODO : Status
                0,
                // TODO : Minigame
                3,
                // TODO : Type = 1
                1,
                // TODO : Prizes
                75
            ),

            new Quest
            (
                // TODO : ID
                5, 
                // TODO : Name
                "Minigame 4",
                // TODO : Description
                "Thu thập ít nhất 20 thú ngọc bội trong một lần chơi.",
                // TODO : Level
                2,
                // TODO : Status
                0,
                // TODO : Minigame
                3,
                // TODO : Type = 1
                1,
                // TODO : Prizes
                100
            ),
            #endregion
            #region TYPE 3
            new Quest
            (
                // TODO : ID
                6, 
                // TODO : Name
                "Minigame 4",
                // TODO : Description
                "Tìm được slime băng trong 120 giây đầu.",
                // TODO : Level
                0,
                // TODO : Status
                0,
                // TODO : Minigame
                3,
                // TODO : Type = 1
                2,
                // TODO : Prizes
                50
            ),

            new Quest
            (
                // TODO : ID
                7, 
                // TODO : Name
                "Minigame 4",
                // TODO : Description
                "Tìm được slime băng trong 100 giây đầu.",
                // TODO : Level
                1,
                // TODO : Status
                0,
                // TODO : Minigame
                3,
                // TODO : Type = 1
                2,
                // TODO : Prizes
                75
            ),

            new Quest
            (
                // TODO : ID
                8, 
                // TODO : Name
                "Minigame 4",
                // TODO : Description
                "Tìm được slime băng trong 80 giây đầu.",
                // TODO : Level
                2,
                // TODO : Status
                0,
                // TODO : Minigame
                3,
                // TODO : Type = 1
                2,
                // TODO : Prizes
                100
            )
            #endregion
        },
        #endregion
        #region MINIGAME 5
        new List<Quest>()
        {
            #region TYPE 1
            new Quest
            (
                // TODO : ID
                0, 
                // TODO : Name
                "Minigame 5",
                // TODO : Description
                "Hoàn thành 50% quãng đường.",
                // TODO : Level
                0,
                // TODO : Status
                0,
                // TODO : Minigame
                4,
                // TODO : Type = 0
                0,
                // TODO : Prizes
                50
            ),

            new Quest
            (
                // TODO : ID
                1, 
                // TODO : Name
                "Minigame 5",
                // TODO : Description
                "Hoàn thành 75% quãng đường.",
                // TODO : Level
                1,
                // TODO : Status
                0,
                // TODO : Minigame
                4,
                // TODO : Type = 0
                0,
                // TODO : Prizes
                75
            ),

            new Quest
            (
                // TODO : ID
                2, 
                // TODO : Name
                "Minigame 5",
                // TODO : Description
                "Hoàn thành 100% quãng đường.",
                // TODO : Level
                2,
                // TODO : Status
                0,
                // TODO : Minigame
                4,
                // TODO : Type = 0
                0,
                // TODO : Prizes
                100
            ),
            #endregion
            #region TYPE 2
            new Quest
            (
                // TODO : ID
                3, 
                // TODO : Name
                "Minigame 5",
                // TODO : Description
                "Thắng trò chơi sử dụng tối đa 50 cà rốt.",
                // TODO : Level
                0,
                // TODO : Status
                0,
                // TODO : Minigame
                4,
                // TODO : Type = 1
                1,
                // TODO : Prizes
                50
            ),

            new Quest
            (
                // TODO : ID
                4, 
                // TODO : Name
                "Minigame 5",
                // TODO : Description
                "Thắng trò chơi sử dụng tối đa 40 cà rốt.",
                // TODO : Level
                1,
                // TODO : Status
                0,
                // TODO : Minigame
                4,
                // TODO : Type = 1
                1,
                // TODO : Prizes
                75
            ),

            new Quest
            (
                // TODO : ID
                5, 
                // TODO : Name
                "Minigame 5",
                // TODO : Description
                "Thắng trò chơi sử dụng tối đa 25 cà rốt.",
                // TODO : Level
                2,
                // TODO : Status
                0,
                // TODO : Minigame
                4,
                // TODO : Type = 1
                1,
                // TODO : Prizes
                100
            )
            #endregion
        },
        #endregion
        #region MINIGAME 6
        new List<Quest>()
        {
            #region TYPE 1
            new Quest
            (
                // TODO : ID
                0, 
                // TODO : Name
                "Minigame 6",
                // TODO : Description
                "Nhặt ít nhất 3 vật phẩm trong một lần chơi.",
                // TODO : Level
                0,
                // TODO : Status
                0,
                // TODO : Minigame
                5,
                // TODO : Type = 0
                0,
                // TODO : Prizes
                50
            ),

            new Quest
            (
                // TODO : ID
                1, 
                // TODO : Name
                "Minigame 6",
                // TODO : Description
                "Nhặt ít nhất 4 vật phẩm trong một lần chơi.",
                // TODO : Level
                1,
                // TODO : Status
                0,
                // TODO : Minigame
                5,
                // TODO : Type = 0
                0,
                // TODO : Prizes
                75
            ),

            new Quest
            (
                // TODO : ID
                2, 
                // TODO : Name
                "Minigame 6",
                // TODO : Description
                "Nhặt ít nhất 5 vật phẩm trong một lần chơi.",
                // TODO : Level
                2,
                // TODO : Status
                0,
                // TODO : Minigame
                5,
                // TODO : Type = 0
                0,
                // TODO : Prizes
                100
            ),
            #endregion
            #region TYPE 2
            new Quest
            (
                // TODO : ID
                3, 
                // TODO : Name
                "Minigame 6",
                // TODO : Description
                "Đạt được ít nhất 30 điểm trong một lần chơi.",
                // TODO : Level
                0,
                // TODO : Status
                0,
                // TODO : Minigame
                5,
                // TODO : Type = 2
                1,
                // TODO : Prizes
                50
            ),

            new Quest
            (
                // TODO : ID
                4, 
                // TODO : Name
                "Minigame 6",
                // TODO : Description
                "Đạt được ít nhất 50 điểm trong một lần chơi.",
                // TODO : Level
                1,
                // TODO : Status
                0,
                // TODO : Minigame
                5,
                // TODO : Type = 2
                1,
                // TODO : Prizes
                75
            ),

            new Quest
            (
                // TODO : ID
                5, 
                // TODO : Name
                "Minigame 6",
                // TODO : Description
                "Đạt được ít nhất 80 điểm trong một lần chơi.",
                // TODO : Level
                2,
                // TODO : Status
                0,
                // TODO : Minigame
                5,
                // TODO : Type = 2
                1,
                // TODO : Prizes
                100
            )
            #endregion
        },
        #endregion
        #region MINIGAME 7
        new List<Quest>()
        {
            #region TYPE 1
            new Quest
            (
                // TODO : ID
                0, 
                // TODO : Name
                "Minigame 7",
                // TODO : Description
                "Giao hàng thành công 2 ngôi nhà.",
                // TODO : Level
                0,
                // TODO : Status
                0,
                // TODO : Minigame
                6,
                // TODO : Type = 0
                0,
                // TODO : Prizes
                50
            ),

            new Quest
            (
                // TODO : ID
                1, 
                // TODO : Name
                "Minigame 7",
                // TODO : Description
                "Giao hàng thành công 3 ngôi nhà.",
                // TODO : Level
                1,
                // TODO : Status
                0,
                // TODO : Minigame
                6,
                // TODO : Type = 0
                0,
                // TODO : Prizes
                75
            ),

            new Quest
            (
                // TODO : ID
                2, 
                // TODO : Name
                "Minigame 7",
                // TODO : Description
                "Giao hàng thành công 4 ngôi nhà.",
                // TODO : Level
                2,
                // TODO : Status
                0,
                // TODO : Minigame
                6,
                // TODO : Type = 0
                0,
                // TODO : Prizes
                100
            )
            #endregion
        },
        #endregion
        #region MINIGAME 8
        new List<Quest>()
        {
            #region TYPE 1
            new Quest
            (
                // TODO : ID
                0, 
                // TODO : Name
                "Minigame 8",
                // TODO : Description
                "Sống sót trong 1 phút.",
                // TODO : Level
                0,
                // TODO : Status
                0,
                // TODO : Minigame
                7,
                // TODO : Type = 0
                0,
                // TODO : Prizes
                50
            ),

            new Quest
            (
                // TODO : ID
                1, 
                // TODO : Name
                "Minigame 8",
                // TODO : Description
                "Sống sót trong 1 phút 30 giây.",
                // TODO : Level
                1,
                // TODO : Status
                0,
                // TODO : Minigame
                7,
                // TODO : Type = 0
                0,
                // TODO : Prizes
                75
            ),

            new Quest
            (
                // TODO : ID
                2, 
                // TODO : Name
                "Minigame 8",
                // TODO : Description
                "Sống sót trong 2 phút.",
                // TODO : Level
                2,
                // TODO : Status
                0,
                // TODO : Minigame
                7,
                // TODO : Type = 0
                0,
                // TODO : Prizes
                100
            ),
            #endregion
            #region TYPE 2
            new Quest
            (
                // TODO : ID
                3, 
                // TODO : Name
                "Minigame 8",
                // TODO : Description
                "Thu thập ít nhất 10 thú ngọc bội trong một lần chơi.",
                // TODO : Level
                0,
                // TODO : Status
                0,
                // TODO : Minigame
                7,
                // TODO : Type = 1
                1,
                // TODO : Prizes
                50
            ),

            new Quest
            (
                // TODO : ID
                4, 
                // TODO : Name
                "Minigame 8",
                // TODO : Description
                "Thu thập ít nhất 20 thú ngọc bội trong một lần chơi.",
                // TODO : Level
                1,
                // TODO : Status
                0,
                // TODO : Minigame
                7,
                // TODO : Type = 1
                1,
                // TODO : Prizes
                75
            ),

            new Quest
            (
                // TODO : ID
                5, 
                // TODO : Name
                "Minigame 8",
                // TODO : Description
                "Thu thập ít nhất 40 thú ngọc bội trong một lần chơi.",
                // TODO : Level
                2,
                // TODO : Status
                0,
                // TODO : Minigame
                7,
                // TODO : Type = 1
                1,
                // TODO : Prizes
                100
            )
            #endregion
        },
        #endregion
        #region MINIGAME 9
        new List<Quest>()
        {
            #region TYPE 1
            new Quest
            (
                // TODO : ID
                0, 
                // TODO : Name
                "Minigame 9",
                // TODO : Description
                "Người tuyết đạt 30 combo.",
                // TODO : Level
                0,
                // TODO : Status
                0,
                // TODO : Minigame
                8,
                // TODO : Type = 0
                0,
                // TODO : Prizes
                50
            ),

            new Quest
            (
                // TODO : ID
                1, 
                // TODO : Name
                "Minigame 9",
                // TODO : Description
                "Người tuyết đạt 40 combo.",
                // TODO : Level
                1,
                // TODO : Status
                0,
                // TODO : Minigame
                8,
                // TODO : Type = 0
                0,
                // TODO : Prizes
                75
            ),

            new Quest
            (
                // TODO : ID
                2, 
                // TODO : Name
                "Minigame 9",
                // TODO : Description
                "Người tuyết đạt 50 combo.",
                // TODO : Level
                2,
                // TODO : Status
                0,
                // TODO : Minigame
                8,
                // TODO : Type = 0
                0,
                // TODO : Prizes
                100
            ),

            #endregion
            #region TYPE 2
            new Quest
            (
                // TODO : ID
                3, 
                // TODO : Name
                "Minigame 9",
                // TODO : Description
                "Người tuyết đạt 30 combo.",
                // TODO : Level
                0,
                // TODO : Status
                0,
                // TODO : Minigame
                8,
                // TODO : Type = 1
                1,
                // TODO : Prizes
                50
            ),

            new Quest
            (
                // TODO : ID
                4, 
                // TODO : Name
                "Minigame 9",
                // TODO : Description
                "Công chúa đạt 30 combo.",
                // TODO : Level
                1,
                // TODO : Status
                0,
                // TODO : Minigame
                8,
                // TODO : Type = 1
                1,
                // TODO : Prizes
                75
            ),

            new Quest
            (
                // TODO : ID
                5, 
                // TODO : Name
                "Minigame 9",
                // TODO : Description
                "Công chúa đạt 60 combo.",
                // TODO : Level
                2,
                // TODO : Status
                0,
                // TODO : Minigame
                8,
                // TODO : Type = 1
                1,
                // TODO : Prizes
                100
            ),
            #endregion
            #region TYPE 3
            new Quest
            (
                // TODO : ID
                6, 
                // TODO : Name
                "Minigame 9",
                // TODO : Description
                "Không trượt bất kì phím nào trong 30 giây đầu.",
                // TODO : Level
                0,
                // TODO : Status
                0,
                // TODO : Minigame
                8,
                // TODO : Type = 1
                2,
                // TODO : Prizes
                50
            ),

            new Quest
            (
                // TODO : ID
                7, 
                // TODO : Name
                "Minigame 9",
                // TODO : Description
                "Không trượt bất kì phím nào trong 60 giây đầu.",
                // TODO : Level
                1,
                // TODO : Status
                0,
                // TODO : Minigame
                8,
                // TODO : Type = 1
                2,
                // TODO : Prizes
                75
            ),

            new Quest
            (
                // TODO : ID
                8, 
                // TODO : Name
                "Minigame 9",
                // TODO : Description
                "Không trượt bất kì phím nào trong 90 giây đầu.",
                // TODO : Level
                2,
                // TODO : Status
                0,
                // TODO : Minigame
                8,
                // TODO : Type = 1
                2,
                // TODO : Prizes
                100
            )
            #endregion
        },
        #endregion
        #region BIG QUEST
        new List<Quest>()
        {
            new Quest
            (
                // TODO : ID
                0, 
                // TODO : Name
                "Nhiệm vụ lớn",
                // TODO : Description
                "Hoàn thành tất cả nhiệm vụ cấp độ dễ.",
                // TODO : Level
                0,
                // TODO : Status
                0,
                // TODO : Minigame => Used to identify as a big quest
                -10,
                // TODO : Type = 0
                0,
                // TODO : Prizes
                1
            ),

            new Quest
            (
                // TODO : ID
                1, 
                // TODO : Name
                "Nhiệm vụ lớn",
                // TODO : Description
                "Hoàn thành tất cả nhiệm vụ cấp độ trung bình.",
                // TODO : Level
                1,
                // TODO : Status
                0,
                // TODO : Minigame => Used to identify as a big quest
                -10,
                // TODO : Type = 1
                1,
                // TODO : Prizes
                1
            ),

            new Quest
            (
                // TODO : ID
                2, 
                // TODO : Name
                "Nhiệm vụ lớn",
                // TODO : Description
                "Hoàn thành tất cả nhiệm vụ cấp độ khó.",
                // TODO : Level
                2,
                // TODO : Status
                0,
                // TODO : Minigame => Used to identify as a big quest
                -10,
                // TODO : Type = 2
                2,
                // TODO : Prizes
                1
            )
        }
        #endregion
    };
}
