using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestDemoData : MonoBehaviour
{
    [Header("Data Demo")]
    public List<Quest> questsData = new List<Quest>();
}
