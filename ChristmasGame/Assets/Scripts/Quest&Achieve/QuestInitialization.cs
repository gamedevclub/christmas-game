// using System;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestInitialization : MonoBehaviour
{
    public bool isDayPassed = false;
    public static QuestInitialization Instance { get; set; }
    QuestDemoData data;
    void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        // TestResponsiveSwitchingStatus();
    }

    public void Load()
    {
        Initialize();
        CheckAllQuest();
    }

    public static void InitialzeQuestList()
    {
        Minigame1.Reset();
        Minigame2.Reset();
        Minigame3.Reset();
        Minigame4.Reset();
        Minigame5.Reset();
        Minigame6.Reset();
        Minigame7.Reset();
        Minigame8.Reset();
        Minigame9.Reset();

        DataManager.instance.SaveMinigameData();
        
        DataManager.instance.IngameData.questList = GenerateRandomQuestsFromData();
        for (int i = 0; i < DataManager.instance.IngameData.questCount.Count; i++)
        {
            DataManager.instance.IngameData.questCount[i] = 0;
        }
    }

    public void Initialize()
    {
        // ? This is test for quest only, reach this scene from Home may cause bugs
        // ? Remove when the system goes well
        // ? This line :
        // DataManager.instance.IngameData.questList = new List<Quest>();
        QuestManager.Instance.questObjects = new List<GameObject>();
        Helper7.DeletChildren(QuestManager.Instance.contentQuests.transform);
        // TODO: If this is new day
        // if (IsDayPassed())
        // {
        //     InitialzeQuestList();
        // }
        foreach (Quest quest in DataManager.instance.IngameData.questList)
        {
            if (quest.minigame == -10)
                break;
            GameObject questObject = Instantiate(QuestManager.Instance.questTemplate, QuestManager.Instance.contentQuests.transform);
            questObject.GetComponent<QuestDisplay>().SetValue(quest);
            QuestManager.Instance.questObjects.Add(questObject);
        }
        // print(DataManager.instance.IngameData.questList.Count);
        GameObject bigQuestObject = Instantiate(QuestManager.Instance.bigQuestTemplate, QuestManager.Instance.contentBigQuest.transform);
        bigQuestObject.GetComponent<QuestDisplay>().SetValue(DataManager.instance.IngameData.questList.Find(q => q.minigame == -10));
        QuestManager.Instance.questObjects.Add(bigQuestObject);
        DataManager.instance.SaveQuest();
    }

    /// <summary>
    /// * Generate a 9-random-easy-quest when starting new day
    /// TODO: Generate 9 quests with easy level
    /// </summary>
    /// <returns></returns>
    public static List<Quest> GenerateRandomQuestsFromData()
    {
        List<Quest> result = new List<Quest>();
        for (int i = 0; i < 9; i++)
        {
            List<Quest> easyQuestLevel = QuestContent.quests[i].FindAll(quest => quest.level == 0);
            result.Add(easyQuestLevel[UnityEngine.Random.Range(0, easyQuestLevel.Count)]);
        }
        result.Add(QuestContent.quests[9].Find(quest => quest.level == 0));
        return result;
    }

    /// <summary>
    /// * Get a quest with the given minigame and level
    /// </summary>
    /// <param name="_minigame"></param>
    /// <param name="_level"></param>
    /// <returns></returns>
    public Quest GetRandomQuestFromData(int _minigame, int _level)
    {
        List<Quest> result = new List<Quest>();
        if (0 <= _level && _level < 3 && 0 <= _minigame && _minigame < 9)
        {
            List<Quest> quests = QuestContent.quests[_minigame].FindAll(quest => quest.level == _level);
            return quests[UnityEngine.Random.Range(0, quests.Count)];
        }
        else
        {
            Debug.LogError("Invalid parameter for QuestInitialization::GenerateRandomQuestFromData(int, int)");
            return null;
        }
    }

    public Quest GetBigQuestFromData(int _level)
    {
        Quest result = null;
        result = QuestContent.quests[9].Find(q => q.level == _level);
        return result;
    }

    /// <summary>
    /// * Update the quest to the next level or remove them if it is the highest level already
    /// * Call when clicking the finish button
    /// </summary>
    /// <param name="_questDisplay"></param>
    public void UpdateQuestLevel(QuestDisplay _questDisplay)
    {
        print("Minigame : " + _questDisplay.quest.minigame.ToString() + ", Level : " + _questDisplay.quest.level.ToString());
        int currentLevel = _questDisplay.quest.level;

        ResetMinigameValue(_questDisplay.quest.minigame);
        DataManager.instance.IngameData.questCount[currentLevel]++;
        
        AwardManager.instance.GetQuestAward(_questDisplay.quest.prizes);

        currentLevel++;
        switch (currentLevel)
        {
            // TODO: If next level is medium or hard => Update
            case 1:
            case 2:
                Quest nextQuest = GetRandomQuestFromData(_questDisplay.quest.minigame, currentLevel);
                DataManager.instance.IngameData.questList[DataManager.instance.IngameData.questList.FindIndex(q => q.minigame == _questDisplay.quest.minigame)] = nextQuest;
                // TODO: To be remove for save load
                // * FindObjectOfType<QuestDemoData>().questsData[_questDisplay.quest.minigame] = nextQuest;
                _questDisplay.SetValue(nextQuest);
                break;
            // TODO: If next level is beyond hard => remove from list
            case 3:
                // TODO: Finished all quests for this minigame
                Quest deletedQuest = DataManager.instance.IngameData.questList.Find(q => q.minigame == _questDisplay.quest.minigame);
                int indexOfDeletedQuest = DataManager.instance.IngameData.questList.IndexOf(deletedQuest);
                DataManager.instance.IngameData.questList.RemoveAt(indexOfDeletedQuest);
                // TODO: To be remove for save load
                // * FindObjectOfType<QuestDemoData>().questsData.RemoveAt(_questDisplay.quest.minigame);
                QuestManager.Instance.questObjects.Remove(_questDisplay.gameObject);
                Destroy(_questDisplay.gameObject);
                break;
            default:
                Debug.LogError("Something went wrong in QuestInitialization::UpdateQuestLevel(QuestDisplay)");
                break;
        }
        QuestManager.Instance.BigQuest();
        // TODO: Save quest afterward
        DataManager.instance.SaveQuest();
    }

    public void UpdateBigQuestLevel(QuestDisplay _questDisplay)
    {
        print("Big quest : " + _questDisplay.quest.minigame.ToString() + ", Level : " + _questDisplay.quest.level.ToString());
        int currentLevel = _questDisplay.quest.level;

        AwardManager.instance.GetQuestChest(currentLevel);

        currentLevel++;
        switch (currentLevel)
        {
            // TODO: If next level is medium or hard => Update
            case 1:
            case 2:
                Quest nextQuest = GetBigQuestFromData(currentLevel);
                DataManager.instance.IngameData.questList[DataManager.instance.IngameData.questList.FindIndex(q => q.minigame == -10)] = nextQuest;
                _questDisplay.SetValue(nextQuest);
                break;
            // TODO: If next level is beyond hard => remove from list
            case 3:
                // TODO: Keep the hardest quest
                // TODO: Call something in QuestDisplay to notice player
                _questDisplay.NoticeBigQuest();
                break;
            default:
                Debug.LogError("Something went wrong in QuestInitialization::UpdateQuestLevel(QuestDisplay)");
                break;
        }
        print("Current lv big quest : " + DataManager.instance.IngameData.questList.Find(q => q.minigame == -10).level);
        // TODO: Save quest afterward
        DataManager.instance.SaveQuest();
    }

    public void ResetMinigameValue(int minigame = -1)
    {
        switch (minigame)
        {
            case 0:
                Minigame1.Reset();
                break;
            case 1:
                Minigame2.Reset();
                break;
            case 2:
                Minigame3.Reset();
                break;
            case 3:
                Minigame4.Reset();
                break;
            case 4:
                Minigame5.Reset();
                break;
            case 5:
                Minigame6.Reset();
                break;
            case 6:
                Minigame7.Reset();
                break;
            case 7:
                Minigame8.Reset();
                break;
            case 8:
                Minigame9.Reset();
                break;
            default:
                break;
        }
        DataManager.instance.SaveMinigameData();
    }

    public int GetCurrentMinigameType(int _minigame)
    {
        int result = -1;
        if (QuestManager.Instance.questObjects.Find(qo => qo.GetComponent<QuestDisplay>().quest.minigame == _minigame))
        {
            result = QuestManager.Instance.questObjects.Find(
                qo => qo.GetComponent<QuestDisplay>().quest.minigame == _minigame
            ).GetComponent<QuestDisplay>().quest.type;
        }
        return result;
    }

    public int GetCurrentMinigameLevel(int _minigame)
    {
        int result = -1;
        if (QuestManager.Instance.questObjects.Find(qo => qo.GetComponent<QuestDisplay>().quest.minigame == _minigame))
        {
            result = QuestManager.Instance.questObjects.Find(
                qo => qo.GetComponent<QuestDisplay>().quest.minigame == _minigame
            ).GetComponent<QuestDisplay>().quest.level;
        }
        return result;
    }

    public bool IsDayPassed()
    {
        int currentday = DateTime.Now.Day;
        int lastday = DataManager.instance.IngameData.timeSave.lastSaveTime.Day;

        return lastday - currentday != 0 || DataManager.instance.firstPlay == true;
    }

    public void CheckAllQuest()
    {
        if (DataManager.instance.IngameData.questCount[0] == 10 &&
            DataManager.instance.IngameData.questCount[1] == 10 &&
            DataManager.instance.IngameData.questCount[2] == 10)
        {
            QuestManager.Instance.questObjects.Find(qo => qo.GetComponent<QuestDisplay>().quest.minigame == -10).GetComponent<QuestDisplay>().NoticeBigQuest();
            return;
        }
        QuestManager.Instance.Minigame_1();
        QuestManager.Instance.Minigame_2();
        QuestManager.Instance.Minigame_3();
        QuestManager.Instance.Minigame_4();
        QuestManager.Instance.Minigame_5();
        QuestManager.Instance.Minigame_6();
        QuestManager.Instance.Minigame_7();
        QuestManager.Instance.Minigame_8();
        QuestManager.Instance.Minigame_9();
        QuestManager.Instance.BigQuest();

        // CheckBigQuest();

        DataManager.instance.SaveQuest();
    }

    public void CheckBigQuest()
    {
        if (DataManager.instance.IngameData.questCount[0] == 10 &&
            DataManager.instance.IngameData.questCount[1] == 10 &&
            DataManager.instance.IngameData.questCount[2] == 10)
        {
            QuestManager.Instance.questObjects.Find(qo => qo.GetComponent<QuestDisplay>().quest.minigame == -10).GetComponent<QuestDisplay>().NoticeBigQuest();
        }
    }

    /// <summary>
    /// * Used to test the reponse of UI when QuestDisplay::Finished() is called to mark
    /// * the finished task
    /// ! Put it in Update() to test if needed
    /// </summary>
    public void TestResponsiveSwitchingStatus()
    {
        if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            QuestManager.Instance.Finished(0);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            QuestManager.Instance.Finished(1);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            QuestManager.Instance.Finished(2);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            QuestManager.Instance.Finished(3);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            QuestManager.Instance.Finished(4);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            QuestManager.Instance.Finished(5);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            QuestManager.Instance.Finished(6);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            QuestManager.Instance.Finished(7);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha8))
        {
            QuestManager.Instance.Finished(8);
        }
    }
}
