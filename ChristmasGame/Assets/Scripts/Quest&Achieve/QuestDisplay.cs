using System.Net.Sockets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestDisplay : MonoBehaviour
{

    public GameObject nameText;
    public GameObject descriptionText;
    public GameObject levels;
    public GameObject status;
    public GameObject prize;
    public string statusOnGoingMessage;
    public Color statusOnGoingColor;
    public string statusFinishedMessage;
    public Color statusFinishedColor;
    public Quest quest;
    [Header("Used for big quest only")]
    public GameObject activeContent;
    public GameObject notificationPanel;

    public void SetValue(int _id, string _name, string _description, int _level, int _status, int _minigame, int _type, int _prizes = -1)
    {
        quest = new Quest(_id, _name, _description, _level, _status, _minigame, _type, _prizes);
        nameText.GetComponent<TMPro.TextMeshProUGUI>().text = _name;
        descriptionText.GetComponent<TMPro.TextMeshProUGUI>().text = _description;

        switch (_level)
        {
            case 0:
                levels.transform.GetChild(0).gameObject.SetActive(true);
                break;
            case 1:
                levels.transform.GetChild(0).gameObject.SetActive(true);
                levels.transform.GetChild(1).gameObject.SetActive(true);
                break;
            case 2:
                levels.transform.GetChild(0).gameObject.SetActive(true);
                levels.transform.GetChild(1).gameObject.SetActive(true);
                levels.transform.GetChild(2).gameObject.SetActive(true);
                break;
            default:
                break;
        }

        if (_status == 0)
        {
            status.GetComponentInParent<Image>().color = statusOnGoingColor;
            status.GetComponent<TMPro.TextMeshProUGUI>().text = statusOnGoingMessage;
        }
        else if (_status == 1)
        {
            status.GetComponentInParent<Image>().color = statusFinishedColor;
            status.GetComponent<TMPro.TextMeshProUGUI>().text = statusFinishedMessage;
        }

        prize.GetComponent<TMPro.TextMeshProUGUI>().text = "x" + _prizes;
    }
    public void SetValue(Quest _quest)
    {
        quest = _quest;
        nameText.GetComponent<TMPro.TextMeshProUGUI>().text = _quest.name;
        descriptionText.GetComponent<TMPro.TextMeshProUGUI>().text = _quest.description;

        switch (_quest.level)
        {
            case 0:
                levels.transform.GetChild(0).gameObject.SetActive(true);
                break;
            case 1:
                levels.transform.GetChild(0).gameObject.SetActive(true);
                levels.transform.GetChild(1).gameObject.SetActive(true);
                break;
            case 2:
                levels.transform.GetChild(0).gameObject.SetActive(true);
                levels.transform.GetChild(1).gameObject.SetActive(true);
                levels.transform.GetChild(2).gameObject.SetActive(true);
                break;
            default:
                break;
        }

        if (_quest.status == 0)
        {
            status.GetComponentInParent<Image>().color = statusOnGoingColor;
            status.GetComponent<TMPro.TextMeshProUGUI>().text = statusOnGoingMessage;
        }
        else if (_quest.status == 1)
        {
            status.GetComponentInParent<Image>().color = statusFinishedColor;
            status.GetComponent<TMPro.TextMeshProUGUI>().text = statusFinishedMessage;
        }
        prize.GetComponent<TMPro.TextMeshProUGUI>().text = "x" + _quest.prizes;

        if (_quest.prizes == 1)
        {
            if (_quest.level == 0)
                prize.transform.parent.GetComponentInChildren<Image>().sprite = QuestManager.Instance.closeB;
            else if (_quest.level == 1)
                prize.transform.parent.GetComponentInChildren<Image>().sprite = QuestManager.Instance.closeA;
            else if (_quest.level == 2)
                prize.transform.parent.GetComponentInChildren<Image>().sprite = QuestManager.Instance.closeS;
            Debug.Log("Big quest here");
        }
    }
    public void Claim()
    {
        // TODO: Claim award
        QuestInitialization.Instance.UpdateQuestLevel(this);
    }

    public void ClaimBigQuest()
    {
        // TODO: Claim award from big quest
        int level = QuestInitialization.Instance.GetCurrentMinigameLevel(-10);
        DataManager.instance.IngameData.questCount[level] = 10;
        QuestInitialization.Instance.UpdateBigQuestLevel(this);
        DataManager.instance.SaveQuest();
    }

    public void NoticeBigQuest()
    {
        activeContent.SetActive(false);
        notificationPanel.SetActive(true);
    }

    public void Finished()
    {
        quest.status = 1;
        status.GetComponentInParent<Image>().color = statusFinishedColor;
        status.GetComponent<TMPro.TextMeshProUGUI>().text = statusFinishedMessage;
    }
}
