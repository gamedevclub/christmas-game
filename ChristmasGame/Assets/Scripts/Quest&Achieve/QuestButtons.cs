using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class QuestButtons : MonoBehaviour
{
    public GameObject questPanel;
    public void OnFinishedButtonClick()
    {
        if (GetComponent<QuestDisplay>().quest.status == 0)
            return;
        GetComponent<QuestDisplay>().Claim();
        AudioManager.instance.GetQuest();
    }

    public void OnFinishedBigQuestButtonClick()
    {
        if (GetComponent<QuestDisplay>().quest.status == 0)
            return;
        GetComponent<QuestDisplay>().ClaimBigQuest();
    }

    public void OnTestSceneButtonClick()
    {
        SceneManager.LoadScene("Quest Test");
    }

    public void OnOpenQuestPanel()
    {
        questPanel.SetActive(true);
        QuestInitialization.Instance.Load();
    }

    public void OnCloseQuestPanel()
    {
        questPanel.SetActive(false);
    }
}
