public enum QuestState
{
    ONGOING = 0,
    FINISHED = 1
}

public enum MinigameLevel
{
    EASY,
    MEDIUM,
    HARD
}
