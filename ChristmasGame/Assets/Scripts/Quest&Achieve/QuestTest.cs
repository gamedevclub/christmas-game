using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class QuestTest : MonoBehaviour
{
    void Start()
    {
        print("TestMinigame1");
        Minigame1.score += 5000;
        Minigame1.tourches += 100;
        Minigame1.bats += 100;
        print("TestMinigame2");
        Minigame2.score += 500;
        Minigame2.hitTime += 500;
        print("TestMinigame3");
        Minigame3.score += 500;
        Minigame3.logs += 100;
        Minigame3.balanceTime += 500;
        print("TestMinigame4");
        Minigame4.gold = 1;
        Minigame4.exp = 1;
        print("TestMinigame5");
        Minigame5.distance = 5;
        Minigame5.meat = 1;
        print("TestMinigame6");
        Minigame6.armors += 10;
        Minigame6.items += 10;
        Minigame6.score += 500;
        print("TestMinigame7");
        Minigame7.items = 4;
        print("TestMinigame8");
        Minigame8.time += 500;
        Minigame8.items += 500;
        print("TestMinigame9");
        Minigame9.comboPrincess += 500;
        Minigame9.comboSnowman += 500;
        Minigame9.time += 500;
        DataManager.instance.SaveMinigameData();
    }
    // Update is called once per frame
    void Update()
    {
        // TestMinigame();
    }

    public void TestInitializeMinigame1()
    {
        Minigame1.Reset();
    }

    public void TestInitializeMinigame2()
    {
        Minigame2.Reset();
    }

    public void TestInitializeMinigame3()
    {
        Minigame3.Reset();
    }

    public void TestInitializeMinigame4()
    {
        Minigame4.Reset();
    }
    public void TestInitializeMinigame5()
    {
        Minigame5.Reset();
    }
    public void TestInitializeMinigame6()
    {
        Minigame6.Reset();
    }
    public void TestInitializeMinigame7()
    {
        Minigame7.Reset();
    }
    public void TestInitializeMinigame8()
    {
        Minigame8.Reset();
    }
    public void TestInitializeMinigame9()
    {
        Minigame9.Reset();
    }

    public void TestMinigame()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            print("TestMinigame1");
            Minigame1.score += 500;
            Minigame1.tourches += 100;
            Minigame1.bats += 100;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            print("TestMinigame2");
            Minigame2.score += 500;
            Minigame2.hitTime += 500;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            print("TestMinigame3");
            Minigame3.score += 500;
            Minigame3.logs += 100;
            Minigame3.balanceTime += 500;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            print("TestMinigame4");
            Minigame4.gold = 1;
            Minigame4.exp = 1;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            print("TestMinigame5");
            Minigame5.distance = 5;
            Minigame5.meat = 1;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            print("TestMinigame6");
            Minigame6.armors += 10;
            Minigame6.items += 10;
            Minigame6.score += 500;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            print("TestMinigame7");
            Minigame7.items = 4;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha8))
        {
            print("TestMinigame8");
            Minigame8.time += 500;
            Minigame8.items += 500;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha9))
        {
            print("TestMinigame9");
            Minigame9.comboPrincess += 500;
            Minigame9.comboSnowman += 500;
            Minigame9.time += 500;
        }
        DataManager.instance.SaveMinigameData();
    }


    public void OnQuestButtonClick()
    {
        SceneManager.LoadScene("Quest&Achieve");
    }
}
