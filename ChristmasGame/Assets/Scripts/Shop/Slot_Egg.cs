﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Slot_Egg : MonoBehaviour
{

    public Sprite starr;
    private string Nameit;
    private int level;
    private int price;
    private Sprite img;
    public void Setvalue(string Nameit, int level, Sprite image, int price)
    {
        this.Nameit = Nameit;
        this.level = level;
        this.img = image;
        this.price = price;
        display();
    }
    void display()
    {
        TextMeshProUGUI[] info = gameObject.GetComponentsInChildren<TextMeshProUGUI>();
        info[1].text = Nameit;
        handleLevel(level + 1);
        info[0].text = price.ToString();
        gameObject.GetComponentsInChildren<Image>()[2].sprite = img;
    }

    public string handleLevel(int i)
    {
        Image[] star = gameObject.GetComponentsInChildren<Image>();
        switch (i)
        {
            case 1:
                {
                    star[3].sprite = starr;
                    star[4].enabled = false;
                    star[5].enabled = false;
                    //star[6].enabled = false;
                    //star[7].enabled = false;
                    return "Trứng cấp N";
                }
            case 2:
                {
                    star[3].sprite = starr;
                    star[4].sprite = starr;
                    star[5].enabled = false;
                    //star[6].enabled = false;
                    //star[7].enabled = false;
                    return "Trứng cấp R";
                }
            case 3:
                {
                    star[3].sprite = starr;
                    star[4].sprite = starr;
                    star[5].sprite = starr;
                    //star[6].enabled = false;
                    //star[7].enabled = false;
                    return "Trứng cấp SR";
                }
            //case 4:
            //    {
            //        star[3].sprite = starr;
            //        star[4].sprite = starr;
            //        star[5].sprite = starr;
            //        star[6].sprite = starr;
            //        star[7].enabled = false;
            //        return "SSR";
            //    }
            //case 5:
            //    {
            //        star[3].sprite = starr;
            //        star[4].sprite = starr;
            //        star[5].sprite = starr;
            //        star[6].sprite = starr;
            //        star[7].sprite = starr;
            //        return "UR";
            //    }
            default: return "";
        };
    }
}
