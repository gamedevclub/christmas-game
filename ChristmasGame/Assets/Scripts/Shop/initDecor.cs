using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class initDecor : MonoBehaviour
{
    public Transform DecoPos;
    public Transform pnlPos;
    public GameObject pnlBuy;
    public GameObject Slot;


    public void Load()
    {
        //Debug.Log(ShopUIManager.instance.DecorIt.Count);

        for (int i = 0; i < ShopUIManager.instance.DecorIt.Count; i++)
        {
            int x = i;
            GameObject pnl = Instantiate(Slot, DecoPos);
            pnl.GetComponent<Slot_Buy>().Setvalue(ShopUIManager.instance.DecorIt[i].name,
                                    -1,
                                    ShopUIManager.instance.DecorIt[i].level,
                                    Resources.Load<Sprite>(ShopUIManager.instance.DecorIt[i].art),
                                    ShopUIManager.instance.DecorIt[i].price);

            pnl.GetComponentInChildren<Button>().onClick.AddListener(delegate { clickBuy(x); });
        }
    }

    public void clickBuy(int index)
    {
        GameObject pnlBuyItem = Instantiate(pnlBuy, pnlPos);
        pnlBuyItem.GetComponent<SetValueForPnlBuy>().SetValue(
            Resources.Load<Sprite>(ShopUIManager.instance.DecorIt[index].art),
            ShopUIManager.instance.DecorIt[index].name,
            ShopUIManager.instance.DecorIt[index].description,
            ShopUIManager.instance.DecorIt[index].price,
            ShopUIManager.instance.DecorIt[index].level
        );
        // Handle click
        Button[] button = pnlBuyItem.GetComponentsInChildren<Button>();
        button[0].onClick.AddListener(delegate { Destroy(pnlBuyItem); });
        button[1].onClick.AddListener(delegate { BuyItem(pnlBuyItem, index); });

    }
    public void BuyItem(GameObject pnlBuyItem, int index)
    {
        string quantityText = pnlBuyItem.GetComponentInChildren<InputField>().text;
        if(quantityText != "")
        {
            int quantitybuy = Int32.Parse(quantityText);
            //Debug.Log(quantitybuy);
            if (!ShopUIManager.instance.checkEnoughCoin(quantitybuy * ShopUIManager.instance.DecorIt[index].price))
            {
                //Debug.Log("Not enough");
                GameManager.instance.Alert("Hong đủ tiền rồi :(");
                return;
            }
            ShopUIManager.instance.PayForPurchase(quantitybuy * ShopUIManager.instance.DecorIt[index].price);
            ShopUIManager.instance.pushItemDecor(ShopUIManager.instance.DecorIt[index], quantitybuy);
            Destroy(pnlBuyItem);
        }
    }
}
