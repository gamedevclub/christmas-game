using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetValueForPnlBuy : MonoBehaviour
{
    // Start is called before the first frame update
    public Image imageItem;
    public Text nameItem;
    public Text description;
    public Text pricetxt;
    public Image star0;
    public Image star1;
    public Image star2;
    public Image star3;
    public Sprite starr;
   public void SetValue(Sprite image,string name,string desc,int price,int level)
   {
       imageItem.sprite = image;
       nameItem.text = name;
       description.text = desc;
       pricetxt.text = "Giá: "+ price.ToString();
       HandleStar(level);
   }
    public string HandleStar(int level)
    {
        switch (level)
        {
            case 0:
                {
                    star0.sprite = starr;
                    star1.enabled = false;
                    star2.enabled = false;
                    star3.enabled = false;
                    return "B";
                }
            case 1:
                {
                    star0.sprite = starr;
                    star1.sprite = starr;
                    star2.enabled = false;
                    star3.enabled = false;
                    return "A";
                }
            case 2:
                {

                    star0.sprite = starr;
                    star1.sprite = starr;
                    star2.sprite = starr;
                    star3.enabled = false;
                    return "S";
                }
            case 3:
                {

                    star0.sprite = starr;
                    star1.sprite = starr;
                    star2.sprite = starr;
                    star3.sprite = starr;
                    return "S";
                }
            default:
                {
                    star0.enabled = false;
                    star1.enabled = false;
                    star2.enabled = false;
                    star3.enabled = false;
                    return "";
                }
        };
    }
}
