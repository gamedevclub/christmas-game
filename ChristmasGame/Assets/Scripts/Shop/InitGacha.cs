using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InitGacha : MonoBehaviour
{
    // Start is called before the first frame update

    /*    private List<Decoration> Decor_S = new List<Decoration>();
        private List<Decoration> Decor_A = new List<Decoration>();
        private List<Decoration> Decor_B = new List<Decoration>();*/

    public GameObject pnlGet_Item;
    public GameObject gachaEffect;
    public Transform Gacha;
    public Sprite starr;
    public List<GameObject> listpnl = new List<GameObject>();
    public GameObject btnX1;
    public GameObject btnX10;
    void Start()
    {
        /*     foreach (Decoration d in ShopUIManager.instance.DecorItems) 
             {
                 if (d.level == 3) Decor_S.Add(d);
                 if (d.level == 2) Decor_A.Add(d);
                 if (d.level == 1) Decor_B.Add(d);
             }*/

    }


    public Decoration RollItem()
    {

        Decoration temp = new Decoration();
        int x = Random.Range(0, 101);
        if (x <= 5) temp = DataLoader.instance.GetDecoRandom(2);
        if (x > 5 && x <= 20) temp = DataLoader.instance.GetDecoRandom(1);
        if (x > 20 && x <= 100) temp = DataLoader.instance.GetDecoRandom(0);
        //Debug.Log(temp.Name + "  " + temp.level);
        return temp;
    }
    public void Roll_X1()
    {

        if (ShopUIManager.instance.checkEnoughCoin(200))
        {
            btnX1.SetActive(false);
            btnX10.SetActive(false);
            ShopUIManager.instance.PayForPurchase(200);
            Decoration temp = RollItem();
            GameObject pnl = Instantiate(pnlGet_Item, Gacha);

            Instantiate(gachaEffect, Vector3.zero, Quaternion.identity);

            Image[] imageItem = pnl.GetComponentsInChildren<Image>();
            imageItem[2].sprite = DataLoader.instance.GetDecorationSprite(temp);

            Text[] info = pnl.GetComponentsInChildren<Text>();
            info[1].text = temp.name;
            info[2].text = HandleStar(pnl, temp.level);

            Button btnPick = pnl.GetComponentInChildren<Button>();
            btnPick.onClick.AddListener(delegate
            {

                btnX1.SetActive(true);
                btnX10.SetActive(true);
                Destroy(pnl);
            });

            ShopUIManager.instance.pushItemDecor(temp, 1);
        }
        else
            GameManager.instance.Alert("Hong đủ tiền rồi :(");

    }
    public string HandleStar(GameObject pnl, int i)
    {
        Image[] star = pnl.GetComponentsInChildren<Image>();
        switch (i)
        {
            case 0:
                {
                    star[3].sprite = starr;
                    star[4].enabled = false;
                    star[5].enabled = false;
                    return "";
                }
            case 1:
                {
                    star[3].sprite = starr;
                    star[4].sprite = starr;
                    star[5].enabled = false;
                    return "";
                }
            case 2:
                {

                    star[3].sprite = starr;
                    star[4].sprite = starr;
                    star[5].sprite = starr;
                    return "";
                }
            default: return "";
        };
    }
    public void Roll_X10()
    {
        if (ShopUIManager.instance.checkEnoughCoin(2000))
        {
            btnX1.SetActive(false);
            btnX10.SetActive(false);
            ShopUIManager.instance.PayForPurchase(2000);
            for (int i = 0; i < 10; i++)
            {
                Decoration temp;
                if (i == 0)
                {
                    temp = DataLoader.instance.GetDecoRandom(1);
                }
                else
                {
                    temp = RollItem();
                }
                // Decoration temp = RollItem();
                GameObject pnl = Instantiate(pnlGet_Item, Gacha);

                pnl.SetActive(false);
                listpnl.Add(pnl);

                Image[] imageItem = pnl.GetComponentsInChildren<Image>();
                imageItem[2].sprite = DataLoader.instance.GetDecorationSprite(temp);

                Text[] info = pnl.GetComponentsInChildren<Text>();
                info[1].text = temp.name;
                info[2].text = HandleStar(pnl, temp.level);


                ShopUIManager.instance.pushItemDecor(temp, 1);


            }
            nextpnl(listpnl, 0);
        }
        else
            GameManager.instance.Alert("Hong đủ tiền rồi :(");
    }
    public void nextpnl(List<GameObject> p, int i)
    {
        p[i].SetActive(true);

        Instantiate(gachaEffect, Vector3.zero, Quaternion.identity);

        Button btnPick = p[i].GetComponentInChildren<Button>();

        AudioManager.instance.Gacha();

        btnPick.onClick.AddListener(delegate
        {
            Destroy(p[i]);
            if (i < p.Count - 1)
            {
                nextpnl(p, i + 1);
            }
            else
            {
                btnX1.SetActive(true);
                btnX10.SetActive(true);
                p.Clear();
            }

        });

    }
}
