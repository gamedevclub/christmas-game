using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class initMaterial : MonoBehaviour
{
    public Transform MarterialPos;
    public Transform pnlPos;
    public GameObject pnlBuy;
    public GameObject Slot;
    public GameObject Slot_exp;

    public Sprite starr;

    public void Load()
    {
        //Debug.Log(ShopUIManager.instance);
        for (int i = 0; i < ShopUIManager.instance.LevelIt.Count; i++)
        {
            int x = i;
            GameObject pnl = Instantiate(Slot, MarterialPos);
            pnl.GetComponent<Slot_Buy>().Setvalue(ShopUIManager.instance.LevelIt[i].Name,
                                                -1,
                                                ShopUIManager.instance.LevelIt[i].level,
                                                ShopUIManager.instance.LevelIt[i].art,
                                                ShopUIManager.instance.LevelIt[i].price);

            pnl.GetComponentInChildren<Button>().onClick.AddListener(delegate { clickBuy(x, pnl); });
        }

        GameObject pnl_exp = Instantiate(Slot_exp, MarterialPos);
        pnl_exp.GetComponent<Slot_Buy>().Setvalue(ShopUIManager.instance.expItem.Name,
                                            DataManager.instance.IngameData.shopExpRemain,
                                            -1,
                                            ShopUIManager.instance.expItem.art,
                                            ShopUIManager.instance.expItem.price);

        pnl_exp.GetComponentInChildren<Button>().onClick.AddListener(delegate { clickBuy(-1, pnl_exp); });

        ShopUIManager.instance.slotBuyExpItem = pnl_exp;
    }

    public void clickBuy(int index, GameObject Slot)
    {
        GameObject pnlBuyItem = Instantiate(pnlBuy, pnlPos);
        if (index == -1)
        {
            pnlBuyItem.GetComponent<SetValueForPnlBuy>().SetValue(
                ShopUIManager.instance.expItem.art,
                ShopUIManager.instance.expItem.Name,
                ShopUIManager.instance.expItem.description,
                ShopUIManager.instance.expItem.price,
                -1
                );
            Button[] button = pnlBuyItem.GetComponentsInChildren<Button>();
            button[0].onClick.AddListener(delegate { Destroy(pnlBuyItem); });
            button[1].onClick.AddListener(delegate { BuyItem(pnlBuyItem, index, Slot); });
        }
        else
        {
             pnlBuyItem.GetComponent<SetValueForPnlBuy>().SetValue(
                 ShopUIManager.instance.LevelIt[index].art,
                 ShopUIManager.instance.LevelIt[index].Name,
                 ShopUIManager.instance.LevelIt[index].description,
                 ShopUIManager.instance.LevelIt[index].price,
                 ShopUIManager.instance.LevelIt[index].level

                 );

            Button[] button = pnlBuyItem.GetComponentsInChildren<Button>();
            button[0].onClick.AddListener(delegate { Destroy(pnlBuyItem); });
            button[1].onClick.AddListener(delegate { BuyItem(pnlBuyItem, index, Slot); });

        }

    }
    public void BuyItem(GameObject pnlBuyItem, int index, GameObject Slot)
    {
        if (pnlBuyItem.GetComponentInChildren<InputField>().text != "")
        {
            int quantitybuy = Int32.Parse(pnlBuyItem.GetComponentInChildren<InputField>().text);


            if (index == -1)
            {
                if (DataManager.instance.IngameData.shopExpRemain >= quantitybuy)
                /*if (10 > quantitybuy)*/
                {
                    if (!ShopUIManager.instance.checkEnoughCoin(quantitybuy * ShopUIManager.instance.expItem.price))
                    {
                        // Debug.Log("Not enough");
                        GameManager.instance.Alert("Hong đủ tiền rồi :(");
                        return;
                    }
                    Debug.Log("OK");
                    ShopUIManager.instance.PayForPurchase(quantitybuy * ShopUIManager.instance.expItem.price);
                    ShopUIManager.instance.pushExpItem(quantitybuy);
                    Slot.GetComponent<Slot_Buy>().Setvalue(ShopUIManager.instance.expItem.Name,
                                                DataManager.instance.IngameData.shopExpRemain,
                                                ShopUIManager.instance.expItem.level,
                                                ShopUIManager.instance.expItem.art,
                                                ShopUIManager.instance.expItem.price);
                }
                else
                {
                    //Debug.Log("Not enough quantity");
                    GameManager.instance.Alert("Shop đã bán hết mảnh ..., mai hãy quay lại mua nhé <3");
                }
            }
            else
            {
                if (!ShopUIManager.instance.checkEnoughCoin(quantitybuy * ShopUIManager.instance.LevelIt[index].price))
                {
                    //Debug.Log("Not enough");
                    GameManager.instance.Alert("Hong đủ tiền rồi :(");
                    return;
                }
                ShopUIManager.instance.PayForPurchase(quantitybuy * ShopUIManager.instance.LevelIt[index].price);
                ShopUIManager.instance.pushMaterial(ShopUIManager.instance.LevelIt[index], quantitybuy);
            }

            Destroy(pnlBuyItem);
        }
    }
}
