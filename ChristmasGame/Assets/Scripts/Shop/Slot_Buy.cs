using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Slot_Buy : MonoBehaviour
{
    public Sprite starr;
    private string _name;
    private int quantity;
    private int level;
    private int price;
    private Sprite img;
    public void Setvalue(string name, int quantity, int level, Sprite image, int price)
    {
        this._name = name;
        this.quantity = quantity;
        this.level = level;
        this.img = image;
        this.price = price;
        display();
    }
    void display()
    {
        // Text[] info = gameObject.GetComponentsInChildren<Text>();
        // info[0].text = _name;
        // if (quantity == -1)
        // {
        //     info[1].enabled = true;
        // }
        // else info[1].text = quantity.ToString();
        // info[2].text = handleLevel(level);
        // info[3].text = price.ToString();
        gameObject.GetComponentsInChildren<Image>()[1].sprite = img;
        if (quantity != -1)
        {
            Text info = gameObject.GetComponentInChildren<Text>();
            info.text = "Còn lại: " + quantity.ToString();
        }
    }

    public string handleLevel(int i)
    {
        Image[] star = gameObject.GetComponentsInChildren<Image>();
        switch (i)
        {
            case 0:
                {
                    star[3].sprite = starr;
                    star[4].enabled = false;
                    star[5].enabled = false;
                    star[6].enabled = false;
                    return "B";
                }
            case 1:
                {
                    star[3].sprite = starr;
                    star[4].sprite = starr;
                    star[5].enabled = false;
                    star[6].enabled = false;
                    return "A";
                }
            case 2:
                {
                    star[3].sprite = starr;
                    star[4].sprite = starr;
                    star[5].sprite = starr;
                    star[6].enabled = false;
                    return "S";
                }
            case 3:
                {
                    star[3].sprite = starr;
                    star[4].sprite = starr;
                    star[5].sprite = starr;
                    star[6].sprite = starr;
                    return "SS";
                }
            default:
                {
                    star[3].enabled = false; ;
                    star[4].enabled = false;
                    star[5].enabled = false;
                    star[6].enabled = false;
                    return "";
                }
        };
    }
}
