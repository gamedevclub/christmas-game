using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class initEggShop1 : MonoBehaviour
{
    public Transform EggPos;
    public GameObject Slot;
    private List<Egg> EgginShop = new List<Egg>();
    public void Load()
    {
        //Debug.Log(ShopUIManager.instance);
        for (int i = EggPos.childCount - 1; i >= 0; i--)
            Destroy(EggPos.GetChild(i).gameObject);
        EgginShop.Clear();

        for (int i = 0; i < ShopUIManager.instance.EggIt.Count; i++)
        {
            if (ShopUIManager.instance.EggIt[i].level <= (ShopUIManager.instance.levelEgg() - 2))
            {
                Debug.Log(ShopUIManager.instance.levelEgg());
                EgginShop.Add(ShopUIManager.instance.EggIt[i]);
            }
        }
        for (int i = 0; i < EgginShop.Count; i++)
        {
            int x = i;
            GameObject pnl = Instantiate(Slot, EggPos);
            pnl.GetComponent<Slot_Egg>().Setvalue(EgginShop[i].Name, EgginShop[i].level, EgginShop[i].art, EgginShop[i].price);
            pnl.GetComponentInChildren<Button>().onClick.AddListener(delegate { clickBuy(x, pnl); });
        }
    }

    public void clickBuy(int index, GameObject pnl)
    {
        if (ShopUIManager.instance.checkQuantityEgg())
        {
            // Egg in inventory greater than 4
            // Debug.Log("Egg in inventory greater than 4");
            GameManager.instance.Alert("Chỉ được sở hữu tối đa 4 trứng thôi nhé");
            return;
        }
        if (ShopUIManager.instance.checkEnoughCoin(EgginShop[index].price))
        {
            ShopUIManager.instance.PayForPurchase(EgginShop[index].price);
            ShopUIManager.instance.pushEgg(EgginShop[index]);
            //ShopUIManager.instance.EggIt.Remove(EgginShop[index]);
            //Destroy(pnl);
        }
        else
            GameManager.instance.Alert("Hong đủ tiền rồi: (");
    }
}
