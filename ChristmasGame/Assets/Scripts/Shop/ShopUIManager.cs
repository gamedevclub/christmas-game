using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopUIManager : MonoBehaviour
{
    public static ShopUIManager instance;

    public GameObject shopPanel;

    public List<int> idDecorList;

    [System.NonSerialized] public List<Decoration> DecorIt = new List<Decoration>();
    public List<Egg> EggIt = new List<Egg>();
    public List<LevelItem> LevelIt = new List<LevelItem>();

    public LevelItem expItem;
    public GameObject slotBuyExpItem;
    protected int Coin = 3000;
    public Text Cointxt;

    private initDecor initDecor;
    private initEggShop1 initEggShop1;
    private initMaterial initMaterial;
    private InitGacha initGacha;
    private bool firstLoad;
    void Start()
    {
        if (instance == null)
            instance = this;

        firstLoad = true;
        initDecor = GetComponent<initDecor>();
        initEggShop1 = GetComponent<initEggShop1>();
        initMaterial = GetComponent<initMaterial>();
        initGacha = GetComponent<InitGacha>();

        DecorIt = new List<Decoration>();
        foreach (var id in idDecorList)
        {
            DecorIt.Add(DataLoader.instance.GetDeco(id));
        }
    }

    public void Load()
    {
        Coin = DataManager.instance.IngameData.money;
        Cointxt.text = Coin.ToString();
        shopPanel.SetActive(true);
        if (firstLoad)
        {
            initDecor.Load();
            initEggShop1.Load();
            initMaterial.Load();
            firstLoad = false;
        }
        else
        {
            initEggShop1.Load();
            ResetDisplayExpItem();
        }
    }

    public void Close()
    {
        shopPanel.transform.parent.gameObject.SetActive(false);
    }

    public void PayForPurchase(int money)
    {
        Coin -= money;
        DataManager.instance.IngameData.money -= money;
        DataManager.instance.SaveUnit();
        Cointxt.text = Coin.ToString();
    }
    public bool checkEnoughCoin(int money)
    {
        return Coin >= money;
    }
    public void pushItemDecor(Decoration item, int quantity)
    {
        AudioManager.instance.BuySell();
        for (int i = 0; i < quantity; i++)
        {
            Decoration newDecor = DataLoader.instance.GetDeco(item.ID);
            newDecor.slot = DataManager.instance.IngameData.decorationList.Count;
            DataManager.instance.IngameData.decorationList.Add(newDecor);
        }
        DataManager.instance.SaveDeco();
    }
    public void pushEgg(Egg e)
    {
        AudioManager.instance.BuySell();
        PlayerEgg newEgg = new PlayerEgg();
        newEgg.level = e.level;
        newEgg.remainTime = e.hatchTime;
        newEgg.exp = 0;
        newEgg.status = 0;
        DataManager.instance.IngameData.eggList.Add(newEgg);
        DataManager.instance.SaveEgg();
    }
    public void pushMaterial(LevelItem l, int quantity)
    {
        AudioManager.instance.BuySell();
        DataManager.instance.IngameData.levelItemList[l.level] += quantity;
        DataManager.instance.SaveLevelItem();
    }
    public void pushExpItem(int quantity)
    {
        AudioManager.instance.BuySell();
        DataManager.instance.IngameData.expItem += quantity;
        DataManager.instance.IngameData.shopExpRemain -= quantity;
        DataManager.instance.SaveUnit();
    }
    public bool checkQuantityEgg()
    {
        // >=4 egg -> true
        return DataManager.instance.IngameData.eggList.Count >= 4;

    }
    public int levelEgg()
    {
        //return maximum level of egg in inventory
        var level = DataManager.instance.IngameData.maxEggLevel;
        return (level < 2 ? 2 : level);

    }
    // public void TimeReset()
    // {
    //     DateTime present = DateTime.Now;
    //     TimeSpan reset = new DateTime(present.Year, present.Month, present.Day + 1, 0, 0, 0) - present;

    //     if (reset <= new TimeSpan(0, 1, 0) && reset > new TimeSpan(0, 0, 0))
    //     {
    //         DataManager.instance.IngameData.shopExpRemain = 10;
    //         DataManager.instance.SaveUnit();
    //     }
    // }

    public void ResetDisplayExpItem()
    {
        if (shopPanel.activeInHierarchy)
        {
            Debug.Log("Active");
            slotBuyExpItem.GetComponent<Slot_Buy>().Setvalue(instance.expItem.Name,
                                            DataManager.instance.IngameData.shopExpRemain,
                                            -1,
                                            instance.expItem.art,
                                            instance.expItem.price);
        }
    }
}
