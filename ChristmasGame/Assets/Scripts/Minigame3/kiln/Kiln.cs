using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Kiln : MonoBehaviour
{
    // Start is called before the first frame update
    public Slider tempratureSlider;
    public int temperatureOfKiln = 50;

    private float timeAddScore = 1;
    private float timeReductTem = DataInGame3.timeReductTem;
    private float timeReductTemForPlayer = 2;
    private GameObject player;
    private float minDistance = DataInGame3.minDistance;

   public void Start()
    {
        temperatureOfKiln = 50;
        player = GameObject.FindGameObjectWithTag("Player");
        tempratureSlider.maxValue = 100;
        tempratureSlider.minValue = 0;
        tempratureSlider.value = temperatureOfKiln;
    
    }

    // Update is called once per frame
    void Update()
    {
        if(!GameManager3.instance.isEndGame)
        {
        tempratureSlider.value = temperatureOfKiln;
        ReduceTemperature();
        AddScore();
        CheckDistance(player);
        }
    }

    public void AddScore()
    {
        if (timeAddScore <= 0)
        {
            if (temperatureOfKiln > 40 && temperatureOfKiln <= 80)
            {
                GameManager3.instance.score++;
            }
            timeAddScore = 1;
        }
        timeAddScore -= Time.deltaTime;
  
    }
    void ReduceTemperature()
    {
        if (timeReductTem <= 0)
        {
            temperatureOfKiln -= DataInGame3.temReduce;
            if (temperatureOfKiln <= 0) temperatureOfKiln = 0;
            timeReductTem = DataInGame3.timeReductTem;
        }
        timeReductTem -= Time.deltaTime;
 
    } 
    public void CheckDistance(GameObject p)
    {
        //Debug.Log(timeReductTemForPlayer);
        if (timeReductTemForPlayer <= 0)
        {
            //Debug.Log(Vector3.Distance(transform.position, p.transform.position));
            if (Vector3.Distance(transform.position, p.transform.position) <= minDistance)
            {
                GameManager3.instance.TaketemperatureOfSnowman(DataInGame3.temForPlayer);
            }
            timeReductTemForPlayer = 2;
        }
        timeReductTemForPlayer -= Time.deltaTime;
    }    
    public void  TakeTemperature(int t)
    {
        temperatureOfKiln += t;
        if (temperatureOfKiln >= 100) temperatureOfKiln = 100;
        if (temperatureOfKiln <= 0) temperatureOfKiln = 0;
    }
    public void ReceiveSignal()
    {
        //Debug.Log("X");
        
    }
    public bool BalanceTemperature()
    {
        if(temperatureOfKiln>40 && temperatureOfKiln <=80) return true;
        else return false;
    }

}
