using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnTrail : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject blueTrail;
    public GameObject redTrail;
    public bool isDeath;
    void Start()
    {
        isDeath = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isDeath)
        {
            if (gameObject.tag == "blueSlime")
            {
                bool checkDistance = true;
                foreach(GameObject t in GameObject.FindGameObjectsWithTag("blueTrail"))
                {
                    if(Vector2.Distance(t.transform.position, transform.position) < 0.15f)checkDistance = false;
                }
                if (checkDistance)
                {
                    Instantiate(blueTrail, transform.position, Quaternion.identity);
                }
            }
            if (gameObject.tag == "redSlime")
            {
                bool checkDistance = true;
                foreach(GameObject t in GameObject.FindGameObjectsWithTag("redTrail"))
                {
                    if(Vector2.Distance(t.transform.position, transform.position) < 0.15f)checkDistance = false;
                }
                if (checkDistance)
                {
                Instantiate(redTrail, transform.position, Quaternion.identity);
                }
            }
        }
    }
}
