using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeColi : MonoBehaviour
{

    public GameObject trail;
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (gameObject.tag == "blueSlime" && collision.transform.tag == "kiln")
        {
            collision.transform.GetComponent<Kiln>().TakeTemperature(-DataInGame3.temperatureForKiln);
            DestroySlime(1);
        }
        if (gameObject.tag == "redSlime" && collision.transform.tag == "kiln")
        {
            collision.transform.GetComponent<Kiln>().TakeTemperature(DataInGame3.temperatureForKiln);
            DestroySlime(1);
        }
        if (gameObject.tag == "blueSlime" && collision.transform.tag == "Player")
        {
            GameManager3.instance.TaketemperatureOfSnowman(-DataInGame3.temperatureForPlayer);
            DestroySlime(2);
        }
        if (gameObject.tag == "redSlime" && collision.transform.tag == "Player")
        {
            GameManager3.instance.TaketemperatureOfSnowman(DataInGame3.temperatureForPlayer);
            DestroySlime(2);
        }

    }
    private void DestroySlime(int i)
    {
        GameManager3.instance.countSlime --;
        gameObject.GetComponent<SpriteRenderer>().enabled = false;
        gameObject.GetComponent<Collider2D>().enabled = false;
        // gameObject.GetComponent<ParticleSystem>().Stop(true, ParticleSystemStopBehavior.StopEmitting);
        gameObject.GetComponentInChildren<ParticleSystem>().Stop(true, ParticleSystemStopBehavior.StopEmitting);
        gameObject.GetComponent<SlimeMove>().speed = 0;
        gameObject.GetComponent<AudioSource>().Stop();
        gameObject.GetComponent<SpawnTrail>().isDeath = true;
        if(i == 1)
        {
            AudioManager3.instance.Play("SlimeHitKiln",false);
        }
        else 
        {
            AudioManager3.instance.Play("PlayerHitSlime",false);
        }

        if(GameManager3.instance.countSlime == 0)
        {
            AudioManager3.instance.Stop("SlimeJump");
        }
        Destroy(gameObject, 5);
    }

}
