using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeMove : MonoBehaviour
{
    // Start is called before the first frame update
    private GameObject[] kilns;
    private Rigidbody2D rb;
    public int speed;
    public float SpaTime;
    private Vector2 dir;
    private float timeRunaround;
    bool select = false;
    Vector2 velocity;
    void Start()
    {
        // AudioManager3.instance.Play("SlimeJump",true);
        timeRunaround = 7;
        SpaTime = DataInGame3.timeCreate;
        speed = DataInGame3.speedOfSlime;
        kilns = GameObject.FindGameObjectsWithTag("kiln");
        rb = gameObject.GetComponent<Rigidbody2D>();
        StartCoroutine(Run());
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameManager3.instance.isEndGame)
        {
            if (SpaTime <= 0)
            {
                speed += 30;
                SpaTime = DataInGame3.timeCreate;
            }
            //        Debug.Log(dir);
            SpaTime -= Time.deltaTime;
            velocity = -(dir.normalized * speed);
            if (timeRunaround <= 0 && !select)
            {
                Move();
                select = true;
            }
            else timeRunaround -= Time.deltaTime;
        }

    }

    private void FixedUpdate()
    {
        rb.velocity = velocity * Time.fixedDeltaTime;
    }
    
    IEnumerator Run()
    {
        if (select) yield break;
        else
        {
            dir = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f));
            yield return new WaitForSeconds(Random.Range(1f, 3f));
            StartCoroutine(Run());
        }
    }
    public void Move()
    {
        List<GameObject> listKiln = new List<GameObject>();
        if (gameObject.tag == "blueSlime")
        {
            int minTem = 100;
            foreach (GameObject e in kilns)
            {
                if (e.GetComponent<Kiln>().temperatureOfKiln <= minTem)
                {
                    minTem = e.GetComponent<Kiln>().temperatureOfKiln;
                }
            }
            foreach (GameObject e in kilns)
            {
                if (e.GetComponent<Kiln>().temperatureOfKiln == minTem)
                {
                    listKiln.Add(e);
                }
            }
        }

        if (gameObject.tag == "redSlime")
        {
            int maxTem = 0;
            foreach (GameObject e in kilns)
            {
                if (e.GetComponent<Kiln>().temperatureOfKiln >= maxTem)
                {
                    maxTem = e.GetComponent<Kiln>().temperatureOfKiln;
                }
            }
            foreach (GameObject e in kilns)
            {
                if (e.GetComponent<Kiln>().temperatureOfKiln == maxTem)
                {
                    listKiln.Add(e);
                }
            }

        }

        int x = Random.Range(0, listKiln.Count);
        listKiln[x].GetComponent<Kiln>().ReceiveSignal();
        dir = new Vector2(transform.position.x - listKiln[x].transform.position.x, transform.position.y - listKiln[x].transform.position.y);
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.tag == "wall")
        {
            dir = -dir;
            //Debug.Log("wall");
        }
    }

}
