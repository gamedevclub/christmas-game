using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateSlime : MonoBehaviour
{
    // Start is called before the first frame update
    private float timeCreate;
    public GameObject blueSlime;
    public GameObject redSlime;
    public GameObject redCave;
    public GameObject blueCave;

    public void Start()
    {
        redCave.SetActive(false);
        blueCave.SetActive(false);
        timeCreate = DataInGame3.timeCreate;
        SpawnCave();

    }
    private void Update()
    {
        if (!GameManager3.instance.isEndGame)
            SpawnSlime();
    }
    void SpawnCave()
    {
        Vector2 newPosBlueCave;
        Vector2 newPosRedCave;
        do
        {
            newPosBlueCave = new Vector2(Random.Range(-5f, 5f), Random.Range(-2f, 2f));
            newPosRedCave = new Vector2(Random.Range(-5f, 5f), Random.Range(-2f, 2f));
        }
        while (Vector2.Distance(newPosBlueCave, newPosRedCave) < 3);
        redCave.transform.position = newPosRedCave;
        blueCave.transform.position = newPosBlueCave;

        redCave.SetActive(true);
        blueCave.SetActive(true);
    }
    void SpawnSlime()
    {
        if (timeCreate <= 0 && GameManager3.instance.time <= 90)
        {
            int x = Random.Range(0, 100);
            if (x < 30)
            {
                CreateSlimee(redSlime, redCave);
                CreateSlimee(blueSlime, blueCave);

            }
            else
            {
                int a = Random.Range(1, 3);
                if (a == 1)
                {

                    CreateSlimee(redSlime, redCave);
                }
                else
                {
                    CreateSlimee(blueSlime, blueCave);
                }
            }
            timeCreate = DataInGame3.timeCreate;
            AudioManager3.instance.Play("SlimeJump",true);
            
        }
        timeCreate -= Time.deltaTime;
    }
    void CreateSlimee(GameObject slime, GameObject Pos)
    {

        Instantiate(slime, Pos.transform.position, Quaternion.identity);
        GameManager3.instance.countSlime++;
    }


}
