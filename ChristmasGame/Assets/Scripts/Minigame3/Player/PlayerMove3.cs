using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove3 : MonoBehaviour
{
    // Start is called before the first frame update
    private float dirX;
    private float dirY;
    private int speed;
    private Rigidbody2D rb;
    private float STime = 10;
    private Animator ani;

    Vector2 velocity;
    public void Start()
    {
        ani = gameObject.GetComponent<Animator>();
        speed = DataInGame3.speedOfPlayer;
        gameObject.transform.position = new Vector3(0, -4, 0);
        rb = gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (STime <= 0)
        {
            speed -= 10;
            STime = 10;
        }
        STime -= Time.deltaTime;
        if (!GameManager3.instance.isEndGame)
        {
            dirX = Input.GetAxis("Horizontal");
            dirY = Input.GetAxis("Vertical");

            if (dirX < -0.1) ani.SetFloat("Blend", 0);
            else if (dirX > 0.1) ani.SetFloat("Blend", 0.3f);
            else if (dirY < -0.1) ani.SetFloat("Blend", 1f);
            else if (dirY > 0.1) ani.SetFloat("Blend", 0.6f);
            else ani.SetFloat("Blend", 1.4f);


            if (GameManager3.instance.isSlow)
            {
                velocity = new Vector2(dirX, dirY).normalized * (int)(speed / 2);
            }
            else velocity = new Vector2(dirX, dirY).normalized * speed;
        }
        else
        {
            velocity = new Vector2(0, 0);
        }
    }

    private void FixedUpdate()
    {
        rb.velocity = velocity * Time.fixedDeltaTime;
    }
}
