using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerColi : MonoBehaviour
{

    public bool havewood;
    Collider2D col;
    public float bluetime;
    public float redtime;
    public void Start()
    {

        bluetime = 1;
        redtime = 1;
        col = gameObject.GetComponent<Collider2D>();
        havewood = false;
        GameManager3.instance.DisplayWood(false);
    }
    private void Update()
    {
        bluetime -= Time.deltaTime;
        redtime -= Time.deltaTime;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "wood" && !havewood)
        {
            Destroy(collision.gameObject);
            havewood = true;
            // Debug.Log("take wood");
            GameManager3.instance.DisplayWood(true);
            GameManager3.instance.countWood--;

            GameManager3.instance.totalWood++;

            AudioManager3.instance.Play("PickWood",false);
        }
        if (bluetime <= 0 && collision.tag == "blueTrail")
        {
            GameManager3.instance.TaketemperatureOfSnowman(-5);
            bluetime = 1;
            //Debug.Log("BLUE");
        }
        if (redtime <= 0 && collision.tag == "redTrail")
        {
            GameManager3.instance.TaketemperatureOfSnowman(5);
            redtime = 1;
            //Debug.Log("Red");
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "kiln" && havewood)
        {
            havewood = false;
            collision.transform.GetComponent<Kiln>().TakeTemperature(DataInGame3.takeTempOfKiln);
            GameManager3.instance.DisplayWood(false);

            AudioManager3.instance.Play("DropWood",false);

        }

    }


}
