using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;


public class AudioManager3 : MonoBehaviour
{
    // Start is called before the first frame update
    public List<Sound7> sounds = new List<Sound7>();
    public static AudioManager3 instance { get; set; }

    private void Awake()
    {
        AudioSource[] audioSource = gameObject.GetComponents<AudioSource>();
        int i = 0;
        foreach (Sound7 s in sounds)
        {
            s.source = audioSource[i];
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
            i++;
        }
        if (instance == null)
        {
            instance = this;
        }

    }
    void Start()
    {
        Play("Theme", false);
        Play("Fire", true);

    }
    public void Play(string name, bool isDelay)
    {
        if (isDelay)
        {

            Sound7 sound = sounds.Find(s => s.name == name);
            if (sound != null && sound.source.isPlaying == false)
            {
                sound.source.Play();
            }
            if (sound == null)
            {
                Debug.LogWarning("Sound : " + name + " not found !");
            }
        }
        else
        {

            Sound7 sound = sounds.Find(s => s.name == name);
            if (sound != null)
            {
                sound.source.Play();
            }
            if (sound == null)
            {
                Debug.LogWarning("Sound : " + name + " not found !");
            }
        }
    }

    public void Stop(string name)
    {
        Sound7 sound = sounds.Find(s => s.name == name);
        if (sound != null && sound.source.isPlaying == true)
        {
            sound.source.Stop();
        }
    }
    public void StopAll()
    {
        foreach (Sound7 s in sounds)
        {
            Stop(s.name);
        }
    }
}
