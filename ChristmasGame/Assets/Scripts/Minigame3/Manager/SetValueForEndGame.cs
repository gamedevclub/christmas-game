using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetValueForEndGame : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject Manager;
    public GameObject player;

    public Text txtScore;
    private void Start() {
        player = GameObject.FindGameObjectWithTag("Player");
    }
    public void SetValue(int score)
    {
        txtScore.text = score.ToString();
    }
    public void ResStart() { 
        GameObject[] kilns = GameObject.FindGameObjectsWithTag("kiln");
        foreach(GameObject k in kilns)
        {
            k.GetComponent<Kiln>().Start();
        }
        GameObject[] woods = GameObject.FindGameObjectsWithTag("wood");
        foreach(GameObject w in woods)
            Destroy(w);
        GameObject[] blueSlimes = GameObject.FindGameObjectsWithTag("blueSlime");
        foreach(GameObject w in blueSlimes)
            Destroy(w);
        GameObject[] redSlimes = GameObject.FindGameObjectsWithTag("redSlime");
        foreach(GameObject w in redSlimes)
            Destroy(w);
        Manager.GetComponent<GameManager3>().ResetValue();
        Manager.GetComponent<CreateSlime>().Start();
        player.GetComponent<PlayerMove3>().Start();
        player.GetComponent<PlayerColi>().Start();

        
        gameObject.SetActive(false); 
    }
    public void BackHome()
    {
         //Time.timeScale = 1;
    }
}
