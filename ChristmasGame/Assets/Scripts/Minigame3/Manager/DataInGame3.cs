using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DataInGame3
{
    //   Kiln
    public static float timeReductTem = 1;   // Thời gian giảm nhiệt độ
    public static float minDistance = 2;    // khoảng cách từ người chơi tới lò -> giảm nhiệt
    public static int temReduce = 1;        // Nhiệt độ lò giảm mỗi timeReductTem (giây)
    public static int temForPlayer = 10;   // Nhiệt độ người tuyết nhận khi lại gần lò


    // Player
    public static int speedOfPlayer = 200;          // Tốc độ di chuyển
    // Wood
    public static float timeCreateWood = 3;         // Khoangr cachs thời gian mỗi lần sinh gỗ
    public static int takeTempOfKiln = 15;          // Nhiệt độ lò tăng khi nhận đc gỗ
    
    // Slime
    public static float timeCreate = 15;     // khoảng cách thời gian sinh ra slime
    public static int temperatureForKiln = 10;//nhiệt độ lò nhận khi chạm slime
    public static int temperatureForPlayer = 15; //Nhiệt độ người chơi nhận khi chạm slime
    public static int speedOfSlime = 100;


    // GameManager

}
