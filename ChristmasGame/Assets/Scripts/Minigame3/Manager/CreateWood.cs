using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateWood : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject wood;
    public Transform Pos;
    private float timeCreateWood = DataInGame3.timeCreateWood;
    private GameObject[] kilns;
    private void Start()
    {
        kilns = GameObject.FindGameObjectsWithTag("kiln");
    }
    public void Update()
    {
        if (!GameManager3.instance.isEndGame)
            Create();
    }

    public void Create()
    {
        if (timeCreateWood <= 0)
        {
            bool checkDistance;
            if (GameManager3.instance.countWood <= 5)
            {
                do
                {

                    checkDistance = false;
                    Pos.position = new Vector3(Random.Range(-8, 8), Random.Range(-3, 3));
                    Collider2D wood = Physics2D.OverlapCircle(Pos.position, 1f);
                    if (wood) checkDistance = true;
                    foreach (GameObject kiln in kilns)
                    {
                        if (Vector2.Distance(Pos.position, kiln.transform.position) < 3)
                        {
                            checkDistance = true;
                        }
                    }
                } while (checkDistance);
                Instantiate(wood, Pos.position, Pos.rotation);
                GameManager3.instance.countWood++;
            }
            timeCreateWood = DataInGame3.timeCreateWood;
        }
        timeCreateWood -= Time.deltaTime;
    }
}
