using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class GameManager3 : MonoBehaviour
{
    public static GameManager3 instance;
    public float time = 90;
    public Text txtTime;

    public int score = 0;
    public Text txtScore;

    public int temperatureOfSnowman;
    public Text txtTemperature;

    public bool isSlow = false;
    private float timeBalance;

    public GameObject pnlEndGame;
    public GameObject imgWood;
    public int countWood;
    public int totalWood;
    public bool isEndGame;
    public int maxBalanceTime;
    public float questTime;
    public GameObject[] kilns;
    public int countSlime;
    public void Awake()

    {
        ResetValue();
        kilns = GameObject.FindGameObjectsWithTag("kiln");
        pnlEndGame.SetActive(false);
        if (instance == null)
            instance = this;
    }

    public void ResetValue()
    {
        countSlime = 0;
        time = 90;
        timeBalance = 1;
        temperatureOfSnowman = -20;
        isSlow = false;
        totalWood = 0;
        countWood = 0;
        maxBalanceTime = 0;
        isEndGame = false;
        score = 0;
        questTime = 1;
    }
    void FixedUpdate()
    {
        Debug.Log(totalWood);
        if (!isEndGame)
        {
            if (questTime <= 0)
            {
                bool balance = true;
                foreach (GameObject k in kilns)
                {
                    if (!k.GetComponent<Kiln>().BalanceTemperature()) balance = false;
                }
                if (balance) maxBalanceTime++;
                questTime = 1;
            }
            questTime -= Time.fixedDeltaTime;


            txtScore.text = score.ToString();
            txtTemperature.text = temperatureOfSnowman.ToString();

            if (time <= 0 || temperatureOfSnowman > 10 || temperatureOfSnowman < -70)
            {
                EndGame();
            }

            if (temperatureOfSnowman < -50)
            {
                isSlow = true;
            }
            else isSlow = false;
            CountDown();
            BalanceTem();
        }
    }
    void CountDown()
    {

        txtTime.text = ((int)(time / 60)).ToString() + " : " + ((int)(time % 60)).ToString();
        time -= Time.fixedDeltaTime;

    }
    public void EndGame()
    {
        AudioManager3.instance.StopAll();
        UpdateQuest();
        GetAward(score);
        isEndGame = true;

    }
    void BalanceTem()
    {
        if (timeBalance <= 0)
        {
            if (temperatureOfSnowman > -20) temperatureOfSnowman -= 1;
            if (temperatureOfSnowman < -20) temperatureOfSnowman += 1;
            timeBalance = 1;
        }
        timeBalance -= Time.fixedDeltaTime;


    }
    public void TaketemperatureOfSnowman(int T)
    {
        temperatureOfSnowman += T;

    }
    public void DisplayWood(bool display)
    {
        imgWood.SetActive(display);

    }
    private void GetAward(int score)
    {
        int money = score;
        int expNum = score / 10;
        bool openChest = score >= 100;
        if(time<= 0)AwardManager.instance.GetGameAward(money, expNum, openChest,Result.Victory);
        else AwardManager.instance.GetGameAward(money, expNum, openChest,Result.GameOver);
    }
    private void UpdateQuest()
    {
        if (Minigame3.score < score) Minigame3.score = score;
        if (Minigame3.logs < totalWood) Minigame3.logs = totalWood;
        if (Minigame3.balanceTime < maxBalanceTime) Minigame3.balanceTime = maxBalanceTime;
        DataManager.instance.SaveMinigameData(3);
    }
}
