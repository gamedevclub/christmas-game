using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PetDetail : PetSlot
{
    public Text description;
    public GameObject sellButton;
    public void Display(Pet pet)
    {
        DisplayNull(false);
        this.pet = pet;
        Name.text = pet.name;
        image.sprite = Resources.Load<Sprite>(pet.art);
        base.SetRarity(pet.level + 1);
        description.text = pet.description;
    }

    public void DisplayNull(bool empty)
    {
        if (empty)
        {
            Name.text = "Không có pet";
            base.SetRarity(0);
            image.enabled = false;
            description.enabled = false;
            sellButton.SetActive(false);
        }
        else
        {
            image.enabled = true;
            description.enabled = true;
            sellButton.SetActive(true);
        }
    }

    public void Sell()
    {
        PetManager.instance.SellPet(pet);
    }
}
