using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DemoDataPet : MonoBehaviour
{
    public static DemoDataPet instance;
    public Text moneyText;
    public PlayerData IngameData = new PlayerData();
    private void Start()
    {
        if (!instance)
            instance = this;

        ShowMoney();
    }

    public void ShowMoney()
    {
        moneyText.text = IngameData.money + "";
    }

    // Test------------------------------------------
    public void test()
    {
        Pet pet1 = new Pet();
        pet1.ID = 1;
        pet1.name = "pet1";
        pet1.description = "asdadasdasdasd";
        pet1.art = "Sprites/Pet/" + pet1.ID.ToString();
        pet1.level = 0;
        pet1.point = 1;
        pet1.price = 100;

        Pet pet2 = new Pet();
        pet2.ID = 2;
        pet2.name = "pet2";
        pet2.description = "adfgdfgsdgdfgdsfgdfgsdasdasd";
        pet2.art = "Sprites/Pet/" + pet2.ID.ToString();
        pet2.level = 1;
        pet2.point = 1;
        pet2.price = 200;

        Pet pet3 = new Pet();
        pet3.ID = 1;
        pet3.name = "pet1";
        pet3.description = "asdadasdasdasd";
        pet3.art = "Sprites/Pet/" + pet3.ID.ToString();
        pet3.level = 0;
        pet3.point = 1;
        pet3.price = 100;

        Pet pet4 = new Pet();
        pet4.ID = 5;
        pet4.name = "pet5";
        pet4.description = "asdadasdasxcvvxnxcvnvcxbvbdasd";
        pet4.art = "Sprites/Pet/" + pet4.ID.ToString();
        pet4.level = 4;
        pet4.point = 1;
        pet4.price = 500;

        PetManager.instance.AddPet(pet1);
        PetManager.instance.AddPet(pet2);
        PetManager.instance.AddPet(pet3);
        PetManager.instance.AddPet(pet4);
    }
}
