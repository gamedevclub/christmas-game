using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PetSlot : MonoBehaviour
{
    public Text Name;
    public Image[] rarity;
    public Image image;
    protected Pet pet;

    public void SetInfo(Pet pet)
    {
        this.pet = pet;
        Name.text = pet.name;
        image.sprite = Resources.Load<Sprite>(pet.art);
        SetRarity(pet.level + 1);
    }

    public void OnClick()
    {
        PetManager.instance.DisplayDetail(pet);
    }

    protected void SetRarity(int num)
    {
        for (int i = 0; i < 5; i++)
            rarity[i].enabled = false;
        for (int i = 0; i < num; i++)
            rarity[i].enabled = true;
    }
}
