using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetList : MonoBehaviour
{
    private List<GameObject> petList = new List<GameObject>();
    public GameObject petSlot;

    public void CreateItem(List<Pet> myPet)
    {
        if (petList.Count > 0)
            Refresh();
        for (int i = 0; i < myPet.Count; i++)
        {
            GameObject temp = Instantiate(petSlot, transform);
            temp.GetComponent<PetSlot>().SetInfo(myPet[i]);
            petList.Add(temp);
        }
    }

    void Refresh()
    {
        for (int i = 0; i < petList.Count; i++)
            Destroy(petList[i]);
        petList.Clear();
    }
}
