using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PetManager : MonoBehaviour
{
    public static PetManager instance;
    public GameObject petBox;
    public PetList petList;
    public PetDetail petDetail;
    public SellPanel sellPanel;
    private void Start()
    {
        if (instance == null)
            instance = this;


    }

    // Display MyPet List UI
    public void DisplayList()
    {
        // Test dùng DemoDataPet
        // petList.CreateItem(DemoDataPet.instance.IngameData.petList);
        // if (DemoDataPet.instance.IngameData.petList.Count > 0)
        //     DisplayDetail(DemoDataPet.instance.IngameData.petList[0]);
        // else petDetail.DisplayNull(true);

        // ------------Merge------------
        petList.CreateItem(DataManager.instance.IngameData.petList);
        if (DataManager.instance.IngameData.petList.Count > 0)
            DisplayDetail(DataManager.instance.IngameData.petList[0]);
        else petDetail.DisplayNull(true);

    }

    // Display Detail a Pet UI
    public void DisplayDetail(Pet pet)
    {
        petDetail.Display(pet);
        sellPanel.Close();
    }

    // Sell Pet
    public void RemovePet(Pet pet)
    {
        // Test dùng DemoDataPet
        // for (int i = 0; i < DemoDataPet.instance.IngameData.petList.Count; i++)
        //     if (pet == DemoDataPet.instance.IngameData.petList[i])
        //     {
        //         DemoDataPet.instance.IngameData.money += pet.price;
        //         DemoDataPet.instance.IngameData.petList.Remove(pet);
        //         // Test money
        //         DemoDataPet.instance.ShowMoney();
        //     }

        // ------------------Merge------------
        for (int i = 0; i < DataManager.instance.IngameData.petList.Count; i++)
            if (pet == DataManager.instance.IngameData.petList[i])
            {
                DataManager.instance.IngameData.money += pet.price;
                DataManager.instance.IngameData.petList.Remove(pet);
                DataManager.instance.SavePet();
            }

        DisplayList();
    }
    public void SellPet(Pet pet)
    {
        sellPanel.Open();
        sellPanel.setInfo(pet);
    }

    // Add Pet --------------------
    public void AddPet(Pet pet)
    {
        // Test dùng DemoDataPet
        //DemoDataPet.instance.IngameData.petList.Add(pet);

        // ------------------Merge------------
        DataManager.instance.IngameData.petList.Add(pet);
        DataManager.instance.SavePet();

        DisplayList();
    }


    // Turn on/ turn off PetBox
    public void On()
    {
        DisplayList();
        petBox.SetActive(true);
    }
    public void Off()
    {
        petBox.transform.parent.gameObject.SetActive(false);
    }
}
