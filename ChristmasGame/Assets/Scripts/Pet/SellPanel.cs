using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SellPanel : MonoBehaviour
{
    public Text sellInfo;
    public int index;
    private Pet pet;
    public void setInfo(Pet pet)
    {
        this.pet = pet;
        sellInfo.text = "Bán " + pet.name + " với giá " + pet.price + '?';
    }

    public void Open()
    {
        gameObject.SetActive(true);
    }
    public void Sell()
    {
        PetGenerate.instance.RemovePet(DataManager.instance.IngameData.petList.IndexOf(pet));
        PetManager.instance.RemovePet(pet);
        AudioManager.instance.BuySell();
        Close();
    }

    public void Close()
    {
        gameObject.SetActive(false);
    }
}
